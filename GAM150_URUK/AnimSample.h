/*
    File Name: AnimSample.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Vector3D.h"
#include <vector>

struct AnimSample
{
	AnimSample(const char* name);

	std::vector<uruk::Vector3D> jointPositions;
	const char* name;
};

