/*
    File Name: AnimationClip.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "AnimationSample.h"

namespace uruk
{
	struct [[nodiscard]] AnimationClip
	{
		std::string name{""};
		bool isLooping = false;
		int nRepetition;
		float playTime;
		float playRate;
		int nAnimSamples;
		int animSpaceWidth;
		int animSpaceHeight;
		std::vector<AnimationSample> animSamples;
	};

	inline void LoadAnimationClipFromEditorFile(std::ifstream& istream,AnimationClip& animClip)
	{
		istream >> animClip.animSpaceWidth;
		istream >> animClip.animSpaceHeight;
		istream >> animClip.name;
		istream >> animClip.isLooping;
		istream >> animClip.nRepetition;
		istream >> animClip.playTime;
		istream >> animClip.playRate;
		istream >> animClip.nAnimSamples;
		animClip.animSamples.clear();
		for(int i = 0; i < animClip.nAnimSamples; i++)
		{
			AnimationSample animationSample{Skeleton("tmp"),SpriteMesh(),"tmp"};
			LoadAnimSampleFromEditorFile(istream, animationSample);
			animClip.animSamples.push_back(animationSample);
		}
	}

	inline void LoadAnimationClip(std::ifstream& istream, AnimationClip& animClip)
	{
		deserialize(istream, animClip.name);
		deserialize(istream, animClip.isLooping);
		deserialize(istream, animClip.nRepetition);
		deserialize(istream, animClip.playTime);
		deserialize(istream, animClip.playRate);
		deserialize(istream, animClip.nAnimSamples);
		deserialize(istream, animClip.animSpaceWidth);
		deserialize(istream, animClip.animSpaceHeight);
		animClip.animSamples.clear();
		for(int i = 0; i < animClip.nAnimSamples; i++)
		{
			LoadAnimSample(istream, animClip.animSamples[i]);
		}
	}

	inline void SaveAnimationClip(std::ofstream& ostream, AnimationClip& animClip)
	{
		serialize(ostream, animClip.name);
		serialize(ostream, animClip.isLooping);
		serialize(ostream, animClip.nRepetition);
		serialize(ostream, animClip.playTime);
		serialize(ostream, animClip.playRate);
		serialize(ostream, animClip.nAnimSamples);
		serialize(ostream, animClip.animSpaceWidth);
		serialize(ostream, animClip.animSpaceHeight);
		for (int i = 0; i < animClip.nAnimSamples; i++)
		{
			SaveAnimSample(ostream, animClip.animSamples[i]);
		}
	}
}

