/*
    File Name: AnimationComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "AnimationComponent.h"
#include "AnimationClip.h"
#include "DefaultStateMachine.h"
#include "StateMachine.h"
#include "doodle/environment.hpp"
#include "Serializer.h"
#include "Math.h"
#include "SkeletonComponent.h"
#include "SpriteMeshComponent.h"
#include "GameResourceManager.h"


namespace  uruk
{


	AnimationComponent::AnimationComponent()
		: mCurrentAnimationSample{ Skeleton{"temp" }, SpriteMesh{}, "temp" },
			mPStateMachine(std::move(std::make_unique<DefaultStateMachine>()))
	{
	}

	AnimationComponent::~AnimationComponent()
	{
	}

	void AnimationComponent::MapStateWithAnimClip(EnumVal state,const std::shared_ptr<AnimationClip>& pAnimClip)
	{
		mStateAnimationClipMap[state] = pAnimClip;
	}

	void AnimationComponent::OnUpdate()
	{
		using doodle::ElapsedTime;
		if (!mbIsPlaying)
			return;

		EnumVal prevState = mCurrentState;

		//check state and if the state is changed, start crossfading.
		mPStateMachine->Update();
		mCurrentState = mPStateMachine->GetCurrentState();
		if (prevState != mCurrentState)
		{
			if (mShouldCrossFade)
			{
				mBIsCrossFading = true;
			}
			Play();
		}

		const std::shared_ptr<AnimationClip>& clip = mStateAnimationClipMap[mCurrentState];
		int nAnimSamples = static_cast<int>(clip->animSamples.size());
		int nRepetition = clip->nRepetition;
		float playRate = clip->playRate;
		float playTime = clip->playTime;
		float local_t;
		float keyTimeChunk = playTime / static_cast<float>(nAnimSamples - 1);
		mCrossFadingDuration = playTime * playRate * mCrossFadingDurationRatioToClip;

		if (nAnimSamples < 2)
		{
			logger.LogError(ELogCategory::CUSTOM_FILE, "There must be over 1 animation samples in animation clip!");
			return;
		}
		
		if (clip->isLooping == true)
		{
			local_t = fmodf(playRate * (static_cast<float>(ElapsedTime) - mGlobalPlayStartTime), playTime);
		}
		else
		{
			local_t = fmodf(clamp(playRate * (static_cast<float>(ElapsedTime) - mGlobalPlayStartTime), 0.f, static_cast<float>(nRepetition)* playTime), playTime);
		}

		//if playedTime of the clip passed the crossfading duration, stop crossfading. 
		if (local_t > mCrossFadingDuration)
		{
			mBIsCrossFading = false;
		}

		int prevAnimSampleIndex = static_cast<int>(local_t/keyTimeChunk);
		int nextAnimSampleIndex = std::clamp((prevAnimSampleIndex + 1), 0, nAnimSamples - 1);
		float keyTime = keyTimeChunk * static_cast<float>(prevAnimSampleIndex);

		/*
		 *ex) if there are 4 anim samples...
		 * index : 0//////////1//////////2/////////3
		 *         keyTime    keyTime    keyTime   keyTime
		 *        (keyTimeChunk)
		*/

		float lerp_t = (local_t - keyTime) / keyTimeChunk;
		AnimationSample& prevAnimSample = clip->animSamples[prevAnimSampleIndex];
		AnimationSample& nextAnimSample = clip->animSamples[nextAnimSampleIndex];
		const AnimationSample& lerpedAnimSample1_2 = LerpAnimSample(prevAnimSample, nextAnimSample, lerp_t);

		if (mBIsCrossFading == true)
		{
			mCurrentAnimationSample = LerpAnimSample(mCurrentAnimationSample, lerpedAnimSample1_2, 0.5f);
		}
		else
		{
			mCurrentAnimationSample = lerpedAnimSample1_2;
		}

		SkeletonComponent* skeletonComponent = GetOwner()->GetComponent<SkeletonComponent>();
		skeletonComponent->RefGetSkeleton() = mCurrentAnimationSample.skeleton;

		SpriteMeshComponent* spriteMeshComponent = GetOwner()->GetComponent<SpriteMeshComponent>();
		spriteMeshComponent->RefGetSpriteMesh() = mCurrentAnimationSample.spriteMesh;
	}

	void AnimationComponent::Play()
	{
		mGlobalPlayStartTime = static_cast<float>(doodle::ElapsedTime);
		mbIsPlaying = true;
	}

	void AnimationComponent::StopPlaying()
	{
		mbIsPlaying = false;
	}

	void AnimationComponent::OnConstructEnd()
	{
		mPStateMachine->SetOwner(mID, AnimationComponent::TYPE_ID);
	}

	void AnimationComponent::SetShouldCrossfade(bool shouldCrossfade)
	{
		mShouldCrossFade = shouldCrossfade;
	}

	void AnimationComponent::SetCrossFadingDurationRatioToClip(float ratio)
	{
		mCrossFadingDurationRatioToClip = ratio;
	}

	void AnimationComponent::SetStateMachine(std::unique_ptr<StateMachine>&& stateMachine)
	{
		mPStateMachine = std::move(stateMachine);
		mCurrentState = mPStateMachine->GetCurrentState();
		mPStateMachine->SetOwner(mID, TYPE_ID);
	}

	void AnimationComponent::SetState(EnumVal newState)
	{
		mCurrentState = newState;
		mPStateMachine->SetState(newState);
	}

	void AnimationComponent::OnBeginPlay()
	{
		Play();
	}

	void AnimationComponent::OnLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mGlobalPlayStartTime);
		deserialize(inputFileStream, mShouldCrossFade);
		deserialize(inputFileStream, mCrossFadingDuration);
		deserialize(inputFileStream, mCrossFadingDurationRatioToClip);
		
		size_t nEnumVals;
		deserialize(inputFileStream, nEnumVals);
		
		for(size_t i = 0; i < nEnumVals; i++)
		{
			std::pair<EnumVal, std::string> pair;
			deserialize(inputFileStream,pair);
			const std::shared_ptr<AnimationClip>& animationClip = GameResourceManager::GetAnimationClip(pair.second);
			mStateAnimationClipMap.insert({ pair.first,animationClip });
		}
		LoadAnimSample(inputFileStream, mCurrentAnimationSample);
		deserialize(inputFileStream, mBIsCrossFading);
		deserialize(inputFileStream, mbIsPlaying);
		deserialize(inputFileStream, mCurrentState);
	}

	void AnimationComponent::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mGlobalPlayStartTime);
		serialize(outputFileStream, mShouldCrossFade);
		serialize(outputFileStream, mCrossFadingDuration);
		serialize(outputFileStream, mCrossFadingDurationRatioToClip);

		serialize(outputFileStream, mStateAnimationClipMap.size());

		for(auto Pair : mStateAnimationClipMap)
		{
			std::pair<EnumVal, std::string> pair;
			pair.first = Pair.first;
			pair.second = Pair.second->name;
			serialize(outputFileStream, pair);
		}
		SaveAnimSample(outputFileStream, mCurrentAnimationSample);
		serialize(outputFileStream, mBIsCrossFading);
		serialize(outputFileStream, mbIsPlaying);
		serialize(outputFileStream, mCurrentState);
	}

	void AnimationComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mPStateMachine->GetStateMachineTypeIDForSerialization());
		mPStateMachine->OnSave(outputFileStream);
	}

	void AnimationComponent::OnEnabled()
	{
		Play();
	}

	void AnimationComponent::OnDisabled()
	{
		StopPlaying();
	}

	IComponent* AnimationComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		TypeID stateMachineType;
		deserialize(inputFileStream, stateMachineType);
		std::unique_ptr<StateMachine> pStateMachine = StateMachineFactory::Generate(stateMachineType);
		pStateMachine->OnLoad(inputFileStream);
		AnimationComponent* animationComponent = static_cast<AnimationComponent*>(level->LoadComponent<AnimationComponent>(id));
		animationComponent->SetStateMachine(std::move(pStateMachine));

		return animationComponent;
	}
}