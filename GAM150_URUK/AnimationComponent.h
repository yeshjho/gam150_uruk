/*
    File Name: AnimationComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"
#include "AnimationSample.h"

namespace uruk
{
	struct AnimationClip;
	class StateMachine;
	using EnumVal = int;

	class AnimationComponent : public Component<AnimationComponent, EUpdateType::BY_FRAME, update_priorities::NORMAL_PRIORITY, 0>
	{
	public:
		AnimationComponent();
		~AnimationComponent();
		void MapStateWithAnimClip(EnumVal state,const std::shared_ptr<AnimationClip>& pAnimClip);//must be set.
		void OnUpdate() override final;
		void Play();
		void StopPlaying();
		void OnConstructEnd() override;
		void SetShouldCrossfade(bool shouldCrossfade);
		void SetCrossFadingDurationRatioToClip(float ratio);
		void SetStateMachine(std::unique_ptr<StateMachine>&& stateMachine);
		void SetState(EnumVal newState);
		void OnBeginPlay() override;
		void OnLoad(std::ifstream& inputFileStream) override;
		void OnSave(std::ofstream& outputFileStream) const override;
		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		void OnEnabled() override;
		void OnDisabled() override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);
	private:
		float mGlobalPlayStartTime = 0.f;
		float mCrossFadingDuration = 0.f;
		float mCrossFadingDurationRatioToClip = 0.2f;
		std::unique_ptr<StateMachine> mPStateMachine;
		std::unordered_map<EnumVal, std::shared_ptr<AnimationClip>> mStateAnimationClipMap;
		mutable AnimationSample mCurrentAnimationSample;//for cross fading
		bool mBIsCrossFading = false;
		bool mShouldCrossFade = false;
		bool mbIsPlaying = false;
		EnumVal mCurrentState;
	};
}

