/*
    File Name: AnimationSample.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Skeleton.h"
#include "SpriteMesh.h"

namespace uruk 
{
	struct [[nodiscard]] AnimationSample
	{
		Skeleton skeleton;
		SpriteMesh spriteMesh;
		std::string name{""};
	};

	AnimationSample LerpAnimSample(const AnimationSample& sample1, const AnimationSample& sample2,float t);

	void LoadAnimSampleFromEditorFile(std::ifstream& istream, AnimationSample& animSample);
	void LoadAnimSample(std::ifstream& istream, AnimationSample& animSample);
	void SaveAnimSample(std::ofstream& ostream, AnimationSample& animSample);
}
