/*
    File Name: AudioBufferManager.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: Joonho Hwang
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "AudioBufferManager.h"
#include <filesystem>
#include <memory>
#include <SFML/Audio.hpp>
#include "Logger.h"



namespace uruk
{
	void AudioBufferManager::LoadFromDirectory(const std::string& subFolderDirectory)
	{
		std::string finalFolderDirectory = baseFolderDirectory + subFolderDirectory;

		for (auto& p : std::filesystem::directory_iterator(finalFolderDirectory)) {

			std::string fileName = p.path().filename().u8string();
			std::string fileKey;

			bool isMusic = false;

			const size_t fileNameLength = fileName.size();
			for (size_t i = 0; i < fileNameLength; i++)
			{
				if (fileName[0] == 'm' && fileName[1] == '_')
				{
					isMusic = true;
				}

				if (fileName[i] == '.') break;
				fileKey += fileName[i];
			}

			logger.LogVerbose(ELogCategory::CUSTOM_FILE, "Loaded " + fileName + " in " += subFolderDirectory + " as:" + '\t' += fileKey);

			if (!isMusic)
			{

				std::unique_ptr<sf::SoundBuffer> pBuffer = std::make_unique<sf::SoundBuffer>();

				std::string finalPath = finalFolderDirectory + "/" += fileName;

				pBuffer->loadFromFile(finalPath);

				mAudioClips[fileKey] = std::move(pBuffer);

			}
			else
			{

				std::unique_ptr<sf::Music> pMusic = std::make_unique<sf::Music>();

				std::string finalPath = finalFolderDirectory + "/" += fileName;

				pMusic->openFromFile(finalPath);

				mMusicList[fileKey] = std::move(pMusic);

			}
		}
	}

	sf::SoundBuffer* uruk::AudioBufferManager::GetAudioClip(const std::string& audioClipName)
	{

		return mAudioClips.at(audioClipName).get();

	}

	void AudioBufferManager::SetVolume(float arg)
	{
		mVolume = arg;
		for (auto& i : mMusicList)
		{
			i.second->setVolume(mVolume);
		}
	}

	sf::Music* uruk::AudioBufferManager::GetMusic(const std::string& audioClipName)
	{

		return mMusicList.at(audioClipName).get();

	}

}
