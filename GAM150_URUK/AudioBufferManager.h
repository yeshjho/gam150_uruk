/*
    File Name: AudioBufferManager.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <memory>
#include <string>
#include <unordered_map>
#include <SFML/Audio.hpp>



namespace uruk
{

	class AudioBufferManager
	{

	public:
		void LoadFromDirectory(const std::string& subFolderDirectory);
		sf::Music* GetMusic(const std::string& audioClipName);
		sf::SoundBuffer* GetAudioClip(const std::string& audioClipName);
		float GetVolume() { return mVolume; }
		void SetVolume(float arg);
		
	private:
		const char* baseFolderDirectory = "resource/audio/";
		std::unordered_map<std::string, std::unique_ptr<sf::SoundBuffer>> mAudioClips;
		std::unordered_map<std::string, std::unique_ptr<sf::Music>> mMusicList;
		float mVolume = 50.f; // 0 is min, 100 is max

	};

}
