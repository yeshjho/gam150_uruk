/*
    File Name: AudioPlayerComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "AudioPlayerComponent.h"
#include "Game.h"



namespace uruk
{
	void AudioPlayerComponent::OnUpdate()
	{
		mAudioSources.remove_if([](sf::Sound& sound){
			if (sound.getStatus() == sf::SoundSource::Status::Stopped)
			{
				return true;
			}
			else
			{
				return false;
			}
		});

	}

	void AudioPlayerComponent::Stop(const std::string& audioClipName)
	{
		
		if (audioClipName[0] == 'm' && audioClipName[1] == '_')
		{

			sf::Music& music = *(Game::GetAudioBuffer().GetMusic(audioClipName));

			music.stop();

		}
		else
		{

		}
		
	}

	void AudioPlayerComponent::Play(const std::string& audioClipName, bool loop)
	{

		if (audioClipName[0] == 'm' && audioClipName[1] == '_')
		{

			sf::Music& music = *(Game::GetAudioBuffer().GetMusic(audioClipName));

			music.setVolume(float(Game::GetAudioBuffer().GetVolume()));

			music.setLoop(loop);
			
			music.play();

		}
		else
		{

			sf::Sound& audioSource = mAudioSources.emplace_back();

			audioSource.setVolume(float(Game::GetAudioBuffer().GetVolume()));

			audioSource.setBuffer(*(Game::GetAudioBuffer().GetAudioClip(audioClipName)));

			// May add various playing modes here based on the parameter

			audioSource.play();

		}

	}

}
