/*
    File Name: AudioPlayerComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"
#include <SFML/Audio.hpp>



namespace uruk
{

	// Applied low update framerate for performance
	class AudioPlayerComponent final : public Component<AudioPlayerComponent, EUpdateType::BY_FRAME, update_priorities::LOWER_PRIORITY, 30> {

	public:
		void OnUpdate() override;
		void Stop(const std::string& audioClipName);
		void Play(const std::string& audioClipName, bool loop = true);

	private:
		std::list<sf::Sound> mAudioSources;
	};

}
