/*
    File Name: BoxColliderComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: Joonho Hwang
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "BoxColliderComponent.h"
#include "Serializer.h"
#include "SphereColliderComponent.h"
#ifdef UI_DEBUG_DRAW
#include <doodle/doodle.hpp>
#include "TransformComponent.h"
#endif

namespace uruk
{

#ifdef  UI_DEBUG_DRAW
	BoxColliderComponent::BoxColliderComponent(const Vector3D& offset, float width, float height, CollisionTag tag, CollisionFilter filter, bool isOnlyForDetection)
		:IColliderComponent(tag, filter, isOnlyForDetection, []() {return ColliderType::BoxCollider; }),
		 ZOrderBase(10000000),
		 mOffset(offset),
		 mWidth(width),
	     mHeight(height)
	{
	}
#else
	BoxColliderComponent::BoxColliderComponent(const Vector3D& offset, float width, float height, CollisionTag tag,CollisionFilter filter, bool isOnlyForDetection)
		:IColliderComponent(tag, filter,isOnlyForDetection,[]() {return ColliderType::BoxCollider; }),
		 mOffset(offset),mWidth(width),mHeight(height)
	{
	}
#endif
	
#ifdef UI_DEBUG_DRAW
	void BoxColliderComponent::OnDraw(const Vector3D& camPosition, float, const Vector3D&) const
	{
		using namespace doodle;

		TransformComponent* parentTransform = GetOwner()->GetComponent<TransformComponent>();
		Vector3D position = parentTransform->GetPosition() + mOffset;
		position -= camPosition;
		
		push_settings();
		no_fill();
		set_outline_width(3.f);
		set_outline_color(Color(255, 0, 0));
		draw_rectangle(Width/2 + position.X(),Height/2 + position.Y(), mWidth, mHeight);
		pop_settings();
	}
#endif

	
	void BoxColliderComponent::OnMove(void* newLocation)
	{
		new (newLocation) BoxColliderComponent(std::move(*this));
	}

	void BoxColliderComponent::OnSave(std::ofstream& outputFileStream) const
	{
		IColliderComponent::OnSave(outputFileStream);
	}

	void BoxColliderComponent::OnLoad(std::ifstream& inputFileStream)
	{
		IColliderComponent::OnLoad(inputFileStream);
	}

	void BoxColliderComponent::OnDisabled()
	{
		IColliderComponent::OnDisabled();
	}

	void BoxColliderComponent::OnConstructEnd()
	{
		IColliderComponent::SetTypeID(GetTypeID());
		IColliderComponent::SetID(GetID());
	}

	void BoxColliderComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mOffset);
		serialize(outputFileStream, mWidth);
		serialize(outputFileStream, mHeight);
		serialize(outputFileStream, mCollisionTag);
		serialize(outputFileStream, mCollisionFilter);
		serialize(outputFileStream, mIsOnlyForDetection);
	}

	void BoxColliderComponent::OnUpdate()
	{
		IColliderComponent::OnUpdate();
	}

	IComponent* BoxColliderComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		Vector3D offset{0,0};
		float width;
		float height;
		CollisionTag tag;
		CollisionFilter filter;
		bool isOnlyForDetection;
		deserialize(inputFileStream, offset);
		deserialize(inputFileStream, width);
		deserialize(inputFileStream, height);
		deserialize(inputFileStream, tag); 
		deserialize(inputFileStream, filter);
		deserialize(inputFileStream, isOnlyForDetection);

		return level->LoadComponent<BoxColliderComponent>(id, offset, width, height, tag, filter, isOnlyForDetection);
	}

	float BoxColliderComponent::GetWidth() const
	{
		return mWidth;
	}

	float BoxColliderComponent::GetHeight() const
	{
		return mHeight;
	}
}
