/*
    File Name: BoxColliderComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "IColliderComponent.h"
#include "Component.h"
#include "Vector3D.h"
#ifdef UI_DEBUG_DRAW
#include "ZOrderBase.h"
#endif

namespace uruk
{
#ifdef UI_DEBUG_DRAW
	class BoxColliderComponent final : public IColliderComponent, public ZOrderBase, public Component<BoxColliderComponent, EUpdateType::BY_MILLISECOND, update_priorities::HIGHEST_PRIORITY, 0, EComponentAttribute::NO_MEMCPY|EComponentAttribute::DRAWABLE>
#else
	class BoxColliderComponent final : public IColliderComponent , public Component<BoxColliderComponent, EUpdateType::BY_MILLISECOND, update_priorities::HIGHEST_PRIORITY,0,EComponentAttribute::NO_MEMCPY>
#endif
	{
		friend class Physics;
	public:
		//offset from object position to bottom left of collider.
		BoxColliderComponent(const Vector3D& offset, float width, float height, CollisionTag tag, CollisionFilter filter, bool isOnlyForDetection);
#ifdef UI_DEBUG_DRAW
		void OnDraw(const Vector3D& camPosition, float, const Vector3D&) const override;
#endif
		void OnMove(void* newLocation) override;
		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;
		void OnDisabled() override;
		void OnConstructEnd() override;
		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		void OnUpdate() override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);
		float GetWidth() const;
		float GetHeight() const;
	private:
		Vector3D mOffset;
		float mWidth;
		float mHeight;
	};
}