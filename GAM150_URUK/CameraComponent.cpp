/*
    File Name: CameraComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "CameraComponent.h"

#include <doodle/environment.hpp>

#include "TransformComponent.h"



namespace uruk
{
	CameraComponent::CameraComponent() noexcept
	{
		Disable();
	}


	void CameraComponent::OnUpdate()
	{
		const TransformComponent* const transformComponent = GetOwner()->GetComponent<TransformComponent>();
		const Vector3D& position = transformComponent->GetPosition();
		const float rotation = transformComponent->GetRotationRadian();
		const Vector3D& zoomScale = transformComponent->GetScale();

		for (const auto& [typeID, id] : mLevel->GetAllComponentsToDraw())
		{
			IComponent* component = mLevel->GetComponent(typeID, id);
			if (component->IsActive())
			{
				component->OnDraw(position, rotation, zoomScale);
			}
		}
	}


	void CameraComponent::OnEnabled()
	{
		for (CameraComponent& component : mLevel->GetAllComponents<CameraComponent>())
		{
			if (component != this)
			{
				component.Disable();
			}
		}
	}

	
	Vector3D CameraComponent::CalculateCoordProjectedOnWorld(const Vector3D& coordOnScreen) const
	{
		const TransformComponent* const cameraTransform = GetOwner()->GetComponent<TransformComponent>();
		const Vector3D& cameraPosition = cameraTransform->GetPosition();
		const Vector3D& cameraScale = cameraTransform->GetScale();

		const float x = (coordOnScreen[0] - float(doodle::Width) / 2.f) / cameraScale[0] + cameraPosition[0];
		const float y = (coordOnScreen[1] - float(doodle::Height) / 2.f) / cameraScale[1] + cameraPosition[1];
		
		return { x, y };
	}
}
