/*
    File Name: CameraComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"



namespace uruk
{
	class CameraComponent final : public Component<CameraComponent, EUpdateType::BY_FRAME, update_priorities::LOWER_PRIORITY, 1>
	{
	public:
		CameraComponent() noexcept;


		void OnUpdate() override;

		void OnEnabled() override;


		[[nodiscard]] Vector3D CalculateCoordProjectedOnWorld(const Vector3D& coordOnScreen) const;
	};
}
