/*
    File Name: CameraObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "CameraObject.h"

#include "CameraComponent.h"
#include "FollowComponent.h"
#include "PhysicalShakeComponent.h"
#include "TransformComponent.h"



namespace uruk
{
	CameraObject::CameraObject(const Vector3D& centerCoord, const bool bActivateImmediately)
		: cacheCenterCoord(centerCoord), cachebActivateImmediately(bActivateImmediately)
	{}

	CameraObject::CameraObject(const float centerX, const float centerY, const bool bActivateImmediately)
		: cacheCenterCoord(centerX, centerY), cachebActivateImmediately(bActivateImmediately)
	{}

	
	void CameraObject::OnConstructEnd()
	{
		AddComponent<TransformComponent>(cacheCenterCoord);
		AddComponent<CameraComponent>();
		AddComponent<PhysicalShakeComponent>();
		AddComponent<FollowComponent>();

		CameraComponent* const cameraComponent = GetComponent<CameraComponent>();

		if (cachebActivateImmediately)
		{
			cameraComponent->Enable();
		}
	}


	float CameraObject::GetZoomScale() const
	{
		return GetComponent<TransformComponent>()->GetScale()[0];
	}


	void CameraObject::SetZoomScale(const float amount) const
	{
		GetComponent<TransformComponent>()->SetScale(amount, amount);
	}

	void CameraObject::AddZoomScale(const float amount) const
	{
		TransformComponent* scale = GetComponent<TransformComponent>();
		scale->SetScale(scale->GetScale() + Vector3D(amount, amount));
	}

	void CameraObject::SetFollowingObject(const IObject* object, const float smoothRate, const Vector3D& offset, const bool isLinear) const
	{
		GetComponent<FollowComponent>()->SetFollowingObject(object, smoothRate, offset, isLinear);
	}

	void CameraObject::StopFollowing() const noexcept
	{
		GetComponent<FollowComponent>()->StopFollowing();
	}


	void CameraObject::Shake(const EShakeDirection direction, const float intensity, const float intensityDecreaseRate, const unsigned int repeat, const float duration) const
	{
		GetComponent<PhysicalShakeComponent>()->Shake(direction, intensity, intensityDecreaseRate, repeat, duration);
	}
}