/*
    File Name: CameraObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"

#include "Vector3D.h"



namespace uruk
{
	enum class EShakeDirection : char;



	class CameraObject final : public Object<CameraObject>
	{
	public:
		CameraObject(const Vector3D& centerCoord, bool bActivateImmediately = true);
		CameraObject(float centerX = 0.f, float centerY = 0.f, bool bActivateImmediately = true);

		void OnConstructEnd() override;


		[[nodiscard]] float GetZoomScale() const;
		void SetZoomScale(float amount) const;
		void AddZoomScale(float amount) const;

		void SetFollowingObject(const IObject* object, float smoothRate = 4.f, const Vector3D& offset = { 0, 0 }, bool isLinear = false) const;
		void StopFollowing() const noexcept;

		// interval and duration are in millisecond.
		void Shake(EShakeDirection direction, float intensity, float intensityDecreaseRate, unsigned int repeat, float duration) const;
	
	private:
		Vector3D cacheCenterCoord;
		bool cachebActivateImmediately;
	};
}
