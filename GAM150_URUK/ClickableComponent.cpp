/*
    File Name: ClickableComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "ClickableComponent.h"

#include <utility>
#ifdef UI_DEBUG_DRAW
#include <doodle/doodle.hpp>
#endif

#include "Serializer.h"
#include "CameraComponent.h"
#include "CustomCallbackFactory.h"
#include "Game.h"
#include "Logger.h"



namespace uruk
{
	ClickableComponent::ClickableComponent(const float width, const float height, const doodle::MouseButtons button, const int zOrder, const bool bConsumeInput)
		: WidthHeightBase(width, height), ZOrderBase(zOrder), mbConsumeInput(bConsumeInput), mButton(button)
	{}

	void ClickableComponent::OnBeginPlay()
	{
		cacheIsOnUI = GetOwner()->GetComponent<TransformComponent>()->IsOnUI();
	}

	void ClickableComponent::OnUpdate()
	{
		mTimer.Update(mLevel->GetDeltaTimeInMillisecond());
	}

	
	void ClickableComponent::OnMove(void* newLocation)
	{
		new (newLocation) ClickableComponent(std::move(*this));
	}

#ifdef UI_DEBUG_DRAW
	void ClickableComponent::OnDraw(const Vector3D& camPosition, float, const Vector3D& camZoomScale) const
	{
		using namespace doodle;


		const TransformComponent* const componentTransform = GetOwner()->GetComponent<TransformComponent>();
		const bool isOnUI = componentTransform->IsOnUI();
		Vector3D componentPosition = GetAdjustedPosition();
		const Vector3D& componentScale = GetAdjustedScale();

		if (!isOnUI)
		{
			componentPosition[0] = (componentPosition[0] - camPosition[0]) * camZoomScale[0] + float(Width) / 2.f;
			componentPosition[1] = (componentPosition[1] - camPosition[1]) * camZoomScale[1] + float(Height) / 2.f;
		}

		push_settings();
		apply_translate(componentPosition[0], componentPosition[1]);
		apply_rotate(GetAdjustedRotation());
		apply_scale(componentScale[0] * camZoomScale[0], componentScale[1] * camZoomScale[1]);
		no_fill();
		set_outline_width(3.f);
		set_outline_color(DEBUG_DRAW_COLOR);
		draw_rectangle(0, 0, GetWidth(), GetHeight());
		pop_settings();
	}
#endif


	void ClickableComponent::OnPressed(const doodle::MouseButtons button)
	{
		if (Game::HasHUDLevel())
		{
			const auto& components = Game::GetCurrentHUDLevel()->GetAllActiveComponents<ClickableComponent>();
			for (auto component = components.rbegin(); component != components.rend(); --component)
			{
				if (component->mButton != button)
				{
					continue;
				}

				if (component->onPressed())
				{
					return;
				}
			}
		}

		const auto& components = Game::GetCurrentLevel()->GetAllActiveComponents<ClickableComponent>();
		for (auto component = components.rbegin(); component != components.rend(); --component)
		{
			if (component->mButton != button)
			{
				continue;
			}

			if (component->onPressed())
			{
				return;
			}
		}
	}

	
	void ClickableComponent::OnReleased(const doodle::MouseButtons button)
	{
		if (Game::HasHUDLevel())
		{
			const auto& components = Game::GetCurrentHUDLevel()->GetAllActiveComponents<ClickableComponent>();
			for (auto component = components.rbegin(); component != components.rend(); --component)
			{
				if (component->mButton != button)
				{
					continue;
				}

				if (component->onReleased())
				{
					return;
				}
			}
		}

		const auto& components = Game::GetCurrentLevel()->GetAllActiveComponents<ClickableComponent>();
		for (auto component = components.rbegin(); component != components.rend(); --component)
		{
			if (component->mButton != button)
			{
				continue;
			}

			if (component->onReleased())
			{
				return;
			}
		}
	}


	void ClickableComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mButton);
		serialize(outputFileStream, mbConsumeInput);
	}


	void ClickableComponent::onClicked()
	{
		logger.LogVerbose(ELogCategory::CUSTOM_EVENT, "OnClicked\t" + to_string(mID));
		mOnClicked->Execute(this);

		if (mTimer.IsPaused())
		{
			mTimer.Reset();
			mTimer.Resume();
		}
		else
		{
			logger.LogVerbose(ELogCategory::CUSTOM_EVENT, "OnDoubleClicked\t" + to_string(mID));
			mOnDoubleClicked->Execute(this);
			mTimer.Pause();
		}
	}

	bool ClickableComponent::onPressed()
	{
		Vector3D position = Vector3D(doodle::get_mouse_x(), doodle::get_mouse_y());
		if (!cacheIsOnUI)
		{
			position = mLevel->GetAllActiveComponents<CameraComponent>().begin()->CalculateCoordProjectedOnWorld(position);
		}

		if (!IsInside(position))
		{
			return false;
		}

		logger.LogVerbose(ELogCategory::CUSTOM_EVENT, "OnPressed\t" + to_string(mID));
		mIsBeingPressed = true;
		mOnPressed->Execute(this);

		return mbConsumeInput;
	}

	bool ClickableComponent::onReleased()
	{
		if (!mIsBeingPressed || doodle::MouseButton != mButton || !IsActive())
		{
			return false;
		}

		logger.LogVerbose(ELogCategory::CUSTOM_EVENT, "OnReleased\t" + to_string(mID));
		mIsBeingPressed = false;
		mOnReleased->Execute(this);

		Vector3D position = Vector3D(doodle::get_mouse_x(), doodle::get_mouse_y());
		if (!cacheIsOnUI)
		{
			position = mLevel->GetAllActiveComponents<CameraComponent>().begin()->CalculateCoordProjectedOnWorld(position);
		}

		if (IsInside(position))
		{
			onClicked();
		}

		return mbConsumeInput;
	}
	
	IComponent* ClickableComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		doodle::MouseButtons button;
		bool bConsumeInput;

		deserialize(inputFileStream, button);
		deserialize(inputFileStream, bConsumeInput);

		
		return level->LoadComponent<ClickableComponent>(id, 0.f, 0.f, button, 0, bConsumeInput);
	}


	void ClickableComponent::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mOnClicked->GetType());
		mOnClicked->OnSave(outputFileStream);

		serialize(outputFileStream, mOnPressed->GetType());
		mOnPressed->OnSave(outputFileStream);

		serialize(outputFileStream, mOnReleased->GetType());
		mOnReleased->OnSave(outputFileStream);

		serialize(outputFileStream, mOnDoubleClicked->GetType());
		mOnDoubleClicked->OnSave(outputFileStream);
	}

	void ClickableComponent::OnLoad(std::ifstream& inputFileStream)
	{
		TypeID typeID;

		deserialize(inputFileStream, typeID);
		mOnClicked = CustomCallbackFactory<IClickableComponentCallbackBase>::Generate(typeID);
		mOnClicked->OnLoad(inputFileStream);

		deserialize(inputFileStream, typeID);
		mOnPressed = CustomCallbackFactory<IClickableComponentCallbackBase>::Generate(typeID);
		mOnPressed->OnLoad(inputFileStream);

		deserialize(inputFileStream, typeID);
		mOnReleased = CustomCallbackFactory<IClickableComponentCallbackBase>::Generate(typeID);
		mOnReleased->OnLoad(inputFileStream);

		deserialize(inputFileStream, typeID);
		mOnDoubleClicked = CustomCallbackFactory<IClickableComponentCallbackBase>::Generate(typeID);
		mOnDoubleClicked->OnLoad(inputFileStream);
	}
}
