/*
    File Name: ClickableComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"
#include "TransformOffsetBase.h"
#include "WidthHeightBase.h"
#include "ZOrderBase.h"

#ifdef UI_DEBUG_DRAW
#include <doodle/color.hpp>
#endif

#include "CustomCallbackBase.h"
#include "IClickableComponentCallbackBase.h"
#include "InputComponent.h"
#include "Timer.h"



namespace uruk
{
	class DefaultClickableComponentCallback final : public CustomCallbackBase<DefaultClickableComponentCallback,IClickableComponentCallbackBase>
	{
	public:
		void OnLoad(std::ifstream&) override {}
		void OnSave(std::ofstream&) override {}
		void Execute(ClickableComponent*) override {}
	};

	
#ifdef UI_DEBUG_DRAW
	class ClickableComponent final : public TransformOffsetBase<ClickableComponent>, public WidthHeightBase<ClickableComponent>, public ZOrderBase, public Component<ClickableComponent, EUpdateType::BY_MILLISECOND, update_priorities::HIGHER_PRIORITY, 0, EComponentAttribute::NO_MEMCPY | EComponentAttribute::DRAWABLE>
#else
	class ClickableComponent final : public TransformOffsetBase<ClickableComponent>, public WidthHeightBase<ClickableComponent>, public ZOrderBase, public Component<ClickableComponent, EUpdateType::BY_MILLISECOND, update_priorities::HIGHER_PRIORITY, 0, EComponentAttribute::NO_MEMCPY>
#endif
	{
	public:
		ClickableComponent(float width, float height, doodle::MouseButtons button = doodle::MouseButtons::Left, int zOrder = 0, bool bConsumeInput = true);

		void OnBeginPlay() override;
		void OnUpdate() override;

		void OnMove(void* newLocation) override;
	#ifdef UI_DEBUG_DRAW
		void OnDraw(const Vector3D& camPosition, float camRotation, const Vector3D& camZoomScale) const override;
	#endif
		
		//forcing user to pass temporary to prevent misuse. same reason for constructor
		inline void SetOnClicked(std::unique_ptr<IClickableComponentCallbackBase>&& onClicked);
		inline void SetOnDoubleClicked(std::unique_ptr<IClickableComponentCallbackBase>&& onDoubleClicked);
		inline void SetOnPressed(std::unique_ptr<IClickableComponentCallbackBase>&& onPressed);
		inline void SetOnReleased(std::unique_ptr<IClickableComponentCallbackBase>&& onReleased);

		constexpr void SetConsumeInput(bool bConsumeInput) noexcept;

		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);

		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;

	private:
		void onClicked();
		bool onPressed();
		bool onReleased();


	public:
		static constexpr float DOUBLE_CLICK_INTERVAL_ENDURANCE = 300;
	#ifdef UI_DEBUG_DRAW
		static constexpr doodle::Color DEBUG_DRAW_COLOR = { 255, 0, 0 };
	#endif

		static void OnPressed(doodle::MouseButtons button);
		static void OnReleased(doodle::MouseButtons button);
	private:

		std::unique_ptr<IClickableComponentCallbackBase> mOnClicked = std::make_unique<DefaultClickableComponentCallback>();
		std::unique_ptr<IClickableComponentCallbackBase> mOnPressed = std::make_unique<DefaultClickableComponentCallback>();
		std::unique_ptr<IClickableComponentCallbackBase> mOnReleased = std::make_unique<DefaultClickableComponentCallback>();
		std::unique_ptr<IClickableComponentCallbackBase> mOnDoubleClicked = std::make_unique<DefaultClickableComponentCallback>();

		bool cacheIsOnUI = true;
		bool mIsBeingPressed = false;
		bool mbConsumeInput;

		doodle::MouseButtons mButton;

		Timer mTimer{ DOUBLE_CLICK_INTERVAL_ENDURANCE, false };
	};
}

#include "ClickableComponent.inl"
