/*
    File Name: ClickableComponent.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	inline void ClickableComponent::SetOnClicked(std::unique_ptr<IClickableComponentCallbackBase>&& onClicked)
	{
		mOnClicked = std::move(onClicked);
	}

	inline void ClickableComponent::SetOnDoubleClicked(std::unique_ptr<IClickableComponentCallbackBase>&& onDoubleClicked)
	{
		mOnDoubleClicked = std::move(onDoubleClicked);
	}

	inline void ClickableComponent::SetOnPressed(std::unique_ptr<IClickableComponentCallbackBase>&& onPressed)
	{
		mOnPressed = std::move(onPressed);
	}

	inline void ClickableComponent::SetOnReleased(std::unique_ptr<IClickableComponentCallbackBase>&& onReleased)
	{
		mOnReleased = std::move(onReleased);
	}
	
	constexpr void ClickableComponent::SetConsumeInput(const bool bConsumeInput) noexcept
	{
		mbConsumeInput = bConsumeInput;
	}
}
