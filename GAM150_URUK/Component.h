/*
    File Name: Component.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "IComponent.h"

// Including Level here because its subclass should be able to use IObject's AddComponent()/GetComponent()
#include "Level.h"



namespace uruk
{
	template<typename T>
	class ComponentOnLoadFunctionPointerStorer
	{
	public:
		ComponentOnLoadFunctionPointerStorer();
	};



	
	
	// If updateType is NEVER, two values below aren't used.
	// updateInterval is in milliseconds for BY_MILLISECOND, frames for BY_FRAME.
	// updateInterval that is less or equal to 0 means every frame (even for BY_MILLISECOND).
	template<typename ComponentType, EUpdateType UpdateType = EUpdateType::NEVER, int UpdatePriority = update_priorities::NORMAL_PRIORITY, int UpdateInterval = 0, int Attributes = 0>
	class Component : public IComponent
	{
	public:
		[[nodiscard]] const TypeID& GetTypeID() const noexcept override final;


		[[nodiscard]] EUpdateType GetUpdateType() const noexcept override final;
		[[nodiscard]] int GetUpdatePriority() const noexcept override final;
		[[nodiscard]] int GetUpdateInterval() const noexcept override final;
		[[nodiscard]] int GetAttributes() const noexcept override final;

		static void SetUpdateInterval(int updateInterval);


		static IComponent* DefaultOnLoadConstruct(Level* level, const ID& id, std::ifstream&);

		
		[[nodiscard]] bool operator==(const IComponent& rhs) const noexcept override final;
		[[nodiscard]] bool operator==(const IComponent* rhs) const noexcept override final;
		[[nodiscard]] bool operator!=(const IComponent& rhs) const noexcept override final;
		[[nodiscard]] bool operator!=(const IComponent* rhs) const noexcept override final;



	public:
		static const TypeID TYPE_ID;

		static constexpr EUpdateType UPDATE_TYPE = UpdateType;
		static constexpr int UPDATE_PRIORITY = UpdatePriority;
		static int UPDATE_INTERVAL;
		static constexpr int ATTRIBUTES = Attributes;

	private:
		// Dirty get-around to store OnLoad function pointer to IComponent's list...
		inline static ComponentOnLoadFunctionPointerStorer<ComponentType> componentOnLoadFunctionPointerStorer;
	};
}

#include "Component.inl"
