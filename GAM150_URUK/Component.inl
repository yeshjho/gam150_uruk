/*
    File Name: Component.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	template<typename T>
	ComponentOnLoadFunctionPointerStorer<T>::ComponentOnLoadFunctionPointerStorer()
	{
		if constexpr (HasOnLoadConstruct<T>::value)
		{
			IComponent::onLoadConstructFunctions[IDManager::GetTypeID<T>()] = T::OnLoadConstruct;
		}
		else if constexpr (!bool(T::ATTRIBUTES & EComponentAttribute::NO_SAVE))
		{
			// C2679 will occur when there's no appropriate OnLoad() function.
			IComponent::onLoadConstructFunctions[IDManager::GetTypeID<T>()] = Component<T>::DefaultOnLoadConstruct;
		}
	}


	
	

	template<typename ComponentType, EUpdateType UpdateType, int UpdatePriority, int UpdateInterval, int Attributes>
	const TypeID Component<ComponentType, UpdateType, UpdatePriority, UpdateInterval, Attributes>::TYPE_ID = IDManager::GetTypeID<ComponentType>();
	
	template<typename ComponentType, EUpdateType UpdateType, int UpdatePriority, int UpdateInterval, int Attributes>
	int Component<ComponentType, UpdateType, UpdatePriority, UpdateInterval, Attributes>::UPDATE_INTERVAL = UpdateInterval;


	template<typename ComponentType, EUpdateType UpdateType, int UpdatePriority, int UpdateInterval, int Attributes>
	const TypeID& Component<ComponentType, UpdateType, UpdatePriority, UpdateInterval, Attributes>::GetTypeID() const noexcept
	{
		return TYPE_ID;
	}

	template<typename ComponentType, EUpdateType UpdateType, int UpdatePriority, int UpdateInterval, int Attributes>
	EUpdateType Component<ComponentType, UpdateType, UpdatePriority, UpdateInterval, Attributes>::GetUpdateType() const noexcept
	{
		return UPDATE_TYPE;
	}

	template<typename ComponentType, EUpdateType UpdateType, int UpdatePriority, int UpdateInterval, int Attributes>
	int Component<ComponentType, UpdateType, UpdatePriority, UpdateInterval, Attributes>::GetUpdatePriority() const noexcept
	{
		return UPDATE_PRIORITY;
	}

	template<typename ComponentType, EUpdateType UpdateType, int UpdatePriority, int UpdateInterval, int Attributes>
	int Component<ComponentType, UpdateType, UpdatePriority, UpdateInterval, Attributes>::GetUpdateInterval() const noexcept
	{
		return UPDATE_INTERVAL;
	}

	template<typename ComponentType, EUpdateType UpdateType, int UpdatePriority, int UpdateInterval, int Attributes>
	int Component<ComponentType, UpdateType, UpdatePriority, UpdateInterval, Attributes>::GetAttributes() const noexcept
	{
		return ATTRIBUTES;
	}

	template<typename ComponentType, EUpdateType UpdateType, int UpdatePriority, int UpdateInterval, int Attributes>
	void Component<ComponentType, UpdateType, UpdatePriority, UpdateInterval, Attributes>::SetUpdateInterval(const int updateInterval)
	{
		UPDATE_INTERVAL = updateInterval;
		logger.LogLog(ELogCategory::COMPONENT_ACTION, "Changing UpdateInterval of " + IDManager::GetTypeName(TYPE_ID) + " to " + std::to_string(updateInterval));
	}

	template<typename ComponentType, EUpdateType UpdateType, int UpdatePriority, int UpdateInterval, int Attributes>
	IComponent* Component<ComponentType, UpdateType, UpdatePriority, UpdateInterval, Attributes>::DefaultOnLoadConstruct(Level* level, const ID& id, std::ifstream&)
	{
		return level->LoadComponent<ComponentType>(id);
	}


	template<typename ComponentType, EUpdateType UpdateType, int UpdatePriority, int UpdateInterval, int Attributes>
	bool Component<ComponentType, UpdateType, UpdatePriority, UpdateInterval, Attributes>::operator==(const IComponent& rhs) const noexcept
	{
		return TYPE_ID == rhs.GetTypeID() && mID == rhs.GetID();
	}

	template<typename ComponentType, EUpdateType UpdateType, int UpdatePriority, int UpdateInterval, int Attributes>
	bool Component<ComponentType, UpdateType, UpdatePriority, UpdateInterval, Attributes>::operator==(const IComponent* rhs) const noexcept
	{
		return TYPE_ID == rhs->GetTypeID() && mID == rhs->GetID();
	}

	template<typename ComponentType, EUpdateType UpdateType, int UpdatePriority, int UpdateInterval, int Attributes>
	bool Component<ComponentType, UpdateType, UpdatePriority, UpdateInterval, Attributes>::operator!=(const IComponent& rhs) const noexcept
	{
		return !(*this == rhs);
	}

	template<typename ComponentType, EUpdateType UpdateType, int UpdatePriority, int UpdateInterval, int Attributes>
	bool Component<ComponentType, UpdateType, UpdatePriority, UpdateInterval, Attributes>::operator!=(const IComponent* rhs) const noexcept
	{
		return !(*this == rhs);
	}
}
