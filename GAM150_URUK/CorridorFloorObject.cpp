/*
    File Name: CorridorFloorObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "CorridorFloorObject.h"

#include "EnemyObject.h"
#include "TransformComponent.h"
#include "TextureComponent.h"



namespace uruk
{
	CorridorFloorObject::CorridorFloorObject(Vector3D cacheCoord)
		: cacheCoord(cacheCoord)
	{}


	void CorridorFloorObject::OnConstructEnd()
	{
		AddComponent<TransformComponent>(cacheCoord);

		AddComponent<TextureComponent>("CorridorFloor", EObjectZOrder::CORRIDOR_FLOOR);
	}
}