/*
    File Name: CorridorFloorObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"
#include "Vector3D.h"



namespace uruk
{
	class CorridorFloorObject final : public Object<CorridorFloorObject>
	{
	public:
		CorridorFloorObject(Vector3D coord = { 0, 0 });

		void OnConstructEnd() override;



	private:
		Vector3D cacheCoord;
	};
}
