/*
    File Name: CorridorWallObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "CorridorWallObject.h"

#include "BoxColliderComponent.h"
#include "EnemyObject.h"
#include "TextureComponent.h"
#include "TransformComponent.h"



namespace uruk
{
	CorridorWallObject::CorridorWallObject(Vector3D cacheCoord)
		: cacheCoord(cacheCoord)
	{}


	void CorridorWallObject::OnConstructEnd()
	{
		AddComponent<TransformComponent>(cacheCoord);

		AddComponent<TextureComponent>("CorridorWall", EObjectZOrder::CORRIDOR_WALL);
		
		AddComponent<BoxColliderComponent>(Vector3D{ 0, 0 }, TILE_WIDTH, TILE_HEIGHT, CollisionTag::Wall, CollisionTag::Uruk, false);
	}
}
