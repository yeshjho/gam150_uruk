/*
    File Name: CustomCallbackBase.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "CustomCallbackFactory.h"

namespace uruk
{
	template<typename T,typename IBase>
	class CustomCallbackBase : public IBase, public CustomCallbackFactory<IBase>
	{
	public:
		TypeID GetType() override;
		static std::unique_ptr<IBase> CustomCallbackFactoryFunction();
	private:
		class CustomFactoryRegisterer
		{
		public:	
			CustomFactoryRegisterer();
		};

		inline static const TypeID TYPE_ID = IDManager::GetTypeID<T>();
		inline static CustomFactoryRegisterer registerer;
	};

	template <typename T,typename IBase>
	TypeID CustomCallbackBase<T, IBase>::GetType()
	{
		return TYPE_ID;
	}

	template <typename T,typename IBase>
	std::unique_ptr<IBase> CustomCallbackBase<T, IBase>::CustomCallbackFactoryFunction()
	{
		return std::make_unique<T>();
	}

	template<typename T,typename IBase>
	CustomCallbackBase<T, IBase>::CustomFactoryRegisterer::CustomFactoryRegisterer()
	{
		CustomCallbackFactory<IBase>::CustomCallbackFactoryFunctions[CustomCallbackBase<T, IBase>::TYPE_ID] = CustomCallbackBase<T, IBase>::CustomCallbackFactoryFunction;
	} 
}
