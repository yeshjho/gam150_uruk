/*
    File Name: CustomCallbackFactory.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <functional>
#include "Internal.h"

namespace uruk
{
	template<typename IBase>
	class CustomCallbackFactory
	{
	public:
		static std::unique_ptr<IBase> Generate(const TypeID& typeId);
	protected:
		inline static std::unordered_map<TypeID, std::function<std::unique_ptr<IBase>()>> CustomCallbackFactoryFunctions{};
	};

	template <typename IBase>
	std::unique_ptr<IBase> CustomCallbackFactory<IBase>::Generate(const TypeID& typeId)
	{
		return CustomCallbackFactoryFunctions.at(typeId)();
	}
}

