/*
    File Name: DefaultStateMachine.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "DefaultStateMachine.h"

namespace uruk
{
	DefaultStateMachine::DefaultStateMachine()
		: CustomStateMachineBase<DefaultStateMachine>(Idle,
			{
				StateTransitionConditionsPair
				{
					DefaultStateMachineStates::Idle,
					{
					}
				}
			}
		)
	{}
}
