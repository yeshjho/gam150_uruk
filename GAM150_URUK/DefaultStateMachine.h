/*
    File Name: DefaultStateMachine.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "StateMachine.h"



namespace uruk
{
	enum DefaultStateMachineStates
	{
		Idle,
	};


	class DefaultStateMachine : public CustomStateMachineBase<DefaultStateMachine>
	{
	public:
		DefaultStateMachine();
	};
}
