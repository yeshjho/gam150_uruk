/*
    File Name: Delegate.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: Joonho Hwang
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include <algorithm>
#include "Delegate.h"



namespace uruk
{
	Delegate::~Delegate()
	{
		for (DelegateFunctionBase* pFunction : mPFunctions)
		{
#pragma warning(push)
#pragma warning(disable: 5205)  // it's OK to delete with non-virtual destructor here
			delete pFunction;
#pragma warning(pop)
		}
	}

	void Delegate::AddFunction(DelegateFunctionBase* function)
	{
		mPFunctions.push_back(function);
	}

	void Delegate::RemoveFunction(DelegateFunctionBase* function)
	{
		const auto& it = std::find(mPFunctions.begin(), mPFunctions.end(), function);
		if (it != mPFunctions.end())
		{
			mPFunctions.erase(it);
		}
	}

	void Delegate::Execute()
	{
		for (DelegateFunctionBase* pFunction : mPFunctions)
		{
			pFunction->Execute();
		}
	}
}
