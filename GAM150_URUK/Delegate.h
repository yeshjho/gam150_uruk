/*
    File Name: Delegate.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <list>
#include <tuple>



namespace uruk
{
	struct DelegateFunctionBase
	{
		virtual void Execute() = 0;
	};



	template <typename Function, typename ...Types>
	struct DelegateFunction final : public DelegateFunctionBase
	{
	public:

		/*using funcType = decltype(bind(declval<Function>(), declval<Types>()...));*/
		DelegateFunction(Function func,Types ...args)//arguments 
			:callback(func), mTupleArgs(args...)
		{}

		void Execute() override
		{
			std::apply(callback, mTupleArgs);
		}
	private:
		Function callback;
		std::tuple<Types...> mTupleArgs;
	};



	class Delegate
	{
	public:
		Delegate() = default;
		Delegate(const Delegate& other) = delete;
		~Delegate();

		template<typename Function, typename ...Types>
		void AddFunction(Function func, Types ...args);
		void AddFunction(DelegateFunctionBase* function);
		void RemoveFunction(DelegateFunctionBase* function);

		void Execute();

	private:
		std::list<DelegateFunctionBase*> mPFunctions;
	};



	template<typename Function, typename ...Types>
	void Delegate::AddFunction(Function func, Types ...args)
	{
		mPFunctions.push_back(new DelegateFunction(func, args...));
	}
}
