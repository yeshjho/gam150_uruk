/*
    File Name: DoorComponent.cpp
    Project Name: U.R.U.K
	Author(s):
		-Seunggeon Kim
		Contribution(s):
			timer-related things
		-Joonho Hwang
		Contribution(s):
			all the others
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "DoorComponent.h"

#include "Game.h"
#include "Serializer.h"
#include "TextureComponent.h"


namespace uruk
{

	void DoorComponent::OnUpdate()
	{
		if (mTimeLeftTillOpen <= 0)
		{
			GetOwner()->GetComponent<TextureComponent>()->SetTexture("DoorOpen");
		}
		else
		{
			GetOwner()->GetComponent<TextureComponent>()->SetTexture("DoorClosed");
			mTimeLeftTillOpen -= Game::GetCurrentLevel()->GetDeltaTimeInMillisecond();
		}
	}

	void DoorComponent::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mTimeLeftTillOpen);
	}
	
	void DoorComponent::OnLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mTimeLeftTillOpen);
	}

}
