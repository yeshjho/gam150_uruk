/*
    File Name: DoorComponent.h
    Project Name: U.R.U.K
	Author(s):
		-Seunggeon Kim
		Contribution(s):
			timer-related things
		-Joonho Hwang
		Contribution(s):
			all the others
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"



namespace uruk
{
	class DoorComponent final : public Component<DoorComponent, EUpdateType::BY_FRAME, update_priorities::HIGH_PRIORITY, 0>
	{
	public:
		void OnUpdate() override;
		float GetTimer() { return mTimeLeftTillOpen; }
		void SetTimer(float arg) { mTimeLeftTillOpen = arg; }

		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;
		
	private:
		float mTimeLeftTillOpen = 2500;
		
	};
}