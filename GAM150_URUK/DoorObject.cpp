/*
    File Name: DoorObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "DoorObject.h"

#include <utility>


#include "BoxColliderComponent.h"
#include "CustomCallbackBase.h"
#include "DoorComponent.h"
#include "Game.h"
#include "GameEnum.h"
#include "LevelLoaderComponent.h"
#include "TextureComponent.h"
#include "TransformComponent.h"



namespace uruk
{
	class DoorTriggerEnterCallback final : public CustomCallbackBase<DoorTriggerEnterCallback, ICollisionCallbackBase>
	{
	public:
		void Execute(IColliderComponent* self, IColliderComponent* collideWith) override
		{
			if (collideWith->GetColliderType() != ColliderType::BoxCollider)
			{
				return;
			}

			IObject* const owner = Game::GetCurrentLevel()->GetComponent(self->GetColliderTypeID(), self->GetColliderID())->GetOwner();
			if (owner->GetComponent<DoorComponent>()->GetTimer() > 0)
			{
				return;
			}

			owner->GetComponent<LevelLoaderComponent>()->Load();
		}
	};


	
	DoorObject::DoorObject(const Vector3D& coord, std::string levelToLoad, const int spawnPointIndex)
		: cacheCoord(coord), cacheLevelToLoad(std::move(levelToLoad)), cacheSpawnPointIndex(spawnPointIndex)
	{}


	void DoorObject::OnConstructEnd()
	{
		AddComponent<TransformComponent>(cacheCoord);
		AddComponent<LevelLoaderComponent>(cacheLevelToLoad, cacheSpawnPointIndex);
		AddComponent<BoxColliderComponent>(Vector3D{ 0, 0 }, TILE_WIDTH, TILE_HEIGHT, CollisionTag::Door, CollisionTag::Uruk, true);
		GetComponent<BoxColliderComponent>()->SetOnCollisionEnter(std::make_unique<DoorTriggerEnterCallback>());
		AddComponent<BoxColliderComponent>(Vector3D{ 20, 20 }, TILE_WIDTH - 40, TILE_HEIGHT - 40, CollisionTag::Wall, CollisionTag::Uruk, false);
		AddComponent<TextureComponent>("DoorClosed", EObjectZOrder::DOOR);
		AddComponent<DoorComponent>();
	}
}
