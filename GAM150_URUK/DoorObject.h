/*
    File Name: DoorObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"

#include "Vector3D.h"



namespace uruk
{
	class DoorObject final : public Object<DoorObject>
	{
	public:
		DoorObject(const Vector3D& coord = { 0, 0 }, std::string levelToLoad = "", int spawnPointIndex = -1);

		void OnConstructEnd() override;
		


	private:
		Vector3D cacheCoord;
		
		std::string cacheLevelToLoad;
		int cacheSpawnPointIndex;
	};
}
