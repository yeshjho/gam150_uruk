/*
    File Name: DraggableComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "DraggableComponent.h"

#include <utility>
#ifdef UI_DEBUG_DRAW
#include <doodle/doodle.hpp>
#endif

#include "Serializer.h"
#include "CameraComponent.h"
#include "Game.h"
#include "Logger.h"



namespace uruk
{

	DraggableComponent::DraggableComponent(const float width, const float height, const doodle::MouseButtons button, const int zOrder, const bool bConsumeInput)
		: WidthHeightBase(width, height), ZOrderBase(zOrder), mbConsumeInput(bConsumeInput), mButton(button)
	{}


	void DraggableComponent::OnBeginPlay()
	{
		GetOwner()->GetComponent<InputComponent>()->BindFunctionToMouseReleaseKey(mButton, [&]() { onReleased(); });
		GetOwner()->GetComponent<InputComponent>()->BindFunctionToMouseMoved([&]() { onMouseMoved(); });

		cacheIsOnUI = GetOwner()->GetComponent<TransformComponent>()->IsOnUI();
	}

	void DraggableComponent::OnMove(void* newLocation)
	{
		new (newLocation) DraggableComponent(std::move(*this));
	}


#ifdef UI_DEBUG_DRAW
	void DraggableComponent::OnDraw(const Vector3D& camPosition, float, const Vector3D& camZoomScale) const
	{
		using namespace doodle;


		const TransformComponent* const componentTransform = GetOwner()->GetComponent<TransformComponent>();
		const bool isOnUI = componentTransform->IsOnUI();
		Vector3D componentPosition = GetAdjustedPosition();
		const Vector3D& componentScale = GetAdjustedScale();

		if (!isOnUI)
		{
			componentPosition[0] = (componentPosition[0] - camPosition[0]) * camZoomScale[0] + float(Width) / 2.f;
			componentPosition[1] = (componentPosition[1] - camPosition[1]) * camZoomScale[1] + float(Height) / 2.f;
		}

		push_settings();
		apply_translate(componentPosition[0], componentPosition[1]);
		apply_rotate(GetAdjustedRotation());
		apply_scale(componentScale[0] * camZoomScale[0], componentScale[1] * camZoomScale[1]);
		no_fill();
		set_outline_width(3.f);
		set_outline_color(DEBUG_DRAW_COLOR);
		draw_rectangle(0, 0, GetWidth(), GetHeight());
		pop_settings();
	}
#endif


	void DraggableComponent::OnPressed(const doodle::MouseButtons button)
	{
		if (Game::HasHUDLevel())
		{
			const auto& components = Game::GetCurrentHUDLevel()->GetAllActiveComponents<DraggableComponent>();
			for (auto component = components.rbegin(); component != components.rend(); --component)
			{
				if (component->mButton != button)
				{
					continue;
				}

				if (component->onPressed())
				{
					return;
				}
			}
		}

		const auto& components = Game::GetCurrentLevel()->GetAllActiveComponents<DraggableComponent>();
		for (auto component = components.rbegin(); component != components.rend(); --component)
		{
			if (component->mButton != button)
			{
				continue;
			}

			if (component->onPressed())
			{
				return;
			}
		}
	}

	IComponent* DraggableComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		doodle::MouseButtons button;
		bool bConsumeInput;

		deserialize(inputFileStream, button);
		deserialize(inputFileStream, bConsumeInput);

		return level->LoadComponent<DraggableComponent>(id, 0.f, 0.f, button, 0, bConsumeInput);
	}


	void DraggableComponent::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mOnDrag->GetType());
		mOnDrag->OnSave(outputFileStream);

		serialize(outputFileStream, mOnMoved->GetType());
		mOnMoved->OnSave(outputFileStream);

		serialize(outputFileStream, mOnDrop->GetType());
		mOnDrop->OnSave(outputFileStream);
	}

	void DraggableComponent::OnLoad(std::ifstream& inputFileStream)
	{
		TypeID typeID;

		deserialize(inputFileStream, typeID);
		mOnDrag = CustomCallbackFactory<IDraggableComponentCallbackBase>::Generate(typeID);
		mOnDrag->OnLoad(inputFileStream);

		deserialize(inputFileStream, typeID);
		mOnMoved = CustomCallbackFactory<IDraggableComponentCallbackBase>::Generate(typeID);
		mOnMoved->OnLoad(inputFileStream);

		deserialize(inputFileStream, typeID);
		mOnDrop = CustomCallbackFactory<IDraggableComponentCallbackBase>::Generate(typeID);
		mOnDrop->OnLoad(inputFileStream);
	}


	void DraggableComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mButton);
		serialize(outputFileStream, mbConsumeInput);
	}


	bool DraggableComponent::onPressed()
	{
		Vector3D position = Vector3D(doodle::get_mouse_x(), doodle::get_mouse_y());
		if (!cacheIsOnUI)
		{
			position = mLevel->GetAllActiveComponents<CameraComponent>().begin()->CalculateCoordProjectedOnWorld(position);
		}

		if (!IsInside(position))
		{
			return false;
		}

		onDrag();
		return mbConsumeInput;
	}

	void DraggableComponent::onReleased()
	{
		if (!mIsBeingDragged || doodle::MouseButton != mButton || !IsActive())
		{
			return;
		}

		onDrop();
	}

	void DraggableComponent::onMouseMoved()
	{
		if (!IsActive() || !mIsBeingDragged)
		{
			return;
		}

		const float moveX = static_cast<float>(doodle::get_mouse_x() - doodle::get_previous_mouse_x());
		const float moveY = static_cast<float>(doodle::get_mouse_y() - doodle::get_previous_mouse_y());
		GetOwner()->GetComponent<TransformComponent>()->MoveBy(moveX, moveY);

		mHasMoved = true;
		mOnMoved->Execute(this, moveX, moveY);
	}

	void DraggableComponent::onDrag()
	{
		logger.LogVerbose(ELogCategory::CUSTOM_EVENT, "OnDrag\t" + to_string(mID));
		mIsBeingDragged = true;
		mHasMoved = false;
		mOnDrag->Execute(this);
	}

	void DraggableComponent::onDrop()
	{
		logger.LogVerbose(ELogCategory::CUSTOM_EVENT, "OnDrop\t" + to_string(mID));
		mIsBeingDragged = false;
		mOnDrop->Execute(this);
	}
}
