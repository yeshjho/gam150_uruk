/*
    File Name: DraggableComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"
#include "TransformOffsetBase.h"
#include "WidthHeightBase.h"
#include "ZOrderBase.h"

#ifdef UI_DEBUG_DRAW
#include <doodle/color.hpp>
#endif

#include "CustomCallbackBase.h"
#include "IDraggableComponentCallbackBase.h"
#include "InputComponent.h"



namespace uruk
{
	class DefaultDraggableComponentCallback final : public CustomCallbackBase<DefaultDraggableComponentCallback, IDraggableComponentCallbackBase>
	{
	public:

		void OnLoad(std::ifstream&) override {};
		void OnSave(std::ofstream&) override {};
		void Execute(DraggableComponent*, float, float) override {};
	};

#ifdef UI_DEBUG_DRAW
	class DraggableComponent final : public TransformOffsetBase<DraggableComponent>, public WidthHeightBase<DraggableComponent>, public ZOrderBase, public Component<DraggableComponent, EUpdateType::NEVER, update_priorities::NORMAL_PRIORITY, 0, EComponentAttribute::NO_MEMCPY | EComponentAttribute::DRAWABLE>
#else
	class DraggableComponent final : public TransformOffsetBase<DraggableComponent>, public WidthHeightBase<DraggableComponent>, public ZOrderBase, public Component<DraggableComponent, EUpdateType::NEVER, update_priorities::NORMAL_PRIORITY, 0, EComponentAttribute::NO_MEMCPY>
#endif
	{
	public:
		DraggableComponent(float width, float height, doodle::MouseButtons button = doodle::MouseButtons::Left, int zOrder = 0, bool bConsumeInput = true);


		void OnBeginPlay() override;

		void OnMove(void* newLocation) override;
	#ifdef UI_DEBUG_DRAW
		void OnDraw(const Vector3D& camPosition, float camRotation, const Vector3D& camZoomScale) const override;
	#endif


		[[nodiscard]] constexpr bool HasMoved() const noexcept;
		
		inline void SetOnDrag(std::unique_ptr<IDraggableComponentCallbackBase>&& onDrag);
		inline void SetOnMoved(std::unique_ptr<IDraggableComponentCallbackBase>&& onMoved);
		inline void SetOnDrop(std::unique_ptr<IDraggableComponentCallbackBase>&& onDrop);

		constexpr void SetConsumeInput(bool bConsumeInput) noexcept;

		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);

		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;

		static void OnPressed(doodle::MouseButtons button);
		
	private:
		void onDrag();
		void onDrop();
		bool onPressed();
		void onReleased();
		void onMouseMoved();



	public:
	#ifdef UI_DEBUG_DRAW
		static constexpr doodle::Color DEBUG_DRAW_COLOR = { 0, 255, 0 };
	#endif
		
	private:
		std::unique_ptr<IDraggableComponentCallbackBase> mOnDrag = std::make_unique<DefaultDraggableComponentCallback>();
		std::unique_ptr<IDraggableComponentCallbackBase> mOnMoved = std::make_unique<DefaultDraggableComponentCallback>();
		std::unique_ptr<IDraggableComponentCallbackBase> mOnDrop = std::make_unique<DefaultDraggableComponentCallback>();

		bool cacheIsOnUI = true;
		bool mIsBeingDragged = false;
		bool mHasMoved = false;
		bool mbConsumeInput;

		doodle::MouseButtons mButton;
	};
}

#include "DraggableComponent.inl"
