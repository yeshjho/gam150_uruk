/*
    File Name: DraggableComponent.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	constexpr bool DraggableComponent::HasMoved() const noexcept
	{
		return mHasMoved;
	}

	
	inline void DraggableComponent::SetOnDrag(std::unique_ptr<IDraggableComponentCallbackBase>&& onDrag)
	{
		mOnDrag = std::move(onDrag);
	}

	inline void DraggableComponent::SetOnMoved(std::unique_ptr<IDraggableComponentCallbackBase>&& onMoved)
	{
		mOnMoved = std::move(onMoved);
	}

	inline void DraggableComponent::SetOnDrop(std::unique_ptr<IDraggableComponentCallbackBase>&& onDrop)
	{
		mOnDrop = std::move(onDrop);
	}
	
	constexpr void DraggableComponent::SetConsumeInput(const bool bConsumeInput) noexcept
	{
		mbConsumeInput = bConsumeInput;
	}
}
