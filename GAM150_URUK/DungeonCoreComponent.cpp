/*
    File Name: DungeonCoreComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "DungeonCoreComponent.h"

#include "AudioPlayerComponent.h"
#include "ShakeDirection.h"
#include "CameraObject.h"
#include "DungeonCoreObject.h"
#include "Game.h"
#include "SphereColliderComponent.h"
#include "TextureComponent.h"
#include "PhysicalShakeComponent.h"


namespace uruk
{
	void DungeonCoreComponent::OnHit()
	{
		--mHealth;

		GetOwner()->GetComponent<AudioPlayerComponent>()->Play("hit_core");

		TextureComponent* const texture = GetOwner()->GetComponent<TextureComponent>();

		if (mHealth <= 0)
		{
			GetOwner()->GetComponent<SphereColliderComponent>()->Disable();
			texture->SetTexture("DungeonCoreBroke");
			CameraObject* const cam = mLevel->GetAllActiveObjects<CameraObject>().begin();
			cam->GetComponent<PhysicalShakeComponent>()->Disable();
			cam->Shake(EShakeDirection::VERTICAL,9.f,25.f,6,2500.f);
			bShouldStartEnding = true;
		}
		else if (mHealth <= 4)
		{
			texture->SetTexture("DungeonCoreDamaged2");
		}
		else if (mHealth <= 7)
		{
			texture->SetTexture("DungeonCoreDamaged1");
		}
	}

	void DungeonCoreComponent::OnBeginPlay()
	{
		GetOwner()->AddComponent<AudioPlayerComponent>();
	}

	void DungeonCoreComponent::OnUpdate()
	{
		if(bShouldStartEnding)
		{
			cameraShakeDuration -= mLevel->GetDeltaTimeInMillisecond();
			if(cameraShakeDuration < 0.f)
			{
				bShouldStartEnding = false;
				bShouldShowEnding = true;
			}
		}
		else if(bShouldShowEnding)
		{
			Game::ShowEnding();
		}
	}
}
