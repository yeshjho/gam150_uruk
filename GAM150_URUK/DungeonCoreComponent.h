/*
    File Name: DungeonCoreComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"



namespace uruk
{
	class DungeonCoreComponent final : public Component<DungeonCoreComponent,EUpdateType::BY_MILLISECOND,update_priorities::NORMAL_PRIORITY>
	{
	public:
		void OnHit();
		void OnBeginPlay() override;
		void OnUpdate() override;

	private:
		static constexpr unsigned int INITIAL_HEALTH = 10;
		
		unsigned int mHealth = INITIAL_HEALTH;
		bool bShouldShowEnding = false;
		bool bShouldStartEnding = false;
		float cameraShakeDuration = 3000.f;
		float endingScenesPlayTime = 10000.f;
	};
}
