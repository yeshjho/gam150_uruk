/*
    File Name: DungeonCoreObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "DungeonCoreObject.h"


#include "BoxColliderComponent.h"
#include "DungeonCoreComponent.h"
#include "Game.h"
#include "GameEnum.h"
#include "SphereColliderComponent.h"
#include "TextureComponent.h"
#include "TransformComponent.h"



namespace uruk
{
	class DungeonCoreBeingAttackedTriggerEnterCallback final : public CustomCallbackBase<DungeonCoreBeingAttackedTriggerEnterCallback, ICollisionCallbackBase>
	{
	public:
		void Execute(IColliderComponent* self, IColliderComponent*) override
		{
			Game::GetCurrentLevel()->GetComponent(self->GetColliderTypeID(), self->GetColliderID())->GetOwner()->GetComponent<DungeonCoreComponent>()->OnHit();
		}
	};



	DungeonCoreObject::DungeonCoreObject(const Vector3D& coord)
		: cacheCoord(coord)
	{}


	void DungeonCoreObject::OnConstructEnd()
	{
		AddComponent<TransformComponent>(cacheCoord);
		
		AddComponent<TextureComponent>("DungeonCore", EObjectZOrder::DUNGEON_CORE);
		
		AddComponent<DungeonCoreComponent>();

		AddComponent<BoxColliderComponent>(Vector3D{ 0, 0 }, TILE_WIDTH, TILE_HEIGHT, CollisionTag::Obstacle, CollisionTag::Uruk, false);

		AddComponent<SphereColliderComponent>(Vector3D{ TILE_WIDTH / 2, TILE_WIDTH / 2 }, TILE_WIDTH / 2, CollisionTag::Obstacle, CollisionTag::UrukAttack);
		GetComponent<SphereColliderComponent>()->SetOnCollisionEnter(std::make_unique<DungeonCoreBeingAttackedTriggerEnterCallback>());
	}
}
