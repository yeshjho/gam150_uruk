/*
	File Name: EndingSceneComponent.cpp
	Project Name: U.R.U.K
	Author(s):
		Primary: Doyoon Kim
		Secondary:
	All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "EndingSceneComponent.h"
#include "Game.h"
#include "InputComponent.h"
#include "TextureComponent.h"


namespace uruk
{
	void EndingSceneComponent::OnUpdate()
	{
		endingScenePlayTime -= mLevel->GetDeltaTimeInMillisecond();
		if (endingScenePlayTime <= 20000.f && 15000.f <= endingScenePlayTime)
		{
			GetOwner()->GetComponent<TextureComponent>()->SetTexture("EndingScene1");
		}
		else if (endingScenePlayTime <= 15000.f && 10000.f <= endingScenePlayTime)
		{
			GetOwner()->GetComponent<TextureComponent>()->SetTexture("EndingScene2");
		}
		else if (endingScenePlayTime >= 0.f)
		{
			GetOwner()->GetComponent<TextureComponent>()->SetTexture("Credit");
		}
		else
		{
			GetOwner()->GetComponent<TextureComponent>()->SetTexture("ReturnToMenuScreen");
			if(InputComponent::IsKeyDown(doodle::KeyboardButtons::R))
			{
				Game::RestartAfterEnding();
			}
		}
	}

	void EndingSceneComponent::OnConstructEnd()
	{
	}

	void EndingSceneComponent::OnEnabled()
	{
		endingScenePlayTime = 20000.f;
	}

	void EndingSceneComponent::OnDisabled()
	{
		endingScenePlayTime = 20000.f;
	}

	float EndingSceneComponent::GetRemainingEndingScenePlayTime()
	{
		return endingScenePlayTime;
	}
}
