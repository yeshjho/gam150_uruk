/*
	File Name: EndingSceneComponent.h
	Project Name: U.R.U.K
	Author(s):
		Primary: Doyoon Kim
		Secondary:
	All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"

namespace uruk
{
	class EndingSceneComponent : public Component<EndingSceneComponent,EUpdateType::BY_FRAME,update_priorities::NORMAL_PRIORITY>
	{
	public:
		void OnUpdate()override;
		void OnConstructEnd() override;
		void OnEnabled() override;
		void OnDisabled() override;
		float GetRemainingEndingScenePlayTime();
	private:
		float endingScenePlayTime = 20000.f;
	};
	
}