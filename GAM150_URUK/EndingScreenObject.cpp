/*
	File Name: EndingScreenObject.cpp
	Project Name: U.R.U.K
	Author(s):
		Primary: Doyoon Kim
		Secondary:
	All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "EndingScreenObject.h"
#include "EndingSceneComponent.h"
#include "InputComponent.h"
#include "TextureComponent.h"
#include "Game.h"
#include "UIPositionAdjustComponent.h"

namespace uruk
{
	void EndingScreenObject::OnConstructEnd()
	{
		AddComponent<TransformComponent>(Vector3D{ 0.f,0.f },true);
		ID texture = AddComponent<TextureComponent>("EndingScene1");
		AddComponent<EndingSceneComponent>();
		Disable();

		AddComponent<UIPositionAdjustComponent>(
			std::vector<ID>{texture}
			);
	}

	void EndingScreenObject::OnEnabled()
	{
		GetComponent<EndingSceneComponent>()->Enable();
	}

	void EndingScreenObject::OnDisabled()
	{
		GetComponent<EndingSceneComponent>()->Disable();
	}
}
