/*
	File Name: EndingScreenObject.h
	Project Name: U.R.U.K
	Author(s):
		Primary: Doyoon Kim
		Secondary:
	All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"

namespace  uruk
{

	class EndingScreenObject : public uruk::Object<EndingScreenObject>
	{
	public:
		void OnConstructEnd() override;
		void OnEnabled();
		void OnDisabled();
	};


}
