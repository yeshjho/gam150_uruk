/*
    File Name: EnemyAnimationStateMachine.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "EnemyAnimationStateMachine.h"
#include "Game.h"
#include "EnemyBehaviorComponent.h"


namespace uruk
{
	EnemyAnimationStateMachine::EnemyAnimationStateMachine()
		: CustomStateMachineBase<EnemyAnimationStateMachine>(Anim_Idle,
			{
				StateTransitionConditionsPair
				{
					EnemyAnimationStateMachineStates::Anim_Moving,
					{
						TransitionCondition
						{
							EnemyAnimationStateMachineStates::Anim_Idle,
							[](ID ownerID,TypeID typeId)
							{
								IComponent* ownerComponent = Game::GetCurrentLevel()->GetComponent(typeId,ownerID);
								EnemyBehaviorComponent* enemyBehaviorComponent = ownerComponent->GetOwner()->GetComponent<EnemyBehaviorComponent>();
								if(enemyBehaviorComponent->GetState() == Idle)
								{
									return true;
								}
								return false;
							}
						},
						TransitionCondition
						{
							EnemyAnimationStateMachineStates::Anim_Anticipation,
							[](ID ownerID,TypeID typeId)
							{
								IComponent* ownerComponent = Game::GetCurrentLevel()->GetComponent(typeId,ownerID);
								EnemyBehaviorComponent* enemyBehaviorComponent = ownerComponent->GetOwner()->GetComponent<EnemyBehaviorComponent>();
								if (enemyBehaviorComponent->GetState() == Enemy_Anticipation)
								{
									return true;
								}
								return false;
							}
						}
					}
				}, 
				StateTransitionConditionsPair
				{
					EnemyAnimationStateMachineStates::Anim_Anticipation,
		  {
						TransitionCondition
						{
							EnemyAnimationStateMachineStates::Anim_Slash,
							[](ID ownerID,TypeID typeId)
							{
								IComponent* ownerComponent = Game::GetCurrentLevel()->GetComponent(typeId,ownerID);
								EnemyBehaviorComponent* enemyBehaviorComponent = ownerComponent->GetOwner()->GetComponent<EnemyBehaviorComponent>();
								if (enemyBehaviorComponent->GetState() == Enemy_Slash)
								{
									return true;
								}
								return false;
							}
						},
					}
				},
				StateTransitionConditionsPair
				{
					EnemyAnimationStateMachineStates::Anim_Idle,
					{
						TransitionCondition
						{
							EnemyAnimationStateMachineStates::Anim_Spot,
							[](ID ownerID,TypeID typeId)
							{
								IComponent* ownerComponent = Game::GetCurrentLevel()->GetComponent(typeId,ownerID);
								EnemyBehaviorComponent* enemyBehaviorComponent = ownerComponent->GetOwner()->GetComponent<EnemyBehaviorComponent>();
								if (enemyBehaviorComponent->GetState() == Spot)
								{
									return true;
								}
								return false;
							}
						},
					}
				},
				StateTransitionConditionsPair
				{
					EnemyAnimationStateMachineStates::Anim_Spot,
					{
						TransitionCondition
						{
							EnemyAnimationStateMachineStates::Anim_Moving,
							[](ID ownerID,TypeID typeId)
							{
								IComponent* ownerComponent = Game::GetCurrentLevel()->GetComponent(typeId,ownerID);
								EnemyBehaviorComponent* enemyBehaviorComponent = ownerComponent->GetOwner()->GetComponent<EnemyBehaviorComponent>();
								if (enemyBehaviorComponent->GetState() == Moving)
								{
									return true;
								}
								return false;
							}
						},
					}
				},
				StateTransitionConditionsPair
				{
					EnemyAnimationStateMachineStates::Anim_Recovery,
					{
						TransitionCondition
						{
							EnemyAnimationStateMachineStates::Anim_Moving,
							[](ID ownerID,TypeID typeId)
							{
								IComponent* ownerComponent = Game::GetCurrentLevel()->GetComponent(typeId,ownerID);
								EnemyBehaviorComponent* enemyBehaviorComponent = ownerComponent->GetOwner()->GetComponent<EnemyBehaviorComponent>();
								if (enemyBehaviorComponent->GetState() == Moving)
								{
									return true;
								}
								return false;
							}
						},
					}
				},
				StateTransitionConditionsPair
				{
					EnemyAnimationStateMachineStates::Anim_Slash,
					{
						TransitionCondition
						{
							EnemyAnimationStateMachineStates::Anim_Recovery,
							[](ID ownerID,TypeID typeId)
							{
								IComponent* ownerComponent = Game::GetCurrentLevel()->GetComponent(typeId,ownerID);
								EnemyBehaviorComponent* enemyBehaviorComponent = ownerComponent->GetOwner()->GetComponent<EnemyBehaviorComponent>();
								if (enemyBehaviorComponent->GetState() == Enemy_Recovery)
								{
									return true;
								}
								return false;
							}
						},
					}
				},
			}
		)
	{}
}
