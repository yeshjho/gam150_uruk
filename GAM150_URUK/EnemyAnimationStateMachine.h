/*
    File Name: EnemyAnimationStateMachine.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "StateMachine.h"



namespace uruk
{
	enum EnemyAnimationStateMachineStates
	{
		Anim_Moving,
		Anim_Anticipation,
		Anim_Idle,
		Anim_Spot,
		Anim_Recovery,
		Anim_Slash,
	};


	class EnemyAnimationStateMachine : public CustomStateMachineBase<EnemyAnimationStateMachine>
	{
	public:
		EnemyAnimationStateMachine();
	};
}
