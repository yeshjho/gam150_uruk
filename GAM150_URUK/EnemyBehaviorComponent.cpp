/*
    File Name: EnemyBehaviorComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "EnemyBehaviorComponent.h"


#include <doodle/random.hpp>
#include "AnimationComponent.h"
#include "AudioPlayerComponent.h"
#include "EnemyAnimationStateMachine.h"
#include "FlowFieldPathfinderComponent.h"
#include "EnemyObject.h"
#include "PlayerBehaviorComponent.h"
#include "PlayerObject.h"
#include "EnemyStateMachine.h"
#include "PlayerStatisticsComponent.h"
#include "SphereColliderComponent.h"
#include "SpriteMeshComponent.h"
#include "TextureComponent.h"
#include "TransformComponent.h"



namespace uruk 
{

	EnemyBehaviorComponent::EnemyBehaviorComponent(EEnemyType enemyType, ID pathfinderID, ID playerID)
		: mEnemyType(enemyType), mPathfinderID(pathfinderID), mPlayerID(playerID)
	{
		switch (mEnemyType)
		{
		case EEnemyType::KonDoori:
			mMoveDistance = clamp(float(doodle::random()), 0.f, 0.05f) + 0.075f;
			break;
		case EEnemyType::SonXaary:
			mMoveDistance = clamp(float(doodle::random()), 0.f, 0.05f) + 0.1f;
			mAnticipationDuration = mSonXaaryAnticipationDuration;
			mRecoveryDuration = mSonxaaryRecoveryDuration;
			mSlashDuration = mSonxaarySlashDuration;
			mDamageDelay = mSonxaaryDamageDelay;
			break;
		case EEnemyType::PohShigi:
			
			mMoveDistance = clamp(float(doodle::random()), 0.f, 0.05f) + 0.05f;
			mAnticipationDuration = mPoshigiAnticipationDuration;
			mRecoveryDuration = mPoshigiRecoveryDuration;
			mDamageDelay = mPoshigiDamageDelay;
			mSlashDuration = mPoshigiSlashDuration;
			break;
		}
	}	

	

	void EnemyBehaviorComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mEnemyType);
		serialize(outputFileStream, mPathfinderID);
		serialize(outputFileStream, mPlayerID);
	}

	IComponent* EnemyBehaviorComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		EEnemyType enemyType;
		ID pathFinderID;
		ID playerID;

		deserialize(inputFileStream, enemyType);
		deserialize(inputFileStream, pathFinderID);
		deserialize(inputFileStream, playerID);

		return level->LoadComponent<EnemyBehaviorComponent>(id, enemyType, pathFinderID, playerID);
	}



	void EnemyBehaviorComponent::OnSave(std::ofstream& outputFileStream) const
	{
		mStateMachine.OnSave(outputFileStream);
		serialize(outputFileStream, mSleepTimer);
		serialize(outputFileStream, mSingleTextureID);
		serialize(outputFileStream, mMoveDistance);
	}


	void EnemyBehaviorComponent::OnLoad(std::ifstream& inputFileStream)
	{
		mStateMachine.OnLoad(inputFileStream);
		deserialize(inputFileStream, mSleepTimer);
		deserialize(inputFileStream, mSingleTextureID);
		deserialize(inputFileStream, mMoveDistance);
	}
	
	

	void EnemyBehaviorComponent::OnBeginPlay()
	{
		mStateMachine.SetOwner(GetID(), GetTypeID());
	}
	
	void EnemyBehaviorComponent::OnUpdate()
	{
			
		LookForPlayer();
		ReachForPlayer();
		if(mMoveDirection.X() > 0)
		{
			mIsFlipped = true;
		}
		else if(mMoveDirection.X() < 0)
		{
			mIsFlipped = false;
		}
		
		TextureComponent* singleTextureComponent = mLevel->GetComponent<TextureComponent>(mSingleTextureID);
		SpriteMeshComponent* spriteMeshComponent = GetOwner()->GetComponent<SpriteMeshComponent>();

		if (mPreviousState != mStateMachine.GetCurrentState())
		{
			mPreviousState = EnemyStateMachineStates(mStateMachine.GetCurrentState());
			mStateTimer = 0;
		}
		else
		{
			mStateTimer += mLevel->GetDeltaTimeInMillisecond();
		}

		if (mSleepTimer > 0)
		{
			singleTextureComponent->SetActive(false);
			spriteMeshComponent->SetActive(false);
			mSleepTimer -= mLevel->GetDeltaTimeInMillisecond();
			return;
		}
		else
		{
			singleTextureComponent->SetActive(true);
			spriteMeshComponent->SetActive(true);
		}
		
		if (mKnockBackTimer > 0)
		{
			mKnockBackTimer -= mLevel->GetDeltaTimeInMillisecond();
		}

		auto playerEnemyTransform = GetPlayerEnemyTransform();

		auto playerTransform = playerEnemyTransform.first;
		auto enemyTransform = playerEnemyTransform.second;

		auto playerPos = playerTransform->GetPosition();
		auto enemyPos = enemyTransform->GetPosition();

		PlayerStateMachineStates playerState;

		switch (mEnemyType)
		{

		case EEnemyType::KonDoori:

			switch (mStateMachine.GetCurrentState())
			{
			default:
			case EnemyStateMachineStates::Idle:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				break;
			case EnemyStateMachineStates::Spot:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				break;
			case EnemyStateMachineStates::Moving:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				MoveTowardsTarget();
				break;
			case EnemyStateMachineStates::Enemy_Anticipation:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				break;
			case EnemyStateMachineStates::Enemy_Slash:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				if (mStateTimer > mDamageDelay)
				GetOwner()->GetComponent<SphereColliderComponent>()->Enable();
				break;
			case EnemyStateMachineStates::Enemy_Recovery:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				GetOwner()->GetComponent<SphereColliderComponent>()->Disable();
				break;
			case EnemyStateMachineStates::Dead:
				spriteMeshComponent->Disable();
				singleTextureComponent->Enable();
				singleTextureComponent->SetTexture("KonDooriDead");
				GetOwner()->GetComponent<SphereColliderComponent>()->Disable();
				break;
			}

			break;

		case EEnemyType::SonXaary:

			playerState = mLevel->GetObject<PlayerObject>(mPlayerID)->GetComponent<PlayerBehaviorComponent>()->GetState();
			
			if (mIsPlayerInSight && mLevel->GetObject<PlayerObject>(mPlayerID)->GetComponent<PlayerStatisticsComponent>()->IsAlive())
			{
				if (playerState == Moving_Lantern || playerState == Idle_Lantern)
				{
					mIsFrightened = true;
					
					if (IsPlayerInSight())
					{
						if(mIsTogether) 
						{
							mStateMachine.SetState(Idle);
							GetOwner()->GetComponent<AnimationComponent>()->SetState(Anim_Idle);
							mActionTimer = 0;
						}
						else
						{
							GetOwner()->GetComponent<AnimationComponent>()->SetState(Anim_Moving); 
							MoveTowardsTarget();
							singleTextureComponent->Disable();
							spriteMeshComponent->Enable();
							mStateMachine.SetState(EnemyStateMachineStates::Moving);
							mActionTimer = 0;
							return;
						}
					}
					else
					{
						mStateMachine.SetState(Idle);
						GetOwner()->GetComponent<AnimationComponent>()->SetState(Anim_Idle); 
					}
				}
				else
				{
					mIsFrightened = false;
				}
			}


			switch (mStateMachine.GetCurrentState())
			{
			default:
			case EnemyStateMachineStates::Idle:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				break;
			case EnemyStateMachineStates::Spot:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				break;
			case EnemyStateMachineStates::Moving:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				MoveTowardsTarget();
				break;
			case EnemyStateMachineStates::Enemy_Anticipation:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				break;
			case EnemyStateMachineStates::Enemy_Slash:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				if (mStateTimer > mDamageDelay)
				GetOwner()->GetComponent<SphereColliderComponent>()->Enable();
				break;
			case EnemyStateMachineStates::Enemy_Recovery:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				GetOwner()->GetComponent<SphereColliderComponent>()->Disable();
				break;
			case EnemyStateMachineStates::Dead:
				singleTextureComponent->Enable();
				spriteMeshComponent->Disable();
				singleTextureComponent->SetTexture("SonXaaryDead");
				GetOwner()->GetComponent<SphereColliderComponent>()->Disable();
				break;
			}

			break;

		case EEnemyType::PohShigi:


			switch (mStateMachine.GetCurrentState())
			{
			default:
			case EnemyStateMachineStates::Idle:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				break;
			case EnemyStateMachineStates::Enemy_Knockback:
				singleTextureComponent->SetTexture("PohShigiStun");
				singleTextureComponent->Enable();
				spriteMeshComponent->Disable();
				break;
			case EnemyStateMachineStates::Spot:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				break;
			case EnemyStateMachineStates::Moving:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				MoveTowardsTarget();
				break;
			case EnemyStateMachineStates::Enemy_Anticipation:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				break;
			case EnemyStateMachineStates::Enemy_Slash:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				if (mStateTimer > mDamageDelay)
				GetOwner()->GetComponent<SphereColliderComponent>()->Enable();
				break;
			case EnemyStateMachineStates::Enemy_Recovery:
				singleTextureComponent->Disable();
				spriteMeshComponent->Enable();
				GetOwner()->GetComponent<SphereColliderComponent>()->Disable();
				break;
			case EnemyStateMachineStates::Dead:
				singleTextureComponent->Enable();
				spriteMeshComponent->Disable();
				singleTextureComponent->SetTexture("PohShigiDead");
				GetOwner()->GetComponent<SphereColliderComponent>()->Disable();
				break;
			}

			break;

		}

		mStateMachine.Update();

		if(mIsFlipped)
		{
			float textureWidth = float(singleTextureComponent->GetTexture()->GetWidth());
			singleTextureComponent->SetOffset(Vector3D{ textureWidth / 2.f ,0.f});
		}
		else
		{
			singleTextureComponent->SetOffset(Vector3D{ 0.f ,0.f });
		}
		
	} // Need to rework the switch structure


	
	// Update mIsPlayerInSight based on the acquisition range
	void EnemyBehaviorComponent::LookForPlayer()
	{

		auto playerEnemyTransform = GetPlayerEnemyTransform();

		auto playerTransform = *playerEnemyTransform.first;
		auto enemyTransform = *playerEnemyTransform.second;

		auto playerPos = playerTransform.GetPosition();
		auto enemyPos = enemyTransform.GetPosition();

		if ((playerPos - enemyPos).GetLength() < mSpotRadius)
		{
			mIsPlayerInSight = true;
		}
		else
		{
			if (mIsPlayerInSight && (playerPos - enemyPos).GetLength() < mLostRadius)
			{
				mIsPlayerInSight = true;
			}
			else
			{
				mIsPlayerInSight = false;
			}
		}

	}

	// Update mIsPlayerInSight based on the acquisition range
	void EnemyBehaviorComponent::ReachForPlayer()
	{

		auto playerEnemyTransform = GetPlayerEnemyTransform();

		auto playerTransform = *playerEnemyTransform.first;
		auto enemyTransform = *playerEnemyTransform.second;

		auto playerPos = playerTransform.GetPosition();
		auto enemyPos = enemyTransform.GetPosition();

		if ((playerPos - enemyPos).GetLength() < mInteractionRadius)
		{
			mIsPlayerInReach = true;
		}
		else 
		{
			mIsPlayerInReach = false;
		}

	}
	

	void EnemyBehaviorComponent::MoveTowardsTarget()
	{

		if (mIsPlayerInReach)
		{
			return;
		}

		auto playerEnemyTransform = GetPlayerEnemyTransform();

		auto enemyTransform = playerEnemyTransform.second;

		Vector3D currentGridPosition = FlowFieldPathfinderComponent::TranslateToUnitVector(enemyTransform->GetPosition());

		FlowFieldPathfinderComponent* pathfinder = mLevel->GetComponent<FlowFieldPathfinderComponent>(mPathfinderID);


		mMoveDirection = pathfinder->GetDirectionAt(currentGridPosition);

		mMoveDirection.Normalize();

		TransformComponent* transformComponent = GetOwner()->GetComponent<TransformComponent>();
		
		if (mMoveDirection.X() > 0) {
			transformComponent->SetScale({ -abs(transformComponent->GetScale().X()), transformComponent->GetScale().Y() });
		} else if (mMoveDirection.X() < 0) {
			transformComponent->SetScale({ abs(transformComponent->GetScale().X()), transformComponent->GetScale().Y() });
		}
		
		Vector3D velocity = (mMoveDirection * mMoveDistance * mLevel->GetDeltaTimeInMillisecond());

		if (!(velocity == Vector3D{ 0, 0, 0 }))
		{
			mIsTogether = false;
			
			if (mEnemyType == EEnemyType::SonXaary)
			if (mWalkTimer > 0)
			{
				mWalkTimer -= mLevel->GetDeltaTimeInMillisecond();
			}
			else
			{
				mWalkTimer = mWalkDuration;
				GetOwner()->GetComponent<AudioPlayerComponent>()->Play("walk_sonxaary");
			}

			if (mIsFrightened)
			{
				if (!(pathfinder->GetDirectionAt(FlowFieldPathfinderComponent::TranslateToUnitVector(enemyTransform->GetPosition() - velocity * 10)) == Vector3D{ 0, 0, 0 }))
					enemyTransform->SetPosition(enemyTransform->GetPosition() - velocity);
			}
			else
			{
				enemyTransform->SetPosition(enemyTransform->GetPosition() + velocity);
			}

		}
		else 
		{
			mIsTogether = true;
		}

	}

	void EnemyBehaviorComponent::Hit()
	{
		switch (mEnemyType)
		{
		case EEnemyType::KonDoori:
			Kill();
			break;
		case EEnemyType::SonXaary:
			GetOwner()->GetComponent<AudioPlayerComponent>()->Play("hit_sonxaary");
			// use light
			break;
		case EEnemyType::PohShigi:
			if (mKnockBackTimer <= 0)
			{
				GetOwner()->GetComponent<AudioPlayerComponent>()->Play("stun");
				mKnockBackTimer = mKnockBackDuration;
				GetOwner()->GetComponent<SphereColliderComponent>()->Disable();
				mStateMachine.SetState(EnemyStateMachineStates::Enemy_Knockback);
				GetOwner()->GetComponent<AnimationComponent>()->SetState(Anim_Idle); 
				mActionTimer = 0;
			}
			break;
		}
	}


	void EnemyBehaviorComponent::Kill()
	{
		mIsAlive = false;

		TextureComponent* singleTextureComponent = mLevel->GetComponent<TextureComponent>(mSingleTextureID);
		SpriteMeshComponent* spriteMeshComponent = GetOwner()->GetComponent<SpriteMeshComponent>();
		
		switch (mEnemyType)
		{
		case EEnemyType::KonDoori:
			GetOwner()->GetComponent<AudioPlayerComponent>()->Play("hit_kondoori");
			GetOwner()->GetComponent<AudioPlayerComponent>()->Play("scream_kondoori");
			spriteMeshComponent->Disable();
			singleTextureComponent->Enable();
			singleTextureComponent->SetTexture("KonDooriDead");
			break;

		case EEnemyType::SonXaary:
			spriteMeshComponent->Disable();
			singleTextureComponent->Enable();
			singleTextureComponent->SetTexture("SonXaaryDead");
			break;

		case EEnemyType::PohShigi:
			spriteMeshComponent->Disable();
			singleTextureComponent->Enable();
			singleTextureComponent->SetTexture("PohShigiDead");
			break;
		}

		GetOwner()->GetComponent<SphereColliderComponent>()->Disable();

		//Disable(); collides with the callback
	}

	void EnemyBehaviorComponent::AddSize()
	{
		mSize++;
		SetScale();
	}

	void EnemyBehaviorComponent::SetScale()
	{
		auto scale = GetOwner()->GetComponent<TransformComponent>()->GetScale();
		GetOwner()->GetComponent<TransformComponent>()->SetScale({ scale.X() * (1 + (mSize / 7.5f)), scale.Y() * (1 + (mSize / 7.5f)) });
	}

	void EnemyBehaviorComponent::SetSleepTimer()
	{
		auto playerEnemyTransform = GetPlayerEnemyTransform();

		auto playerTransform = *playerEnemyTransform.first;
		auto enemyTransform = *playerEnemyTransform.second;

		auto playerPos = playerTransform.GetPosition();
		auto enemyPos = enemyTransform.GetPosition();
		
		/*mSleepTimer = (playerPos - enemyPos).GetLength() / mMoveDistance * 10;*/ 
		mSleepTimer = 2500 + ((playerPos - enemyPos).GetLength() / 60) * 200;
	}

	void EnemyBehaviorComponent::SetEnemySingleTextureID(const ID& id)
	{
		mSingleTextureID = id; 
	}

	EnemyStateMachineStates EnemyBehaviorComponent::GetState()
	{
		return EnemyStateMachineStates(mStateMachine.GetCurrentState());
	}


	std::pair<TransformComponent*, TransformComponent*> EnemyBehaviorComponent::GetPlayerEnemyTransform() const
	{
		TransformComponent* playerTransform = mLevel->GetObject<PlayerObject>(mPlayerID)->GetComponent<TransformComponent>();
		TransformComponent* enemyTransform = GetOwner()->GetComponent<TransformComponent>();
		return std::make_pair(playerTransform, enemyTransform);
	}
	
}
