/*
    File Name: EnemyBehaviorComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"
#include "EnemyStateMachine.h"
#include "GameEnum.h"
#include "TransformComponent.h"
#include "Serializer.h"



namespace uruk
{
	class EnemyBehaviorComponent final : public Component<EnemyBehaviorComponent, EUpdateType::BY_FRAME, update_priorities::HIGH_PRIORITY, 0>
	{
	public:
		EnemyBehaviorComponent(EEnemyType enemyType, ID pathFinderID, ID playerID);

		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;
		
		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);

		void OnBeginPlay() override;
		void OnUpdate() override;

		[[nodiscard]] float GetActionTimer() const noexcept { return mActionTimer; }
		void AddToActionTimerBy(float arg) { mActionTimer += arg; }
		void ResetActionTimer() { mActionTimer = 0; }

		[[nodiscard]] EEnemyType GetEnemyType() const noexcept { return mEnemyType; };
		[[nodiscard]] bool IsAlive() const noexcept { return mIsAlive; }

		void Hit();
		void Kill();

		[[nodiscard]] bool IsPlayerInSight() const noexcept { return mIsPlayerInSight; }
		[[nodiscard]] bool IsPlayerInReach() const noexcept { return mIsPlayerInReach; }

		[[nodiscard]] int GetSize() const noexcept { return mSize; }
		void AddSize();
		void SetScale();

		void SetPlayerID(ID arg) noexcept { mPlayerID = arg; }
		void SetPathfinderID(ID arg) noexcept { mPathfinderID = arg; }

		void SetSleepTimer();
		float GetSleepTimer() { return mSleepTimer; }

		bool IsFrightened() { return mIsFrightened; }
		bool IsTogether() { return mIsTogether; }

		float GetKnockBackTimer() { return mKnockBackTimer; }

		void SetState(EnemyStateMachineStates arg) { mStateMachine.SetState(arg); };

		void SetEnemySingleTextureID(const ID& id);
		
		EnemyStateMachineStates GetState();
		
		const float mSpotDuration = 1000;
		
		float mAnticipationDuration = 500; // Default is Kondoori
		float mSlashDuration = 300;
		float mRecoveryDuration = 500;

		const float mPoshigiAnticipationDuration = 700;
		const float mPoshigiSlashDuration = 130;
		const float mPoshigiRecoveryDuration = 900;
		
		const float mSonXaaryAnticipationDuration = 500;
		const float mSonxaarySlashDuration = 340;
		const float mSonxaaryRecoveryDuration = 500;
		
		const float mKnockBackDuration = 3000;

		const float mWalkDuration = 200;
		float mDamageDelay = 240;

		const float mSonxaaryDamageDelay = 250;
		const float mPoshigiDamageDelay = 20;
		
	private:
		EnemyStateMachine mStateMachine;

		bool mIsFrightened = false;
		
		EnemyStateMachineStates mPreviousState;
		float mStateTimer = 0;

		float mKnockBackTimer = 0;

		int mSize = 0;
		float mWalkTimer = 0;

		Vector3D mTargetGridPosition = {0, 0};

		void LookForPlayer();
		void ReachForPlayer();
		
		void MoveTowardsTarget();

		[[nodiscard]] std::pair<TransformComponent*, TransformComponent*> GetPlayerEnemyTransform() const;
		
		const EEnemyType mEnemyType;
		ID mPlayerID;
		ID mPathfinderID;
		ID mSingleTextureID;
		
		bool mIsAlive = true; // This will only stay inside here (no external statistics)

		float mActionTimer = 0;

		float mSleepTimer = 0;

		const float mInteractionRadius = TILE_WIDTH * 1.5f; // Fighting range
		const float mSpotRadius = 400.0f; // Player spot range
		const float mLostRadius = 600.0f; // Player lose range

		bool mIsPlayerInSight = false;
		bool mIsPlayerInReach = false;

		bool mIsTogether = false;

		bool mIsFlipped = false;

		float mMoveDistance = 0; // Will be initialized in the constructor

		Vector3D mMoveDirection = { 0, 0 };
		
	};
}