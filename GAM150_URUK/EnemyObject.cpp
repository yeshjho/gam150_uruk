/*
    File Name: EnemyObject.cpp
    Project Name: U.R.U.K
	Author(s):
		-Joonho Hwang
		Contribution(s):
			collision-related things
		-Doyoon Kim
		Contribution(s):
			animation-related things
		-Seunggeon Kim
		Contribution(s):
			all the others
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "EnemyObject.h"


#include "AudioPlayerComponent.h"
#include "EnemyBehaviorComponent.h"
#include "Game.h"
#include "SphereColliderComponent.h"
#include "TextureComponent.h"
#include "TransformComponent.h"
#include "AnimationComponent.h"
#include "EnemyAnimationStateMachine.h"
#include "GameResourceManager.h"
#include "SpriteMeshComponent.h"
#include "SkeletonComponent.h"


namespace uruk
{
	class EnemyBeingAttackedTriggerEnterCallback final : public CustomCallbackBase<EnemyBeingAttackedTriggerEnterCallback, ICollisionCallbackBase>
	{
	public:
		void Execute(IColliderComponent* self, IColliderComponent*) override
		{
			if (Game::GetCurrentLevel()->GetComponent(self->GetColliderTypeID(), self->GetColliderID())->GetOwner()->GetComponent<EnemyBehaviorComponent>()->GetSleepTimer() <= 0)
			Game::GetCurrentLevel()->GetComponent(self->GetColliderTypeID(), self->GetColliderID())->GetOwner()->GetComponent<EnemyBehaviorComponent>()->Hit();
#ifdef UI_DEBUG_DRAW
			std::cout << " EnemyAttacked Trigger Enter!" << std::endl;
#endif
		}
	};


	
	EnemyObject::EnemyObject(EEnemyType enemyType, const Vector3D& coord, ID roomID, ID playerID) :
	cacheEnemyType(enemyType), cacheCoord(coord), cachePathfinderID(roomID), cachePlayerID(playerID)
	{
		
	}

	EnemyObject::EnemyObject() : cacheCoord({0, 0}), cacheEnemyType(EEnemyType::KonDoori)
	{
		
	}

	void EnemyObject::OnConstructEnd()
	{

		AddComponent<TransformComponent>(cacheCoord);
		ID enemyBehaviorComponentID = AddComponent<EnemyBehaviorComponent>(cacheEnemyType, cachePathfinderID, cachePlayerID);
		EnemyBehaviorComponent* enemyBehaviorComponent = GetComponent<EnemyBehaviorComponent>();
		
		AddComponent<AnimationComponent>();
		std::unique_ptr<EnemyAnimationStateMachine> enemyAnimStateMachine = std::make_unique<EnemyAnimationStateMachine>();
		AnimationComponent* animComponent = GetComponent<AnimationComponent>(); 
		animComponent->SetStateMachine(std::move(enemyAnimStateMachine));
		animComponent->SetShouldCrossfade(true);
		animComponent->SetCrossFadingDurationRatioToClip(0.2f);
		
		std::string textureString;
		CollisionTag collisionTag;
		CollisionTag attackCollisionTag;
		ID textureComponentID;

		int animWidth = 0;
		int animHeight = 0;
		switch (cacheEnemyType)
		{
		case EEnemyType::KonDoori:
		{
			textureString = "KonDoori";
			collisionTag = CollisionTag::KonDoori;
			attackCollisionTag = CollisionTag::KonDooriAttack;
			textureComponentID = AddComponent<TextureComponent>(textureString, EObjectZOrder::KONDOORI, 0.0f, 0.0f, 0, 0, 0, 0, Vector3D{ -TILE_WIDTH / 2, -TILE_HEIGHT / 2 });
			auto KonDooriAnimClip = GameResourceManager::GetAnimationClip("KonDooriIdleAnimation");
			AddComponent<SpriteMeshComponent>(KonDooriAnimClip->animSamples[0].spriteMesh, EObjectZOrder::KONDOORI, KonDooriAnimClip->animSpaceWidth, KonDooriAnimClip->animSpaceHeight);
			animWidth = KonDooriAnimClip->animSpaceWidth;
			animHeight = KonDooriAnimClip->animSpaceHeight;
			GetComponent<SpriteMeshComponent>()->SetOffsetFromTransformOrigin({ -(float)animWidth / 4.f,0.f });
			AddComponent<SkeletonComponent>(KonDooriAnimClip->animSamples[0].skeleton);
			animComponent->MapStateWithAnimClip(Anim_Moving, GameResourceManager::GetAnimationClip("KonDooriTrackAnimation"));
			animComponent->MapStateWithAnimClip(Anim_Anticipation, GameResourceManager::GetAnimationClip("KonDooriAnticipationAnimation"));
			animComponent->MapStateWithAnimClip(Anim_Idle, GameResourceManager::GetAnimationClip("KonDooriIdleAnimation"));
			animComponent->MapStateWithAnimClip(Anim_Spot, GameResourceManager::GetAnimationClip("KonDooriSeenAnimation"));
			animComponent->MapStateWithAnimClip(Anim_Recovery, GameResourceManager::GetAnimationClip("KonDooriRecoveryAnimation"));
			animComponent->MapStateWithAnimClip(Anim_Slash, GameResourceManager::GetAnimationClip("KonDooriSlashAnimation"));
		}
			break;
		case EEnemyType::SonXaary:
		{
			textureString = "SonXaary";
			collisionTag = CollisionTag::SonXaary;
			attackCollisionTag = CollisionTag::SonXaaryAttack;
			auto SonXaaryAnimClip = GameResourceManager::GetAnimationClip("SonXaaryIdleAnimation");
			AddComponent<SpriteMeshComponent>(SonXaaryAnimClip->animSamples[0].spriteMesh, EObjectZOrder::SONXAARY, SonXaaryAnimClip->animSpaceWidth, SonXaaryAnimClip->animSpaceHeight);
			animWidth = SonXaaryAnimClip->animSpaceWidth;
			animHeight = SonXaaryAnimClip->animSpaceHeight;
			textureComponentID = AddComponent<TextureComponent>(textureString, EObjectZOrder::SONXAARY, 0.0f, 0.0f, 0, 0, 0, 0, Vector3D{ -TILE_WIDTH/2, 0.f });
			GetComponent<SpriteMeshComponent>()->SetOffsetFromTransformOrigin({ -40.f,0.f });
			AddComponent<SkeletonComponent>(SonXaaryAnimClip->animSamples[0].skeleton);
			animComponent->MapStateWithAnimClip(Anim_Moving, GameResourceManager::GetAnimationClip("SonXaaryTrackAnimation"));
			animComponent->MapStateWithAnimClip(Anim_Anticipation, GameResourceManager::GetAnimationClip("SonXaaryAnticipationAnimation"));
			animComponent->MapStateWithAnimClip(Anim_Idle, GameResourceManager::GetAnimationClip("SonXaaryIdleAnimation"));
			animComponent->MapStateWithAnimClip(Anim_Spot, GameResourceManager::GetAnimationClip("SonXaarySeenAnimation"));
			animComponent->MapStateWithAnimClip(Anim_Recovery, GameResourceManager::GetAnimationClip("SonXaaryRecoveryAnimation"));
			animComponent->MapStateWithAnimClip(Anim_Slash, GameResourceManager::GetAnimationClip("SonXaarySlashAnimation"));
		}
			break;
		case EEnemyType::PohShigi:
		{
			textureString = "PohShigi";
			collisionTag = CollisionTag::PohShigi;
			attackCollisionTag = CollisionTag::PohShigiAttack;
			textureComponentID = AddComponent<TextureComponent>(textureString, EObjectZOrder::POHSHIGI, 0.0f, 0.0f, 0, 0, 0, 0, Vector3D{ -10, -10 });
			auto PoshigiAnimClip = GameResourceManager::GetAnimationClip("PohshigiIdleAnimation");
			AddComponent<SpriteMeshComponent>(PoshigiAnimClip->animSamples[0].spriteMesh, EObjectZOrder::POHSHIGI, PoshigiAnimClip->animSpaceWidth, PoshigiAnimClip->animSpaceHeight);
			animWidth = PoshigiAnimClip->animSpaceWidth;
			animHeight = PoshigiAnimClip->animSpaceHeight;
			GetComponent<SpriteMeshComponent>()->SetOffsetFromTransformOrigin({ -(float)animWidth / 4.f,0.f });
			AddComponent<SkeletonComponent>(PoshigiAnimClip->animSamples[0].skeleton);
			animComponent->MapStateWithAnimClip(Anim_Moving, GameResourceManager::GetAnimationClip("PohshigiTrackAnimation"));
			animComponent->MapStateWithAnimClip(Anim_Anticipation, GameResourceManager::GetAnimationClip("PohshigiAnticipationAnimation"));
			animComponent->MapStateWithAnimClip(Anim_Idle, GameResourceManager::GetAnimationClip("PohshigiIdleAnimation"));
			animComponent->MapStateWithAnimClip(Anim_Spot, GameResourceManager::GetAnimationClip("PohshigiSeenAnimation"));
			animComponent->MapStateWithAnimClip(Anim_Recovery, GameResourceManager::GetAnimationClip("PohshigiRecoveryAnimation"));
			animComponent->MapStateWithAnimClip(Anim_Slash, GameResourceManager::GetAnimationClip("PohshigiSlashAnimation"));
		}
			break;
		}
		mLevel->GetComponent<TextureComponent>(textureComponentID)->Disable();
		enemyBehaviorComponent->SetEnemySingleTextureID(textureComponentID);

		switch (GetComponent<EnemyBehaviorComponent>()->GetEnemyType())
		{
		default:
		case EEnemyType::KonDoori:
		{
			// This should be the first sphere collider
			AddComponent<SphereColliderComponent>(Vector3D{ TILE_WIDTH / 2.f, TILE_HEIGHT / 2.f }, TILE_WIDTH * 1.5f, attackCollisionTag, CollisionTag::Uruk);
			GetComponent<SphereColliderComponent>()->Disable();
			
			const ID hitbox = AddComponent<SphereColliderComponent>(Vector3D{ TILE_WIDTH / 2.f, TILE_HEIGHT / 2.f }, TILE_WIDTH / 2.f, collisionTag, CollisionTag::UrukAttack);
			mLevel->GetComponent<SphereColliderComponent>(hitbox)->SetOnCollisionEnter(std::make_unique<EnemyBeingAttackedTriggerEnterCallback>());
		}
		break;
		case EEnemyType::PohShigi:
		{
			// This should be the first sphere collider
			AddComponent<SphereColliderComponent>(Vector3D{ TILE_WIDTH / 2.f, TILE_HEIGHT / 2.f }, TILE_WIDTH * 1.5f, attackCollisionTag, CollisionTag::Uruk);
			GetComponent<SphereColliderComponent>()->Disable();
			
			const ID hitbox = AddComponent<SphereColliderComponent>(Vector3D{ TILE_WIDTH / 2.f, TILE_HEIGHT / 2.f }, TILE_WIDTH / 2.f, collisionTag, CollisionTag::UrukAttack);
			mLevel->GetComponent<SphereColliderComponent>(hitbox)->SetOnCollisionEnter(std::make_unique<EnemyBeingAttackedTriggerEnterCallback>());
		}
		break;
		case EEnemyType::SonXaary:
		{
			// This should be the first sphere collider
			AddComponent<SphereColliderComponent>(Vector3D{ TILE_WIDTH / 2.f, animHeight / 2.f }, TILE_WIDTH * 1.75f, attackCollisionTag, CollisionTag::Uruk);
			GetComponent<SphereColliderComponent>()->Disable();
			
			const ID hitbox = AddComponent<SphereColliderComponent>(Vector3D{ TILE_WIDTH / 2.f, animHeight / 2.f }, animWidth / 2.f, collisionTag, CollisionTag::UrukAttack);
			mLevel->GetComponent<SphereColliderComponent>(hitbox)->SetOnCollisionEnter(std::make_unique<EnemyBeingAttackedTriggerEnterCallback>());
		}
		break;
		}

		AddComponent<AudioPlayerComponent>();
	}
	
}
