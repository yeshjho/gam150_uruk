/*
    File Name: EnemyObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"
#include "GameEnum.h"
#include "Vector3D.h"



namespace uruk
{

	class EnemyObject final : public Object<EnemyObject>
	{
	public:
		EnemyObject();
		
		EnemyObject(EEnemyType enemyType, const Vector3D& coord, ID pathFinderID, ID mPlayerID);
		
		void OnConstructEnd() override;

	private:
		Vector3D cacheCoord;
		ID cachePathfinderID;

		// Player attributes (above) + enemy specific attributes (below)
		EEnemyType cacheEnemyType;
		ID cachePlayerID;
	};

}