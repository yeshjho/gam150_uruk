/*
    File Name: EnemyStateMachine.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "EnemyStateMachine.h"


#include "AudioPlayerComponent.h"
#include "EnemyBehaviorComponent.h"
#include "Game.h"
#include "SphereColliderComponent.h"


namespace uruk
{
	EnemyStateMachine::EnemyStateMachine()
		: CustomStateMachineBase<EnemyStateMachine>(Idle,
			{
				StateTransitionConditionsPair
				{
					EnemyStateMachineStates::Spot,
					{
						TransitionCondition
						{
							EnemyStateMachineStates::Moving,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto enemyBehavior = currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID);
	if (enemyBehavior->GetActionTimer() > enemyBehavior->mSpotDuration)
	{
		if (enemyBehavior->GetEnemyType() == EEnemyType::SonXaary &&
			enemyBehavior->IsTogether()) 
		{
			return false;
		}
		enemyBehavior->ResetActionTimer();
		return true;
	}
	else
	{
		enemyBehavior->AddToActionTimerBy(currentLevel->GetDeltaTimeInMillisecond());
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					EnemyStateMachineStates::Moving,
					{
						TransitionCondition
						{
							EnemyStateMachineStates::Enemy_Anticipation,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto enemyBehavior = currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID);
	if (enemyBehavior->IsPlayerInReach())
	{
		if (enemyBehavior->GetEnemyType() == EEnemyType::SonXaary)
		{
			if (!enemyBehavior->IsFrightened())
			return true;
		}
		else
		{
			return true;
		}
	}
	return false;
}
						},
						TransitionCondition
						{
							EnemyStateMachineStates::Idle,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto enemyBehavior = currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID);
	if (!enemyBehavior->IsPlayerInSight())
	{
		return true;
	}
	return false;
}
						},
						TransitionCondition
						{
							EnemyStateMachineStates::Dead,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto enemyBehavior = currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID);
	if (!enemyBehavior->IsAlive())
	{
		return true;
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					EnemyStateMachineStates::Idle,
					{
						TransitionCondition
						{
							EnemyStateMachineStates::Spot,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto enemyBehavior = currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID);
	if (enemyBehavior->IsPlayerInSight() && enemyBehavior->IsAlive())
	{
		switch (enemyBehavior->GetEnemyType())
		{
		case EEnemyType::KonDoori:
			currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID)->GetOwner()->GetComponent<AudioPlayerComponent>()->Play("scream_kondoori");
			break;
		case EEnemyType::SonXaary:
			currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID)->GetOwner()->GetComponent<AudioPlayerComponent>()->Play("scream_sonxaary");
			break;
		case EEnemyType::PohShigi:
			currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID)->GetOwner()->GetComponent<AudioPlayerComponent>()->Play("scream_pohshigi");
			break;
		}
		
		return true;
	}
	return false;
}
						},
						TransitionCondition
						{
							EnemyStateMachineStates::Dead,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto enemyBehavior = currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID);
	if (!enemyBehavior->IsAlive())
	{
		return true;
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					EnemyStateMachineStates::Enemy_Anticipation,
					{
						TransitionCondition
						{
							EnemyStateMachineStates::Enemy_Slash,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto enemyBehavior = currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID);
	if (enemyBehavior->GetActionTimer() > enemyBehavior->mAnticipationDuration)
	{
		switch (enemyBehavior->GetEnemyType())
		{
		case EEnemyType::KonDoori:
			currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID)->GetOwner()->GetComponent<AudioPlayerComponent>()->Play("scream_kondoori");
			break;
		case EEnemyType::SonXaary:
			currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID)->GetOwner()->GetComponent<AudioPlayerComponent>()->Play("scream_sonxaary");
			break;
		case EEnemyType::PohShigi:
			currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID)->GetOwner()->GetComponent<AudioPlayerComponent>()->Play("scream_pohshigi");
			break;
		}
		//enemyBehavior->GetOwner()->GetComponent<SphereColliderComponent>()->Enable();
		enemyBehavior->ResetActionTimer();
		return true;
	}
	else
	{
		enemyBehavior->AddToActionTimerBy(currentLevel->GetDeltaTimeInMillisecond());
	}
	return false;
}
						},
						TransitionCondition
						{
							EnemyStateMachineStates::Dead,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto enemyBehavior = currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID);
	if (!enemyBehavior->IsAlive())
	{
		return true;
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					EnemyStateMachineStates::Enemy_Slash,
					{
						TransitionCondition
						{
							EnemyStateMachineStates::Enemy_Recovery,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto enemyBehavior = currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID);
	if (enemyBehavior->GetActionTimer() > enemyBehavior->mSlashDuration)
	{
		enemyBehavior->GetOwner()->GetComponent<SphereColliderComponent>()->Disable();
		enemyBehavior->ResetActionTimer();
		return true;
	}
	else
	{
		enemyBehavior->AddToActionTimerBy(currentLevel->GetDeltaTimeInMillisecond());
	}
	return false;
}
						},
						TransitionCondition
						{
							EnemyStateMachineStates::Dead,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto enemyBehavior = currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID);
	if (!enemyBehavior->IsAlive())
	{
		return true;
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					EnemyStateMachineStates::Enemy_Recovery,
					{
						TransitionCondition
						{
							EnemyStateMachineStates::Moving,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto enemyBehavior = currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID);
	if (enemyBehavior->GetActionTimer() > enemyBehavior->mRecoveryDuration)
	{
		enemyBehavior->ResetActionTimer();
		return true;
	}
	else
	{
		enemyBehavior->AddToActionTimerBy(currentLevel->GetDeltaTimeInMillisecond());
	}
	return false;
}
						},
						TransitionCondition
						{
							EnemyStateMachineStates::Dead,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto enemyBehavior = currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID);
	if (!enemyBehavior->IsAlive())
	{
		return true;
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					EnemyStateMachineStates::Dead,
					{
					}
				},
				StateTransitionConditionsPair
				{
					EnemyStateMachineStates::Enemy_Knockback,
					{
						TransitionCondition
						{
							EnemyStateMachineStates::Idle,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto enemyBehavior = currentLevel->GetComponent<EnemyBehaviorComponent>(ownerID);
	if (enemyBehavior->GetKnockBackTimer() <= 0)
	{
		return true;
	}
	return false;
}
						},
					}
				},
			}
			)
	{}
}
