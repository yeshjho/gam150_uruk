/*
    File Name: EnemyStateMachine.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "StateMachine.h"



namespace uruk
{
	enum EnemyStateMachineStates
	{
		Spot,
		Moving,
		Idle,
		Enemy_Anticipation,
		Enemy_Slash,
		Enemy_Recovery,
		Enemy_Knockback,
		Dead,
	};


	class EnemyStateMachine : public CustomStateMachineBase<EnemyStateMachine>
	{
	public:
		EnemyStateMachine();
	};
}
