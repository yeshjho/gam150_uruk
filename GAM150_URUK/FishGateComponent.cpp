/*
    File Name: FishGateComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "FishGateComponent.h"

#include "EnemyBehaviorComponent.h"
#include "Game.h"
#include "GameEnum.h"
#include "GameResourceManager.h"
#include "PlayerObject.h"
#include "Serializer.h"
#include "TextureComponent.h"


namespace uruk
{
	FishGateComponent::FishGateComponent(const unsigned int desiredLifeCount)
		: mDesiredLifeCount(desiredLifeCount)
	{}


	void FishGateComponent::OnBeginPlay()
	{
		if (!mIndicatorTextureIDs.empty())
		{
			return;
		}
		
		static constexpr float INDICATOR_INTERVAL = 5.f;
		const float INDICATOR_TEXTURE_WIDTH = static_cast<float>(GameResourceManager::GetImage("FishGateIndicatorEmpty")->GetWidth());
		
		IObject* const owner = GetOwner();

		for (unsigned int i = 0; i < mDesiredLifeCount; ++i)
		{
			const ID textureID = owner->AddComponent<TextureComponent>("FishGateIndicatorEmpty", EObjectZOrder::FISHGATE_INDICATOR);
			const float OFFSET_X = TILE_WIDTH / 2.f + (INDICATOR_INTERVAL + INDICATOR_TEXTURE_WIDTH) * (static_cast<float>(i) - static_cast<float>(mDesiredLifeCount) / 2.f);
			mLevel->GetComponent<TextureComponent>(textureID)->SetOffset({ OFFSET_X , TILE_HEIGHT * 4.f / 2.f });

			mIndicatorTextureIDs.push_back(textureID);
		}
	}

	void FishGateComponent::OnUpdate()
	{
		// Starting with player
		unsigned int lifeCount = 1;

		// Had to do it this way since GetAllActive() doesn't actually return only the active ones.
		// Rather, it checks the next one is active or not while running ++ operation.
		for (auto it = mLevel->GetAllActiveComponents<EnemyBehaviorComponent>().begin(); it != it.end(); ++it)
		{
			if (it->IsAlive() && it->GetSleepTimer() <= 0)
			lifeCount++;
		}

		for (unsigned int i = 0; i < std::min(lifeCount, mDesiredLifeCount); ++i)
		{
			mLevel->GetComponent<TextureComponent>(mIndicatorTextureIDs.at(i))->SetTexture("FishGateIndicatorFull");
		}
		for (unsigned int i = std::min(lifeCount, mDesiredLifeCount); i < mDesiredLifeCount; ++i)
		{
			mLevel->GetComponent<TextureComponent>(mIndicatorTextureIDs.at(i))->SetTexture("FishGateIndicatorEmpty");
		}
		for (int i = 0; i < clamp(static_cast<int>(lifeCount) - static_cast<int>(mDesiredLifeCount), 0, static_cast<int>(mDesiredLifeCount)); ++i)
		{
			mLevel->GetComponent<TextureComponent>(mIndicatorTextureIDs.at(i))->SetTexture("FishGateIndicatorOverrun");
		}

		const bool isOpen = lifeCount == mDesiredLifeCount;

		if (!mIsOpen && isOpen)
		{
			GetOwner()->GetComponent<TextureComponent>()->SetTexture("FishGateOpen");
		}
		else if (mIsOpen && !isOpen)
		{
			GetOwner()->GetComponent<TextureComponent>()->SetTexture("FishGateClosed");
		}

		mIsOpen = isOpen;
	}
	

	void FishGateComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mDesiredLifeCount);
	}

	IComponent* FishGateComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		unsigned int desiredLifeCount;

		deserialize(inputFileStream, desiredLifeCount);

		return level->LoadComponent<FishGateComponent>(id, desiredLifeCount);
	}

	
	void FishGateComponent::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mIndicatorTextureIDs);
	}

	
	void FishGateComponent::OnLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mIndicatorTextureIDs);
	}
}
