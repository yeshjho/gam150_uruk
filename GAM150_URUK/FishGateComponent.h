/*
    File Name: FishGateComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"



namespace uruk
{
	class FishGateComponent final : public Component<FishGateComponent, EUpdateType::BY_FRAME, update_priorities::HIGH_PRIORITY, 0>
	{
	public:
		FishGateComponent(unsigned int desiredLifeCount);


		void OnBeginPlay() override;
		
		void OnUpdate() override;

		
		[[nodiscard]] constexpr unsigned int GetDesiredLifeCount() const noexcept { return mDesiredLifeCount; }
		[[nodiscard]] constexpr bool IsOpen() const noexcept { return mIsOpen; }

		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);

		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;


		
	private:
		unsigned int mDesiredLifeCount;
		bool mIsOpen = false;

		std::vector<ID> mIndicatorTextureIDs;
	};
}