/*
    File Name: FishGateObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "FishGateObject.h"


#include "BoxColliderComponent.h"
#include "FishGateComponent.h"
#include "Game.h"
#include "GameEnum.h"
#include "LevelLoaderComponent.h"
#include "PlayerBehaviorComponent.h"
#include "TextureComponent.h"



namespace uruk
{
	class FishGateTriggerGoingCallback final : public CustomCallbackBase<FishGateTriggerGoingCallback, ICollisionCallbackBase>
	{
	public:
		void Execute(IColliderComponent* self, IColliderComponent* collideWith) override
		{
			if (collideWith->GetColliderType() != ColliderType::BoxCollider)
			{
				return;
			}

			IObject* owner = Game::GetCurrentLevel()->GetComponent(self->GetColliderTypeID(), self->GetColliderID())->GetOwner();
			if (!owner->GetComponent<FishGateComponent>()->IsOpen())
			{
				return;
			}

			LevelLoaderComponent* const levelLoader = owner->GetComponent<LevelLoaderComponent>();
			PlayerBehaviorComponent* const playerBehavior = Game::GetCurrentLevel()->GetAllActiveComponents<PlayerBehaviorComponent>().begin();
			int levelIndex = playerBehavior->GetCurrentLevelIndex();
			playerBehavior->SetCurrentLevelIndex(++levelIndex);
			
			std::apply(&LevelLoaderComponent::SetLevelToLoad, std::tuple_cat(std::make_tuple(levelLoader), LevelLoaderComponent::FIRST_LEVEL_BY_FLOOR.at(levelIndex)));
			LevelLoaderComponent::LoadLevelsOnFloor("levels/levels.txt", levelIndex);
			
			levelLoader->Load();
		}
	};


	
	FishGateObject::FishGateObject(const Vector3D& coord, const unsigned int lifeCount)
		: cacheCoord(coord), cacheLifeCount(lifeCount)
	{}


	void FishGateObject::OnConstructEnd()
	{
		AddComponent<FishGateComponent>(cacheLifeCount);
		AddComponent<TransformComponent>(cacheCoord);

		AddComponent<LevelLoaderComponent>("");

		AddComponent<BoxColliderComponent>(Vector3D{ -TILE_WIDTH / 2.f, -TILE_HEIGHT / 2.f }, TILE_WIDTH * 2, TILE_HEIGHT * 2, CollisionTag::Fishgate, CollisionTag::Uruk, true);
		GetComponent<BoxColliderComponent>()->SetOnCollisionGoing(std::make_unique<FishGateTriggerGoingCallback>());
		
		AddComponent<TextureComponent>("FishGateClosed", EObjectZOrder::FISH_GATE);
		GetComponent<TextureComponent>()->SetOffset(-TILE_WIDTH / 2.f, -TILE_HEIGHT / 2.f);
	}
}
