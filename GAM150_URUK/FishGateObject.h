/*
    File Name: FishGateObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"

#include "Vector3D.h"



namespace uruk
{
	class FishGateObject final : public Object<FishGateObject>
	{
	public:
		FishGateObject(const Vector3D& coord = { 0, 0 }, unsigned int lifeCount = 0);

		void OnConstructEnd() override;

	private:
		Vector3D cacheCoord;
		
		unsigned int cacheLifeCount;
	};
}