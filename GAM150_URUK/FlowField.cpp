/*
    File Name: FlowField.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "FlowField.h"



namespace uruk
{

	FlowField::FlowField(Vector3D dimension) : mDimension(dimension)
	{

		mNodes.resize(size_t(dimension.X()));
		for (auto& e : mNodes)
		{
			e.resize(size_t(dimension.Y()));
		}

		for (int i = 0; i < GetHeight(); i++)
		{
			for (int j = 0; j < GetWidth(); j++)
			{
				mNodes[j][i].SetPosition({ double(j), double(i) });
			}
		}

	}

	void FlowField::ResetHeatMap()
	{

		for (auto& i : mNodes)
		{
			for (auto& j : i)
			{
				j.SetEvaluated(false);
			}
		}

	}

	Node* FlowField::GetNodePtr(Vector3D nodePos) noexcept
	{

		return &mNodes[int(nodePos.X())][int(nodePos.Y())];

	}

	const Node* FlowField::GetNodePtr (Vector3D nodePos) const noexcept
	{
		return &mNodes[int(nodePos.X())][int(nodePos.Y())];
	}

	int FlowField::GetCostAt(Vector3D nodePos) const noexcept
	{

		return mNodes[int(nodePos.X())][int(nodePos.Y())].GetCost();

	}

	bool FlowField::IsEvaluatedAt(Vector3D nodePos) const noexcept
	{

		return mNodes[int(nodePos.X())][int(nodePos.Y())].IsEvaluated();

	}



	// Get all adjacent nodes except for the ones tagged as obstacles
	std::vector<Node*> FlowField::GetAdjacentNodes(Vector3D nodePos, bool is2D)
	{
		std::vector<Node*> tempNodes;

		Vector3D from{ nodePos.X() - 1, nodePos.Y() - 1 };
		Vector3D to{ nodePos.X() + 1, nodePos.Y() + 1 };

		// Check for border up
		if (nodePos.Y() == 0) { from = Vector3D(from.X(), 0.0f); }

		// Check for border down
		if (nodePos.Y() == double(GetHeight()) - 1) { to = Vector3D(double(to.X()), double(GetHeight()) - 1); }

		// Check for border left
		if (nodePos.X() == 0) { from = Vector3D(0.0f, from.Y()); }

		// Check for border right
		if (nodePos.X() == double(GetWidth()) - 1) { to = Vector3D(double(GetWidth()) - 1, double(to.Y())); }

		for (int i = int(from.Y()); i < int(to.Y()) + 1; i++)
		{
			for (int j = int(from.X()); j < int(to.X()) + 1; j++)
			{
				if (j == nodePos.X() && i == nodePos.Y()) { continue; }

				if (is2D)
				{
					// Limit the search to the horizontal & vertical lines
					if (j == nodePos.X() - 1 && i == nodePos.Y() - 1) { continue; }
					if (j == nodePos.X() + 1 && i == nodePos.Y() + 1) { continue; }
					if (j == nodePos.X() + 1 && i == nodePos.Y() - 1) { continue; }
					if (j == nodePos.X() - 1 && i == nodePos.Y() + 1) { continue; }
				}

				if (mNodes[j][i].GetNodeType() == ENodeType::Obstacle) { continue; }
				tempNodes.push_back(&mNodes[j][i]);
			}
		}

		return tempNodes;
	}

}