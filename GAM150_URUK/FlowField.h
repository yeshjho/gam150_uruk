/*
    File Name: FlowField.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <vector>
#include "Serializer.h"
#include "Node.h"



namespace uruk
{

	class FlowField
	{
	public:
		FlowField(Vector3D dimension);
		
		void ResetHeatMap();

		[[nodiscard]] Node* GetNodePtr(Vector3D) noexcept;
		[[nodiscard]] const Node* GetNodePtr (Vector3D) const noexcept;

		[[nodiscard]] constexpr int GetWidth() const noexcept { return int(mDimension.X()); }
		[[nodiscard]] constexpr int GetHeight() const noexcept { return int(mDimension.Y()); }

		[[nodiscard]] int GetCostAt(Vector3D) const noexcept;
		[[nodiscard]] bool IsEvaluatedAt(Vector3D) const noexcept;

		std::vector<Node*> GetAdjacentNodes(Vector3D, bool is2D);
		
		template<typename>
		friend void serialize (std::ofstream& outputFileStream, const FlowField& t);
		
		template<typename>
		friend void deserialize (std::ifstream& inputFileStream, FlowField& t);

	private:
		std::vector<std::vector<Node>> mNodes; // The container for all nodes, it is first initialized by width, then initialized by height
		Vector3D mDimension = { 0, 0 };
	};

	template<typename = std::true_type>
	void serialize(std::ofstream& outputFileStream, const FlowField& t)
	{
		serialize (outputFileStream, t.mNodes);
		serialize (outputFileStream, t.mDimension);
	}

	template<typename = std::true_type>
	void deserialize (std::ifstream& inputFileStream, FlowField& t)
	{
		deserialize (inputFileStream, t.mNodes);
		deserialize (inputFileStream, t.mDimension);
	}

}
