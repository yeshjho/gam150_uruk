/*
    File Name: FlowFieldPathfinderComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include <doodle/doodle.hpp>
#include "Serializer.h"
#include <queue> // For BFS Algorithm
#include "FlowFieldPathfinderComponent.h"
#include "TransformComponent.h"
#include "GameEnum.h"

using namespace doodle;



namespace uruk
{

	FlowFieldPathfinderComponent::FlowFieldPathfinderComponent() : mFlowField({ 0, 0 }), mPathfinderDimension({ 0, 0 }),
		ZOrderBase(EObjectZOrder::Debug)
	{

	}

	FlowFieldPathfinderComponent::FlowFieldPathfinderComponent(Vector3D dimension) : mFlowField(dimension), mPathfinderDimension(dimension),
		ZOrderBase(EObjectZOrder::Debug)
	{

	}

	void FlowFieldPathfinderComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mPathfinderDimension);
	}

	IComponent* FlowFieldPathfinderComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		Vector3D containerDimension = { 0, 0 };

		deserialize(inputFileStream, containerDimension);

		return level->LoadComponent<FlowFieldPathfinderComponent>(id, containerDimension);
	}

	void FlowFieldPathfinderComponent::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mFlowField);
	}

	void FlowFieldPathfinderComponent::OnLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mFlowField);
	}

	void FlowFieldPathfinderComponent::SetGoalNode(Vector3D nodePos)
	{

		// What if the target goes out of bounds?
		if (nodePos.X() < 0 || nodePos.Y() < 0 || nodePos.X() > mFlowField.GetWidth() - 1 || nodePos.Y() > mFlowField.GetHeight() - 1)
		{
			return;
		}

		nodePos = { std::clamp(nodePos.X(), 0.f, float(mFlowField.GetWidth() - 1)), nodePos.Y() };
		nodePos = { nodePos.X(), std::clamp( nodePos.Y(), 0.f, float(mFlowField.GetHeight() - 1)) };
		
		mpGoalNode = mFlowField.GetNodePtr(nodePos);
		if (mpPreviousGoalNode != nullptr) 
		{
			SetTypeAt(mpPreviousGoalNode->GetPosition(), ENodeType::Empty);
		}
		mpPreviousGoalNode = mpGoalNode;
		SetTypeAt(nodePos, ENodeType::Target);

	}

	void FlowFieldPathfinderComponent::SetTypeAt(Vector3D nodePos, ENodeType nodeType)
	{

		mFlowField.GetNodePtr(nodePos)->SetNodeType(nodeType);

	}

	ENodeType FlowFieldPathfinderComponent::GetTypeAt(Vector3D nodePos)
	{
		return mFlowField.GetNodePtr(nodePos)->GetNodeType();
	}

	Vector3D FlowFieldPathfinderComponent::GetDirectionAt(Vector3D nodePos)
	{
		if (mpGoalNode != nullptr)
		if (nodePos == mpGoalNode->GetPosition())
		{
			return { 0, 0 };
		}
		return mFlowField.GetNodePtr(nodePos)->GetDirection();
	}

	void FlowFieldPathfinderComponent::Reset()
	{

		mFlowField.ResetHeatMap();
		mpGoalNode = nullptr;

	}



	// BFS Algorithm going on, set costs for all nodes (except for the obstacles)
	void FlowFieldPathfinderComponent::GenerateHeatMap()
	{
		// Queue of nodes to be evaluated in one shot
		std::queue<Node*> queue_pNode;

		queue_pNode.push(mpGoalNode);

		// Just to be explicit... (the cost of the goal node must be 0, it works even if it is not 0, but I like 0...)
		mpGoalNode->SetCost(0);
		// This also will be handled properly in the loop, but it's there to just make it explicit
		mpGoalNode->SetEvaluated(true);

		while (!queue_pNode.empty())
		{
			// Get the new pBaseNode
			Node* pBaseNode = queue_pNode.front();
			// Remove from the queue
			queue_pNode.pop();

			std::vector<Node*> pNeighbors = mFlowField.GetAdjacentNodes(pBaseNode->GetPosition(), true);

			for (auto pNode : pNeighbors)
			{
				// Skip if it is already evaluated 
				if (pNode->IsEvaluated()) { continue; }

				// Evaluate its cost
				pNode->SetCost(pBaseNode->GetCost() + 1);
				pNode->SetEvaluated(true);

				// Push into the queue to use it as the new pBaseNode in future iterations
				queue_pNode.push(pNode);
			}
		}
	}



	// Generate direction for all nodes based on the HeatMap (except for some like obstacles)
	void FlowFieldPathfinderComponent::CalculateFlowField()
	{
		for (int i = 0; i < mFlowField.GetHeight(); i++)
		{
			for (int j = 0; j < mFlowField.GetWidth(); j++)
			{
				Node* pTempNode = mFlowField.GetNodePtr({ double(j), double(i) });

				// Skip the obstacles
				if (pTempNode->GetNodeType() == ENodeType::Obstacle) { continue; }



				Node* nodeDown = nullptr;
				Node* nodeDownLeft = nullptr;
				Node* nodeDownRight = nullptr;
				Node* nodeUp = nullptr;
				Node* nodeUpLeft = nullptr;
				Node* nodeUpRight = nullptr;
				Node* nodeLeft = nullptr;
				Node* nodeRight = nullptr;

				// Sort them based on the index limit
				if (i != 0) { nodeDown = mFlowField.GetNodePtr({ double(j), double(i) - 1 }); }

				if (i != 0 && j != 0) { nodeDownLeft = mFlowField.GetNodePtr({ double(j) - 1, double(i) - 1 }); }
				if (i != 0 && j != mFlowField.GetWidth() - 1) { nodeDownRight = mFlowField.GetNodePtr({ double(j) + 1, double(i) - 1 }); }

				if (i != mFlowField.GetHeight() - 1) { nodeUp = mFlowField.GetNodePtr({ double(j), double(i) + 1 }); }

				if (i != mFlowField.GetHeight() - 1 && j != 0) { nodeUpLeft = mFlowField.GetNodePtr({ double(j) - 1, double(i) + 1 }); }
				if (i != mFlowField.GetHeight() - 1 && j != mFlowField.GetWidth() - 1) { nodeUpRight = mFlowField.GetNodePtr({ double(j) + 1, double(i) + 1 }); }

				if (j != 0) { nodeLeft = mFlowField.GetNodePtr({ double(j) - 1, double(i) }); }
				if (j != mFlowField.GetWidth() - 1) { nodeRight = mFlowField.GetNodePtr({ double(j) + 1, double(i) }); }

				// Sort them based on the type
				if (nodeDown != nullptr && nodeDown->GetNodeType() == ENodeType::Obstacle)
				{
					nodeDown = nullptr;
				}

				if (nodeDownLeft != nullptr && nodeDownLeft->GetNodeType() == ENodeType::Obstacle)
				{
					nodeDownLeft = nullptr;
				}
				if (nodeDownRight != nullptr && nodeDownRight->GetNodeType() == ENodeType::Obstacle)
				{
					nodeDownRight = nullptr;
				}

				if (nodeUp != nullptr && nodeUp->GetNodeType() == ENodeType::Obstacle)
				{
					nodeUp = nullptr;
				}

				if (nodeUpLeft != nullptr && nodeUpLeft->GetNodeType() == ENodeType::Obstacle)
				{
					nodeUpLeft = nullptr;
				}
				if (nodeUpRight != nullptr && nodeUpRight->GetNodeType() == ENodeType::Obstacle)
				{
					nodeUpRight = nullptr;
				}

				if (nodeLeft != nullptr && nodeLeft->GetNodeType() == ENodeType::Obstacle)
				{
					nodeLeft = nullptr;
				}
				if (nodeRight != nullptr && nodeRight->GetNodeType() == ENodeType::Obstacle)
				{
					nodeRight = nullptr;
				}



				// If the node is on the edge / close with any obstacles
				if (nodeDown == nullptr || nodeUp == nullptr || nodeLeft == nullptr || nodeRight == nullptr ||
					nodeDownLeft == nullptr || nodeDownRight == nullptr || nodeUpLeft == nullptr || nodeUpRight == nullptr)
				{

					// Get the neighbors of the current node
					std::vector<Node*> pNeighbors = mFlowField.GetAdjacentNodes({ double(j), double(i) }, true);
					const int neighborsCount = int(pNeighbors.size());

					// Skip if there's no neighbors (note that obstacles are sorted out when we get the neighbors)
					// There shouldn't be such cases usually, but it's here just to make everything safe
					if (neighborsCount == 0) { continue; }

					Node* pLowestNeighbor = nullptr;

					// Find the neighbor with the lowest cost, and store it
					int lowestNeighborCost = 0x7FFFFFFF;

					for (int k = 0; k < neighborsCount; k++)
					{
						if (pNeighbors[k]->GetNodeType() != ENodeType::Obstacle && pNeighbors[k]->GetCost() < lowestNeighborCost)
						{
							lowestNeighborCost = pNeighbors[k]->GetCost();
							pLowestNeighbor = pNeighbors[k];
						}
					}

					// Set current node's direction strictly to the lowest neighbor
					const Vector3D coolestNodePos = pLowestNeighbor->GetPosition();
					const Vector3D currentNodePos = { double(j), double(i) };

					pTempNode->SetDirection((coolestNodePos - currentNodePos)); // Not normalized for grid-based delta reference

				}
				else
				{

					// Set direction based on the surrounding tiles' cost
					const double dirX = double(nodeLeft->GetCost()) - double(nodeRight->GetCost());
					const double dirY = double(nodeDown->GetCost()) - double(nodeUp->GetCost());

					Vector3D direction = { dirX, dirY };

					pTempNode->SetDirection(direction); // Not normalized for grid-based delta reference

				}
			}
		}
	}

	void FlowFieldPathfinderComponent::OnDraw(const Vector3D& camPosition, float, const Vector3D& camZoomScale) const
	{
		if (!mIsDebugDraw) { return; }

		const TransformComponent* const componentTransform = GetOwner()->GetComponent<TransformComponent>();
		const bool isOnUI = componentTransform->IsOnUI();
		Vector3D componentPosition = componentTransform->GetPosition();

		if (!isOnUI)
		{
			componentPosition[0] = (componentPosition[0] - camPosition[0]) * camZoomScale[0] + float(Width) / 2.f;
			componentPosition[1] = (componentPosition[1] - camPosition[1]) * camZoomScale[1] + float(Height) / 2.f;
		}

		push_settings();

		apply_translate(componentPosition[0], componentPosition[1]);



		const int maxCost = (mFlowField.GetWidth() + mFlowField.GetHeight());

		for (int i = 0; i < mFlowField.GetHeight(); i++)
		{
			for (int j = 0; j < mFlowField.GetWidth(); j++)
			{
				const Node* pTempNode = mFlowField.GetNodePtr({ double(j), double(i) });

				const ENodeType tempNodeType = pTempNode->GetNodeType();
				const int tempCost = pTempNode->GetCost();

				int heat = int((double(tempCost) / double(maxCost)) * 0xFF); // Cost converted into a HexColor format



				set_outline_color(HexColor(0xFF0000FF));

				switch (tempNodeType)
				{

				default:
				case ENodeType::Empty:
					if (heat < 0x7F) { set_fill_color(HexColor(0x7F << 16 | 0x000000FF)); }
					set_fill_color(HexColor(heat << 16 | 0x000000FF));
					break;
				case ENodeType::Obstacle:
					set_fill_color(HexColor(0x3F0000FF));
					no_outline();
					break;
				case ENodeType::Start:
					set_fill_color(HexColor(0xFF0000FF));
					break;
				case ENodeType::Debug:
					set_fill_color(HexColor(0x00FF00FF));
					break;
				case ENodeType::Target:
					set_fill_color(HexColor(0x0000FFFF));
					break;

				}



				set_outline_width(1.5f);

				const Vector3D worldVector = TranslateToWorldVector({ j, i });

				draw_ellipse(worldVector.X(), worldVector.Y(), TILE_WIDTH / 2.f);

				const Vector3D direction = pTempNode->GetDirection();

				draw_line(worldVector.X(), worldVector.Y(),
					double(worldVector.X()) + direction.X() * TILE_WIDTH / 2.f,
					double(worldVector.Y()) + direction.Y() * TILE_WIDTH / 2.f);



				set_font_size(10);
				set_fill_color(HexColor(0xFFFFFFFF));
				set_outline_color(HexColor(0xFF0000FF));

				draw_text(std::to_string(tempCost), worldVector.X(), worldVector.Y());
			}
		}

		pop_settings();
	}

}