/*
    File Name: FlowFieldPathfinderComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
	#pragma once
#include "Component.h"
#include "FlowField.h"
#include "GameEnum.h"



namespace uruk
{

	class FlowFieldPathfinderComponent : public Component<FlowFieldPathfinderComponent, EUpdateType::BY_FRAME, update_priorities::HIGH_PRIORITY, 600, DRAWABLE>, public ZOrderBase
	{
	public:
		FlowFieldPathfinderComponent();
		FlowFieldPathfinderComponent(Vector3D dimension);

		void OnConstructorArgumentSave (std::ofstream& outputFileStream) const override;
		static IComponent* OnLoadConstruct (Level* level, const ID& id, std::ifstream& inputFileStream);
		
		void OnSave (std::ofstream& outputFileStream) const override;
		void OnLoad (std::ifstream& inputFileStream) override;
		
		void SetGoalNode(Vector3D nodePos);
		void SetTypeAt(Vector3D, ENodeType);
		ENodeType GetTypeAt(Vector3D nodePos);
		Vector3D GetDirectionAt(Vector3D nodePos);
		
		void Reset();

		void GenerateHeatMap(); // Call this AFTER resetting the HeatMap, mpGoalNode needs to be reset beforehand
		void CalculateFlowField(); // Call this AFTER generating a HeatMap

		static constexpr Vector3D TranslateToWorldVector (Vector3D original) noexcept;
		static constexpr Vector3D TranslateToUnitVector (Vector3D original) noexcept;
		static constexpr Vector3D TranslateToUnitVectorRaw(Vector3D original) noexcept;

		void OnDraw(const Vector3D& camPosition, float camRotation, const Vector3D& camZoomScale) const override;

	private:
		bool mIsDebugDraw = false;
		Vector3D mPathfinderDimension;
		FlowField mFlowField;
		Node* mpPreviousGoalNode = nullptr;
		Node* mpGoalNode = nullptr;
	};

	constexpr Vector3D FlowFieldPathfinderComponent::TranslateToWorldVector (Vector3D original) noexcept
	{
		return Vector3D (
			(original.X () + 0.5f) * TILE_WIDTH,
			(original.Y () + 0.5f) * TILE_HEIGHT);
	}

	constexpr Vector3D FlowFieldPathfinderComponent::TranslateToUnitVector (Vector3D original)  noexcept
	{
		return Vector3D(
			round((original.X()) / TILE_WIDTH),
			round((original.Y()) / TILE_HEIGHT));
	}

	constexpr Vector3D FlowFieldPathfinderComponent::TranslateToUnitVectorRaw(Vector3D original) noexcept
	{
		return Vector3D(
			((original.X()) / TILE_WIDTH),
			((original.Y()) / TILE_HEIGHT));
	}

}
