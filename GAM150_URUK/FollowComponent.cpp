/*
    File Name: FollowComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "FollowComponent.h"

#include "Serializer.h"
#include "TransformComponent.h"



namespace uruk
{
	void FollowComponent::OnConstructEnd()
	{
		Disable();
	}


	void FollowComponent::OnUpdate()
	{
		TransformComponent* const transformComponent = GetOwner()->GetComponent<TransformComponent>();
		
		const Vector3D targetLocation = mLevel->GetComponent<TransformComponent>(mFollowingTransformID)->GetPosition();
		const Vector3D currentLocation = transformComponent->GetPosition();

		if (mIsLinear)
		{
			transformComponent->SetPosition((targetLocation - currentLocation) / mSmoothRate + currentLocation + mOffset);
		}
		else
		{
			transformComponent->SetPosition(smoothStep(currentLocation, targetLocation, clamp(1 / mSmoothRate, 0.f, 1.f)) + mOffset);
		}
	}


	void FollowComponent::SetFollowingObject(const IObject* object, const float smoothRate, const Vector3D& offset, const bool isLinear)
	{
		mFollowingTransformID = object->GetComponent<TransformComponent>()->GetID();
		mOffset = offset;
		mSmoothRate = smoothRate;
		mIsLinear = isLinear;

		Enable();
	}


	void FollowComponent::OnSave(std::ofstream& outputFileStream) const
	{
		if (!IsActive())
		{
			return;
		}

		serialize(outputFileStream, mFollowingTransformID);
		serialize(outputFileStream, mOffset);
		serialize(outputFileStream, mSmoothRate);
		serialize(outputFileStream, mIsLinear);
	}

	void FollowComponent::OnLoad(std::ifstream& inputFileStream)
	{
		if (!IsActive())
		{
			return;
		}

		deserialize(inputFileStream, mFollowingTransformID);
		deserialize(inputFileStream, mOffset);
		deserialize(inputFileStream, mSmoothRate);
		deserialize(inputFileStream, mIsLinear);
	}
}
