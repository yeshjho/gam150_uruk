/*
    File Name: FollowComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"

#include "Vector3D.h"



namespace uruk
{
	class FollowComponent final : public Component<FollowComponent, EUpdateType::BY_MILLISECOND, update_priorities::LOW_PRIORITY, 0>
	{
	public:
		void OnConstructEnd() override;


		void OnUpdate() override;


		// moveDuration is in millisecond.
		void SetFollowingObject(const IObject* object, float smoothRate, const Vector3D& offset, bool isLinear);
		inline void StopFollowing() noexcept;


		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;



	private:
		ID mFollowingTransformID;
		Vector3D mOffset = { 0, 0 };
		float mSmoothRate = 4.f;
		bool mIsLinear = false;
	};
}

#include "FollowComponent.inl"
