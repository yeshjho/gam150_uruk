/*
    File Name: FollowComponent.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	inline void FollowComponent::StopFollowing() noexcept
	{
		Disable();
	}
}
