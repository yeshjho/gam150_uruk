/*
    File Name: Game.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Doyoon Kim, Seunggeon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "Game.h"

#include <chrono>
#include <filesystem>

#include <doodle/doodle.hpp>

#include "ClickableComponent.h"
#include "DraggableComponent.h"
#include "GameResourceManager.h"
#include "HoverableComponent.h"
#include "InputComponent.h"
#include "Level.h"
#include "Physics.h"
#include "Serializer.h"
#include "EndingScreenObject.h"
#include "StartScreenObject.h"


namespace uruk
{
	std::map<ID, std::unique_ptr<Level>> Game::levels = {};

	
	void Game::Setup(const std::filesystem::path& resourceFilePath)
	{
		doodle::create_window("U.R.U.K.");
		doodle::toggle_full_screen();
		doodle::set_frame_of_reference(doodle::FrameOfReference::RightHanded_OriginBottomLeft);
		doodle::set_image_mode(doodle::RectMode::Corner);
		doodle::set_rectangle_mode(doodle::RectMode::Corner);

	#ifndef _DEBUG
		double splashScreenTime = 2.0;
		doodle::Image splashScreen("resource/texture/digipen_logo.png");
		int drawX = doodle::Width / 2 - splashScreen.GetWidth() / 2;
		int drawY = doodle::Height / 2 - splashScreen.GetHeight() / 2;
		while (splashScreenTime > 0.)
		{
			doodle::update_window();
			doodle::push_settings();
			doodle::clear_background(0);
			doodle::draw_image(splashScreen, drawX, drawY);
			splashScreenTime -= doodle::DeltaTime;
		}
	#endif

		doodle::set_callback_key_pressed(InputComponent::OnKeyboardPressed);
		doodle::set_callback_key_released(InputComponent::OnKeyboardReleased);
		doodle::set_callback_mouse_pressed(InputComponent::OnMousePressed);
		doodle::set_callback_mouse_released(InputComponent::OnMouseReleased);
		doodle::set_callback_mouse_moved(InputComponent::OnMouseMoved);
		doodle::set_callback_mouse_wheel(InputComponent::OnMouseWheeled);
		
		GameResourceManager::Setup(resourceFilePath);

		GetAudioBuffer().LoadFromDirectory("");
		
		InputComponent::StaticBindFunctionToMousePressKey(doodle::MouseButtons::Left, ClickableComponent::OnPressed, doodle::MouseButtons::Left);
		InputComponent::StaticBindFunctionToMousePressKey(doodle::MouseButtons::Right, ClickableComponent::OnPressed, doodle::MouseButtons::Right);
		InputComponent::StaticBindFunctionToMousePressKey(doodle::MouseButtons::Middle, ClickableComponent::OnPressed, doodle::MouseButtons::Middle);
		InputComponent::StaticBindFunctionToMouseReleaseKey(doodle::MouseButtons::Left, ClickableComponent::OnReleased, doodle::MouseButtons::Left);
		InputComponent::StaticBindFunctionToMouseReleaseKey(doodle::MouseButtons::Right, ClickableComponent::OnReleased, doodle::MouseButtons::Right);
		InputComponent::StaticBindFunctionToMouseReleaseKey(doodle::MouseButtons::Middle, ClickableComponent::OnReleased, doodle::MouseButtons::Middle);
		InputComponent::StaticBindFunctionToMousePressKey(doodle::MouseButtons::Left, DraggableComponent::OnPressed, doodle::MouseButtons::Left);
		InputComponent::StaticBindFunctionToMousePressKey(doodle::MouseButtons::Right, DraggableComponent::OnPressed, doodle::MouseButtons::Right);
		InputComponent::StaticBindFunctionToMousePressKey(doodle::MouseButtons::Middle, DraggableComponent::OnPressed, doodle::MouseButtons::Middle);

		InputComponent::StaticBindFunctionToMouseMoved(HoverableComponent::OnMouseMoved);
	}

	void Game::CleanUp()
	{
		for (auto& [_, level] : levels)
		{
			level.reset(nullptr);
		}
		GameResourceManager::CleanUp();
	}


	void Game::BeginPlay()
	{
		logger.LogVerbose(ELogCategory::GAME_EVENT, "Game BeginPlay");
		
		GetCurrentLevel()->OnBeginPlay();
		if (!currentHUDLevel.is_nil())
		{
			GetCurrentHUDLevel()->OnBeginPlay();
		}

		bCalledBeginPlay = true;
	}

	void Game::Update()
	{
		doodle::update_window();
		doodle::clear_background();

		executePendingActions();

		using namespace std::chrono;

		static time_point<steady_clock> previous = steady_clock::now();
		const time_point<steady_clock> current = steady_clock::now();
		const long long deltaTimeInNanosecond = duration_cast<nanoseconds>(current - previous).count();
		previous = current;
		
		logger.LogVerbose(ELogCategory::GAME_EVENT, "Game Update");

		if (!currentLevel.is_nil())
		{
			Physics::Update();
			GetCurrentLevel()->OnUpdate(deltaTimeInNanosecond);
		}
		if (!currentHUDLevel.is_nil())
		{
			GetCurrentHUDLevel()->OnUpdate(deltaTimeInNanosecond);
		}
	}


	bool Game::ShouldQuit()
	{
		return doodle::is_window_closed();
	}

	void Game::SetMenuLevelID(ID menulevel)
	{
		mMenuID = menulevel;
;	}

	ID Game::GetMenuLevelID()
	{
		return mMenuID;
	}

	void Game::SetStartScreenID(ID startScreen)
	{
		startScreenID = startScreen;
	}

	void Game::SetEndingScreenID(ID endingScreen)
	{
		endingScreenID = endingScreen;
	}

	void Game::ShowEnding()
	{                     
		RemoveLevel(GetCurrentLevelID());
		RemoveLevel(GetCurrentHUDLevelID());
		SetCurrentLevel(mMenuID);
		SetCurrentHUDLevel(mMenuID);
		GetLevel(mMenuID)->GetObject<EndingScreenObject>(endingScreenID)->Enable();
		GetLevel(mMenuID)->GetObject<EndingScreenObject>(endingScreenID)->OnEnabled();
	}

	void Game::RestartAfterEnding()
	{
		
		SetCurrentLevel(mMenuID);
		SetCurrentHUDLevel(mMenuID);
		GetLevel(mMenuID)->GetObject<EndingScreenObject>(endingScreenID)->Disable();
		GetLevel(mMenuID)->GetObject<EndingScreenObject>(endingScreenID)->OnDisabled();
		StartScreenObject* startScreenObject = GetLevel(mMenuID)->GetObject<StartScreenObject>(startScreenID);
		startScreenObject->Enable();
		startScreenObject->OnEnabled();
	}


	Level* Game::GetCurrentLevel()
	{
		return levels.at(currentLevel).get();
	}

	Level* Game::GetCurrentHUDLevel()
	{
		return levels.at(currentHUDLevel).get();
	}

	Level* Game::GetLevel(const ID& levelID)
	{
		return levels.at(levelID).get();
	}

	void Game::SetCurrentLevel(const ID& levelID, std::function<void()> callbackOnSet)
	{
		if (currentLevel.is_nil())
		{
			logger.LogDisplay(ELogCategory::GAME_ACTION, "Setting current Level to " + to_string(levelID));
			currentLevel = levelID;
			if (bCalledBeginPlay)
			{
				levels.at(levelID)->OnBeginPlay();
			}
		}
		else
		{
			pendingStates |= EGamePendingState::CHANGE_LEVEL;
			changeLevelID = levelID;
			levelSetCallback = std::move(callbackOnSet);
		}
	}

	void Game::SetCurrentHUDLevel(const ID& hudLevelID, std::function<void()> callbackOnSet)
	{
		if (currentHUDLevel.is_nil())
		{
			logger.LogDisplay(ELogCategory::GAME_ACTION, "Setting current HUD Level to " + to_string(hudLevelID));
			currentHUDLevel = hudLevelID;
			if (bCalledBeginPlay)
			{
				levels.at(hudLevelID)->OnBeginPlay();
			}
		}
		else
		{
			pendingStates |= EGamePendingState::CHANGE_HUD_LEVEL;
			changeHUDLevelID = hudLevelID;
			hudLevelSetCallback = std::move(callbackOnSet);
		}
	}

	void Game::ResetHUDLevel() noexcept
	{
		pendingStates |= EGamePendingState::RESET_HUD_LEVEL;
	}

	void Game::RemoveLevel(const ID& levelID)
	{
		if ((levelID != currentLevel && levelID != currentHUDLevel) || GetLevel(levelID)->IsPaused())
		{
			logger.LogDisplay(ELogCategory::GAME_ACTION, "Removing Level " + to_string(levelID));
			levels.at(levelID).reset(nullptr);
		}
		else
		{
			pendingStates |= EGamePendingState::REMOVE_LEVEL;
			removeLevelIDs.push_back(levelID);
		}
	}


	void Game::Save(const std::filesystem::path& filePath)
	{
		std::filesystem::create_directories(filePath.parent_path());
		std::filesystem::path saveFilePath{ filePath };
		saveFilePath.replace_extension(std::filesystem::path{ SAVE_FILE_EXTENSION });
		
		logger.LogLog(ELogCategory::GAME_FILE, "Saving Game to " + saveFilePath.string());

		std::ofstream outputFileStream{ saveFilePath, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary };
		if (!outputFileStream)
		{
			logger.LogFatal(ELogCategory::GAME_FILE, "Cannot open the given file for writing.");
			return;
		}

		serialize(outputFileStream, currentLevel);
		serialize(outputFileStream, currentHUDLevel);
		serialize(outputFileStream, levels.size());
		for (const auto& [_, level] : levels)
		{
			if (level)
			{
				logger.LogLog(ELogCategory::LEVEL_FILE, "Saving a Level");
				level->OnSave(outputFileStream);
			}
			serialize(outputFileStream, LEVEL_SEPARATOR);
		}
		logger.LogLog(ELogCategory::GAME_FILE, "Finished saving Game.");
	}

	void Game::Load(const std::filesystem::path& filePath)
	{
		logger.LogLog(ELogCategory::GAME_FILE, "Loading Game from " + filePath.string());

		const std::filesystem::path extension = filePath.extension();
		if (std::wcscmp(extension.c_str(), SAVE_FILE_EXTENSION) != 0)
		{
			logger.LogFatal(ELogCategory::GAME_FILE, "File extension is not supported.");
			return;
		}

		std::ifstream inputFileStream{ filePath, std::ios_base::in | std::ios_base::binary };
		if (!inputFileStream)
		{
			logger.LogFatal(ELogCategory::GAME_FILE, "Cannot open the given file for reading.");
			return;
		}

		deserialize(inputFileStream, currentLevel);
		deserialize(inputFileStream, currentHUDLevel);
		size_t levelCount;
		deserialize(inputFileStream, levelCount);

		std::set<ID> loadedLevelIDs;
		
		unsigned long long separatorChecker;

		ID levelID;
		
		size_t componentMemorySize;
		unsigned int maxComponentCountPerType;
		size_t objectMemorySize;
		unsigned int maxObjectCountPerType;
		
		for (size_t i = 0; i < levelCount; ++i)
		{
			logger.LogVerbose(ELogCategory::GAME_FILE, "Loading a Level");

			deserialize(inputFileStream, levelID);
			deserialize(inputFileStream, componentMemorySize);
			deserialize(inputFileStream, maxComponentCountPerType);
			deserialize(inputFileStream, objectMemorySize);
			deserialize(inputFileStream, maxObjectCountPerType);

			loadedLevelIDs.insert(levelID);

			auto result = levels.insert({ levelID, nullptr });
			auto& level = result.first->second;
			if (result.second)
			{
				level = std::make_unique<Level>(levelID, componentMemorySize, maxComponentCountPerType, objectMemorySize, maxObjectCountPerType);
			}
			else
			{
				level->mComponents.Reset<IComponent>(componentMemorySize, maxComponentCountPerType);
				level->mObjects.Reset<IObject>(objectMemorySize, maxObjectCountPerType);
			}
			level->OnLoad(inputFileStream);
			
			deserialize(inputFileStream, separatorChecker);
			if (separatorChecker != LEVEL_SEPARATOR)
			{
				logger.LogFatal(ELogCategory::GAME_FILE, "File is corrupted!");
			}
		}

		for (auto& [id, level] : levels)
		{
			if (!loadedLevelIDs.count(id))
			{
				level.reset(nullptr);
			}
		}

		if (inputFileStream.peek() != std::ifstream::traits_type::eof())
		{
			logger.LogError(ELogCategory::GAME_FILE, "File may be corrupted.");
		}
		logger.LogLog(ELogCategory::GAME_FILE, "Finished loading Game.");
	}


	void Game::SaveLevel(const ID& levelID, const std::filesystem::path& filePath)
	{
		std::filesystem::create_directories(filePath.parent_path());
		std::filesystem::path saveFilePath{ filePath };
		saveFilePath.replace_extension(std::filesystem::path{ Level::SAVE_FILE_EXTENSION });

		logger.LogLog(ELogCategory::LEVEL_FILE, "Saving Level to " + saveFilePath.string());

		std::ofstream file{ saveFilePath, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary };
		if (!file)
		{
			logger.LogFatal(ELogCategory::LEVEL_FILE, "Cannot open the given file for writing");
			return;
		}

		GetLevel(levelID)->OnSave(file);
		logger.LogLog(ELogCategory::LEVEL_FILE, "Finished saving Level.");
	}

	ID Game::LoadLevel(const std::filesystem::path& filePath)
	{
		logger.LogLog(ELogCategory::LEVEL_FILE, "Loading Level from " + filePath.string());

		const std::filesystem::path extension = filePath.extension();
		if (std::wcscmp(extension.c_str(), Level::SAVE_FILE_EXTENSION) != 0)
		{
			logger.LogFatal(ELogCategory::LEVEL_FILE, "File extension is not supported");
			return ID{};
		}

		std::ifstream inputFileStream{ filePath, std::ios_base::in | std::ios_base::binary };
		if (!inputFileStream)
		{
			logger.LogFatal(ELogCategory::LEVEL_FILE, "Cannot open the given file for reading");
			return ID{};
		}

		ID levelID;

		size_t componentMemorySize;
		unsigned int maxComponentCountPerType;
		size_t objectMemorySize;
		unsigned int maxObjectCountPerType;

		deserialize(inputFileStream, levelID);
		deserialize(inputFileStream, componentMemorySize);
		deserialize(inputFileStream, maxComponentCountPerType);
		deserialize(inputFileStream, objectMemorySize);
		deserialize(inputFileStream, maxObjectCountPerType);

		auto result = levels.insert({ levelID, nullptr });
		auto& level = result.first->second;
		if (!level || result.second)
		{
			level = std::make_unique<Level>(levelID, componentMemorySize, maxComponentCountPerType, objectMemorySize, maxObjectCountPerType);
		}
		else
		{
			level->mComponents.Reset<IComponent>(componentMemorySize, maxComponentCountPerType);
			level->mObjects.Reset<IObject>(objectMemorySize, maxObjectCountPerType);
		}

		level->OnLoad(inputFileStream);

		if (inputFileStream.peek() != std::ifstream::traits_type::eof())
		{
			logger.LogWarning(ELogCategory::LEVEL_FILE, "File may be corrupted.");
		}
		logger.LogLog(ELogCategory::LEVEL_FILE, "Finished loading Level.");

		return levelID;
	}


	void Game::executePendingActions()
	{
		if (bool(pendingStates & EGamePendingState::REMOVE_LEVEL))
		{
			for (const ID& id : removeLevelIDs)
			{
				logger.LogDisplay(ELogCategory::GAME_ACTION, "Removing Level " + to_string(id));
				levels.at(id).reset(nullptr);
			}
			removeLevelIDs.clear();

			pendingStates &= ~EGamePendingState::REMOVE_LEVEL;
		}
		
		if (bool(pendingStates & EGamePendingState::CHANGE_LEVEL))
		{
			logger.LogDisplay(ELogCategory::GAME_ACTION, "Setting current Level to " + to_string(changeLevelID));
			levels.at(changeLevelID)->OnBeginPlay();
			currentLevel = changeLevelID;
			levelSetCallback();

			pendingStates &= ~EGamePendingState::CHANGE_LEVEL;
		}
		
		if (bool(pendingStates & EGamePendingState::RESET_HUD_LEVEL))
		{
			logger.LogDisplay(ELogCategory::GAME_ACTION, "Resetting HUD Level");
			currentHUDLevel = ID{};

			pendingStates &= ~EGamePendingState::RESET_HUD_LEVEL;
		}
		
		if (bool(pendingStates & EGamePendingState::CHANGE_HUD_LEVEL))
		{
			logger.LogDisplay(ELogCategory::GAME_ACTION, "Setting current HUD Level to " + to_string(changeHUDLevelID));
			levels.at(changeHUDLevelID)->OnBeginPlay();
			currentHUDLevel = changeHUDLevelID;
			hudLevelSetCallback();

			pendingStates &= ~EGamePendingState::CHANGE_HUD_LEVEL;
		}
	}
}
