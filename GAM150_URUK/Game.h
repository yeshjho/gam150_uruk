/*
    File Name: Game.h
    Project Name: U.R.U.K
	Author(s):
		-Seunggeon Kim
		Contribution(s):
			audio-related things
		-Joonho Hwang
		Contribution(s):
			all the others
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <deque>
#include <memory>
#include <map>

#include "AudioBufferManager.h"
#include "Internal.h"



namespace std { namespace filesystem { class path; } }



namespace uruk
{
	class Level;



	enum EGamePendingState : int
	{
		CHANGE_LEVEL = 1 << 0,
		CHANGE_HUD_LEVEL = 1 << 1,
		REMOVE_LEVEL = 1 << 2,
		RESET_HUD_LEVEL = 1 << 3
	};
	


	class Game
	{
	public:
		static void Setup(const std::filesystem::path& resourceFilePath);
		static void CleanUp();
		
		// Call it only once at the beginning.
		// It will automatically call BeginPlay() on Level if the current level changes.
		static void BeginPlay();
		static void Update();
		static bool ShouldQuit();

		static void SetMenuLevelID(ID menulevel);
		static ID GetMenuLevelID();

		static void SetStartScreenID(ID startScreen);
		static void SetEndingScreenID(ID endingScreen);
		
		static void ShowEnding();
		static void RestartAfterEnding();
		
		template<typename ...Args>
		static ID AddLevel(Args&&... args);
		
		[[nodiscard]] static Level* GetCurrentLevel();
		[[nodiscard]] static Level* GetCurrentHUDLevel();
		[[nodiscard]] static Level* GetLevel(const ID& levelID);
		
		static void SetCurrentLevel(const ID& levelID, std::function<void()> callbackOnSet = [](){});
		static void SetCurrentHUDLevel(const ID& hudLevelID, std::function<void()> callbackOnSet = []() {});
		
		static void ResetHUDLevel() noexcept;
		
		static void RemoveLevel(const ID& levelID);

		[[nodiscard]] static constexpr bool HasHUDLevel() noexcept;

		[[nodiscard]] static constexpr const ID& GetCurrentLevelID() noexcept;
		[[nodiscard]] static constexpr const ID& GetCurrentHUDLevelID() noexcept;

		[[nodiscard]] static constexpr AudioBufferManager& GetAudioBuffer() noexcept;

		static void Save(const std::filesystem::path& filePath);
		static void Load(const std::filesystem::path& filePath);

		static void SaveLevel(const ID& levelID, const std::filesystem::path& filePath);
		static ID LoadLevel(const std::filesystem::path& filePath);

	private:
		static void executePendingActions();
		


	public:
		static inline const wchar_t* SAVE_FILE_EXTENSION = L".urkg";
		
	private:
		static inline constexpr unsigned long long LEVEL_SEPARATOR = 0xFEEDDEADDEAFBEEF;

		
		static std::map<ID, std::unique_ptr<Level>> levels;
		static inline ID currentLevel;
		static inline ID currentHUDLevel;
		static inline ID startScreenID;
		static inline ID endingScreenID;
		
		static inline AudioBufferManager audioBuffer;

		static inline bool bCalledBeginPlay = false;
		

		static inline int pendingStates = 0;
		static inline ID changeLevelID;
		static inline ID changeHUDLevelID;
		static inline std::deque<ID> removeLevelIDs;
		static inline std::function<void()> levelSetCallback;
		static inline std::function<void()> hudLevelSetCallback;

		static inline ID mMenuID;
	};
}

#include "Game.inl"
