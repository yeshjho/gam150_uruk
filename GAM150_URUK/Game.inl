/*
    File Name: Game.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Seunggeon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	template<typename ...Args>
	ID Game::AddLevel(Args&&... args)
	{
		const ID id = IDManager::ClaimID();
		levels.insert({ id, std::make_unique<Level>(id, std::forward<Args>(args)...) });

		return id;
	}

	constexpr bool Game::HasHUDLevel() noexcept
	{
		return !currentHUDLevel.is_nil();
	}

	constexpr const ID& Game::GetCurrentLevelID() noexcept
	{
		return currentLevel;
	}

	constexpr const ID& Game::GetCurrentHUDLevelID() noexcept
	{
		return currentHUDLevel;
	}

	constexpr AudioBufferManager& Game::GetAudioBuffer() noexcept
	{
		return audioBuffer;
	}
	
}
