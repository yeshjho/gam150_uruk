/*
    File Name: GameEnum.h
    Project Name: U.R.U.K
	Author(s):
		-Joonho Hwang
		Contribution(s):
			EObjectZOrder, constants
		-Seunggeon Kim
		Contribution(s):
			EEnemyType
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



enum class EEnemyType
{
	SonXaary,
	KonDoori,
	PohShigi,
};



enum EObjectZOrder : int
{
	ROOM_FLOOR = -100,
	CORRIDOR_FLOOR = -100,

	FISH_GATE = 3,
	DOOR = 3,

	DUNGEON_CORE = 10,

	TOP_ROOM_WALL = 99,
	ROOM_WALL = 100,
	CORRIDOR_WALL = 100,

	OBSTACLE = 100,

	HEALTH_RESTORE = 200,

	ENEMY = 500,
	KONDOORI = 501,
	SONXAARY = 502,
	POHSHIGI = 503,

	HEALTH = 600,

	FISHGATE_INDICATOR = 900,
	
	PLAYER = 999,
	BUTTONS = 1000,
	
	Debug = 10000,
};



inline constexpr float TILE_WIDTH = 60.f;
inline constexpr float TILE_HEIGHT = 60.f;