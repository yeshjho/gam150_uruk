/*
    File Name: GameResourceManager.cpp
    Project Name: U.R.U.K
	Author(s):
		-Joonho Hwang
		Contribution(s):
			Setup, font-related things
		-Doyoon Kim
		Contribution(s):
			Cleanup, animation-related things, getters
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "GameResourceManager.h"

#include "TextComponent.h"



namespace uruk
{
	void GameResourceManager::Setup(const std::filesystem::path& resourceFilePath)
	{
		TextComponent::DEFAULT_FONT_ID = fontIDs["NanumSquareRoundEB"] = TextComponent::RegisterFont("resource/font/NanumSquareRoundEB.fnt");

		std::ifstream resourceFile{ resourceFilePath };
		if (!resourceFile)
		{
			logger.LogFatal(ELogCategory::GAME_FILE, "Resource file does not exist or cannot be open");
		}
		
		std::string _, category, name, path;
		while (resourceFile >> category)
		{
			std::getline(std::getline(resourceFile, _, '"'), name, '"');
			std::getline(std::getline(resourceFile, _, '"'), path, '"');
			if (category == "image")
			{
				images[name] = std::make_shared<Texture>(name, path);
			}
			else if (category == "font")
			{
				fontIDs[name] = TextComponent::RegisterFont(path);
			}
			else if(category == "animationclip")
			{
				AnimationClip animClip;
				std::ifstream animClipFile(path);
				LoadAnimationClipFromEditorFile(animClipFile, animClip);
				animClips[name] = std::make_shared<AnimationClip>(animClip);
			}
		}
	}

	void GameResourceManager::CleanUp()
	{
		for (auto& [_, texture] : images)
		{
			texture.reset();
		}
		for(auto& [_,animationClip] : animClips)
		{
			for(int i = 0; i < animationClip->nAnimSamples; i++)
			{
				for(int j = 0; j < animationClip->animSamples[i].spriteMesh.nSprites; j++)
				{
					animationClip->animSamples[i].spriteMesh.sprites[j].pTexture.reset();
				}
			}
		}
	}

	std::shared_ptr<Texture> GameResourceManager:: GetImage(const std::string& name)
	{
		return images.at(name);
	}

	std::shared_ptr<AnimationClip> GameResourceManager::GetAnimationClip(const std::string& name)
	{
		return animClips.at(name);
	}


	int GameResourceManager::GetFont(const std::string& name)
	{
		return fontIDs.at(name);
	}
}
