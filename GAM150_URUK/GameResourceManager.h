/*
    File Name: GameResourceManager.h
    Project Name: U.R.U.K
	Author(s):
		-Joonho Hwang
		Contribution(s):
			Setup, Cleanup, font-related things
		-Doyoon Kim
		Contribution(s):
			getters, member variables
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <memory>
#include <unordered_map>
#include "Texture.h"
#include "AnimationClip.h"

namespace uruk
{
	class GameResourceManager
	{
	public:
		static void Setup(const std::filesystem::path& resourceFilePath);
		static void CleanUp();
		static std::shared_ptr<Texture> GetImage(const std::string& name);
		static std::shared_ptr<AnimationClip> GetAnimationClip(const std::string& name);
		static int GetFont(const std::string& name);
		
	private:
		inline static std::unordered_map<std::string, std::shared_ptr<Texture>> images;
		inline static std::unordered_map<std::string, std::shared_ptr<AnimationClip>> animClips;
		inline static std::unordered_map<std::string, int> fontIDs;
	};
}