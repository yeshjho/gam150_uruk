/*
    File Name: HealthBarComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Yeongju Lee
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "HealthBarComponent.h"
#include "PlayerStatisticsComponent.h"
#include "TextureComponent.h"
#include "GameResourceManager.h"
#include "GameEnum.h"
#include "PlayerBehaviorComponent.h"


namespace uruk
{
	void HealthBarComponent::OnBeginPlay() 
	{
		cacheHeartWidth = GameResourceManager::GetImage("HeartFull")->GetWidth();
	}

	void HealthBarComponent::OnUpdate() 
	{
		int maxHealth = GetOwner()->GetComponent<PlayerStatisticsComponent>()->GetMaxHealth();
		int currHealth = GetOwner()->GetComponent<PlayerStatisticsComponent>()->GetCurrentHealth();

		int spriteHeight = GameResourceManager::GetImage(GetOwner()->GetComponent<TextureComponent>()->GetImageName())->GetHeight();

		if (mHeartTextureIDs.empty())
		{
			for (int i = 0; i < maxHealth; ++i)
			{
				mHeartTextureIDs.push_back(GetOwner()->AddComponent<TextureComponent>("HeartFull", EObjectZOrder::HEALTH, 0.0f, 0.0f, 0, 0, 0, 0, Vector3D{ -(cacheHeartWidth / 2) * (maxHealth - 1) + cacheHeartWidth * (i + 3/2), -cacheHeartWidth + spriteHeight}));
			}
		}

		for (int i = 0; i < maxHealth; ++i)
		{
			if (i < currHealth) 
			{
				mLevel->GetComponent<TextureComponent>(mHeartTextureIDs[i])->SetTexture("HeartFull");
				if (GetOwner()->GetComponent<PlayerBehaviorComponent>()->IsFlipped())
				{
					mLevel->GetComponent<TextureComponent>(mHeartTextureIDs[i])->SetScaleOffset({ -1, 1 });
				}
				else
				{
					mLevel->GetComponent<TextureComponent>(mHeartTextureIDs[i])->SetScaleOffset({ 1, 1 });
				}
			}
			else
			{
				mLevel->GetComponent<TextureComponent>(mHeartTextureIDs[i])->SetTexture("HeartEmpty");

				if (GetOwner()->GetComponent<PlayerBehaviorComponent>()->IsFlipped())
				{
					mLevel->GetComponent<TextureComponent>(mHeartTextureIDs[i])->SetScaleOffset({ -1, 1 });
				}
				else
				{
					mLevel->GetComponent<TextureComponent>(mHeartTextureIDs[i])->SetScaleOffset({ 1, 1 });
				}
			}
		}
	}


	void HealthBarComponent::OnConstructEnd()
	{
		cacheHeartWidth = GameResourceManager::GetImage("HeartFull")->GetWidth();
	}


	void HealthBarComponent::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mHeartTextureIDs);
	}

	void HealthBarComponent::OnLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mHeartTextureIDs);
	}
}
