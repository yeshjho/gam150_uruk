/*
    File Name: HealthBarComponent.h
    Project Name: U.R.U.K
	Author(s):
		-Joonho Hwang
		Contribution(s):
			OnConstructEnd, OnSave, OnLoad
		-Yeongju Lee
		Contribution(s):
			all the others
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"



namespace uruk
{
	class HealthBarComponent final : public Component<HealthBarComponent, EUpdateType::BY_FRAME, update_priorities::HIGH_PRIORITY, 0>
	{
	public:
		void OnUpdate() override;
		void OnBeginPlay() override;
		void OnConstructEnd() override;

		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;


		
	private:
		std::vector<ID> mHeartTextureIDs;
		int cacheHeartWidth = 0;
	};
}