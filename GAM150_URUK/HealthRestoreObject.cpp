/*
    File Name: HealthRestoreObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary:
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "HealthRestoreObject.h"

#include "BoxColliderComponent.h"
#include "Game.h"
#include "GameEnum.h"
#include "PlayerStatisticsComponent.h"
#include "TextureComponent.h"
#include "TransformComponent.h"



namespace uruk
{
	class HealthRestoreTriggerEnterCallback final : public CustomCallbackBase<HealthRestoreTriggerEnterCallback, ICollisionCallbackBase>
	{
	public:
		void Execute(IColliderComponent* self, IColliderComponent* collideWith) override
		{
			Level* const level = Game::GetCurrentLevel();
			
			level->GetComponent(self->GetColliderTypeID(), self->GetColliderID())->GetOwner()->Disable();
			level->GetComponent(collideWith->GetColliderTypeID(), collideWith->GetColliderID())->GetOwner()->GetComponent<PlayerStatisticsComponent>()->AddCurrHealth(1);
		}
	};


	
	HealthRestoreObject::HealthRestoreObject(const Vector3D& coord)
        : cacheCoord(coord)
	{}


	void HealthRestoreObject::OnConstructEnd()
	{
		AddComponent<TransformComponent>(cacheCoord);
		
		AddComponent<TextureComponent>("HealthRestore", EObjectZOrder::HEALTH_RESTORE);

		AddComponent<BoxColliderComponent>(Vector3D{0, 0}, TILE_WIDTH, TILE_HEIGHT, CollisionTag::HealthRestore, CollisionTag::Uruk, true);
		GetComponent<BoxColliderComponent>()->SetOnCollisionEnter(std::make_unique<HealthRestoreTriggerEnterCallback>());
	}
}
