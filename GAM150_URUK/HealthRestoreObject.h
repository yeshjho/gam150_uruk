/*
    File Name: HealthRestoreObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary:
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"

#include "Vector3D.h"



namespace uruk
{
	class HealthRestoreObject final : public Object<HealthRestoreObject>
	{
	public:
        HealthRestoreObject(const Vector3D& coord = { 0, 0 });

		void OnConstructEnd() override;



	private:
        Vector3D cacheCoord;
	};
}
