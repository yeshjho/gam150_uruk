/*
    File Name: HoverableComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "HoverableComponent.h"

#include <utility>
#ifdef UI_DEBUG_DRAW
#include <doodle/doodle.hpp>
#endif

#include "Serializer.h"
#include "CameraComponent.h"
#include "Game.h"
#include "InputComponent.h"
#include "Logger.h"



namespace uruk
{
	HoverableComponent::HoverableComponent(const float width, const float height, const int zOrder, bool bConsumeInput)
		: WidthHeightBase(width, height), ZOrderBase(zOrder), mbConsumeInput(bConsumeInput)
	{}


	void HoverableComponent::OnBeginPlay()
	{
		cacheIsOnUI = GetOwner()->GetComponent<TransformComponent>()->IsOnUI();
	}

	void HoverableComponent::OnMove(void* newLocation)
	{
		new (newLocation) HoverableComponent(std::move(*this));
	}


#ifdef UI_DEBUG_DRAW
	void HoverableComponent::OnDraw(const Vector3D& camPosition, float, const Vector3D& camZoomScale) const
	{
		using namespace doodle;


		const TransformComponent* const componentTransform = GetOwner()->GetComponent<TransformComponent>();
		const bool isOnUI = componentTransform->IsOnUI();
		Vector3D componentPosition = GetAdjustedPosition();
		const Vector3D& componentScale = GetAdjustedScale();

		if (!isOnUI)
		{
			componentPosition[0] = (componentPosition[0] - camPosition[0]) * camZoomScale[0] + float(Width) / 2.f;
			componentPosition[1] = (componentPosition[1] - camPosition[1]) * camZoomScale[1] + float(Height) / 2.f;
		}

		push_settings();
		apply_translate(componentPosition[0], componentPosition[1]);
		apply_rotate(GetAdjustedRotation());
		apply_scale(componentScale[0] * camZoomScale[0], componentScale[1] * camZoomScale[1]);
		no_fill();
		set_outline_width(3.f);
		set_outline_color(DEBUG_DRAW_COLOR);
		draw_rectangle(0, 0, GetWidth(), GetHeight());
		pop_settings();
	}
#endif


	void HoverableComponent::OnMouseMoved()
	{
		if (Game::HasHUDLevel())
		{
			const auto& components = Game::GetCurrentHUDLevel()->GetAllActiveComponents<HoverableComponent>();
			for (auto component = components.rbegin(); component != components.rend(); --component)
			{
				if (component->onMouseMoved())
				{
					return;
				}
			}
		}
		
		const auto& components = Game::GetCurrentLevel()->GetAllActiveComponents<HoverableComponent>();
		for (auto component = components.rbegin(); component != components.rend(); --component)
		{
			if (component->onMouseMoved())
			{
				return;
			}
		}
	}

	IComponent* HoverableComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		bool bConsumeInput;

		deserialize(inputFileStream, bConsumeInput);
		
		return level->LoadComponent<HoverableComponent>(id, 0.f, 0.f, 0, bConsumeInput);
	}


	void HoverableComponent::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mOnHovered->GetType());
		mOnHovered->OnSave(outputFileStream);

		serialize(outputFileStream, mOnUnhovered->GetType());
		mOnUnhovered->OnSave(outputFileStream);
	}

	void HoverableComponent::OnLoad(std::ifstream& inputFileStream)
	{
		TypeID typeID;

		deserialize(inputFileStream, typeID);
		mOnHovered = CustomCallbackFactory<IHoverableComponentCallbackBase>::Generate(typeID);
		mOnHovered->OnLoad(inputFileStream);

		deserialize(inputFileStream, typeID);
		mOnUnhovered = CustomCallbackFactory<IHoverableComponentCallbackBase>::Generate(typeID);
		mOnUnhovered->OnLoad(inputFileStream);
	}


	void HoverableComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mbConsumeInput);
	}


	bool HoverableComponent::onMouseMoved()
	{
		Vector3D position = Vector3D(doodle::get_mouse_x(), doodle::get_mouse_y());
		if (!cacheIsOnUI)
		{
			position = mLevel->GetAllActiveComponents<CameraComponent>().begin()->CalculateCoordProjectedOnWorld(position);
		}

		if (IsInside(position))
		{
			if (!mIsHovered)
			{
				onHovered();
				return mbConsumeInput;
			}
		}
		else if (mIsHovered)
		{
			onUnhovered();
		}

		return false;
	}

	void HoverableComponent::onHovered()
	{
		logger.LogVerbose(ELogCategory::CUSTOM_EVENT, "OnHovered\t" + to_string(mID));
		mIsHovered = true;
		mOnHovered->Execute(this);
	}

	void HoverableComponent::onUnhovered()
	{
		logger.LogVerbose(ELogCategory::CUSTOM_EVENT, "OnUnhovered\t" + to_string(mID));
		mIsHovered = false;
		mOnUnhovered->Execute(this);
	}
}
