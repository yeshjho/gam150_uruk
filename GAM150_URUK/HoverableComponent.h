/*
    File Name: HoverableComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"
#include "TransformOffsetBase.h"
#include "WidthHeightBase.h"

#ifdef UI_DEBUG_DRAW
#include <doodle/color.hpp>
#endif

#include "CustomCallbackBase.h"
#include "IHoverableComponentCallbackBase.h"


namespace uruk
{
	class DefaultHoverableComponentCallback final : public CustomCallbackBase<DefaultHoverableComponentCallback, IHoverableComponentCallbackBase>
	{
	public:

		void OnLoad(std::ifstream&) override {};
		void OnSave(std::ofstream&) override {};
		void Execute(HoverableComponent*) override {};
	};

#ifdef UI_DEBUG_DRAW
	class HoverableComponent final : public TransformOffsetBase<HoverableComponent>, public WidthHeightBase<HoverableComponent>, public ZOrderBase, public Component<HoverableComponent, EUpdateType::NEVER, update_priorities::NORMAL_PRIORITY, 0, EComponentAttribute::NO_MEMCPY | EComponentAttribute::DRAWABLE>
#else
	class HoverableComponent final : public TransformOffsetBase<HoverableComponent>, public WidthHeightBase<HoverableComponent>, public ZOrderBase, public Component<HoverableComponent, EUpdateType::NEVER, update_priorities::NORMAL_PRIORITY, 0, EComponentAttribute::NO_MEMCPY>
#endif
	{
	public:
		HoverableComponent(float width, float height, int zOrder = 0, bool bConsumeInput = true);


		void OnBeginPlay() override;

		void OnMove(void* newLocation) override;
	#ifdef UI_DEBUG_DRAW
		void OnDraw(const Vector3D& camPosition, float camRotation, const Vector3D& camZoomScale) const override;
	#endif


		inline void SetOnHovered(std::unique_ptr<IHoverableComponentCallbackBase>&& onHovered);
		inline void SetOnUnhovered(std::unique_ptr<IHoverableComponentCallbackBase>&& onUnhovered);

		constexpr void SetConsumeInput(bool bConsumeInput) noexcept;

		[[nodiscard]] constexpr bool IsHovered() const noexcept;
		

		static void OnMouseMoved();

		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);

		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;
		
	private:
		void onHovered();
		void onUnhovered();
		bool onMouseMoved();



	public:
	#ifdef UI_DEBUG_DRAW
		static constexpr doodle::Color DEBUG_DRAW_COLOR = { 0, 0, 255 };
	#endif
		
	private:
		std::unique_ptr<IHoverableComponentCallbackBase> mOnHovered = std::make_unique<DefaultHoverableComponentCallback>();
		std::unique_ptr<IHoverableComponentCallbackBase> mOnUnhovered = std::make_unique<DefaultHoverableComponentCallback>();

		bool cacheIsOnUI = true;
		bool mIsHovered = false;
		bool mbConsumeInput;
	};
}

#include "HoverableComponent.inl"
