/*
    File Name: HoverableComponent.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	inline void HoverableComponent::SetOnHovered(std::unique_ptr<IHoverableComponentCallbackBase>&& onHovered)
	{
		mOnHovered = std::move(onHovered);
	}

	inline void HoverableComponent::SetOnUnhovered(std::unique_ptr<IHoverableComponentCallbackBase>&& onUnhovered)
	{
		mOnUnhovered = std::move(onUnhovered);
	}
	
	constexpr void HoverableComponent::SetConsumeInput(const bool bConsumeInput) noexcept
	{
		mbConsumeInput = bConsumeInput;
	}

	
	constexpr bool HoverableComponent::IsHovered() const noexcept
	{
		return mIsHovered;
	}
}
