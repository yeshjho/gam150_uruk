/*
	File Name: EnemyAnimationStateMachine.cpp
	Project Name: U.R.U.K
	Author(s):
		Primary: Doyoon Kim
		Secondary:
	All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/

#include "HowToPlayScreenObject.h"
#include "UICallbacks.h"
#include "ClickableComponent.h"
#include "GameEnum.h"
#include "HoverableComponent.h"
#include "StartScreenObject.h"
#include "TextureComponent.h"
#include "UIPositionAdjustComponent.h"

namespace uruk
{

	
	void HowToPlayScreenObject::OnConstructEnd()
	{
		const ID textureID = AddComponent<TextureComponent>("HowToPlay",EObjectZOrder::CORRIDOR_FLOOR);
		mBackToMenuButtonTextureID = AddComponent<TextureComponent>("Back", EObjectZOrder::BUTTONS);
		AddComponent<TransformComponent>(Vector3D{ 0.f,0.f },true);
		mLevel->GetComponent<TextureComponent>(mBackToMenuButtonTextureID)->SetOffset(Vector3D{ 1680.f,955.f });
		const ID clickID = AddComponent<ClickableComponent>(189.f, 71.f);
		ClickableComponent* clickableComponent = GetComponent<ClickableComponent>();
		clickableComponent->SetOffset(1680.f, 955.f);
		clickableComponent->SetOnClicked(std::make_unique<HowToPlayBackToMenuClickCallback>());

		const ID hoverID = AddComponent<HoverableComponent>(189.f, 71.f);
		HoverableComponent* hoverableComponent = GetComponent<HoverableComponent>();
		hoverableComponent->SetOffset(1680.f, 955.f);
		hoverableComponent->SetOnHovered(std::make_unique<HowToPlayBackToMenuHoveredCallback>());
		hoverableComponent->SetOnUnhovered(std::make_unique<HowToPlayBackToMenuUnHoveredCallback>());

		AddComponent<UIPositionAdjustComponent>(
			std::vector<ID>{ textureID,mBackToMenuButtonTextureID }, std::vector<ID>{ clickID }, std::vector<ID>{ hoverID }
		);
	}

	void HowToPlayScreenObject::SetStartingScreenID(const ID& startingScreenID)
	{
		mStartScreenObjectID = startingScreenID;
	}

	void HowToPlayScreenObject::OnBackToMenuClicked()
	{
		Disable();
		mLevel->GetObject<StartScreenObject>(mStartScreenObjectID)->Enable();
		mLevel->GetObject<StartScreenObject>(mStartScreenObjectID)->OnEnabled(true);
	}

	void HowToPlayScreenObject::OnBackToMenuHovered()
	{
		mLevel->GetComponent<TextureComponent>(mBackToMenuButtonTextureID)->SetTexture("BackHovered");
	}

	void HowToPlayScreenObject::OnBackToMenuUnHovered()
	{
		mLevel->GetComponent<TextureComponent>(mBackToMenuButtonTextureID)->SetTexture("Back");
	}
}
