/*
	File Name: EnemyAnimationStateMachine.cpp
	Project Name: U.R.U.K
	Author(s):
		Primary: Doyoon Kim
		Secondary:
	All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once
#include "Object.h"

namespace uruk
{
	class HowToPlayScreenObject : public uruk::Object<HowToPlayScreenObject>
	{
	public:
		void OnConstructEnd() override;
		void SetStartingScreenID(const ID& startingScreenID);
		void OnBackToMenuClicked();
		void OnBackToMenuHovered();
		void OnBackToMenuUnHovered();
	private:
		ID mStartScreenObjectID;
		ID mBackToMenuButtonTextureID;
	};
}