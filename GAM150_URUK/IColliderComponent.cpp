/*
    File Name: IColliderComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "IColliderComponent.h"
#include "Game.h"
#include "Level.h"
#include "BoxColliderComponent.h"
#include "Serializer.h"
#include "SphereColliderComponent.h"

namespace uruk
{
	IColliderComponent::IColliderComponent(CollisionTag tag, CollisionFilter filter, bool isOnlyForDetection, const std::function<ColliderType()>& getColliderType)
		:mCollisionTag(tag),mCollisionFilter(filter),mGetColliderType(getColliderType),mIsOnlyForDetection(isOnlyForDetection)
	{
		mCollidingColliders.reserve(8);
	}

	ColliderType IColliderComponent::GetColliderType() const
	{
		return mGetColliderType();
	}

	TypeID IColliderComponent::GetColliderTypeID() const
	{
		return mColliderTypeID;
	}

	ID IColliderComponent::GetColliderID() const
	{
		return mColliderID;
	}

	CollisionTag IColliderComponent::GetCollisionTag() const
	{
		return mCollisionTag;
	}

	CollisionFilter IColliderComponent::GetCollisionFilter() const
	{
		return mCollisionFilter;
	}

	void IColliderComponent::SetOnCollisionEnter(std::unique_ptr<ICollisionCallbackBase>&& callback)
	{
		mOnCollisionEnterCallback = std::move(callback);
	}

	void IColliderComponent::SetOnCollisionGoing(std::unique_ptr<ICollisionCallbackBase>&& callback)
	{
		mOnCollisionGoingCallback = std::move(callback);
	}

	void IColliderComponent::SetOnCollisionEnd(std::unique_ptr<ICollisionCallbackBase>&& callback)
	{
		mOnCollisionEndCallback = std::move(callback);
	}

	void IColliderComponent::OnCollisionEnter(IColliderComponent* collidedWith)
	{
#ifdef UI_DEBUG_DRAW
		std::cout << "onCollisionEnter!" << std::endl;
#endif
		mCollidingColliders.push_back({ collidedWith->GetColliderID(),collidedWith->GetColliderType() });
		if(mOnCollisionEnterCallback != nullptr)
		{
			mOnCollisionEnterCallback->Execute(this,collidedWith);
		}
	}

	void IColliderComponent::OnCollisionGoing(IColliderComponent* collidedWith)
	{
#ifdef UI_DEBUG_DRAW
		std::cout << "onCollisionGoing!" << std::endl;
#endif
		if (mOnCollisionGoingCallback != nullptr)
		{
			mOnCollisionGoingCallback->Execute(this,collidedWith);
		}
	}

	void IColliderComponent::OnCollisionEnd(IColliderComponent* collidedWith)
	{
#ifdef UI_DEBUG_DRAW
		std::cout << "onCollisionEnd!" << std::endl;
#endif
		const auto& it = std::find(mCollidingColliders.begin(), mCollidingColliders.end(), std::pair{ collidedWith->GetColliderID() ,collidedWith->GetColliderType()});
		if (it != mCollidingColliders.end())
		{
#ifdef UI_DEBUG_DRAW
			std::cout << "removed " << it->first << "from collising list of " <<GetColliderID()<<std::endl;
#endif
			mCollidingColliders.erase(it);
		}
		if (mOnCollisionEndCallback != nullptr)
		{
			mOnCollisionEndCallback->Execute(this,collidedWith);
		}
	}

	bool IColliderComponent::IsTriggerCollider() const
	{
		return mIsOnlyForDetection;
	}

	bool IColliderComponent::WasCollidingWith(const ID& collidedWithID, ColliderType colliderType) const
	{
		const auto& it = std::find(mCollidingColliders.begin(), mCollidingColliders.end(), std::pair{ collidedWithID ,colliderType});
		if(it != mCollidingColliders.end())
		{
			return true;
		}
		return false;
	}

	void IColliderComponent::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mColliderTypeID);
		serialize(outputFileStream, mColliderID);
		serialize(outputFileStream, mCollidingColliders);
		serialize(outputFileStream, mOnCollisionEnterCallback->GetType());
		mOnCollisionEnterCallback->OnSave(outputFileStream);
		serialize(outputFileStream, mOnCollisionGoingCallback->GetType());
		mOnCollisionGoingCallback->OnSave(outputFileStream);
		serialize(outputFileStream, mOnCollisionEndCallback->GetType());
		mOnCollisionEndCallback->OnSave(outputFileStream);
	}

	void IColliderComponent::OnLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mColliderTypeID);
		deserialize(inputFileStream, mColliderID);
		deserialize(inputFileStream, mCollidingColliders);
		TypeID onCollisionEnterTypeID;
		deserialize(inputFileStream, onCollisionEnterTypeID);
		std::unique_ptr<ICollisionCallbackBase> onCollisionEnterCallback = CustomCallbackFactory<ICollisionCallbackBase>::Generate(onCollisionEnterTypeID);
		onCollisionEnterCallback->OnLoad(inputFileStream);
		mOnCollisionEnterCallback = std::move(onCollisionEnterCallback);
		
		TypeID onCollisionGoingTypeID;
		deserialize(inputFileStream, onCollisionGoingTypeID);
		std::unique_ptr<ICollisionCallbackBase> onCollisionGoingCallback = CustomCallbackFactory<ICollisionCallbackBase>::Generate(onCollisionGoingTypeID);
		onCollisionGoingCallback->OnLoad(inputFileStream);
		mOnCollisionGoingCallback = std::move(onCollisionGoingCallback);

		TypeID onCollisionEndTypeID;
		deserialize(inputFileStream, onCollisionEndTypeID);
		std::unique_ptr<ICollisionCallbackBase> onCollisionEndCallback = CustomCallbackFactory<ICollisionCallbackBase>::Generate(onCollisionEndTypeID);
		onCollisionEndCallback->OnLoad(inputFileStream);
		mOnCollisionEndCallback = std::move(onCollisionEndCallback);
	}

	void IColliderComponent::OnUpdate()
	{
		for(auto& pair : mCollidingColliders)
		{
			if (pair.second == ColliderType::BoxCollider)
			{
				BoxColliderComponent* collidingWith = Game::GetCurrentLevel()->GetComponent<BoxColliderComponent>(pair.first);
				if (collidingWith == nullptr)
				{
					mCollidingColliders.erase(std::find(mCollidingColliders.begin(),mCollidingColliders.end(),pair));
					return;
				}
				if(!collidingWith->IsActive())
				{
					const auto& it = std::find(mCollidingColliders.begin(), mCollidingColliders.end(), std::pair{ pair.first,pair.second });
					mCollidingColliders.erase(it);
				}
			}
			else
			{
				SphereColliderComponent* collidingWith = Game::GetCurrentLevel()->GetComponent<SphereColliderComponent>(pair.first);
				if (collidingWith == nullptr)
				{
					mCollidingColliders.erase(std::find(mCollidingColliders.begin(), mCollidingColliders.end(), pair));
					return;
				}
				if (!collidingWith->IsActive())
				{
					const auto& it = std::find(mCollidingColliders.begin(), mCollidingColliders.end(), std::pair{ pair.first,pair.second });
					mCollidingColliders.erase(it);
				}
			}
		}
	}


	void IColliderComponent::OnDisabled()
	{
		for (auto& pair : mCollidingColliders)
		{
			IColliderComponent* collidingWith;
			if (pair.second == ColliderType::BoxCollider)
			{
				collidingWith = Game::GetCurrentLevel()->GetComponent<BoxColliderComponent>(pair.first);
			}
			else
			{
				collidingWith = Game::GetCurrentLevel()->GetComponent<SphereColliderComponent>(pair.first);
			}
			collidingWith->OnCollisionEnd(this);
			OnCollisionEnd(collidingWith);
		}
		mCollidingColliders.clear();
	}

	void IColliderComponent::SetTypeID(const ID& typeID)
	{
		mColliderTypeID = typeID;
	}

	void IColliderComponent::SetID(const ID& id)
	{
		mColliderID = id;
	}
}
