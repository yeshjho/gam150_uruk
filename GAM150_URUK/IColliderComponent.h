/*
    File Name: IColliderComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: Joonho Hwang
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <functional>

#include "CustomCallbackBase.h"
#include "ICustomCallbackCommon.h"
#include "Internal.h"

namespace uruk
{
	class IColliderComponent;

	enum class ColliderType
	{
		SphereCollider,
		BoxCollider
	};

	enum class CollisionTag : size_t
	{
		//result of & operation between any two elements are always 0.
		//ex) Hero = 0x1,Ball = 0x2,Tree = 0x4 ... good
		//ex) Sword = 0x2, Skeleton = 0x3 -> Not Good! Sword & Skeleton != 0.
		None = 0,
		Uruk = 1 << 0,
		UrukAttack = 1 << 1,
		
		KonDoori = 1 << 5,
		SonXaary = 1 << 6,
		PohShigi = 1 << 7,

		KonDooriAttack = 1 << 10,
		SonXaaryAttack = 1 << 11,
		PohShigiAttack = 1 << 12,
		

		Obstacle = 1 << 15,
		Wall = 1 << 16,

		HealthRestore = 1 << 20,
		
		Door = 1 << 25,
		Fishgate = 1 << 26,

		All = -1
	};


	using CollisionFilter = CollisionTag;

	inline CollisionFilter operator|(CollisionTag tag1,CollisionTag tag2)
	{
		return static_cast<CollisionFilter>((static_cast<size_t>(tag1) | static_cast<size_t>(tag2)));
	}

	inline CollisionFilter operator&(CollisionTag tag1, CollisionTag tag2)
	{
		return static_cast<CollisionFilter>((static_cast<size_t>(tag1) & static_cast<size_t>(tag2)));
	}

	inline CollisionFilter operator~(CollisionTag tag1)
	{
		return static_cast<CollisionFilter>(~static_cast<size_t>(tag1));
	}
	
	class ICollisionCallbackBase : public ICustomCallbackCommon
	{
	public:
		virtual void Execute(IColliderComponent* self, IColliderComponent* collideWith) = 0;
	};

	class DefaultColliderComponentCallback final : public CustomCallbackBase<DefaultColliderComponentCallback, ICollisionCallbackBase>
	{
	public:
		void Execute(IColliderComponent*, IColliderComponent*) override {}
	};

	
	class IColliderComponent
	{
	public:
		IColliderComponent(CollisionTag tag, CollisionFilter filter,bool isOnlyForDetection,const std::function<ColliderType()>&);
		ColliderType GetColliderType() const;
		TypeID GetColliderTypeID() const;
		ID GetColliderID() const;
		CollisionTag GetCollisionTag() const;
		CollisionFilter GetCollisionFilter() const;
		void SetOnCollisionEnter(std::unique_ptr<ICollisionCallbackBase>&& callback);
		void SetOnCollisionGoing(std::unique_ptr<ICollisionCallbackBase>&& callback);
		void SetOnCollisionEnd(std::unique_ptr<ICollisionCallbackBase>&& callback);
		void OnCollisionEnter(IColliderComponent* collidedWith);
		void OnCollisionGoing(IColliderComponent* collidedWith);
		void OnCollisionEnd(IColliderComponent* collidedWith);
		bool IsTriggerCollider() const;
		bool WasCollidingWith(const ID& collidedWithID,ColliderType colliderType) const;
		void OnSave(std::ofstream& outputFileStream) const;
		void OnLoad(std::ifstream& inputFileStream);
		void OnUpdate();
		void OnDisabled();
		void SetTypeID(const ID& typeID);
		void SetID(const ID& id);
	protected:
		CollisionTag mCollisionTag;
		CollisionFilter mCollisionFilter;
		std::function<ColliderType()> mGetColliderType;
		ID mColliderTypeID;
		ID mColliderID;
		bool mIsOnlyForDetection = false;
		std::vector<std::pair<ID,ColliderType>> mCollidingColliders;
		std::unique_ptr<ICollisionCallbackBase> mOnCollisionEnterCallback = std::make_unique<DefaultColliderComponentCallback>();
		std::unique_ptr<ICollisionCallbackBase> mOnCollisionGoingCallback = std::make_unique<DefaultColliderComponentCallback>();
		std::unique_ptr<ICollisionCallbackBase> mOnCollisionEndCallback = std::make_unique<DefaultColliderComponentCallback>();
	};
}
