/*
    File Name: IComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "IComponent.h"

#include "Level.h"
#include "Logger.h"
#include "Serializer.h"



namespace uruk
{
	IObject* IComponent::GetOwner() const noexcept
	{
		return mLevel->GetObject(mOwnerTypeID, mOwnerID);
	}


	void IComponent::OnEngineSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mOwnerTypeID);
		serialize(outputFileStream, mOwnerID);
		serialize(outputFileStream, mIsActive);
		serialize(outputFileStream, mCountSinceLastUpdate);
	}

	void IComponent::OnEngineLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mOwnerTypeID);
		deserialize(inputFileStream, mOwnerID);
		deserialize(inputFileStream, mIsActive);
		deserialize(inputFileStream, mCountSinceLastUpdate);
	}


	void IComponent::SetActive(const bool isActive) noexcept
	{
		if (isActive == mIsActive)
		{
			return;
		}

		logger.LogVerbose(ELogCategory::COMPONENT_EVENT, (isActive ? "Enabling" : "Disabling") + std::string(" Component ") + to_string(mID));

		if (isActive && !GetOwner()->IsActive())
		{
			logger.LogError(ELogCategory::COMPONENT_EVENT, "Turning a component " + to_string(mID) + " on of an inactive object!");
		}

		mIsActive = isActive;
		isActive ? OnEnabled() : OnDisabled();
	}
}
