/*
    File Name: IComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <fstream>
#include <limits>
#include <map>

#include "Internal.h"



namespace uruk
{
	class Vector3D;

	class IObject;
	class Level;


	
	enum class EUpdateType : char
	{
		BY_MILLISECOND,
		BY_FRAME,
		// You can still update manually by calling Update() directly.
		NEVER
	};

	enum EComponentAttribute : int
	{
		NO_MEMCPY = 1 << 0,
		NO_SAVE = 1 << 1,
		DRAWABLE = 1 << 15
	};



	namespace update_priorities
	{
		inline constexpr int LOWEST_PRIORITY = std::numeric_limits<int>::min();
		inline constexpr int LOWER_PRIORITY = LOWEST_PRIORITY + 100000;
		inline constexpr int LOW_PRIORITY = LOWER_PRIORITY + 100000;
		inline constexpr int NORMAL_PRIORITY = 0;
		inline constexpr int HIGH_PRIORITY = std::numeric_limits<int>::max() - 200000;
		inline constexpr int HIGHER_PRIORITY = HIGH_PRIORITY + 100000;
		inline constexpr int HIGHEST_PRIORITY = HIGHER_PRIORITY + 100000;
	}



	/// NOTE: Do NOT access its member variables in a component's constructor. Ex) GetOwner(), mID, etc.
	/// Use OnConstructEnd() instead.
	class IComponent
	{
		friend class Level;
		template<typename T>
		friend class ComponentOnLoadFunctionPointerStorer;

	public:
		virtual ~IComponent() = default;


		[[nodiscard]] IObject* GetOwner() const noexcept;
		[[nodiscard]] constexpr const ID& GetID() const noexcept;
		[[nodiscard]] virtual const TypeID& GetTypeID() const noexcept = 0;

		[[nodiscard]] virtual EUpdateType GetUpdateType() const noexcept = 0;
		[[nodiscard]] virtual int GetUpdateInterval() const noexcept = 0;
		[[nodiscard]] virtual int GetUpdatePriority() const noexcept = 0;
		[[nodiscard]] virtual int GetAttributes() const noexcept = 0;


		virtual void OnConstructEnd() {}
		virtual void OnMove([[maybe_unused]] void* newLocation) {}

		virtual void OnConstructorArgumentSave([[maybe_unused]] std::ofstream& outputFileStream) const {}
		virtual void OnSave([[maybe_unused]] std::ofstream& outputFileStream) const {}
		virtual void OnLoad([[maybe_unused]] std::ifstream& inputFileStream) {}

		void OnEngineSave(std::ofstream& outputFileStream) const;
		void OnEngineLoad(std::ifstream& inputFileStream);

		virtual void OnBeginPlay() {}
		virtual void OnUpdate() {}

		virtual void OnDraw([[maybe_unused]] const Vector3D& camPosition, [[maybe_unused]] float camRotation, [[maybe_unused]] const Vector3D& camZoomScale) const {};

		virtual void OnEnabled() {}
		virtual void OnDisabled() {}


		[[nodiscard]] constexpr bool IsActive() const noexcept;
		void SetActive(bool isActive) noexcept;
		inline void Enable() noexcept;
		inline void Disable() noexcept;
		inline void ToggleActive() noexcept;


		[[nodiscard]] virtual bool operator==(const IComponent& rhs) const noexcept = 0;
		[[nodiscard]] virtual bool operator==(const IComponent* rhs) const noexcept = 0;
		[[nodiscard]] virtual bool operator!=(const IComponent& rhs) const noexcept = 0;
		[[nodiscard]] virtual bool operator!=(const IComponent* rhs) const noexcept = 0;

		
		[[nodiscard]] inline static const std::function<IComponent*(Level* level, const ID& id, std::ifstream& inputFileStream)>& GetOnLoadConstruct(const TypeID& typeID);



	protected:
		TypeID mOwnerTypeID;
		ID mOwnerID;
		ID mID;

		bool mIsActive = true;

		long long mCountSinceLastUpdate = 0;
		Level* mLevel = nullptr;

		 
		inline static std::map<TypeID, std::function<IComponent*(Level* level, const ID& id, std::ifstream& inputFileStream)>> onLoadConstructFunctions;
	};





	template<typename T, typename = void>
	struct HasOnLoadConstruct : std::false_type
	{};

	template<typename T>
	struct HasOnLoadConstruct<T, std::void_t<std::enable_if_t<std::is_same_v<decltype(T::OnLoadConstruct(std::declval<Level*>(), std::declval<const ID&>(), std::declval<std::ifstream&>())), IComponent*>>>> : std::true_type
	{};
}

#include "IComponent.inl"
