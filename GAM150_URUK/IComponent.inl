/*
    File Name: IComponent.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	constexpr const ID& IComponent::GetID() const noexcept
	{
		return mID;
	}


	constexpr bool IComponent::IsActive() const noexcept
	{
		return mIsActive;
	}

	inline void IComponent::Enable() noexcept
	{
		SetActive(true);
	}

	inline void IComponent::Disable() noexcept
	{
		SetActive(false);
	}

	inline void IComponent::ToggleActive() noexcept
	{
		SetActive(!IsActive());
	}


	inline const std::function<IComponent*(Level* level, const ID& id, std::ifstream& inputFileStream)>& IComponent::GetOnLoadConstruct(const TypeID& typeID)
	{
		return onLoadConstructFunctions.at(typeID);
	}
}
