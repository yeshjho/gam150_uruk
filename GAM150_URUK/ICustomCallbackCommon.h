/*
    File Name: ICustomCallbackCommon.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Internal.h"

namespace uruk
{
	class ICustomCallbackCommon
	{
	public:
		virtual ~ICustomCallbackCommon() = default;
		virtual TypeID GetType() = 0;
		virtual void OnLoad([[maybe_unused]] std::ifstream& inputFileStream) {}
		virtual void OnSave([[maybe_unused]] std::ofstream& outputFileStream) {}
	};
}
