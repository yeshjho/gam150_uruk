/*
    File Name: IDBasedContiguousContainer.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "IDBasedContiguousContainer.h"

#include <cstdlib>



namespace uruk
{
	IDBasedContiguousContainer::IDBasedContiguousContainer(const size_t containerSize, const unsigned int maxCountPerType)
		: CONTAINER_SIZE(containerSize), MAX_COUNT_PER_TYPE(maxCountPerType), mContainer(std::malloc(containerSize)), mCurrentAddress(static_cast<char*>(mContainer))
	{
		logger.LogLog(ELogCategory::CONTAINER_MEMORY, "Claimed memories at " + toString(mContainer) + " with size of " + std::to_string(CONTAINER_SIZE));
	}

	IDBasedContiguousContainer::~IDBasedContiguousContainer()
	{
		logger.LogLog(ELogCategory::CONTAINER_MEMORY, "Freeing memories at " + toString(mContainer) + " with size of " + std::to_string(CONTAINER_SIZE));
		std::free(mContainer);
	}


	void IDBasedContiguousContainer::claimMemories(const TypeID& typeID, const size_t objectSize)
	{
		void* const newAddress = mCurrentAddress;

		const size_t newMemorySize = objectSize * MAX_COUNT_PER_TYPE;
		mCurrentAddress += newMemorySize;

		/// Align the address with 8.
		const char padding = char(reinterpret_cast<size_t>(mCurrentAddress) % 8);
		mCurrentAddress += padding;

		if(mCurrentAddress - padding > static_cast<char*>(mContainer) + CONTAINER_SIZE)
		{
			logger.LogFatal(ELogCategory::CONTAINER_MEMORY, "Container Size Overflow");
		}

		mContainerBoundaries[typeID] = ContainerBoundaries{ newAddress, mCurrentAddress - padding, nullptr, nullptr, objectSize };
		logger.LogLog(ELogCategory::CONTAINER_MEMORY, "Assigned memories for " + IDManager::GetTypeName(typeID) + " at " + toString(newAddress) + " with size of " + std::to_string(size_t(mCurrentAddress) - size_t(newAddress)));
	}
	
	void* IDBasedContiguousContainer::getFreeSlot(const TypeID& typeID, const size_t objectSize)
	{
		if (!mContainerBoundaries.count(typeID))
		{
			claimMemories(typeID, objectSize);
		}

		const ContainerBoundaries& containerBoundaries = mContainerBoundaries.at(typeID);
		void* const next = containerBoundaries.back ? static_cast<char*>(containerBoundaries.back) + objectSize : containerBoundaries.begin;
		if (next == containerBoundaries.end)
		{
			logger.LogFatal(ELogCategory::CONTAINER_MEMORY, "Reached the max count for this type of object");
		}

		logger.LogLog(ELogCategory::CONTAINER_MEMORY, "Claimed " + toString(next) + " for a new slot");
		return next;
	}

	
	ID IDBasedContiguousContainer::claimID(const TypeID& typeID)
	{
		const ID newId = IDManager::ClaimID();
		mAddressByID[typeID][newId] = nullptr;
		return newId;
	}
}
