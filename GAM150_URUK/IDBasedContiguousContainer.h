/*
    File Name: IDBasedContiguousContainer.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <algorithm>
#include <type_traits>
#include <unordered_map>

#include "Internal.h"
#include "Logger.h"
#include "ObjectIterator.h"



namespace uruk
{
	/// Leading _s are there to avoid warning C4455.
	constexpr unsigned long long operator ""_KB(const unsigned long long kb)
	{
		return kb * 1024;
	}

	constexpr unsigned long long operator ""_MB(const unsigned long long mb)
	{
		return mb * 1024 * 1024;
	}

	constexpr unsigned long long operator ""_GB(const unsigned long long gb)
	{
		return gb * 1024 * 1024 * 1024;
	}



	struct ContainerBoundaries
	{
		void* begin;
		void* end;

		void* front;
		void* back;

		size_t size;


		template<typename T>
		[[nodiscard]] size_t GetCount() const noexcept;
	};



	template<typename T, typename = void>
	struct HasAttributes : std::false_type
	{};

	template<typename T>
	struct HasAttributes<T, std::void_t<decltype(std::declval<T>().GetAttributes())>> : std::true_type
	{};



	class IDBasedContiguousContainer
	{
	public:
		IDBasedContiguousContainer(size_t containerSize, unsigned int maxCountPerType);
		~IDBasedContiguousContainer();

		IDBasedContiguousContainer(const IDBasedContiguousContainer&) = delete;
		IDBasedContiguousContainer& operator=(const IDBasedContiguousContainer&) = delete;

		IDBasedContiguousContainer(IDBasedContiguousContainer&&) = default;
		IDBasedContiguousContainer& operator=(IDBasedContiguousContainer&&) = delete;


		template<typename T, typename ...Args>
		ID Add(ID id, Args&&... args);

		template<typename T>
		[[nodiscard]] T* Get(const ID& id) const noexcept;

		// Cast the object as T*.
		template<typename T>
		[[nodiscard]] T* Get(const TypeID& typeID, const ID& id) const noexcept;

		// Warning: DO NOT Remove objects using this function.
		template<typename T, bool ActiveOnly = false>
		[[nodiscard]] ObjectIterator<T, ActiveOnly> GetAll() const noexcept;

		// Cast the objects as T.
		// Warning: DO NOT Remove objects using this function.
		template<typename T, bool ActiveOnly = false>
		[[nodiscard]] ObjectIterator<T, ActiveOnly> GetAll(const TypeID& typeID) const noexcept;

		template<typename T>
		void Remove(const ID& id);

		// Call the destructor of type T.
		template<typename T>
		void Remove(const TypeID& typeID, const ID& id);


		// Casts the objects as T.
		// Warning: DO NOT Remove objects using this function.
		template<typename T, typename Function>
		void ForEach(Function f);

		// Casts the objects as T.
		// Warning: DO NOT Remove objects using this function.
		template<typename T, typename Function>
		void ForEach(Function f) const;

		template<typename T, typename Comp>
		void Sort(Comp comp);


		// Call the destructor of type T.
		template<typename T>
		void Reset(size_t containerSize, unsigned int maxCountPerType);

		
		[[nodiscard]] constexpr const std::unordered_map<TypeID, ContainerBoundaries>& GetContainerBoundaries() const;

		template<typename T>
		[[nodiscard]] size_t GetObjectCount() const noexcept;

		[[nodiscard]] constexpr size_t GetContainerSize() const noexcept;
		[[nodiscard]] constexpr unsigned int GetMaxCountPerType() const noexcept;

	private:
		void claimMemories(const TypeID& typeID, size_t objectSize);
		void* getFreeSlot(const TypeID& typeID, size_t objectSize);

		ID claimID(const TypeID& typeID);



	private:
		size_t CONTAINER_SIZE;
		unsigned int MAX_COUNT_PER_TYPE;
		
		void* mContainer;
		char* mCurrentAddress;
		std::unordered_map<TypeID, std::unordered_map<ID, void*>> mAddressByID;
		std::unordered_map<TypeID, std::unordered_map<void*, ID>> mIDByAddress;
		std::unordered_map<TypeID, ContainerBoundaries> mContainerBoundaries;
	};
}

#include "IDBasedContiguousContainer.inl"
