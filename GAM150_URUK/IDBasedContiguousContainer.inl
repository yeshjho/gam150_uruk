/*
    File Name: IDBasedContiguousContainer.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	template<typename T>
	size_t ContainerBoundaries::GetCount() const noexcept
	{
		return (reinterpret_cast<char*>(back) - reinterpret_cast<char*>(front)) / size + 1;
	}


	
	template<typename T, typename ...Args>
	ID IDBasedContiguousContainer::Add(ID id, Args&& ...args)
	{
		static const TypeID TYPE_ID = IDManager::GetTypeID<T>();
		static const std::string TYPE_NAME = IDManager::GetTypeName(TYPE_ID);
		static constexpr size_t OBJECT_SIZE = sizeof(T);

		logger.LogVerbose(ELogCategory::CONTAINER_ACTION, "Adding\t" + TYPE_NAME);
		void* const address = getFreeSlot(TYPE_ID, OBJECT_SIZE);
		id = id.is_nil() ? claimID(TYPE_ID) : id;

		/// Make the object.
		/// Note: C2440 will occur when there's no constructor available for given arguments.
		new (address) T(std::forward<Args>(args)...);
		logger.LogLog(ELogCategory::CONTAINER_ACTION, "Added\t" + TYPE_NAME + "\t\t" + to_string(id));

		/// Store the information to lookup tables.
		mAddressByID[TYPE_ID][id] = address;
		mIDByAddress[TYPE_ID][address] = id;

		ContainerBoundaries& containerBoundaries = mContainerBoundaries.at(TYPE_ID);
		_ASSERT(containerBoundaries.front < address);
		if (!containerBoundaries.front)
		{
			containerBoundaries.front = address;
		}
		containerBoundaries.back = address;

		return id;
	}

	template<typename T>
	T* IDBasedContiguousContainer::Get(const ID& id) const noexcept
	{
		static const TypeID TYPE_ID = IDManager::GetTypeID<T>();
		
		return Get<T>(TYPE_ID, id);
	}

	template<typename T>
	T* IDBasedContiguousContainer::Get(const TypeID& typeID, const ID& id) const noexcept
	{
		T* toReturn;
		
		if (!mAddressByID.count(typeID) || !mAddressByID.at(typeID).count(id))
		{
			toReturn = nullptr;
			logger.LogWarning(ELogCategory::CONTAINER_ACTION, "Returning nullptr as " + IDManager::GetTypeName(typeID));
		}
		else
		{
			toReturn = const_cast<T*>(static_cast<const T*>(mAddressByID.at(typeID).at(id)));
		}

		return toReturn;
	}

	template<typename T, bool ActiveOnly>
	ObjectIterator<T, ActiveOnly> IDBasedContiguousContainer::GetAll() const noexcept
	{
		static const TypeID TYPE_ID = IDManager::GetTypeID<T>();
		
		return GetAll<T, ActiveOnly>(TYPE_ID);
	}

	template<typename T, bool ActiveOnly>
	ObjectIterator<T, ActiveOnly> IDBasedContiguousContainer::GetAll(const TypeID& typeID) const noexcept
	{
		if (const auto result = mContainerBoundaries.find(typeID); result != mContainerBoundaries.end())
		{
			const ContainerBoundaries& containerBoundaries = result->second;
			return ObjectIterator<T, ActiveOnly>{ static_cast<T*>(containerBoundaries.front), static_cast<T*>(containerBoundaries.back), containerBoundaries.size };
		}
		
		return ObjectIterator<T, ActiveOnly>{ nullptr, nullptr };
	}

	
	template<typename T>
	void IDBasedContiguousContainer::Remove(const ID& id)
	{
		static const TypeID TYPE_ID = IDManager::GetTypeID<T>();
		
		Remove<T>(TYPE_ID, id);
	}

	template<typename T>
	void IDBasedContiguousContainer::Remove(const TypeID& typeID, const ID& id)
	{
		std::unordered_map<ID, void*>& idTable = mAddressByID.at(typeID);
		ContainerBoundaries& containerBoundaries = mContainerBoundaries.at(typeID);

		/// Remove the object.
		void* const toRemove = idTable.at(id);
		logger.LogVerbose(ELogCategory::CONTAINER_ACTION, "Removing\t" + IDManager::GetTypeName(typeID) + "\t\t" + to_string(id) + "\t at " + toString(toRemove));
		static_cast<T*>(toRemove)->~T();
		
		/// Clean up the changed information.
		idTable.at(id) = nullptr;

		if (toRemove == containerBoundaries.front && toRemove == containerBoundaries.back)
		{
			containerBoundaries.front = nullptr;
			containerBoundaries.back = nullptr;
		}
		else
		{
			if (toRemove != containerBoundaries.back)
			{
				/// Swap the removed object with the last element of the type for contiguous memory.
				void* const back = containerBoundaries.back;
				if constexpr (HasAttributes<T>::value)
				{
					// 1 == NO_MEMCPY
					if (bool(static_cast<T*>(back)->GetAttributes() & 1))
					{
						static_cast<T*>(back)->OnMove(toRemove);
					}
					else
					{
						std::memcpy(toRemove, back, containerBoundaries.size);
					}
				}
				else
				{
					std::memcpy(toRemove, back, containerBoundaries.size);
				}
				logger.LogLog(ELogCategory::CONTAINER_MEMORY, "Swapped " + toString(back) + " with " + toString(toRemove));

				mAddressByID.at(typeID).at(mIDByAddress.at(typeID).at(back)) = toRemove;
				mIDByAddress.at(typeID).at(toRemove) = mIDByAddress.at(typeID).at(back);
			}
			
			containerBoundaries.back = static_cast<char*>(containerBoundaries.back) - containerBoundaries.size;
		}
		logger.LogLog(ELogCategory::CONTAINER_ACTION, "Removed\t" + IDManager::GetTypeName(typeID) + "\t\t" + to_string(id) + "\t at " + toString(toRemove));
	}


	template<typename T, typename Function>
	void IDBasedContiguousContainer::ForEach(Function f)
	{
		for (auto [typeID, containerBoundaries] : mContainerBoundaries)
		{
			for (T& object : GetAll<T>(typeID))
			{
				f(object);
			}
		}
	}

	template<typename T, typename Function>
	void IDBasedContiguousContainer::ForEach(Function f) const
	{
		for (auto [typeID, containerBoundaries] : mContainerBoundaries)
		{
			for (const T& object : GetAll<T>(typeID))
			{
				f(object);
			}
		}
	}


	template<typename T, typename Comp>
	void IDBasedContiguousContainer::Sort(Comp comp)
	{
		static const TypeID TYPE_ID = IDManager::GetTypeID<T>();
		logger.LogVerbose(ELogCategory::CONTAINER_ACTION, "Sorting " + IDManager::GetTypeName(TYPE_ID));

		ObjectIterator<T, false> it = GetAll<T>();
		std::sort(it.begin(), it.end(), comp);

		for (T* address = it.begin(); address != it.end(); address = reinterpret_cast<T*>(reinterpret_cast<char*>(address) + it.mSize))
		{
			const ID id = address->GetID();

			mAddressByID.at(TYPE_ID).at(id) = static_cast<void*>(address);
			mIDByAddress.at(TYPE_ID).at(static_cast<void*>(address)) = id;
		}
		
		logger.LogLog(ELogCategory::CONTAINER_ACTION, "Sorted " + IDManager::GetTypeName(TYPE_ID));
	}


	template<typename T>
	void IDBasedContiguousContainer::Reset(const size_t containerSize, const unsigned int maxCountPerType)
	{
		logger.LogLog(ELogCategory::CONTAINER_ACTION, "Resetting container " + toString(mContainer));
		
		ForEach<T>([](const T& t) { t.~T(); });

		mAddressByID.clear();
		mIDByAddress.clear();
		mContainerBoundaries.clear();
		
		if (containerSize >= CONTAINER_SIZE && maxCountPerType >= MAX_COUNT_PER_TYPE)
		{
			CONTAINER_SIZE = containerSize;
			MAX_COUNT_PER_TYPE = maxCountPerType;
#pragma warning(push)
#pragma warning(disable:6308)  // We're checking the size previously, won't return nullptr
			mContainer = std::realloc(mContainer, CONTAINER_SIZE);
#pragma warning(pop)
			mCurrentAddress = static_cast<char*>(mContainer);

			return;
		}
		
		logger.LogLog(ELogCategory::CONTAINER_MEMORY, "Freeing memories at " + toString(mContainer) + " with size of " + std::to_string(CONTAINER_SIZE));
		std::free(mContainer);
		CONTAINER_SIZE = containerSize;
		MAX_COUNT_PER_TYPE = maxCountPerType;
		mContainer = std::malloc(containerSize);
		logger.LogLog(ELogCategory::CONTAINER_MEMORY, "Claimed memories at " + toString(mContainer) + " with size of " + std::to_string(CONTAINER_SIZE));
		mCurrentAddress = static_cast<char*>(mContainer);
	}

	
	constexpr const std::unordered_map<TypeID, ContainerBoundaries>& IDBasedContiguousContainer::GetContainerBoundaries() const
	{
		return mContainerBoundaries;
	}


	constexpr size_t IDBasedContiguousContainer::GetContainerSize() const noexcept
	{
		return CONTAINER_SIZE;
	}

	constexpr unsigned int IDBasedContiguousContainer::GetMaxCountPerType() const noexcept
	{
		return MAX_COUNT_PER_TYPE;
	}

	template<typename T>
	size_t IDBasedContiguousContainer::GetObjectCount() const noexcept
	{
		size_t sum = 0;
		for (const auto& [_, containerBoundary] : mContainerBoundaries)
		{
			if constexpr (HasAttributes<T>::value)
			{
				// 2 == NO_SAVE
				if (containerBoundary.size != 0 && !bool(static_cast<T*>(containerBoundary.back)->GetAttributes() & 2))
				{
					sum += containerBoundary.GetCount<T>();
				}
			}
			else
			{
				sum += containerBoundary.GetCount<T>();
			}
		}

		return sum;
	}
}
