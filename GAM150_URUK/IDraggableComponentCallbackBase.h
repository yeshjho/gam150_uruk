/*
    File Name: IDraggableComponentCallbackBase.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "ICustomCallbackCommon.h"

namespace uruk
{
	class DraggableComponent;

	class IDraggableComponentCallbackBase : public ICustomCallbackCommon
	{
	public:
		virtual void Execute(DraggableComponent*, float xMoveAmount = 0, float yMoveAmount = 0) = 0;
	};

}
