/*
    File Name: IObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "IObject.h"

#include "Level.h"
#include "Serializer.h"


namespace uruk
{
	void IObject::OnEngineSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mIsActive);
		serialize(outputFileStream, mComponents);
		serialize(outputFileStream, mPriorComponentActiveStats);
	}

	void IObject::OnEngineLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mIsActive);
		deserialize(inputFileStream, mComponents);
		deserialize(inputFileStream, mPriorComponentActiveStats);
	}


	void IObject::SetActive(const bool isActive)
	{
		if (isActive == mIsActive)
		{
			return;
		}

		logger.LogVerbose(ELogCategory::OBJECT_EVENT, (isActive ? "Enabling" : "Disabling") + std::string(" Object ") + to_string(mID));

		/// If the object is being disabled, store all the components' activenesses.
		if (!isActive)
		{
			mPriorComponentActiveStats.clear();

			for (auto [typeID, IDs] : mComponents)
			{
				for (auto id : IDs)
				{
					mPriorComponentActiveStats[typeID][id] = mLevel->GetComponent(typeID, id)->IsActive();
				}
			}
		}

		mIsActive = isActive;
		
		/// Restore the activenesses.
		for (auto [typeID, IDs] : mComponents)
		{
			for (auto id : IDs)
			{
				if (mPriorComponentActiveStats.at(typeID).at(id))
				{
					mLevel->GetComponent(typeID, id)->SetActive(isActive);
				}
			}
		}
	}


	void IObject::Emigrate(const ID& levelID) const
	{
		mLevel->TransferObject(GetTypeID(), GetID(), levelID);
	}


	void IObject::Emigrate(const ID& levelID, const Vector3D& newLocation) const
	{
		mLevel->TransferObject(GetTypeID(), GetID(), levelID, newLocation);
	}
}
