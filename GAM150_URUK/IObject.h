/*
    File Name: IObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <list>
#include <unordered_map>

#include "IComponent.h"



namespace uruk
{
	/// NOTE: Do NOT access its member variables in a component's constructor. Ex) mID, etc.
	/// Use OnConstructEnd() instead.
	class IObject
	{
		friend class Level;
		template<typename T>
		friend class ObjectOnLoadFunctionPointerStorer;

	public:
		IObject() = default;
		
		virtual ~IObject() = default;


		[[nodiscard]] constexpr const ID& GetID() const noexcept;
		[[nodiscard]] virtual const TypeID& GetTypeID() const noexcept = 0;


		virtual void OnConstructEnd() {}

		void OnEngineSave(std::ofstream& outputFileStream) const;
		void OnEngineLoad(std::ifstream& inputFileStream);


		[[nodiscard]] constexpr bool IsActive() const noexcept;
		void SetActive(bool isActive);
		inline void Enable();
		inline void Disable();
		inline void ToggleActive();


		template<typename ComponentType, typename ...Args>
		ID AddComponent(Args&&... args);

		// Return the FIRST component of the given type.
		template<typename ComponentType>
		[[nodiscard]] ComponentType* GetComponent() const;

		template<typename ComponentType>
		[[nodiscard]] const std::list<ID>& GetAllComponentIDs() const;

		[[nodiscard]] inline const std::unordered_map<TypeID, std::list<ID>>& GetAllComponents() const noexcept;

		template<typename ComponentType>
		[[nodiscard]] bool HasComponent() const;
		[[nodiscard]] inline bool HasComponent(const TypeID& typeID) const;


		void Emigrate(const ID& levelID) const;
		void Emigrate(const ID& levelID, const Vector3D& newLocation) const;


		[[nodiscard]] virtual bool operator==(const IObject& rhs) const noexcept = 0;
		[[nodiscard]] virtual bool operator==(const IObject* rhs) const noexcept = 0;
		[[nodiscard]] virtual bool operator!=(const IObject& rhs) const noexcept = 0;
		[[nodiscard]] virtual bool operator!=(const IObject* rhs) const noexcept = 0;


		[[nodiscard]] inline static const std::function<IObject*(Level* level, const ID& id)>& GetOnLoadConstruct(const TypeID& typeID);
		


	protected:
		ID mID;

		bool mIsActive = true;

		Level* mLevel = nullptr;
		std::unordered_map<TypeID, std::list<ID>> mComponents;
		std::unordered_map<TypeID, std::unordered_map<ID, bool>> mPriorComponentActiveStats;


		inline static std::map<TypeID, std::function<IObject* (Level* level, const ID& id)>> onLoadConstructFunctions;
	};
}

#include "IObject.inl"
