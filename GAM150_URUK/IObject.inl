/*
    File Name: IObject.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	constexpr const ID& IObject::GetID() const noexcept
	{
		return mID;
	}


	constexpr bool IObject::IsActive() const noexcept
	{
		return mIsActive;
	}

	inline void IObject::Enable()
	{
		SetActive(true);
	}

	inline void IObject::Disable()
	{
		SetActive(false);
	}

	inline void IObject::ToggleActive()
	{
		SetActive(!IsActive());
	}
	

	template<typename ComponentType, typename ...Args>
	ID IObject::AddComponent(Args&& ...args)
	{
		return this->mLevel->AddComponent<ComponentType>(this, std::forward<Args>(args)...);
	}

	template<typename ComponentType>
	ComponentType* IObject::GetComponent() const
	{
		static const TypeID TYPE_ID = IDManager::GetTypeID<ComponentType>();
		
		return this->mLevel->GetComponent<ComponentType>(mComponents.at(TYPE_ID).front());
	}

	template<typename ComponentType>
	const std::list<ID>& IObject::GetAllComponentIDs() const
	{
		static const TypeID TYPE_ID = IDManager::GetTypeID<ComponentType>();
		
		return mComponents.at(TYPE_ID);
	}

	inline const std::unordered_map<TypeID, std::list<ID>>& IObject::GetAllComponents() const noexcept
	{
		return mComponents;
	}

	template<typename ComponentType>
	bool IObject::HasComponent() const
	{
		static const TypeID TYPE_ID = IDManager::GetTypeID<ComponentType>();
		
		return HasComponent(TYPE_ID);
	}
	
	inline bool IObject::HasComponent(const TypeID& typeID) const
	{
		return mComponents.count(typeID) ? !mComponents.at(typeID).empty() : false;
	}


	const std::function<IObject* (Level* level, const ID& id)>& IObject::GetOnLoadConstruct(const TypeID& typeID)
	{
		return onLoadConstructFunctions.at(typeID);
	}
}
