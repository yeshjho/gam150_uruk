/*
    File Name: ITextComponentCallbackBase.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "ICustomCallbackCommon.h"

namespace uruk
{
	class TextComponent;
	class ITextComponentCallbackBase : public ICustomCallbackCommon
	{
	public:
		virtual void Execute(const TextComponent*) = 0;
	};
}