/*
    File Name: ITimerCallbackBase.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "ICustomCallbackCommon.h"


namespace uruk
{
	class ITimerCallbackBase : public ICustomCallbackCommon
	{
	public:
		virtual void Execute() = 0;
	};
}
