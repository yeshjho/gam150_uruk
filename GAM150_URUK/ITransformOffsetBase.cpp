/*
    File Name: ITransformOffsetBase.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "ITransformOffsetBase.h"

#include "Serializer.h"



namespace uruk
{
	void ITransformOffsetBase::OnEngineSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mOffset);
		serialize(outputFileStream, mRotationOffset);
		serialize(outputFileStream, mScaleOffset);
	}

	void ITransformOffsetBase::OnEngineLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mOffset);
		deserialize(inputFileStream, mRotationOffset);
		deserialize(inputFileStream, mScaleOffset);
	}
}
