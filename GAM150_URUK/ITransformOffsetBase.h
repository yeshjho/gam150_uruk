/*
    File Name: ITransformOffsetBase.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <fstream>

#include "Math.h"



namespace uruk
{
	class ITransformOffsetBase
	{
	public:
		constexpr ITransformOffsetBase(const Vector3D& offset, float rotationOffset, const Vector3D& scaleOffset) noexcept;


		[[nodiscard]] constexpr const Vector3D& GetOffset() const;
		[[nodiscard]] constexpr float GetRotationOffset() const;
		[[nodiscard]] constexpr const Vector3D& GetScaleOffset() const;


		constexpr void SetOffset(const Vector3D& offset);
		constexpr void SetOffset(float offsetX, float offsetY);
		constexpr void AddOffset(const Vector3D& offset);
		constexpr void AddOffset(float offsetX, float offsetY);
		inline void SetRotationOffset(float offset);
		inline void AddRotationOffset(float offset);
		constexpr void SetScaleOffset(const Vector3D& offset);
		constexpr void AddScaleOffset(const Vector3D& offset);
		inline void MultiplyScaleOffset(const Vector3D& offset);


		void OnEngineSave(std::ofstream& outputFileStream) const;
		void OnEngineLoad(std::ifstream& inputFileStream);


		
	protected:
		Vector3D mOffset;
		float mRotationOffset;
		Vector3D mScaleOffset;
	};
}

#include "ITransformOffsetBase.inl"
