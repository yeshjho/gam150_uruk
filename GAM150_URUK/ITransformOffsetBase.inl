/*
    File Name: ITransformOffsetBase.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	constexpr ITransformOffsetBase::ITransformOffsetBase(const Vector3D& offset, const float rotationOffset, const Vector3D& scaleOffset) noexcept
		: mOffset(offset), mRotationOffset(rotationOffset), mScaleOffset(scaleOffset)
	{}


	constexpr const Vector3D& ITransformOffsetBase::GetOffset() const
	{
		return mOffset;
	}

	constexpr float ITransformOffsetBase::GetRotationOffset() const
	{
		return mRotationOffset;
	}

	constexpr const Vector3D& ITransformOffsetBase::GetScaleOffset() const
	{
		return mScaleOffset;
	}

	
	constexpr void ITransformOffsetBase::SetOffset(const Vector3D& offset)
	{
		mOffset = offset;
	}
	
	constexpr void ITransformOffsetBase::SetOffset(const float offsetX, const float offsetY)
	{
		mOffset = Vector3D{ offsetX, offsetY };
	}
	
	constexpr void ITransformOffsetBase::AddOffset(const Vector3D& offset)
	{
		mOffset += offset;
	}
	
	constexpr void ITransformOffsetBase::AddOffset(const float offsetX, const float offsetY)
	{
		mOffset += Vector3D{ offsetX, offsetY };
	}
	
	constexpr void ITransformOffsetBase::SetScaleOffset(const Vector3D& offset)
	{
		mScaleOffset = offset;
	}
	
	constexpr void ITransformOffsetBase::AddScaleOffset(const Vector3D& offset)
	{
		mScaleOffset += offset;
	}
	
	inline void ITransformOffsetBase::MultiplyScaleOffset(const Vector3D& offset)
	{
		mScaleOffset[0] *= offset.X();
		mScaleOffset[1] *= offset.Y();
	}
	
	void ITransformOffsetBase::SetRotationOffset(const float offset)
	{
		mRotationOffset = std::fmodf(offset, constants::TWO_PI);
	}
	
	void ITransformOffsetBase::AddRotationOffset(const float offset)
	{
		mRotationOffset += offset;
		mRotationOffset = std::fmodf(mRotationOffset, constants::TWO_PI);
	}
}
