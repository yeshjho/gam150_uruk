/*
    File Name: IWidthHeightBase.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "IWidthHeightBase.h"

#include "Serializer.h"


namespace uruk
{
	void IWidthHeightBase::OnEngineSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mWidth);
		serialize(outputFileStream, mHeight);
	}

	void IWidthHeightBase::OnEngineLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mWidth);
		deserialize(inputFileStream, mHeight);
	}
}
