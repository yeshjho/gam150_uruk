/*
    File Name: IWidthHeightBase.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <fstream>



namespace uruk
{
	class IWidthHeightBase
	{
	public:
		constexpr IWidthHeightBase(float width, float height) noexcept;


		[[nodiscard]] constexpr float GetWidth() const;
		[[nodiscard]] constexpr float GetHeight() const;


		constexpr void SetWidth(float width);
		constexpr void SetHeight(float height);


		void OnEngineSave(std::ofstream& outputFileStream) const;
		void OnEngineLoad(std::ifstream& inputFileStream);



	protected:
		float mWidth;
		float mHeight;
	};
}

#include "IWidthHeightBase.inl"
