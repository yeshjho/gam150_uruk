/*
    File Name: IWidthHeightBase.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	constexpr IWidthHeightBase::IWidthHeightBase(const float width, const float height) noexcept
		: mWidth(width), mHeight(height)
	{}


	constexpr float IWidthHeightBase::GetWidth() const
	{
		return mWidth;
	}

	constexpr float IWidthHeightBase::GetHeight() const
	{
		return mHeight;
	}

	
	constexpr void IWidthHeightBase::SetWidth(const float width)
	{
		mWidth = width;
	}
	
	constexpr void IWidthHeightBase::SetHeight(const float height)
	{
		mHeight = height;
	}
}
