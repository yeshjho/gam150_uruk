/*
    File Name: InputComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: Joonho Hwang
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "InputComponent.h"

using namespace std;

vector<bool> uruk::InputComponent::keyDown = vector<bool>(static_cast<size_t>(doodle::KeyboardButtons::Count));
vector<bool> uruk::InputComponent::mouseKeyDown = vector<bool>(static_cast<size_t>(doodle::MouseButtons::Count));

uruk::InputComponent::~InputComponent()
{
	for (auto& it : keyboardPressedKeyMap)
	{
		for (DelegateFunctionBase* p_function : p_functions)
		{
			it.second.RemoveFunction(p_function);
		}
	}

	for (auto& it : keyboardReleasedKeyMap)
	{
		for (DelegateFunctionBase* p_function : p_functions)
		{
			it.second.RemoveFunction(p_function);
		}
	}

	for(auto& it : keyboardDownKeyMap)
	{
		for(DelegateFunctionBase* p_function : p_functions)
		{
			it.second.RemoveFunction(p_function);
		}
	}
	
	for (DelegateFunctionBase* p_function : p_functions)
	{
		keyboardPressedDelegate.RemoveFunction(p_function);
	}

	for (DelegateFunctionBase* p_function : p_functions)
	{
		keyboardReleasedDelegate.RemoveFunction(p_function);
	}

	for (auto& it : mousePressedKeyMap)
	{
		for (DelegateFunctionBase* p_function : p_functions)
		{
			it.second.RemoveFunction(p_function);
		}
	}

	for (auto& it : mouseReleasedKeyMap)
	{
		for (DelegateFunctionBase* p_function : p_functions)
		{
			it.second.RemoveFunction(p_function);
		}
	}

	for (DelegateFunctionBase* p_function : p_functions)
	{
		mousePressedDelegate.RemoveFunction(p_function);
	}

	for (DelegateFunctionBase* p_function : p_functions)
	{
		mouseReleasedDelegate.RemoveFunction(p_function);
	}

	for (DelegateFunctionBase* p_function : p_functions)
	{
		mouseMovedDelegate.RemoveFunction(p_function);
	}

	for (DelegateFunctionBase* p_function : p_functions)
	{
		mouseWheeledDelegate.RemoveFunction(p_function);
	}
}

void uruk::InputComponent::OnUpdate()
{
	for (int i = 0; i < keyDown.size(); i++)
	{
		if (keyDown[i] == true)
		{
			keyboardDownKeyMap[static_cast<doodle::KeyboardButtons>(i)].Execute();
		}
	}
}

void uruk::InputComponent::OnKeyboardPressed(doodle::KeyboardButtons key)
{
	keyDown[static_cast<size_t>(key)] = true;
	
	if (keyboardPressedKeyMap.count(key) != 0)
		keyboardPressedKeyMap[key].Execute();

	keyboardPressedDelegate.Execute();
}

void uruk::InputComponent::OnKeyboardReleased(doodle::KeyboardButtons key)
{
	keyDown[static_cast<size_t>(key)] = false;
	
	if (keyboardReleasedKeyMap.count(key) != 0)
		keyboardReleasedKeyMap[key].Execute();

	keyboardReleasedDelegate.Execute();
}

void uruk::InputComponent::OnMousePressed(doodle::MouseButtons key)
{
	mouseKeyDown[static_cast<size_t>(key)] = true;
	
	if (mousePressedKeyMap.count(key) != 0)
		mousePressedKeyMap[key].Execute();

	mousePressedDelegate.Execute();
}

void uruk::InputComponent::OnMouseReleased(doodle::MouseButtons key)
{
	mouseKeyDown[static_cast<size_t>(key)] = false;
	
	if (mouseReleasedKeyMap.count(key) != 0)
		mouseReleasedKeyMap[key].Execute();

	mouseReleasedDelegate.Execute();
}

void uruk::InputComponent::OnMouseMoved(int, int)
{
	mouseMovedDelegate.Execute();
}

void uruk::InputComponent::OnMouseWheeled(int dWheel)
{
	wheelAmount = dWheel;
	mouseWheeledDelegate.Execute();
}

bool uruk::InputComponent::IsMouseDown(doodle::MouseButtons key)
{
	return mouseKeyDown[static_cast<size_t>(key)];
}
