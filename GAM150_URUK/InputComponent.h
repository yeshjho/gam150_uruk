/*
    File Name: InputComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: Joonho Hwang
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <unordered_map>
#include "doodle/input.hpp"
#include "Component.h"
#include "Delegate.h"

namespace uruk
{
	class InputComponent final : public Component<InputComponent, EUpdateType::BY_MILLISECOND, update_priorities::HIGHER_PRIORITY, 0>
	{
	public:
		~InputComponent() override;

		void OnUpdate()override;
		
		template <typename Function, typename ...Types>
		void BindFunctionToKeyboardPressKey(doodle::KeyboardButtons key, Function func, Types ... args);

		template <typename Function, typename ...Types>
		void BindFunctionToKeyboardDownKey(doodle::KeyboardButtons key, Function func, Types ...args);
		
		template <typename Function, typename ...Types>
		void BindFunctionToKeyboardReleaseKey(doodle::KeyboardButtons key, Function func, Types ... args);

		template <typename Function, typename ...Types>
		void BindFunctionToKeyboardPress(Function func, Types ... args);

		template <typename Function, typename ...Types>
		void BindFunctionToKeyboardRelease(Function func, Types ... args);

		template <typename Function, typename ...Types>
		void BindFunctionToMousePressKey(doodle::MouseButtons key, Function func, Types ... args);

		template <typename Function, typename ...Types>
		void BindFunctionToMouseReleaseKey(doodle::MouseButtons key, Function func, Types ... args);

		template <typename Function, typename ...Types>
		void BindFunctionToMousePress(Function func, Types ... args);

		template <typename Function, typename ...Types>
		void BindFunctionToMouseRelease(Function func, Types ... args);

		template <typename Function, typename ...Types>
		void BindFunctionToMouseMoved(Function func, Types ... args);

		template <typename Function, typename ...Types>
		void BindFunctionToMouseWheeled(Function func, Types ... args);

	protected:
	private:
		std::vector<DelegateFunctionBase*> p_functions;
	public:
		static void OnKeyboardPressed(doodle::KeyboardButtons key);
		static void OnKeyboardReleased(doodle::KeyboardButtons key);
		static void OnMousePressed(doodle::MouseButtons key);
		static void OnMouseReleased(doodle::MouseButtons key);
		static void OnMouseMoved(int, int);
		static void OnMouseWheeled(int dWheel);
		static bool IsKeyDown(doodle::KeyboardButtons key);
		static bool IsMouseDown(doodle::MouseButtons key);
		
		template <typename Function, typename ...Types>
		static void StaticBindFunctionToKeyboardPressKey(doodle::KeyboardButtons key, Function func, Types ... args);

		template <typename Function, typename ...Types>
		static void StaticBindFunctionToKeyboardDownKey(doodle::KeyboardButtons key, Function func, Types ... args);
		
		template <typename Function, typename ...Types>
		static void StaticBindFunctionToKeyboardReleaseKey(doodle::KeyboardButtons key, Function func,Types ... args);

		template <typename Function, typename ...Types>
		static void StaticBindFunctionToKeyboardPress(Function func, Types ... args);

		template <typename Function, typename ...Types>
		static void StaticBindFunctionToKeyboardRelease(Function func, Types ... args);

		template <typename Function, typename ...Types>
		static void StaticBindFunctionToMousePressKey(doodle::MouseButtons key, Function func, Types ... args);

		template <typename Function, typename ...Types>
		static void StaticBindFunctionToMouseReleaseKey(doodle::MouseButtons key, Function func, Types ... args);

		template <typename Function, typename ...Types>
		static void StaticBindFunctionToMousePress(Function func, Types ... args);

		template <typename Function, typename ...Types>
		static void StaticBindFunctionToMouseRelease(Function func, Types ... args);

		template <typename Function, typename ...Types>
		static void StaticBindFunctionToMouseMoved(Function func, Types ... args);

		template <typename Function, typename ...Types>
		static void StaticBindFunctionToMouseWheeled(Function func, Types ... args);

		constexpr static int GetWheelAmount() noexcept;

	private:
		inline static std::unordered_map<doodle::KeyboardButtons, Delegate> keyboardPressedKeyMap;
		inline static std::unordered_map<doodle::KeyboardButtons, Delegate> keyboardReleasedKeyMap;
		inline static std::unordered_map<doodle::KeyboardButtons, Delegate> keyboardDownKeyMap;
		inline static std::unordered_map<doodle::MouseButtons, Delegate> mousePressedKeyMap;
		inline static std::unordered_map<doodle::MouseButtons, Delegate> mouseReleasedKeyMap;
		inline static Delegate keyboardPressedDelegate;
		inline static Delegate keyboardReleasedDelegate;
		inline static Delegate mousePressedDelegate;
		inline static Delegate mouseReleasedDelegate;
		inline static Delegate mouseMovedDelegate;
		inline static Delegate mouseWheeledDelegate;
		static std::vector<bool> keyDown;
		static std::vector<bool> mouseKeyDown;
		inline static int wheelAmount;
	};

	inline bool InputComponent::IsKeyDown(doodle::KeyboardButtons key)
	{
		return keyDown[static_cast<size_t>(key)];
	}


	template <typename Function, typename ...Types>
	void InputComponent::BindFunctionToKeyboardPressKey(doodle::KeyboardButtons key, Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		keyboardPressedKeyMap[key].AddFunction(p_function);
		p_functions.push_back(p_function);
	}

	template <typename Function, typename ... Types>
	void InputComponent::BindFunctionToKeyboardDownKey(doodle::KeyboardButtons key, Function func, Types... args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		keyboardDownKeyMap[key].AddFunction(p_function);
		p_functions.push_back(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::BindFunctionToKeyboardReleaseKey(doodle::KeyboardButtons key, Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		keyboardReleasedKeyMap[key].AddFunction(p_function);
		p_functions.push_back(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::BindFunctionToKeyboardPress(Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		keyboardPressedDelegate.AddFunction(p_function);
		p_functions.push_back(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::BindFunctionToKeyboardRelease(Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		keyboardReleasedDelegate.AddFunction(p_function);
		p_functions.push_back(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::BindFunctionToMousePressKey(doodle::MouseButtons key, Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		mousePressedKeyMap[key].AddFunction(p_function);
		p_functions.push_back(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::BindFunctionToMouseReleaseKey(doodle::MouseButtons key, Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		mouseReleasedKeyMap[key].AddFunction(p_function);
		p_functions.push_back(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::BindFunctionToMousePress(Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		mousePressedDelegate.AddFunction(p_function);
		p_functions.push_back(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::BindFunctionToMouseRelease(Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		mouseReleasedDelegate.AddFunction(p_function);
		p_functions.push_back(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::BindFunctionToMouseMoved(Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		mouseMovedDelegate.AddFunction(p_function);
		p_functions.push_back(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::BindFunctionToMouseWheeled(Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		mouseWheeledDelegate.AddFunction(p_function);
		p_functions.push_back(p_function);
	}


	template <typename Function, typename ...Types>
	void InputComponent::StaticBindFunctionToKeyboardPressKey(doodle::KeyboardButtons key, Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		keyboardPressedKeyMap[key].AddFunction(p_function);
	}

	template <typename Function, typename ... Types>
	void InputComponent::StaticBindFunctionToKeyboardDownKey(doodle::KeyboardButtons key, Function func, Types... args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		keyboardDownKeyMap[key].AddFunction(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::StaticBindFunctionToKeyboardReleaseKey(doodle::KeyboardButtons key, Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		keyboardReleasedKeyMap[key].AddFunction(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::StaticBindFunctionToKeyboardPress(Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		keyboardPressedDelegate.AddFunction(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::StaticBindFunctionToKeyboardRelease(Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		keyboardReleasedDelegate.AddFunction(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::StaticBindFunctionToMousePressKey(doodle::MouseButtons key, Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		mousePressedKeyMap[key].AddFunction(p_function);
	}


	template <typename Function, typename ...Types>
	void InputComponent::StaticBindFunctionToMouseReleaseKey(doodle::MouseButtons key, Function func,
	                                                               Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		mouseReleasedKeyMap[key].AddFunction(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::StaticBindFunctionToMousePress(Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		mousePressedDelegate.AddFunction(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::StaticBindFunctionToMouseRelease(Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		mouseReleasedDelegate.AddFunction(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::StaticBindFunctionToMouseMoved(Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		mouseMovedDelegate.AddFunction(p_function);
	}

	template <typename Function, typename ...Types>
	void InputComponent::StaticBindFunctionToMouseWheeled(Function func, Types ...args)
	{
		DelegateFunctionBase* p_function = new DelegateFunction(func, args...);
		mouseWheeledDelegate.AddFunction(p_function);
	}

	constexpr int InputComponent::GetWheelAmount() noexcept
	{
		return wheelAmount;
	}
}
