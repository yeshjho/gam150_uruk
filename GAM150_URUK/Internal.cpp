/*
    File Name: Internal.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "Internal.h"



namespace uruk
{
	ID IDManager::ClaimID()
	{
		std::random_device rd;
		auto seedData = std::array<int, std::mt19937::state_size>{};
		std::generate(std::begin(seedData), std::end(seedData), std::ref(rd));
		std::seed_seq seq(std::begin(seedData), std::end(seedData));
		std::mt19937 generator(seq);
		uuids::uuid_random_generator uuidGenerator{ generator };

		return uuidGenerator();
	}


	TypeID IDManager::getTypeID(const char* typeName)
	{
		static uuids::uuid_name_generator uuidGenerator{ uuids::uuid_namespace_oid };

		return uuidGenerator(typeName);
	}
}
