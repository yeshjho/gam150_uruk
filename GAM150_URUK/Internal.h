/*
    File Name: Internal.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <typeinfo>

#include <uuid/uuid.h>



namespace uruk
{
	using ID = uuids::uuid;
	using TypeID = uuids::uuid;



	class IDManager
	{
	public:
		template<typename T>
		[[nodiscard]] static TypeID GetTypeID();

		template<typename T>
		[[nodiscard]] static const std::string& GetTypeName();
		[[nodiscard]] inline static const std::string& GetTypeName(TypeID typeID);

		[[nodiscard]] static ID ClaimID();

		[[nodiscard]] static constexpr bool IsIDValid(const ID& id) noexcept;

	private:
		[[nodiscard]] static TypeID getTypeID(const char* typeName);
		


	private:
		inline static std::unordered_map<TypeID, std::string> typeNames;
	};
}

#include "Internal.inl"
