/*
    File Name: Internal.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	template <typename T>
	TypeID IDManager::GetTypeID()
	{
		static const char* typeName = []()
		{
			const char* toReturn = typeid(T).name();
			typeNames.insert({ getTypeID(toReturn), std::string{ toReturn } });

			return toReturn;
		}();
		
		return getTypeID(typeName);
	}

	template<typename T>
	const std::string& IDManager::GetTypeName()
	{
		return GetTypeName(GetTypeID<T>());
	}

	const std::string& IDManager::GetTypeName(const TypeID typeID)
	{
		return typeNames.at(typeID);
	}
	
	constexpr bool IDManager::IsIDValid(const uuids::uuid& id) noexcept
	{
		return !id.is_nil();
	}
}
