/*
    File Name: Joint.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "Joint.h"

uruk::Joint::Joint(int parentIndex, const Vector3D& pos, const char* joint_name)
	:parentIndex(parentIndex),position(pos),name(joint_name)
{
}
