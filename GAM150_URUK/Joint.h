/*
    File Name: Joint.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Vector3D.h"
#include "Serializer.h"

namespace uruk
{
	struct [[nodiscard]] Joint
	{
		Joint(int parentIndex, const Vector3D& pos, const char* joint_name);
		Joint(){}

		int parentIndex = -1; //-1 means it is the origin joint in the skeleton hierarchy
		Vector3D position = { 0.f,0.f,0.f };//relative to parent. For the root joint, object transform position is the parent.
		std::string name{ "" };
	};

	inline void LoadJointFromEditorFile(std::ifstream& istream, Joint& joint)
	{
		istream >> joint.parentIndex;
		char c;//to handle , between position values
		istream >> joint.position[0];
		istream >> c;
		istream >> joint.position[1];
		istream >> c;
		istream >> joint.position[2];
		istream >> joint.name;
	}
	
	inline void LoadJoint(std::ifstream& istream, Joint& joint)
	{
		deserialize(istream, joint.parentIndex);
		deserialize(istream, joint.position);
		deserialize(istream, joint.name);
	}
	
	inline void SaveJoint(std::ofstream& ostream, Joint& joint)
	{
		serialize(ostream, joint.parentIndex);
		serialize(ostream, joint.position);
		serialize(ostream, joint.name);
	}
}