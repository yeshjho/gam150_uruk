/*
    File Name: Level.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "Level.h"

#include <filesystem>

#include "Game.h"
#include "ITransformOffsetBase.h"
#include "IWidthHeightBase.h"
#include "Serializer.h"
#include "TransformComponent.h"
#include "UserDirectoryGetter.h"



namespace uruk
{
	Level::~Level()
	{
		for (const auto& [typeID, _] : mObjects.GetContainerBoundaries())
		{
			RemoveAllObjects(typeID);
		}
	}


	IComponent* Level::GetComponent(const TypeID& typeID, const ID& id) const noexcept
	{
		return mComponents.Get<IComponent>(typeID, id);
	}

	ObjectIterator<IComponent, false> Level::GetAllComponents(const TypeID& typeID) const noexcept
	{
		return mComponents.GetAll<IComponent>(typeID);
	}

	ObjectIterator<IComponent, true> Level::GetAllActiveComponents(const TypeID& typeID) const noexcept
	{
		return mComponents.GetAll<IComponent, true>(typeID);
	}

	void Level::RemoveComponent(const TypeID& typeID, const ID& id)
	{
		logger.LogVerbose(ELogCategory::COMPONENT_DESTRUCTION, "Destructing\t" + IDManager::GetTypeName(typeID) + "\t\t" + to_string(id) + "\ton level " + to_string(mID));
		IObject* const owner = GetComponent(typeID, id)->GetOwner();
		logger.LogVerbose(ELogCategory::COMPONENT_DESTRUCTION, "Owner is " + to_string(owner->GetID()));

		IComponent* const component = GetComponent(typeID, id);
		if (component->GetAttributes() & EComponentAttribute::DRAWABLE)
		{
			logger.LogVerbose(ELogCategory::COMPONENT_DESTRUCTION, "Component was Drawable. Erasing from mToDrawComponentList");
			mComponentsToDraw.erase(std::find(mComponentsToDraw.begin(), mComponentsToDraw.end(), std::make_pair(typeID, id)));
		}

		mComponents.Remove<IComponent>(typeID, id);

		owner->mComponents[typeID].remove(id);
		logger.LogLog(ELogCategory::COMPONENT_DESTRUCTION, "Component Destruction\t" + IDManager::GetTypeName(typeID) + "\t\t" + to_string(id) + "\ton level " + to_string(mID));
	}

	void Level::RemoveAllComponents(const TypeID& typeID)
	{
		ObjectIterator<IComponent, false>&& components = GetAllComponents(typeID);
		for (auto it = components.rbegin(); it != components.rend(); --it)
		{
			RemoveComponent(typeID, it->GetID());
		}
	}

	void Level::RemoveAllInactiveComponents(const TypeID& typeID)
	{
		ObjectIterator<IComponent, false>&& components = GetAllComponents(typeID);
		for (auto it = components.rbegin(); it != components.rend(); --it)
		{
			if (!it->IsActive())
			{
				RemoveComponent(typeID, it->GetID());
			}
		}
	}


	IObject* Level::GetObject(const TypeID& typeID, const ID& id) const noexcept
	{
		return mObjects.Get<IObject>(typeID, id);
	}

	ObjectIterator<IObject, false> Level::GetAllObjects(const TypeID& typeID) const noexcept
	{
		return mObjects.GetAll<IObject>(typeID);
	}

	ObjectIterator<IObject, true> Level::GetAllActiveObjects(const TypeID& typeID) const noexcept
	{
		return mObjects.GetAll<IObject, true>(typeID);
	}


	void Level::RemoveObject(const TypeID& typeID, const ID& id)
	{
		logger.LogVerbose(ELogCategory::OBJECT_DESTRUCTION, "Destructing\t" + IDManager::GetTypeName(typeID) + "\t\t" + to_string(id) + "\ton level " + to_string(mID));
		// Intentional copy here since removing a component modifies its owner's mComponents.
		auto components = GetObject(typeID, id)->mComponents;
		for (const auto& [componentTypeID, componentIDs] : components)
		{
			for (auto it = componentIDs.rbegin(); it != componentIDs.rend(); ++it)
			{
				RemoveComponent(componentTypeID, *it);
			}
		}

		mObjects.Remove<IObject>(typeID, id);
		logger.LogLog(ELogCategory::OBJECT_DESTRUCTION, "Object Destruction\t" + IDManager::GetTypeName(typeID) + "\t\t" + to_string(id) + "\ton level " + to_string(mID));
	}

	void Level::RemoveAllObjects(const TypeID& typeID)
	{
		ObjectIterator<IObject, false>&& objects = GetAllObjects(typeID);
		for (auto it = objects.rbegin(); it != objects.rend(); --it)
		{
			RemoveObject(typeID, it->GetID());
		}
	}

	void Level::RemoveAllInactiveObjects(const TypeID& typeID)
	{
		ObjectIterator<IObject, false>&& objects = GetAllObjects(typeID);
		for (auto it = objects.rbegin(); it != objects.rend(); --it)
		{
			if (!it->IsActive())
			{
				RemoveObject(typeID, it->GetID());
			}
		}
	}


	void Level::TransferObject(const TypeID& typeID, const ID& id, const ID& levelID)
	{
		IObject* objectToTransfer = GetObject(typeID, id);
		logger.LogVerbose(ELogCategory::OBJECT_ACTION, "Transferring " + IDManager::GetTypeName(typeID) + "\t" + to_string(id) + " from " + to_string(mID) + " to " + to_string(levelID));
		
		{
			std::ofstream tempFile{ get_user_directory() += "\\temp.urktmp", std::ios_base::out | std::ios_base::trunc | std::ios_base::binary };

			/// Save Object Data
			saveObject(*objectToTransfer, tempFile);

			/// Save Component Data, even if it's NO_SAVE
			const auto& objectComponents = objectToTransfer->mComponents;
			serialize(tempFile, std::accumulate(begin(objectComponents), end(objectComponents), 0LL,
				[](size_t sum, const auto& e)
				{
					return sum + e.second.size();
				}
			));
			for (const auto& [componentTypeID, ids] : objectComponents)
			{
				for (const ID& componentID : ids)
				{
					IComponent* component = GetComponent(componentTypeID, componentID);
					
					serialize(tempFile, component->GetTypeID());
					serialize(tempFile, component->GetID());
					saveComponent(*component, tempFile);
				}
			}
		}

		RemoveObject(typeID, id);

		{
			std::ifstream tempFile{ get_user_directory() += "\\temp.urktmp", std::ios_base::in | std::ios_base::binary };
			Level* level = Game::GetLevel(levelID);
			level->mComponentAddMode = EComponentAddMode::TRANSFER;

			size_t componentCount;
			TypeID componentTypeID;
			ID componentID;

			/// Load Object Data
			level->loadObject(typeID, id, tempFile);

			/// Load Component Data
			deserialize(tempFile, componentCount);
			for (size_t i = 0; i < componentCount; ++i)
			{
				deserialize(tempFile, componentTypeID);
				deserialize(tempFile, componentID);

				logger.LogVerbose(ELogCategory::OBJECT_ACTION, "Transferring " + IDManager::GetTypeName(componentTypeID) + "\t" + to_string(componentID));
				level->loadComponent(componentTypeID, componentID, tempFile);
			}

			level->mComponentAddMode = EComponentAddMode::NORMAL;
		}

		std::filesystem::remove(get_user_directory() += "\\temp.urktmp");
		logger.LogLog(ELogCategory::OBJECT_ACTION, "Transferred " + IDManager::GetTypeName(typeID) + "\t" + to_string(id) + " from " + to_string(mID) + " to " + to_string(levelID));
	}

	void Level::TransferObject(const TypeID& typeID, const ID& id, const ID& levelID, const Vector3D& newLocation)
	{
		TransferObject(typeID, id, levelID);
		Game::GetLevel(levelID)->GetObject(typeID, id)->GetComponent<TransformComponent>()->SetPosition(newLocation);
	}


	void Level::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mID);
		serialize(outputFileStream, mComponents.GetContainerSize());
		serialize(outputFileStream, mComponents.GetMaxCountPerType());
		serialize(outputFileStream, mObjects.GetContainerSize());
		serialize(outputFileStream, mObjects.GetMaxCountPerType());
		
		serialize(outputFileStream, mShouldUpdate);
		serialize(outputFileStream, mComponentsToDraw);

		serialize(outputFileStream, mObjects.GetObjectCount<IObject>());
		logger.LogVerbose(ELogCategory::LEVEL_FILE, "Object count is: " + std::to_string(mObjects.GetObjectCount<IObject>()));
		mObjects.ForEach<IObject>([&](const IObject& object)
			{
				logger.LogVerbose(ELogCategory::OBJECT_FILE, "Saving an Object of " + IDManager::GetTypeName(object.GetTypeID()));
				serialize(outputFileStream, object.GetTypeID());
				serialize(outputFileStream, object.GetID());
				saveObject(object, outputFileStream);
			});

		serialize(outputFileStream, mComponents.GetObjectCount<IComponent>());
		logger.LogVerbose(ELogCategory::LEVEL_FILE, "Component count is: " + std::to_string(mComponents.GetObjectCount<IComponent>()));
		mComponents.ForEach<IComponent>([&](const IComponent& component)
			{
				if (component.GetAttributes() & EComponentAttribute::NO_SAVE)
				{
					return;
				}

				logger.LogVerbose(ELogCategory::COMPONENT_FILE, "Saving a Component of " + IDManager::GetTypeName(component.GetTypeID()));
				serialize(outputFileStream, component.GetTypeID());
				serialize(outputFileStream, component.GetID());
				saveComponent(component, outputFileStream);
			});
	}

	void Level::OnLoad(std::ifstream& inputFileStream)
	{
		mByMillisecondComponentTypeIDs.clear();
		mByFrameComponentTypeIDs.clear();
		mComponentTypeIDsAddedToUpdateList.clear();
		mbCalledBeginPlay = false;

		deserialize(inputFileStream, mShouldUpdate);
		deserialize(inputFileStream, mComponentsToDraw);

		size_t count;
		TypeID typeID;
		ID id;
		
		deserialize(inputFileStream, count);
		logger.LogVerbose(ELogCategory::LEVEL_FILE, "Object count is: " + std::to_string(count));
		for (size_t i = 0; i < count; ++i)
		{
			logger.LogVerbose(ELogCategory::OBJECT_FILE, "Loading an Object");

			deserialize(inputFileStream, typeID);
			deserialize(inputFileStream, id);
			loadObject(typeID, id, inputFileStream);
		}

		mComponentAddMode = EComponentAddMode::LOAD;
		deserialize(inputFileStream, count);
		logger.LogVerbose(ELogCategory::LEVEL_FILE, "Component count is: " + std::to_string(count));
		for (size_t i = 0; i < count; ++i)
		{
			logger.LogVerbose(ELogCategory::COMPONENT_FILE, "Loading a Component");
			deserialize(inputFileStream, typeID);
			deserialize(inputFileStream, id);
			loadComponent(typeID, id, inputFileStream);
		}

		mComponentAddMode = EComponentAddMode::NORMAL;
	}


	void Level::OnBeginPlay()
	{
		if (mbCalledBeginPlay)
		{
			return;
		}

		logger.LogVerbose(ELogCategory::LEVEL_EVENT, "BeginPlay Level " + to_string(mID));
		
		mComponents.ForEach<IComponent>([](IComponent& component)
		{
			logger.LogVerbose(ELogCategory::COMPONENT_EVENT, "BeginPlay Component " + to_string(component.GetID()));
			component.OnBeginPlay();
		});
		mbCalledBeginPlay = true;
	}

	void Level::OnUpdate(const long long& deltaTimeInNanosecond)
	{
		if (!mShouldUpdate)
		{
			return;
		}

		
		static constexpr long long NANOSECONDS_PER_MILLISECOND = 1'000'000;

		mDeltaTimeInMillisecond = static_cast<float>(static_cast<double>(deltaTimeInNanosecond) / NANOSECONDS_PER_MILLISECOND);

		logger.LogVerbose(ELogCategory::LEVEL_EVENT, "Updating Level " + to_string(mID));
		
		for (auto it = mByMillisecondComponentTypeIDs.rbegin(); it != mByMillisecondComponentTypeIDs.rend(); ++it)
		{
			for (const TypeID& typeID : it->second)
			{
				for (IComponent& component : GetAllActiveComponents(typeID))
				{
					const int componentUpdateInterval = component.GetUpdateInterval();
					if (componentUpdateInterval <= 0)
					{
						logger.LogVerbose(ELogCategory::COMPONENT_EVENT, "Updating Component " + to_string(component.GetID()));
						component.OnUpdate();
						continue;
					}

					long long& componentCountSinceLastUpdate = component.mCountSinceLastUpdate;
					const long long updateIntervalInNanoseconds = componentUpdateInterval * NANOSECONDS_PER_MILLISECOND;
				
					componentCountSinceLastUpdate += deltaTimeInNanosecond;
					while (componentCountSinceLastUpdate >= updateIntervalInNanoseconds)
					{
						logger.LogVerbose(ELogCategory::COMPONENT_EVENT, "Updating Component " + to_string(component.GetID()));
						component.OnUpdate();
						componentCountSinceLastUpdate -= updateIntervalInNanoseconds;
					}
				}
			}
		}

		for (auto it = mByFrameComponentTypeIDs.rbegin(); it != mByFrameComponentTypeIDs.rend(); ++it)
		{
			for (const TypeID& typeID : it->second)
			{
				for (IComponent& component : GetAllActiveComponents(typeID))
				{
					const int componentUpdateInterval = component.GetUpdateInterval();
					if (componentUpdateInterval <= 1 || ++component.mCountSinceLastUpdate == componentUpdateInterval)
					{
						logger.LogVerbose(ELogCategory::COMPONENT_EVENT, "Updating Component " + to_string(component.GetID()));
						component.OnUpdate();
						component.mCountSinceLastUpdate = 0;
					}
				}
			}
		}
	}


	void Level::saveComponent(const IComponent& component, std::ofstream& outputFileStream) const
	{
		component.OnConstructorArgumentSave(outputFileStream);
		component.OnEngineSave(outputFileStream);
		if (const ITransformOffsetBase* transformOffsetBase = dynamic_cast<const ITransformOffsetBase*>(&component))
		{
			transformOffsetBase->OnEngineSave(outputFileStream);
		}
		if (const IWidthHeightBase* widthHeightBase = dynamic_cast<const IWidthHeightBase*>(&component))
		{
			widthHeightBase->OnEngineSave(outputFileStream);
		}
		if (const ZOrderBase* zOrderBase = dynamic_cast<const ZOrderBase*>(&component))
		{
			zOrderBase->OnEngineSave(outputFileStream);
		}
		component.OnSave(outputFileStream);

		serialize(outputFileStream, COMPONENT_SEPARATOR);
	}

	void Level::saveObject(const IObject& object, std::ofstream& outputFileStream) const
	{
		object.OnEngineSave(outputFileStream);

		serialize(outputFileStream, OBJECT_SEPARATOR);
	}


	void Level::loadComponent(const TypeID& typeID, const ID& id, std::ifstream& inputFileStream)
	{
		unsigned long long separatorChecker;

		IComponent* component = IComponent::GetOnLoadConstruct(typeID)(this, id, inputFileStream);
		component->OnEngineLoad(inputFileStream);
		if (ITransformOffsetBase* transformOffsetBase = dynamic_cast<ITransformOffsetBase*>(component))
		{
			transformOffsetBase->OnEngineLoad(inputFileStream);
		}
		if (IWidthHeightBase* widthHeightBase = dynamic_cast<IWidthHeightBase*>(component))
		{
			widthHeightBase->OnEngineLoad(inputFileStream);
		}
		if (ZOrderBase* zOrderBase = dynamic_cast<ZOrderBase*>(component))
		{
			zOrderBase->OnEngineLoad(inputFileStream);
		}
		component->OnLoad(inputFileStream);

		deserialize(inputFileStream, separatorChecker);
		if (separatorChecker != COMPONENT_SEPARATOR)
		{
			logger.LogFatal(ELogCategory::LEVEL_FILE, "File is corrupted.");
		}
	}

	void Level::loadObject(const TypeID& typeID, const ID& id, std::ifstream& inputFileStream)
	{
		unsigned long long separatorChecker;

		IObject* object = IObject::GetOnLoadConstruct(typeID)(this, id);
		object->OnEngineLoad(inputFileStream);

		deserialize(inputFileStream, separatorChecker);
		if (separatorChecker != OBJECT_SEPARATOR)
		{
			logger.LogFatal(ELogCategory::LEVEL_FILE, "File is corrupted.");
		}
	}
}
