/*
    File Name: Level.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <map>
#include <set>
#include <type_traits>
#include <vector>

#include "IComponent.h"
#include "IDBasedContiguousContainer.h"
#include "IObject.h"
#include "Logger.h"
#include "ZOrderBase.h"



namespace uruk
{
	enum class EComponentAddMode : char
	{
		NORMAL,
		LOAD,
		TRANSFER
	};


	
	class Level
	{
		friend class Game;
		
	public:
		inline Level(const ID& id, size_t memorySize, unsigned int maxCountPerType);
		inline Level(const ID& id, size_t componentMemorySize, unsigned int maxComponentCountPerType, size_t objectMemorySize, unsigned int maxObjectCountPerType);

		Level(const Level&) = delete;
		Level& operator=(const Level&) = delete;

		Level(Level&&) = default;
		Level& operator=(Level&&) = delete;

		~Level();


		template<typename ComponentType, typename ...Args>
		ID AddComponent(IObject* owner, Args&&... args);

		template<typename ComponentType, typename ...Args>
		[[nodiscard]] IComponent* LoadComponent(const ID& id, Args&&... args);

		template<typename ComponentType>
		[[nodiscard]] ComponentType* GetComponent(const ID& id) const noexcept;
		[[nodiscard]] IComponent* GetComponent(const TypeID& typeID, const ID& id) const noexcept;

		template<typename ComponentType>
		[[nodiscard]] ObjectIterator<ComponentType, false> GetAllComponents() const noexcept;
		[[nodiscard]] ObjectIterator<IComponent, false> GetAllComponents(const TypeID& typeID) const noexcept;

		template<typename ComponentType>
		[[nodiscard]] ObjectIterator<ComponentType, true> GetAllActiveComponents() const noexcept;
		[[nodiscard]] ObjectIterator<IComponent, true> GetAllActiveComponents(const TypeID& typeID) const noexcept;

		template<typename ComponentType>
		void RemoveComponent(const ID& id);
		void RemoveComponent(const TypeID& typeID, const ID& id);

		template<typename ComponentType>
		void RemoveAllComponents();
		void RemoveAllComponents(const TypeID& typeID);

		template<typename ComponentType>
		void RemoveAllInactiveComponents();
		void RemoveAllInactiveComponents(const TypeID& typeID);


		template<typename ObjectType, typename ...Args, typename = std::enable_if_t<std::is_default_constructible_v<ObjectType>>>
		ID AddObject(Args&&... args);

		template<typename ObjectType>
		[[nodiscard]] IObject* LoadObject(const ID& id);

		template<typename ObjectType>
		[[nodiscard]] ObjectType* GetObject(const ID& id) const noexcept;
		[[nodiscard]] IObject* GetObject(const TypeID& typeID, const ID& id) const noexcept;

		template<typename ObjectType>
		[[nodiscard]] ObjectIterator<ObjectType, false> GetAllObjects() const noexcept;
		[[nodiscard]] ObjectIterator<IObject, false> GetAllObjects(const TypeID& typeID) const noexcept;

		template<typename ObjectType>
		[[nodiscard]] ObjectIterator<ObjectType, true> GetAllActiveObjects() const noexcept;
		[[nodiscard]] ObjectIterator<IObject, true> GetAllActiveObjects(const TypeID& typeID) const noexcept;

		template<typename ObjectType>
		void RemoveObject(const ID& id);
		void RemoveObject(const TypeID& typeID, const ID& id);

		template<typename ObjectType>
		void RemoveAllObjects();
		void RemoveAllObjects(const TypeID& typeID);
		
		template<typename ObjectType>
		void RemoveAllInactiveObjects();
		void RemoveAllInactiveObjects(const TypeID& typeID);


		template<typename ObjectType>
		void TransferObject(const ID& id, const ID& levelID);
		void TransferObject(const TypeID& typeID, const ID& id, const ID& levelID);

		template<typename ObjectType>
		void TransferObject(const ID& id, const ID& levelID, const Vector3D& newLocation);
		void TransferObject(const TypeID& typeID, const ID& id, const ID& levelID, const Vector3D& newLocation);


		inline void SetPaused(bool isPaused) noexcept;
		inline void Pause() noexcept;
		inline void Resume() noexcept;
		inline void TogglePause() noexcept;
		[[nodiscard]] constexpr bool IsPaused() const noexcept;

		
		void OnSave(std::ofstream& outputFileStream) const;
		void OnLoad(std::ifstream& inputFileStream);


		[[nodiscard]] constexpr const ID& GetID() const noexcept;
		
		[[nodiscard]] constexpr const std::vector<std::pair<TypeID, ID>>& GetAllComponentsToDraw() const noexcept;
		
		[[nodiscard]] constexpr float GetDeltaTimeInMillisecond() const noexcept;


		void OnBeginPlay();
		void OnUpdate(const long long& deltaTimeInNanosecond);

	private:
		template<typename ComponentType>
		void addToUpdateList();

		template<typename ComponentType, typename ...Args>
		IComponent* addComponent(ID id, IObject* owner, Args&&... args);
		
		template<typename ObjectType, typename ...Args>
		IObject* addObject(ID id, Args&&... args);

		void saveComponent(const IComponent& component, std::ofstream& outputFileStream) const;
		void saveObject(const IObject& object, std::ofstream& outputFileStream) const;

		void loadComponent(const TypeID& typeID, const ID& id, std::ifstream& inputFileStream);
		void loadObject(const TypeID& typeID, const ID& id, std::ifstream& inputFileStream);


		
	public:
		static inline const wchar_t* SAVE_FILE_EXTENSION = L".urkl";
		
	private:
		IDBasedContiguousContainer mComponents;
		IDBasedContiguousContainer mObjects;

		std::map<int, std::vector<TypeID>> mByMillisecondComponentTypeIDs;
		std::map<int, std::vector<TypeID>> mByFrameComponentTypeIDs;

		std::vector<std::pair<TypeID, ID>> mComponentsToDraw;
		std::set<TypeID> mComponentTypeIDsAddedToUpdateList;

		float mDeltaTimeInMillisecond = 0.f;
		bool mbCalledBeginPlay = false;

		bool mShouldUpdate = true;

		EComponentAddMode mComponentAddMode = EComponentAddMode::NORMAL;

		ID mID;


		inline static constexpr unsigned long long OBJECT_SEPARATOR = 0xBA0BABC0FFEECAFE;
		inline static constexpr unsigned long long COMPONENT_SEPARATOR = 0xCACA05FEEDC0C0A5;
	};
}

#include "Level.inl"
