/*
    File Name: Level.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	inline Level::Level(const ID& id, const size_t memorySize, const unsigned int maxCountPerType)
		: Level(id, memorySize, maxCountPerType, memorySize, maxCountPerType)
	{}

	inline Level::Level(const ID& id, const size_t componentMemorySize, const unsigned int maxComponentCountPerType, const size_t objectMemorySize, const unsigned int maxObjectCountPerType)
		: mComponents{ componentMemorySize, maxComponentCountPerType }, mObjects{ objectMemorySize, maxObjectCountPerType }, mID(id)
	{}


	template<typename ComponentType, typename ...Args>
	ID Level::AddComponent(IObject* owner, Args&& ...args)
	{
		const IComponent* const component = addComponent<ComponentType>(ID{}, owner, std::forward<Args>(args)...);
		return component ? component->mID : ID{};
	}

	template<typename ComponentType, typename ...Args>
	IComponent* Level::LoadComponent(const ID& id, Args&& ...args)
	{
		return addComponent<ComponentType>(id, nullptr, std::forward<Args>(args)...);
	}

	template<typename ComponentType, typename ...Args>
	IComponent* Level::addComponent(ID id, IObject* owner, Args&& ...args)
	{
		static_assert(std::is_base_of_v<IComponent, ComponentType>);

		static const TypeID TYPE_ID = IDManager::GetTypeID<ComponentType>();
		static const std::string TYPE_NAME = IDManager::GetTypeName(TYPE_ID);

		logger.LogVerbose(ELogCategory::COMPONENT_CONSTRUCTION, "Constructing\t" + TYPE_NAME + "\ton level " + to_string(mID) + (mComponentAddMode == EComponentAddMode::NORMAL ? "\tattached to " + to_string(owner->GetID()) : ""));

		if (mComponentAddMode == EComponentAddMode::NORMAL && owner->mLevel != this)
		{
			logger.LogError(ELogCategory::COMPONENT_CONSTRUCTION, "The owner is not on this level.\tOwner's level: " + to_string(owner->mLevel->GetID()) + "\tThis level: " + to_string(mID) + "\t Returning invalid ID");
			return nullptr;
		}

		id = mComponents.Add<ComponentType>(id, std::forward<Args>(args)...);
		ComponentType* const component = mComponents.Get<ComponentType>(id);
		component->mID = id;
		component->mLevel = this;
		if (mComponentAddMode == EComponentAddMode::NORMAL)
		{
			component->mOwnerTypeID = owner->GetTypeID();
			component->mOwnerID = owner->mID;
			
			owner->mComponents[TYPE_ID].push_back(id);
			owner->mPriorComponentActiveStats[TYPE_ID][id] = true;

			component->OnConstructEnd();
			if (mbCalledBeginPlay)
			{
				component->OnBeginPlay();
			}

			if (!owner->IsActive())
			{
				component->mIsActive = false;
			}
		}

		addToUpdateList<ComponentType>();
		
		if (mComponentAddMode != EComponentAddMode::LOAD)
		{
			if constexpr (bool(ComponentType::ATTRIBUTES & EComponentAttribute::DRAWABLE))
			{
				static_assert(std::is_base_of_v<ZOrderBase, ComponentType>);

				logger.LogVerbose(ELogCategory::COMPONENT_CONSTRUCTION, "Component is Drawable. Appending to mToDrawComponentList and sorting");
				std::pair toInsert = { TYPE_ID, id };
				mComponentsToDraw.insert(std::upper_bound(mComponentsToDraw.begin(), mComponentsToDraw.end(), toInsert,
					[&](const std::pair<TypeID, ID>& a, const std::pair<TypeID, ID>& b)
					{
						return dynamic_cast<ZOrderBase*>(GetComponent(a.first, a.second))->GetZOrder() < dynamic_cast<ZOrderBase*>(GetComponent(b.first, b.second))->GetZOrder();
					})
				, toInsert);
			}
			if constexpr (std::is_base_of_v<ZOrderBase, ComponentType>)
			{
				logger.LogVerbose(ELogCategory::COMPONENT_CONSTRUCTION, "Component has ZOrder. Sorting components");
				mComponents.Sort<ComponentType>([](const ComponentType& a, const ComponentType& b) { return a.GetZOrder() < b.GetZOrder(); });
			}
		}

		logger.LogLog(ELogCategory::COMPONENT_CONSTRUCTION, "Component Construction\t" + TYPE_NAME + "\t\t" + to_string(id) + "\ton level " + to_string(mID) + (mComponentAddMode == EComponentAddMode::NORMAL ? "\tattached to " + to_string(owner->GetID()) : ""));
		return mComponents.Get<ComponentType>(id);
	}

	template<typename ComponentType>
	ComponentType* Level::GetComponent(const ID& id) const noexcept
	{
		static_assert(std::is_base_of_v<IComponent, ComponentType>);

		return mComponents.Get<ComponentType>(id);
	}

	template<typename ComponentType>
	ObjectIterator<ComponentType, false> Level::GetAllComponents() const noexcept
	{
		static_assert(std::is_base_of_v<IComponent, ComponentType>);

		return mComponents.GetAll<ComponentType>();
	}

	template<typename ComponentType>
	ObjectIterator<ComponentType, true> Level::GetAllActiveComponents() const noexcept
	{
		static_assert(std::is_base_of_v<IComponent, ComponentType>);
		
		return mComponents.GetAll<ComponentType, true>();
	}

	template<typename ComponentType>
	void Level::RemoveComponent(const ID& id)
	{
		static_assert(std::is_base_of_v<IComponent, ComponentType>);

		static const TypeID TYPE_ID = IDManager::GetTypeID<ComponentType>();

		logger.LogVerbose(ELogCategory::COMPONENT_DESTRUCTION, "Destructing\t" + IDManager::GetTypeName(TYPE_ID) + "\t\t" + to_string(id) + "\ton level " + to_string(mID));
		IObject* const owner = GetComponent<ComponentType>(id)->GetOwner();
		logger.LogVerbose(ELogCategory::COMPONENT_DESTRUCTION, "Owner is " + to_string(owner->GetID()));

		if constexpr (bool(ComponentType::ATTRIBUTES & EComponentAttribute::DRAWABLE))
		{
			logger.LogVerbose(ELogCategory::COMPONENT_DESTRUCTION, "Component was Drawable. Erasing from mToDrawComponentList");
			mComponentsToDraw.erase(std::find(mComponentsToDraw.begin(), mComponentsToDraw.end(), std::make_pair(TYPE_ID, id)));
		}

		mComponents.Remove<ComponentType>(id);

		owner->mComponents[TYPE_ID].remove(id);
		logger.LogLog(ELogCategory::COMPONENT_DESTRUCTION, "Component Destruction\t" + IDManager::GetTypeName(TYPE_ID) + "\t\t" + to_string(id) + "\ton level " + to_string(mID));
	}

	template<typename ComponentType>
	void Level::RemoveAllComponents()
	{
		static_assert(std::is_base_of_v<IComponent, ComponentType>);

		ObjectIterator<ComponentType, false>&& components = GetAllComponents<ComponentType>();
		for (auto it = components.rbegin(); it != components.rend(); --it)
		{
			RemoveComponent<ComponentType>(it->GetID());
		}
	}

	template <typename ComponentType>
	void Level::RemoveAllInactiveComponents()
	{
		static_assert(std::is_base_of_v<IComponent, ComponentType>);

		ObjectIterator<ComponentType, false>&& components = GetAllComponents<ComponentType>();
		for (auto it = components.rbegin(); it != components.rend(); --it)
		{
			if (!it->IsActive())
			{
				RemoveComponent<ComponentType>(it->GetID());
			}
		}
	}


	template<typename ObjectType, typename ...Args, typename>
	ID Level::AddObject(Args&& ...args)
	{
		return addObject<ObjectType>(ID{}, std::forward<Args>(args)...)->mID;
	}

	template<typename ObjectType>
	IObject* Level::LoadObject(const ID& id)
	{
		return addObject<ObjectType>(id);
	}

	template<typename ObjectType, typename ...Args>
	IObject* Level::addObject(ID id, Args&& ...args)
	{
		static_assert(std::is_base_of_v<IObject, ObjectType>);

		static const std::string TYPE_NAME = IDManager::GetTypeName<ObjectType>();

		const bool isFromLoading = !id.is_nil();

		logger.LogVerbose(ELogCategory::OBJECT_CONSTRUCTION, "Constructing\t" + TYPE_NAME + "\ton level " + to_string(mID));
		id = mObjects.Add<ObjectType>(id, std::forward<Args>(args)...);
		IObject* const object = mObjects.Get<ObjectType>(id);
		object->mID = id;
		object->mLevel = this;

		if (!isFromLoading)
		{
			object->OnConstructEnd();
		}

		logger.LogLog(ELogCategory::OBJECT_CONSTRUCTION, "Object Construction\t" + TYPE_NAME + "\t\t" + to_string(id) + "\ton level " + to_string(mID));
		return object;
	}

	template<typename ObjectType>
	ObjectType* Level::GetObject(const ID& id) const noexcept
	{
		static_assert(std::is_base_of_v<IObject, ObjectType>);

		return mObjects.Get<ObjectType>(id);
	}

	template<typename ObjectType>
	ObjectIterator<ObjectType, false> Level::GetAllObjects() const noexcept
	{
		static_assert(std::is_base_of_v<IObject, ObjectType>);

		return mObjects.GetAll<ObjectType>();
	}

	template<typename ObjectType>
	ObjectIterator<ObjectType, true> Level::GetAllActiveObjects() const noexcept
	{
		static_assert(std::is_base_of_v<IObject, ObjectType>);
		
		return mObjects.GetAll<ObjectType, true>();
	}

	template<typename ObjectType>
	void Level::RemoveObject(const ID& id)
	{
		static_assert(std::is_base_of_v<IObject, ObjectType>);

		static const TypeID TYPE_ID = IDManager::GetTypeID<ObjectType>();
		static const std::string TYPE_NAME = IDManager::GetTypeName(TYPE_ID);
		
		logger.LogVerbose(ELogCategory::OBJECT_DESTRUCTION, "Destructing\t" + TYPE_NAME + "\t\t" + to_string(id) + "\ton level " + to_string(mID));
		// Intentional copy here since removing a component modifies its owner's mComponents.
		auto components = GetObject<ObjectType>(id)->mComponents;
		for (auto [componentTypeID, componentIDs] : components)
		{
			for (auto it = componentIDs.rbegin(); it != componentIDs.rend(); ++it)
			{
				RemoveComponent(componentTypeID, *it);
			}
		}

		mObjects.Remove<ObjectType>(id);
		logger.LogLog(ELogCategory::OBJECT_DESTRUCTION, "Object Destruction\t" + TYPE_NAME + "\t\t" + to_string(id) + "\ton level " + to_string(mID));
	}

	template<typename ObjectType>
	void Level::RemoveAllObjects()
	{
		static_assert(std::is_base_of_v<IObject, ObjectType>);
		
		ObjectIterator<ObjectType, false>&& objects = GetAllObjects<ObjectType>();
		for (auto it = objects.rbegin(); it != objects.rend(); --it)
		{
			RemoveObject<ObjectType>(it->GetID());
		}
	}

	template <typename ObjectType>
	void Level::RemoveAllInactiveObjects()
	{
		static_assert(std::is_base_of_v<IObject, ObjectType>);

		ObjectIterator<ObjectType, false>&& objects = GetAllObjects<ObjectType>();
		for (auto it = objects.rbegin(); it != objects.rend(); --it)
		{
			if (!it->IsActive())
			{
				RemoveObject<ObjectType>(it->GetID());
			}
		}
	}

	template<typename ObjectType>
	void Level::TransferObject(const ID& id, const ID& levelID)
	{
		static_assert(std::is_base_of_v<IObject, ObjectType>);

		static const TypeID TYPE_ID = IDManager::GetTypeID<ObjectType>();

		TransferObject(TYPE_ID, id, levelID);
	}

	template<typename ObjectType>
	void Level::TransferObject(const ID& id, const ID& levelID, const Vector3D& newLocation)
	{
		static_assert(std::is_base_of_v<IObject, ObjectType>);

		static const TypeID TYPE_ID = IDManager::GetTypeID<ObjectType>();

		TransferObject(TYPE_ID, id, levelID, newLocation);
	}
	

	void Level::SetPaused(const bool isPaused) noexcept
	{
		if (!isPaused == mShouldUpdate)
		{
			return;
		}
		
		mShouldUpdate = !isPaused;
		
		logger.LogDisplay(ELogCategory::LEVEL_EVENT, (mShouldUpdate ? "Resuming" : "Pausing") + std::string(" Level ") + to_string(mID));
	}

	void Level::TogglePause() noexcept
	{
		SetPaused(!IsPaused());
	}

	inline void Level::Pause() noexcept
	{
		SetPaused(true);
	}

	inline void Level::Resume() noexcept
	{
		SetPaused(false);
	}

	constexpr bool Level::IsPaused() const noexcept
	{
		return !mShouldUpdate;
	}


	constexpr const ID& Level::GetID() const noexcept
	{
		return mID;
	}

	constexpr const std::vector<std::pair<TypeID, ID>>& Level::GetAllComponentsToDraw() const noexcept
	{
		return mComponentsToDraw;
	}

	constexpr float Level::GetDeltaTimeInMillisecond() const noexcept
	{
		return mDeltaTimeInMillisecond;
	}

	

	template<typename ComponentType>
	void Level::addToUpdateList()
	{
		static const TypeID TYPE_ID = IDManager::GetTypeID<ComponentType>();
		if (!mComponentTypeIDsAddedToUpdateList.insert(TYPE_ID).second)
		{
			return;
		}

		constexpr int updatePriority = ComponentType::UPDATE_PRIORITY;

		switch (ComponentType::UPDATE_TYPE)
		{
			case EUpdateType::BY_MILLISECOND:
				mByMillisecondComponentTypeIDs[updatePriority].push_back(TYPE_ID);
				break;

			case EUpdateType::BY_FRAME:
				mByFrameComponentTypeIDs[updatePriority].push_back(TYPE_ID);
				break;

			default:
				break;
		}
	}
}
