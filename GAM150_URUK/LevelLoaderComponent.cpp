/*
    File Name: LevelLoaderComponent.cpp
    Project Name: U.R.U.K
	Author(s):
		-Seunggeon Kim
		Contribution(s):
			SetCurrentLevel
		-Joonho Hwang
		Contribution(s):
			all the others
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "LevelLoaderComponent.h"

#include <utility>
#include <doodle/random.hpp>


#include "AnimationComponent.h"
#include "AudioPlayerComponent.h"
#include "CameraObject.h"
#include "DoorComponent.h"
#include "DoorObject.h"
#include "EnemyBehaviorComponent.h"
#include "EnemyObject.h"
#include "FlowFieldPathfinderComponent.h"
#include "Game.h"
#include "MinimapComponent.h"
#include "MovementComponent.h"
#include "PathfinderObject.h"
#include "PlayerBehaviorComponent.h"
#include "PlayerObject.h"
#include "RemotePoshigiComponent.h"
#include "EnemyAnimationStateMachine.h"
#include "Serializer.h"
#include "SpawnPointObject.h"
#include "TransformComponent.h"



namespace uruk
{
	LevelLoaderComponent::LevelLoaderComponent(std::string levelToLoad, const int spawnPointIndex, std::string minimapLevelToLoad)
		: mLevelToLoad(std::move(levelToLoad)), mMinimapLevelToLoad(std::move(minimapLevelToLoad)), mSpawnPointIndex(spawnPointIndex)
	{}


	void LevelLoaderComponent::Load() const
	{
		const ID levelIDToLoad = levelIDsByName.at(mLevelToLoad);

		Level* const currentLevel = Game::GetCurrentLevel();
		Level* const levelToLoad = Game::GetLevel(levelIDToLoad);

		if (MovementComponent* movement = currentLevel->GetAllActiveComponents<MovementComponent>().begin(); movement)
		{
			movement->SetVelocity({ 0, 0 });
		}


		if (mSpawnPointIndex != -1 && mSpawnPointIndex != 0)
		{
			PlayerObject* player = currentLevel->GetAllActiveObjects<PlayerObject>().begin();

			if (player != nullptr)
			{

				if (!player->GetComponent<RemotePoshigiComponent>()->IsFirstLoad())
				{

					player->GetComponent<RemotePoshigiComponent>()->OnRoomExit(Game::GetCurrentLevelID());
					player->GetComponent<RemotePoshigiComponent>()->OnRoomEnter(levelIDToLoad);
				}
				else
				{
					player->GetComponent<RemotePoshigiComponent>()->SetFirstLoad(false);
				}

			}
			else
			{
				player = levelToLoad->GetAllActiveObjects<PlayerObject>().begin();
				player->GetComponent<RemotePoshigiComponent>()->SetFirstLoad(false);
			}
		}


		Game::SetCurrentLevel(levelIDToLoad,
			[&, levelToLoad, currentLevel, levelIDToLoad]()
			{
				if (mSpawnPointIndex != -1)
				{
					for (SpawnPointObject& spawnPoint : levelToLoad->GetAllActiveObjects<SpawnPointObject>())
					{
						if (spawnPoint.GetIndex() == mSpawnPointIndex)
						{
							const Vector3D newLocation = spawnPoint.GetComponent<TransformComponent>()->GetPosition();
							
							//Enemy first
							if (mSpawnPointIndex != 0)
							{
								auto currentEnemies = currentLevel->GetAllActiveObjects<EnemyObject>();
								for (auto enemy : currentEnemies)
								{
									enemy.GetComponent<AnimationComponent>()->SetState(Anim_Idle);

									if (!enemy.GetComponent<EnemyBehaviorComponent>()->IsAlive() ||
										!enemy.GetComponent<EnemyBehaviorComponent>()->IsPlayerInSight() ||
										enemy.GetComponent<EnemyBehaviorComponent>()->GetState() == EnemyStateMachineStates::Enemy_Knockback ||
										enemy.GetComponent<EnemyBehaviorComponent>()->IsFrightened())
									{
										enemy.GetComponent<EnemyBehaviorComponent>()->SetState(Idle);
										continue;
									}
									
									enemy.GetComponent<EnemyBehaviorComponent>()->SetState(Idle);
									enemy.GetComponent<EnemyBehaviorComponent>()->SetSleepTimer();
									enemy.Emigrate(levelIDToLoad, newLocation);
								}
							}

							if (PlayerObject* playerObject = currentLevel->GetAllActiveObjects<PlayerObject>().begin(); playerObject)
							{
								playerObject->Emigrate(levelIDToLoad, newLocation);
							}

							break;
						}
					}
				}

				for (DoorObject& doorObj : levelToLoad->GetAllActiveObjects<DoorObject>())
				{
					doorObj.GetComponent<DoorComponent>()->SetTimer(2500);
				}
			
				PlayerObject* player = levelToLoad->GetAllActiveObjects<PlayerObject>().begin();

				if (doodle::random() > 0.5)
				{
					player->GetComponent<AudioPlayerComponent>()->Play("door_open_1");
				}
				else if (doodle::random() > 0.5)
				{
					player->GetComponent<AudioPlayerComponent>()->Play("door_open_2");
				}
				else
				{
					player->GetComponent<AudioPlayerComponent>()->Play("door_open_3");
				}

				auto enemies = levelToLoad->GetAllActiveObjects<EnemyObject>();
				for (auto enemy : enemies)
				{
					enemy.GetComponent<EnemyBehaviorComponent>()->SetPlayerID(player->GetID());
					enemy.GetComponent<EnemyBehaviorComponent>()->SetPathfinderID(
					levelToLoad->GetAllActiveObjects<PathfinderObject>().begin()->GetComponent<FlowFieldPathfinderComponent>()->GetID());
				}
			
				player->GetComponent<PlayerBehaviorComponent>()->BindMovementKeys();
				levelToLoad->GetAllActiveObjects<CameraObject>().begin()->SetFollowingObject(player);
				player->GetComponent<PlayerBehaviorComponent>()->SetPathFinderID(levelToLoad->GetAllActiveObjects<PathfinderObject>().begin()->GetComponent<FlowFieldPathfinderComponent>()->GetID());
			
			}
		);
		
		if (!mMinimapLevelToLoad.empty())
		{
			const ID minimapLevelID = levelIDsByName.at(mMinimapLevelToLoad);
			Game::SetCurrentHUDLevel(minimapLevelID);
			Game::GetLevel(minimapLevelID)->GetAllActiveComponents<MinimapComponent>().begin()->OnLevelChange(levelIDToLoad);
		}
		else
		{
			Game::GetCurrentHUDLevel()->GetAllActiveComponents<MinimapComponent>().begin()->OnLevelChange(levelIDToLoad);
		}
	}


	void LevelLoaderComponent::SetLevelToLoad(std::string levelToLoad, int spawnPointIndex, std::string minimapLevelToLoad)
	{
		mLevelToLoad = std::move(levelToLoad);
		mSpawnPointIndex = spawnPointIndex;
		mMinimapLevelToLoad = std::move(minimapLevelToLoad);
	}


	void LevelLoaderComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mLevelToLoad);
		serialize(outputFileStream, mSpawnPointIndex);
		serialize(outputFileStream, mMinimapLevelToLoad);
	}


	IComponent* LevelLoaderComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		std::string levelToLoad;
		std::string minimapLevelToLoad;
		int spawnPointNumber;

		deserialize(inputFileStream, levelToLoad);
		deserialize(inputFileStream, spawnPointNumber);
		deserialize(inputFileStream, minimapLevelToLoad);

		return level->LoadComponent<LevelLoaderComponent>(id, levelToLoad, spawnPointNumber, minimapLevelToLoad);
	}


	void LevelLoaderComponent::LoadAllLevels(const std::filesystem::path& levelListFilePath)
	{
		std::ifstream levelFile{ levelListFilePath };
		
		std::string _, category, name, path;
		int floor;
		while (levelFile >> category >> floor)
		{
			std::getline(std::getline(levelFile, _, '"'), name, '"');
			std::getline(std::getline(levelFile, _, '"'), path, '"');

			if (category == "level")
			{
				const ID id = Game::LoadLevel(path);
				levelIDsByName[name] = id;
				levelNamesByID[id] = name;
				levelPathsByID[id] = path;
				levelIDsByFloor[floor].push_back(id);
			}
		}
	}


	void LevelLoaderComponent::LoadLevelsOnFloor(const std::filesystem::path& levelListFilePath, const int floor)
	{
		std::ifstream levelFile{ levelListFilePath };

		std::string _, category, name, path;
		int levelFloor;
		while (levelFile >> category >> levelFloor)
		{
			std::getline(std::getline(levelFile, _, '"'), name, '"');
			std::getline(std::getline(levelFile, _, '"'), path, '"');

			if (levelFloor != floor)
			{
				continue;
			}

			if (category == "level")
			{
				const ID id = Game::LoadLevel(path);
				levelIDsByName[name] = id;
				levelNamesByID[id] = name;
				levelPathsByID[id] = path;
				levelIDsByFloor[floor].push_back(id);
			}
		}
	}


	void LevelLoaderComponent::RemoveLevelsOnFloor(const int floor)
	{
		for (const ID& id : levelIDsByFloor.at(floor))
		{
			Game::RemoveLevel(id);
		}
	}


	void LevelLoaderComponent::ReloadLevelsOnFloor(const int floor)
	{
		for (const ID& id : levelIDsByFloor.at(floor))
		{
			Game::RemoveLevel(id);
			Game::LoadLevel(levelPathsByID.at(id));
		}
	}


	ID LevelLoaderComponent::GetLevelID(const std::string& levelName)
	{
		return levelIDsByName.at(levelName);
	}


	const std::string& LevelLoaderComponent::GetLevelName(const ID& levelID)
	{
		return levelNamesByID.at(levelID);
	}


	void LevelLoaderComponent::RemoveAllLevels()
	{
		for (auto [_, id] : levelIDsByName)
		{
			Game::RemoveLevel(id);
		}
	}
}
