/*
    File Name: LevelLoaderComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"

#include <filesystem>



namespace uruk
{
	class LevelLoaderComponent final : public Component<LevelLoaderComponent>
	{
	public:
		LevelLoaderComponent(std::string levelToLoad, int spawnPointIndex = -1, std::string minimapLevelToLoad = "");

		void Load() const;

		void SetLevelToLoad(std::string levelToLoad, int spawnPointIndex = -1, std::string minimapLevelToLoad = "");


		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);

		

		static void LoadAllLevels(const std::filesystem::path& levelListFilePath);
		static void LoadLevelsOnFloor(const std::filesystem::path& levelListFilePath, int floor);
		static void RemoveLevelsOnFloor(int floor);
		static void ReloadLevelsOnFloor(int floor);
		[[nodiscard]] static ID GetLevelID(const std::string& levelName);
		[[nodiscard]] static const std::string& GetLevelName(const ID& levelID);
		static void RemoveAllLevels();


	public:
		static inline const std::map<int, std::tuple<std::string, int, std::string>> FIRST_LEVEL_BY_FLOOR = {
			{1, {"Level1.1-Room2", -1, "Level1.1-Minimap"}},
			{2, {"Level2.1-Room2", 0, "Level2.1-Minimap"}},
			{3, {"Level3.1-Room8", 0, "Level3.1-Minimap"}},
			{4, {"Level4.1-Room11", 0, "Level4.1-Minimap"}},
			{5, {"Level5.1-Room1", 0, "Level5.1-Minimap"}}
		};
		
	private:
		std::string mLevelToLoad;
		std::string mMinimapLevelToLoad;
		int mSpawnPointIndex;

		static inline std::map<std::string, ID> levelIDsByName;
		static inline std::map<int, std::vector<ID>> levelIDsByFloor;
		static inline std::map<ID, std::string> levelPathsByID;
		static inline std::map<ID, std::string> levelNamesByID;
	};
}
