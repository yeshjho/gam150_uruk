/*
    File Name: Line.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "Line.h"

namespace uruk
{
	Vector3D Line::BeginPoint() const
	{
		return start;
	}

	Vector3D Line::EndPoint() const
	{
		return end;
	}

	Vector3D Line::GetDirectionVector() const
	{
		Vector3D vec = end - start;
		return vec;
	}

	Vector3D Line::GetNormalDirectionVector() const
	{
		Vector3D vec = end - start;
		vec.Normalize();
		return vec;
	}

	float Line::GetLength() const
	{
		return (end - start).GetLength();
	}

	float Line::GetLengthSqr() const
	{
		return (end - start).GetLengthSqr();
	}

	std::pair<bool, Vector3D> Line::GetIntersectingPoint(const Line& other) const
	{
		Vector3D line1Dir = GetDirectionVector();
		Vector3D line2Dir = other.GetDirectionVector();
		Vector3D line1DirPerp(-line1Dir.Y(), line1Dir.X());
		Vector3D line2DirPerp(-line2Dir.Y(), line2Dir.X());
		
		float t = (other.start - start).DotProduct(line2DirPerp) / line1Dir.DotProduct(line2DirPerp);
		float s = (start - other.start).DotProduct(line1DirPerp) / line2Dir.DotProduct(line1DirPerp);

		if(t >= 0.f && t <= 1.f && s >= 0.f && s <= 1.f)
		{
			return { true , start + line1Dir * t };
		}
		
		return { false , Vector3D(0.f,0.f,0.f) };
	}

	bool Line::IsIntersectingWith(const Line& other) const
	{
		auto [isIntersecting, _] = GetIntersectingPoint(other);
		return isIntersecting;
	}

}