/*
    File Name: Line.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Vector3D.h"

namespace uruk
{
	class [[nodiscard]] Line
	{
	public:
		constexpr Line(const Vector3D& start,const Vector3D& end);
		
		Vector3D BeginPoint() const;
		Vector3D EndPoint() const;

		Vector3D GetDirectionVector() const;
		Vector3D GetNormalDirectionVector() const;
		//assuming Line1 and Line1 are not skew!
		float GetLength() const;
		float GetLengthSqr()const;
		std::pair<bool,Vector3D> GetIntersectingPoint(const Line& other) const;
		//assuming Line1 and Line1 are not skew! 
		bool IsIntersectingWith(const Line& other) const;
	private:
		Vector3D start, end;
	};

	constexpr Line::Line(const Vector3D& start, const Vector3D& end)
		:start(start),end(end)
	{
	}
}
