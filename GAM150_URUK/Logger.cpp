/*
    File Name: Logger.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "Logger.h"

#include <filesystem>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <Windows.h>
#undef ERROR

#include "UserDirectoryGetter.h"



namespace uruk
{
	Logger logger{ (ELogCategory::ALL & ~ELogCategory::EVENT) | ELogCategory::CUSTOM_EVENT, ELogVerbosity::VERBOSE, false };


	std::ofstream Logger::logFile = []()
	{
		std::filesystem::path logPath = get_user_directory() += "logs\\";

		const std::string time = getCurrentTimeInString("%Y-%m-%d %H-%M-%S");
		
		std::filesystem::create_directories(logPath);
		return std::ofstream{ std::wstring(logPath.c_str()) + std::wstring(time.begin(), time.end()) + L".log", std::wofstream::app };
	}();


	void Logger::Log(const ELogCategory category, const ELogVerbosity verbosity, const std::string& message) const
	{
		static constexpr unsigned short LIGHT_GREY = 7;
		static constexpr unsigned short RED = 4;
		static constexpr unsigned short YELLOW = 14;
		
		if (verbosity != ELogVerbosity::FATAL && (!(mCategories & category) || verbosity < mMinimumVerbosity))
		{
			return;
		}
		
		logFile << getCurrentTimeInString("[%H:%M:%S]\t") << std::left << std::setw(22) << CATEGORY_NAMES.at(category) << "\t" << std::left << std::setw(7) << VERBOSITY_NAMES.at(verbosity) << "\t\t" << message << '\n';

	#ifndef _DEBUG
		return;
	#endif
		
		switch (verbosity)
		{
			case ELogVerbosity::VERBOSE:
			case ELogVerbosity::LOG:
				if (!mbDisplayAll)
				{
					break;
				}
				[[fallthrough]];
			
			case ELogVerbosity::DISPLAY:
				setConsoleTextColor(LIGHT_GREY);
				std::cout << getCurrentTimeInString("[%H:%M:%S]\t") << std::left << std::setw(22) << CATEGORY_NAMES.at(category) << "\t" << std::left << std::setw(7) << VERBOSITY_NAMES.at(verbosity) << "\t\t" << message << std::endl;
				break;
			
			case ELogVerbosity::WARNING:
				setConsoleTextColor(YELLOW);
				std::cout << getCurrentTimeInString("[%H:%M:%S]\t") << std::left << std::setw(22) << CATEGORY_NAMES.at(category) << "\t" << std::left << std::setw(7) << VERBOSITY_NAMES.at(verbosity) << "\t\t" << message << std::endl;
				break;
			
			case ELogVerbosity::ERROR:
				setConsoleTextColor(RED);
				std::cout << getCurrentTimeInString("[%H:%M:%S]\t") << std::left << std::setw(22) << CATEGORY_NAMES.at(category) << "\t" << std::left << std::setw(7) << VERBOSITY_NAMES.at(verbosity) << "\t\t" << message << std::endl;
				break;
			
			case ELogVerbosity::FATAL:
				setConsoleTextColor(RED);
				std::cout << getCurrentTimeInString("[%H:%M:%S]\t") << std::left << std::setw(22) << CATEGORY_NAMES.at(category) << "\t" << std::left << std::setw(7) << VERBOSITY_NAMES.at(verbosity) << "\t\t" << message << std::endl;
				abort();
			
			default:
				break;
		}
	}
	

	std::string Logger::getCurrentTimeInString(const char* format)
	{
		const std::time_t TIME = std::time(nullptr);
	#pragma warning(push)
	#pragma warning(disable:4996)  // 'localtime' may be unsafe
		const std::tm* const TM = std::localtime(&TIME);
	#pragma warning(pop)
		std::stringstream ss;
		ss << std::put_time(TM, format);
		
		return ss.str();
	}
	
	void Logger::setConsoleTextColor(const unsigned short color)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
	}
	



	
	std::string toString(void* ptr)
	{
		std::stringstream ss;
		ss << std::hex << size_t(ptr);
		
		return ss.str();
	}
}
