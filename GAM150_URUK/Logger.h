/*
    File Name: Logger.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <fstream>
#include <map>
#include <string>



namespace uruk
{
	enum ELogCategory : int
	{
		// Allocation on construction, deallocation on destruction, claimMemories and getFreeSlot
		CONTAINER_MEMORY = 1 << 0,
		// Add, Get(nullptr), Remove, Sort and Reset
		CONTAINER_ACTION = 1 << 1,
		CONTAINER = CONTAINER_ACTION | CONTAINER_MEMORY,

		// BeginPlay and Update
		GAME_EVENT = 1 << 4,
		// AddLevel, RemoveLevel, GetLevel, GetCurrent(HUD)Level, SetCurrent(HUD)Level and ResetHUDLevel
		GAME_ACTION = 1 << 5,
		// Save and Load
		GAME_FILE = 1 << 6,
		GAME = GAME_EVENT | GAME_ACTION | GAME_FILE,

		// BeginPlay, Update, Pause and Resume
		LEVEL_EVENT = 1 << 9,
		// Save and Load
		LEVEL_FILE = 1 << 10,
		LEVEL = LEVEL_EVENT | LEVEL_FILE,

		COMPONENT_CONSTRUCTION = 1 << 13,
		COMPONENT_DESTRUCTION = 1 << 14,
		// BeginPlay, Update, Enable, Disable
		COMPONENT_EVENT = 1 << 15,
		// SetUpdateInterval
		COMPONENT_ACTION = 1 << 16,
		// Save and Load
		COMPONENT_FILE = 1 << 17,
		COMPONENT = COMPONENT_CONSTRUCTION | COMPONENT_DESTRUCTION | COMPONENT_EVENT | COMPONENT_ACTION | COMPONENT_FILE,

		OBJECT_CONSTRUCTION = 1 << 20,
		OBJECT_DESTRUCTION = 1 << 21,
		// Enable, Disable
		OBJECT_EVENT = 1 << 22,
		// Transfer
		OBJECT_ACTION = 1 << 23,
		// Save and Load
		OBJECT_FILE = 1 << 24,
		OBJECT = OBJECT_CONSTRUCTION | OBJECT_DESTRUCTION | OBJECT_EVENT | OBJECT_ACTION | OBJECT_FILE,

		CUSTOM_EVENT = 1 << 26,
		CUSTOM_ACTION = 1 << 27,
		CUSTOM_FILE = 1 << 28,

		OTHER = 1 << 31,

		CONSTRUCTION = COMPONENT_CONSTRUCTION | OBJECT_CONSTRUCTION,
		DESTRUCTION = COMPONENT_DESTRUCTION | OBJECT_DESTRUCTION,
		EVENT = GAME_EVENT | LEVEL_EVENT | COMPONENT_EVENT | OBJECT_EVENT | CUSTOM_EVENT,
		ACTION = CONTAINER_ACTION | GAME_ACTION | COMPONENT_ACTION | OBJECT_ACTION | CUSTOM_ACTION,
		FILE = GAME_FILE | LEVEL_FILE | COMPONENT_FILE | OBJECT_FILE | CUSTOM_FILE,
		CUSTOM = CUSTOM_EVENT | CUSTOM_ACTION | CUSTOM_FILE,
		
		ALL = -1
	};



	enum class ELogVerbosity
	{
		// File
		VERBOSE,
		// File
		LOG,
		// Console(White), File
		DISPLAY,
		// Console(Yellow), File
		WARNING,
		// Console(Red), File
		ERROR,
		// Console(Red), File, Crash, Always logged regardless of category.
		FATAL
	};



#pragma warning(push)
#pragma warning(disable: 26812)  // enum is unscoped
	class Logger
	{
	public:
		constexpr Logger(int categories, ELogVerbosity minimumVerbosity, bool bDisplayAll = false) noexcept;


		void Log(ELogCategory category, ELogVerbosity verbosity, const std::string& message) const;
		inline void LogVerbose(ELogCategory category, const std::string& message) const;
		inline void LogLog(ELogCategory category, const std::string& message) const;
		inline void LogDisplay(ELogCategory category, const std::string& message) const;
		inline void LogWarning(ELogCategory category, const std::string& message) const;
		inline void LogError(ELogCategory category, const std::string& message) const;
		inline void LogFatal(ELogCategory category, const std::string& message) const;
		
		
		constexpr void SetCategories(int categories) noexcept;
		constexpr void AddCategories(int categories) noexcept;
		constexpr void RemoveCategories(int categories) noexcept;

		constexpr void SetMinimumVerbosity(ELogVerbosity verbosity) noexcept;


	private:
		static std::string getCurrentTimeInString(const char* format);
		static void setConsoleTextColor(unsigned short color);



		
		
	private:
		int mCategories;
		ELogVerbosity mMinimumVerbosity;
		bool mbDisplayAll;

		static std::ofstream logFile;
		static inline const std::map<ELogVerbosity, std::string> VERBOSITY_NAMES{
			{ ELogVerbosity::VERBOSE, "Verbose"},
			{ ELogVerbosity::LOG, "Log" },
			{ ELogVerbosity::DISPLAY, "Display" },
			{ ELogVerbosity::WARNING, "WARNING" },
			{ ELogVerbosity::ERROR, "ERROR" },
			{ ELogVerbosity::FATAL, "!FATAL!" }
		};
		static inline const std::map<ELogCategory, std::string> CATEGORY_NAMES{
			{ ELogCategory::CONTAINER_MEMORY, "Container-Memory" },
			{ ELogCategory::CONTAINER_ACTION, "Container-Action" },
			
			{ ELogCategory::GAME_EVENT, "Game-Event" },
			{ ELogCategory::GAME_ACTION, "Game-Action" },
			{ ELogCategory::GAME_FILE, "Game-File" },
			
			{ ELogCategory::LEVEL_EVENT, "Level-Event" },
			{ ELogCategory::LEVEL_FILE, "Level-File" },
			
			{ ELogCategory::COMPONENT_CONSTRUCTION, "Component-Construction" },
			{ ELogCategory::COMPONENT_DESTRUCTION, "Component-Destruction" },
			{ ELogCategory::COMPONENT_EVENT, "Component-Event" },
			{ ELogCategory::COMPONENT_ACTION, "Component-Action" },
			{ ELogCategory::COMPONENT_FILE, "Component-File" },
			
			{ ELogCategory::OBJECT_CONSTRUCTION, "Object-Construction" },
			{ ELogCategory::OBJECT_DESTRUCTION, "Object-Destruction" },
			{ ELogCategory::OBJECT_EVENT, "Object-Event" },
			{ ELogCategory::OBJECT_ACTION, "Object-Action" },
			{ ELogCategory::OBJECT_FILE, "Object-File" },

			{ ELogCategory::CUSTOM_EVENT, "Custom-Event" },
			{ ELogCategory::CUSTOM_ACTION, "Custom-Action" },
			{ ELogCategory::CUSTOM_FILE, "Custom-File" },

			{ ELogCategory::OTHER, "Other" }
		};
	};
#pragma warning(pop)



	extern Logger logger;
	




	std::string toString(void* ptr);
}

#include "Logger.inl"
