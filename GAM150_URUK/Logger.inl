/*
    File Name: Logger.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	constexpr Logger::Logger(const int categories, const ELogVerbosity minimumVerbosity, const bool bDisplayAll) noexcept
		: mCategories(categories), mMinimumVerbosity(minimumVerbosity), mbDisplayAll(bDisplayAll)
	{}


	inline void Logger::LogVerbose(const ELogCategory category, const std::string& message) const
	{
		Log(category, ELogVerbosity::VERBOSE, message);
	}

	inline void Logger::LogLog(const ELogCategory category, const std::string& message) const
	{
		Log(category, ELogVerbosity::LOG, message);
	}

	inline void Logger::LogDisplay(const ELogCategory category, const std::string& message) const
	{
		Log(category, ELogVerbosity::DISPLAY, message);
	}

	inline void Logger::LogWarning(const ELogCategory category, const std::string& message) const
	{
		Log(category, ELogVerbosity::WARNING, message);
	}

	inline void Logger::LogError(const ELogCategory category, const std::string& message) const
	{
		Log(category, ELogVerbosity::ERROR, message);
	}

	inline void Logger::LogFatal(const ELogCategory category, const std::string& message) const
	{
		Log(category, ELogVerbosity::FATAL, message);
	}


	constexpr void Logger::SetCategories(const int categories) noexcept
	{
		mCategories = categories;
	}
	
	constexpr void Logger::AddCategories(const int categories) noexcept
	{
		mCategories |= categories;
	}
	
	constexpr void Logger::RemoveCategories(const int categories) noexcept
	{
		mCategories &= ~categories;
	}
	
	constexpr void Logger::SetMinimumVerbosity(const ELogVerbosity verbosity) noexcept
	{
		mMinimumVerbosity = verbosity;
	}
}
