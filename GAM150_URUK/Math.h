/*
    File Name: Math.h
    Project Name: U.R.U.K
	Author(s):
		-Joonho Hwang
		Contribution(s):
			constants, clamp, lerp, inverseLerp, smoothStep, dampedSineSecondDerivative
		-Doyoon Kim
		Contribution(s):
			Get_Rotated_Vector_Around_Given_Axis, GetRadBetweenTwoVectors, GetRadOffset,
			rotation_lerp, slerp
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Matrix3X3.h"



namespace uruk
{
	namespace constants
	{
		constexpr float PI = 3.141592653589793238462643384727f;
		constexpr float TWO_PI = 6.283185307179586476925286769454f;

		constexpr float E = 2.718281828459045235360287471352662497757247093f;
	}



	template<typename T>
	constexpr T clamp(T x, T lowerLimit, T upperLimit) noexcept
	{
		return x < lowerLimit ? lowerLimit : x > upperLimit ? upperLimit : x;
	}

	template<typename T>
	constexpr T lerp(T start, T end, float alpha, bool bClamp = true) noexcept
	{
		if (bClamp)
		{
			if (alpha < 0.f)
			{
				return start;
			}
			if (alpha > 1.f)
			{
				return end;
			}
		}

		return start + alpha * (end - start);
	}

	constexpr float inverseLerp(float start, float end, float current) noexcept
	{
		return end - start == 0 ? 1.f : (clamp(current, start, end) - start) / (end - start);
	}

	constexpr float smoothStep(float start, float end, float alpha) noexcept
	{
		alpha = clamp(alpha, 0.f, 1.f);
		return start + (end - start) * (alpha * alpha * alpha * (alpha * (6 * alpha - 15) + 10));
	}

	constexpr Vector3D smoothStep(Vector3D start, Vector3D end, float alpha)
	{
		return Vector3D{ smoothStep(start[0], end[0], alpha), smoothStep(start[1], end[1], alpha), smoothStep(start[2], end[2], alpha) };
	}

	inline float dampedSineSecondDerivative(float decreaseRate, unsigned int repeat, float x)
	{
		return std::powf(constants::E, -decreaseRate * x) * ((decreaseRate * decreaseRate - repeat * repeat) * std::sinf(repeat * x) - 2 * repeat * decreaseRate * std::cosf(repeat * x));
	}


	inline Vector3D Get_Rotated_Vector_Around_Given_Axis(Vector3D src, Vector3D axis, float dRadian)
	{
		Vector3D&& diff = src - axis;
		Matrix3X3&& rotationMatrix = Matrix3X3::GetRotationMatrix(-dRadian);
		Vector3D&& rotatedDiff = rotationMatrix * diff;
		return axis + rotatedDiff;
	}
	
	inline float GetRadBetweenTwoVectors(Vector3D v1, Vector3D v2)
	{
		v1.Normalize();
		v2.Normalize();
		float cosTheta = v1.DotProduct(v2);
		cosTheta = clamp(cosTheta, -1.f, 1.f);
		
		return acos(cosTheta);
	}

	inline float GetRadOffset(Vector3D p1, Vector3D p2)//- -> ccw
	{
		float rad = GetRadBetweenTwoVectors(p1, p2);
		float rotatedDirection = (p1.CrossProduct(p2)[2] >= 0.f) ? -1.f : 1.f;
		return rad * rotatedDirection;
	}

	inline float rotation_lerp(float rot1, float rot2, float t)
	{
		if (rot2 - rot1 > constants::PI)
		{
			rot2 -= constants::TWO_PI;
		}
		else if (rot2 - rot1 < -constants::PI)
		{
			rot1 -= constants::TWO_PI;
		}

		if (t < 0.f)
		{
			return rot1;
		}
		if (t > 1.f)
		{
			return rot2;
		}

		float result = rot1 + t * (rot2 - rot1);
		if (result < 0.f)
		{
			result += constants::TWO_PI;
		}

		return result;
	}
	
	inline Vector3D slerp(Vector3D p1, Vector3D p2,float t)
	{
		float radOffset = GetRadOffset(p1, p2);
		const Matrix3X3& rotationMatrix = Matrix3X3::GetRotationMatrix(radOffset * t);
		return Vector3D(rotationMatrix * p1);
	}
}
