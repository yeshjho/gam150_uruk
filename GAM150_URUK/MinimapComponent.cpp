/*
    File Name: MinimapComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "MinimapComponent.h"

#include <doodle/doodle.hpp>

#include "CameraObject.h"
#include "Game.h"
#include "GameResourceManager.h"
#include "HoverableComponent.h"
#include "InputComponent.h"
#include "LevelLoaderComponent.h"
#include "Serializer.h"



namespace uruk
{
	MinimapComponent::MinimapComponent(const int width, const int height, const std::vector<doodle::Image::color>& colors, std::map<std::string, std::vector<std::pair<int, int>>> corridorPixelCoords, std::map<std::string, std::pair<int, int>> roomPixelCoord)
		: mCorridorPixelCoords(std::move(corridorPixelCoords)), mRoomPixelCoord(std::move(roomPixelCoord))
	{
		mImage.reset(new doodle::Image{ width, height });
		
		std::copy(begin(colors), end(colors), mImage->begin());
	}


	void MinimapComponent::OnDraw(const Vector3D& camPosition, float, const Vector3D& camZoomScale) const
	{
		using namespace doodle;

		static const auto& playerIcon = GameResourceManager::GetImage("PlayerIcon");


		double x, y, width, height;
		int texelWidth, texelHeight, texelX, texelY;
		
		if (mExpanded)
		{
			x = MARGIN_ON_EXPAND;
			y = MARGIN_ON_EXPAND;
			width = Width - 2 * MARGIN_ON_EXPAND;
			height = Height - 2 * MARGIN_ON_EXPAND;

			texelWidth = mImage->GetWidth();
			texelHeight = mImage->GetHeight();
			texelX = 0;
			texelY = 0;
		}
		else
		{
			x = Width - MARGIN_FROM_RIGHT - SIDE_LENGTH;
			y = MARGIN_FROM_BOTTOM;
			width = SIDE_LENGTH;
			height = SIDE_LENGTH;

			texelWidth = static_cast<int>(SIDE_LENGTH / camZoomScale[0]);
			texelHeight = static_cast<int>(SIDE_LENGTH / camZoomScale[1]);
			texelX = static_cast<int>(camPosition[0]) - texelWidth / 2;
			texelY = static_cast<int>(camPosition[1]) - texelHeight / 2;

			if (texelX < 0)
			{
				texelX = 0;
			}
			if (texelY < 0)
			{
				texelY = 0;
			}
		}

		

		push_settings();
		
		draw_image(*mImage, x, y, width, height, texelX, texelY, texelWidth, texelHeight);
		if (mExpanded)
		{
			set_image_mode(RectMode::Center);
			const double iconX = camPosition[0] * double(Width) / mImage->GetWidth() - MARGIN_ON_EXPAND / 2;
			const double iconY = (mImage->GetHeight() - camPosition[1]) * double(Height) / mImage->GetHeight() + MARGIN_ON_EXPAND / 2;
			draw_image(*playerIcon, iconX, iconY, playerIcon->GetWidth(), playerIcon->GetHeight());
		}
		
		set_outline_color(0);
		set_outline_width(2);
		no_fill();
		draw_rectangle(x, y, width, height);
		
		pop_settings();
	}


	void MinimapComponent::OnBeginPlay()
	{
		GetOwner()->GetComponent<InputComponent>()->BindFunctionToMouseWheeled(&MinimapComponent::OnWheel, this);
		GetOwner()->GetComponent<InputComponent>()->BindFunctionToKeyboardPressKey(doodle::KeyboardButtons::M, &MinimapComponent::ToggleExpanded, this);
	}


	void MinimapComponent::OnUpdate()
	{
		GetOwner()->GetComponent<TransformComponent>()->SetPosition(static_cast<float>(doodle::Width - MARGIN_FROM_RIGHT - SIDE_LENGTH), MARGIN_FROM_BOTTOM);
	}


	void MinimapComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mImage->GetWidth());
		serialize(outputFileStream, mImage->GetHeight());

		serialize(outputFileStream, size_t(mImage->GetWidth()) * mImage->GetHeight());
		for (const auto& color : *mImage)
		{
			serialize(outputFileStream, color);
		}

		serialize(outputFileStream, mCorridorPixelCoords);
		serialize(outputFileStream, mRoomPixelCoord);
	}


	IComponent* MinimapComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		int width, height;
		deserialize(inputFileStream, width);
		deserialize(inputFileStream, height);

		std::vector<doodle::Image::color> colors;
		deserialize(inputFileStream, colors);

		std::map<std::string, std::vector<std::pair<int, int>>> corridorPixelCoords;
		std::map<std::string, std::pair<int, int>> roomPixelCoord;
		deserialize(inputFileStream, corridorPixelCoords);
		deserialize(inputFileStream, roomPixelCoord);
		
		return level->LoadComponent<MinimapComponent>(id, width, height, colors, std::move(corridorPixelCoords), std::move(roomPixelCoord));
	}


	void MinimapComponent::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mExpanded);
	}


	void MinimapComponent::OnLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mExpanded);
	}


	void MinimapComponent::OnWheel() const
	{
		if (!mExpanded && GetOwner()->GetComponent<HoverableComponent>()->IsHovered())
		{
			CameraObject* camera = mLevel->GetAllActiveObjects<CameraObject>().begin();
			camera->AddZoomScale(static_cast<float>(InputComponent::GetWheelAmount()) / 5.f);
			if (camera->GetZoomScale() < 0.1f)
			{
				camera->SetZoomScale(0.1f);
			}
		}
	}


	void MinimapComponent::OnLevelChange(const ID& levelID)
	{
		const std::string levelName = LevelLoaderComponent::GetLevelName(levelID);

		if (!cacheLevelID.is_nil())
		{
			const std::string cacheLevelName = LevelLoaderComponent::GetLevelName(cacheLevelID);
			if (mCorridorPixelCoords.count(cacheLevelName))
			{
				for (auto [x, y] : mCorridorPixelCoords.at(cacheLevelName))
				{
					auto& color = (*mImage)(x, y);
					color.red = color.blue;
				}
			}
		}

		TransformComponent* camTransform = mLevel->GetAllActiveObjects<CameraObject>().begin()->GetComponent<TransformComponent>();
		
		if (mRoomPixelCoord.count(levelName))
		{
			const auto [x, y] = mRoomPixelCoord.at(levelName);
			
			camTransform->SetPosition(static_cast<float>(x), static_cast<float>(y));
		}
		else
		{
			const auto& pixelCoords = mCorridorPixelCoords.at(levelName);
			const auto [middleX, middleY] = pixelCoords.at(pixelCoords.size() / 2);
			
			camTransform->SetPosition(static_cast<float>(middleX), static_cast<float>(middleY));
			
			for (auto [x, y] : pixelCoords)
			{
				auto& color = (*mImage)(x, y);
				color.red = 255;
			}
		}

		cacheLevelID = levelID;
	}
}
