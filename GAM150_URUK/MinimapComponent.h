/*
    File Name: MinimapComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"
#include "ZOrderBase.h"

#include <doodle/image.hpp>



namespace uruk
{
	class MinimapComponent final : public ZOrderBase, public Component<MinimapComponent, EUpdateType::BY_FRAME, update_priorities::LOWEST_PRIORITY, 0, EComponentAttribute::DRAWABLE>
	{
	public:
		MinimapComponent(int width, int height, const std::vector<doodle::Image::color>& colors, std::map<std::string, std::vector<std::pair<int, int>>> corridorPixelCoords, std::map<std::string, std::pair<int, int>> roomPixelCoord);
		
		void OnDraw(const Vector3D& camPosition, float camRotation, const Vector3D& camZoomScale) const override;

		void OnBeginPlay() override;
		void OnUpdate() override;
		
		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);

		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;

		void OnWheel() const;


		[[nodiscard]] constexpr bool IsExpanded() const noexcept;
		constexpr void SetExpanded(bool isExpanded) noexcept;
		constexpr void Expand() noexcept;
		constexpr void Shrink() noexcept;
		constexpr void ToggleExpanded() noexcept;

		void OnLevelChange(const ID& levelID);
		

		
	public:
		static constexpr double MARGIN_ON_EXPAND = 50;
		static constexpr double MARGIN_FROM_RIGHT = 50;
		static constexpr double MARGIN_FROM_BOTTOM = 50;
		static constexpr double SIDE_LENGTH = 150;

	private:
		std::map<std::string, std::vector<std::pair<int, int>>> mCorridorPixelCoords;
		std::map<std::string, std::pair<int, int>> mRoomPixelCoord;

		std::unique_ptr<doodle::Image> mImage;

		bool mExpanded = false;

		ID cacheLevelID;
	};



	constexpr bool MinimapComponent::IsExpanded() const noexcept
	{
		return mExpanded;
	}


	constexpr void MinimapComponent::SetExpanded(const bool isExpanded) noexcept
	{
		mExpanded = isExpanded;
	}


	constexpr void MinimapComponent::Expand() noexcept
	{
		SetExpanded(true);
	}


	constexpr void MinimapComponent::Shrink() noexcept
	{
		SetExpanded(false);
	}


	constexpr void MinimapComponent::ToggleExpanded() noexcept
	{
		SetExpanded(!IsExpanded());
	}
}
