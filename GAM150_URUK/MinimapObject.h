/*
    File Name: MinimapObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"



namespace uruk
{
	class MinimapObject final : public Object<MinimapObject>
	{};
}
