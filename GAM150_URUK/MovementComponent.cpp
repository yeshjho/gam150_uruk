/*
    File Name: MovementComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "MovementComponent.h"
#include "Serializer.h"
#include "TransformComponent.h"

namespace uruk
{
	MovementComponent::MovementComponent()
		:mVelocity({0,0,0}),mAcceleration({0,0,0})
	{
	}

	MovementComponent::MovementComponent(Vector3D velocity)
		:mVelocity(velocity),mAcceleration({0,0,0})
	{
	}

	MovementComponent::MovementComponent(Vector3D velocity, Vector3D acceleration)
		:mVelocity(velocity),mAcceleration(acceleration)
	{
		
	}
	
	void MovementComponent::OnUpdate()
	{
		float dt = mLevel->GetDeltaTimeInMillisecond()/1000.f;
		mVelocity += mAcceleration * dt;
		mVelocity *= mDrag;
		TransformComponent* transformComponent = GetOwner()->GetComponent<TransformComponent>();
		transformComponent->MoveBy(mVelocity*dt);
		mAcceleration = { 0,0,0 };
	}

	void MovementComponent::SetVelocity(Vector3D velocity)
	{
		mVelocity = velocity;
	}

	void MovementComponent::AddVelocity(Vector3D dVelocity)
	{
		mVelocity += dVelocity;
	}

	Vector3D MovementComponent::GetVelocity() const
	{
		return mVelocity;
	}

	void MovementComponent::SetAcceleration(Vector3D acceleration)
	{
		mAcceleration = acceleration;
	}

	void MovementComponent::AddAcceleration(Vector3D dAcceleration)
	{
		mAcceleration += dAcceleration;
	}

	Vector3D MovementComponent::GetAcceleration() const
	{
		return mAcceleration;
	}

	void MovementComponent::SetDrag(float drag)
	{
		mDrag = drag;
	}

	bool MovementComponent::ShouldApplyGravity() const
	{
		return mShouldApplyGravity;
	}

	void MovementComponent::SetShouldApplyGravity(bool shouldApplyGravity)
	{
		mShouldApplyGravity = shouldApplyGravity;
	}

	void MovementComponent::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mVelocity);
		serialize(outputFileStream, mAcceleration);
		serialize(outputFileStream, mShouldApplyGravity);
		serialize(outputFileStream, mDrag);
	}

	void MovementComponent::OnLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mVelocity);
		deserialize(inputFileStream, mAcceleration);
		deserialize(inputFileStream, mShouldApplyGravity);
		deserialize(inputFileStream, mDrag);
	}
}
