/*
    File Name: MovementComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"
#include "Vector3D.h"

namespace uruk
{
	class MovementComponent final : public Component<MovementComponent, EUpdateType::BY_MILLISECOND, update_priorities::HIGHEST_PRIORITY,0>
	{
	public:
		MovementComponent();
		MovementComponent(Vector3D velocity);
		MovementComponent(Vector3D velocity, Vector3D acceleration);
		
		void OnUpdate() override;
		
		void SetVelocity(Vector3D velocity);
		void AddVelocity(Vector3D dVelocity);
		Vector3D GetVelocity() const;
		
		void SetAcceleration(Vector3D acceleration);
		void AddAcceleration(Vector3D dAcceleration);
		Vector3D GetAcceleration() const;

		void SetDrag(float mDrag);
		
		bool ShouldApplyGravity() const;
		void SetShouldApplyGravity(bool shouldApplyGravity);

		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;
	private:
		Vector3D mVelocity;
		Vector3D mAcceleration;
		bool mShouldApplyGravity = false;
		float mDrag = 1.f;
	};
}
