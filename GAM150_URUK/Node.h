/*
    File Name: Node.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Vector3D.h"



namespace uruk
{

	enum class ENodeType
	{
		Empty, Start, Target, Obstacle, Debug
	};

	class Node
	{
	public:
		[[nodiscard]] constexpr ENodeType GetNodeType() const noexcept { return mNodeType; }
		constexpr void SetNodeType(ENodeType nodeType) noexcept { mNodeType = nodeType; }

		[[nodiscard]] constexpr Vector3D GetPosition() const noexcept { return mPosition; }
		constexpr void SetPosition(Vector3D nodePos) noexcept { mPosition = nodePos; }

		[[nodiscard]] constexpr Vector3D GetDirection() const noexcept { return mDirection; }
		constexpr void SetDirection(Vector3D nodeDir) noexcept { mDirection = nodeDir; }

		[[nodiscard]] constexpr int GetCost() const noexcept { return mCost; }
		constexpr void SetCost(int cost) noexcept { mCost = cost; }

		[[nodiscard]] constexpr bool IsEvaluated() const noexcept { return mIsEvaluated; }
		constexpr void SetEvaluated(bool isEvaluated) noexcept { mIsEvaluated = isEvaluated; }

	private:
		ENodeType mNodeType = ENodeType::Empty;
		Vector3D mPosition = { 0, 0 };
		Vector3D mDirection = { 0, 0 };
		int mCost = 0;
		bool mIsEvaluated = false; // Flag for BFS Algorithm (to prevent multiple checks being made over a single node)
	};

}