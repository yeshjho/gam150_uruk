/*
    File Name: Object.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "IObject.h"

// Including Level here because its subclass should be able to use IObject's AddComponent()/GetComponent()
// That 2 functions depend on Level.
#include "Level.h"



namespace uruk
{
	template<typename T>
	class ObjectOnLoadFunctionPointerStorer
	{
	public:
		ObjectOnLoadFunctionPointerStorer();
	};




	
	template<typename ObjectType>
	class Object : public IObject
	{
	public:
		[[nodiscard]] const TypeID& GetTypeID() const noexcept override final;

		
		[[nodiscard]] bool operator==(const IObject& rhs) const noexcept override final;
		[[nodiscard]] bool operator==(const IObject* rhs) const noexcept override final;
		[[nodiscard]] bool operator!=(const IObject& rhs) const noexcept override final;
		[[nodiscard]] bool operator!=(const IObject* rhs) const noexcept override final;

		
		static IObject* OnLoadConstruct(Level* level, const ID& id);



	public:
		static const TypeID TYPE_ID;

	private:
		// Dirty get-around to store OnLoad function pointer to IObject's list...
		inline static ObjectOnLoadFunctionPointerStorer<ObjectType> objectOnLoadFunctionPointerStorer;
	};
}

#include "Object.inl"
