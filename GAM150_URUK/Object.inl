/*
    File Name: Object.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	template <typename T>
	ObjectOnLoadFunctionPointerStorer<T>::ObjectOnLoadFunctionPointerStorer()
	{
		IObject::onLoadConstructFunctions[IDManager::GetTypeID<T>()] = Object<T>::OnLoadConstruct;
	}



	
	
	template<typename ObjectType>
	const TypeID Object<ObjectType>::TYPE_ID = IDManager::GetTypeID<ObjectType>();


	template<typename ObjectType>
	const TypeID& Object<ObjectType>::GetTypeID() const noexcept
	{
		return TYPE_ID;
	}
	
	
	template<typename ObjectType>
	bool Object<ObjectType>::operator==(const IObject& rhs) const noexcept
	{
		return TYPE_ID == rhs.GetTypeID() && mID == rhs.GetID();
	}

	template<typename ObjectType>
	bool Object<ObjectType>::operator==(const IObject* rhs) const noexcept
	{
		return TYPE_ID == rhs->GetTypeID() && mID == rhs->GetID();
	}

	template<typename ObjectType>
	bool Object<ObjectType>::operator!=(const IObject& rhs) const noexcept
	{
		return !(*this == rhs);
	}

	template<typename ObjectType>
	bool Object<ObjectType>::operator!=(const IObject* rhs) const noexcept
	{
		return !(*this == rhs);
	}

	
	template<typename ObjectType>
	IObject* Object<ObjectType>::OnLoadConstruct(Level* level, const ID& id)
	{
		return level->LoadObject<ObjectType>(id);
	}
}
