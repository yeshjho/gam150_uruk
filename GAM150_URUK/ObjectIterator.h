/*
    File Name: ObjectIterator.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <type_traits>



namespace uruk
{
	template<typename T, typename = void>
	struct HasActiveness : std::false_type
	{};

	template<typename T>
	struct HasActiveness<T, std::void_t<decltype(std::declval<T>().IsActive())>> : std::true_type
	{};
	

	
	template<typename T, bool ActiveOnly>
	struct ObjectIterator
	{
		using difference_type = int;
		using value_type = T;
		using pointer = T*;
		using reference = T&;
		using iterator_category = std::random_access_iterator_tag;



		constexpr ObjectIterator<T, ActiveOnly> begin() const noexcept;
		constexpr ObjectIterator<T, ActiveOnly> end() const noexcept;

		// NOTE: You need to --it to iterate over.
		constexpr ObjectIterator<T, ActiveOnly> rbegin() const noexcept;
		constexpr ObjectIterator<T, ActiveOnly> rend() const noexcept;

		
		constexpr operator T*();

		constexpr reference operator[](difference_type index);
		constexpr reference operator[](difference_type index) const;

		constexpr reference operator*();
		constexpr reference operator*() const;
		constexpr pointer operator->() noexcept;
		constexpr pointer operator->() const noexcept;

		constexpr ObjectIterator<T, ActiveOnly> operator+(difference_type amount) const noexcept;
		friend constexpr ObjectIterator<T, ActiveOnly> operator+(int amount, const ObjectIterator<T, ActiveOnly>& rhs) noexcept;
		constexpr ObjectIterator<T, ActiveOnly> operator-(difference_type amount) const noexcept;
		constexpr difference_type operator-(const ObjectIterator<T, ActiveOnly>& rhs) const noexcept;

		constexpr ObjectIterator<T, ActiveOnly>& operator+=(difference_type amount) noexcept;
		constexpr ObjectIterator<T, ActiveOnly>& operator-=(difference_type amount) noexcept;

		constexpr ObjectIterator<T, ActiveOnly>& operator++() noexcept;
		constexpr ObjectIterator<T, ActiveOnly> operator++(int) noexcept;
		constexpr ObjectIterator<T, ActiveOnly>& operator--() noexcept;
		constexpr ObjectIterator<T, ActiveOnly> operator--(int) noexcept;

		constexpr bool operator==(const ObjectIterator<T, ActiveOnly>& rhs) const noexcept;
		constexpr bool operator!=(const ObjectIterator<T, ActiveOnly>& rhs) const noexcept;

		constexpr bool operator<(const ObjectIterator<T, ActiveOnly>& rhs) const noexcept;
		constexpr bool operator>(const ObjectIterator<T, ActiveOnly>& rhs) const noexcept;
		constexpr bool operator<=(const ObjectIterator<T, ActiveOnly>& rhs) const noexcept;
		constexpr bool operator>=(const ObjectIterator<T, ActiveOnly>& rhs) const noexcept;

	private:
		static constexpr T* addSize(T* p, size_t size) noexcept;
		static constexpr T* subtractSize(T* p, size_t size) noexcept;



	public:
		T* mBegin;
		T* mBack;

		// Separated size because its actual type can be different from T.
		/// Ex) T = IComponent but its actual type is TransformComponent
		size_t mSize = sizeof(T);

		T* mCurrent = mBegin;
	};
}

#include "ObjectIterator.inl"
