/*
    File Name: ObjectIterator.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	template<typename T, bool ActiveOnly>
	constexpr ObjectIterator<T, ActiveOnly> ObjectIterator<T, ActiveOnly>::begin() const noexcept
	{
		if (!mBegin)
		{
			return end();
		}
		
		if constexpr (ActiveOnly && HasActiveness<T>::value)
		{
			T* begin_ = mBegin;
			while (!begin_->IsActive())
			{
				if (begin_ == mBack)
				{
					return end();
				}
				
				begin_ = addSize(begin_, mSize);
			}

			return ObjectIterator<T, ActiveOnly>{ mBegin, mBack, mSize, begin_ };
		}
		else
		{
			return ObjectIterator<T, ActiveOnly>{ mBegin, mBack, mSize, mBegin };
		}
	}

	template<typename T, bool ActiveOnly>
	constexpr ObjectIterator<T, ActiveOnly> ObjectIterator<T, ActiveOnly>::end() const noexcept
	{
		if (!mBegin)
		{
			return ObjectIterator<T, ActiveOnly>{ nullptr, nullptr, mSize, nullptr };
		}
		
		if constexpr (ActiveOnly && HasActiveness<T>::value)
		{
			T* back = mBack;
			while (!back->IsActive())
			{
				if (back == mBegin)
				{
					return ObjectIterator<T, ActiveOnly>{ mBegin, mBack, mSize, addSize(mBegin, mSize) };
				}
				
				back = reinterpret_cast<T*>(reinterpret_cast<char*>(back) - mSize);
			}

			return ObjectIterator<T, ActiveOnly>{ mBegin, mBack, mSize, addSize(back, mSize) };
		}
		else
		{
			return ObjectIterator<T, ActiveOnly>{ mBegin, mBack, mSize, addSize(mBack, mSize) };
		}
	}

	template<typename T, bool ActiveOnly>
	constexpr ObjectIterator<T, ActiveOnly> ObjectIterator<T, ActiveOnly>::rbegin() const noexcept
	{
		if (!mBegin)
		{
			return rend();
		}
		
		if constexpr (ActiveOnly && HasActiveness<T>::value)
		{
			ObjectIterator<T, ActiveOnly> toReturn{ end() };
			toReturn.mCurrent = subtractSize(toReturn.mCurrent, mSize);
			return toReturn;
		}
		else
		{
			return ObjectIterator<T, ActiveOnly>{ mBegin, mBack, mSize, mBack };
		}
	}

	template<typename T, bool ActiveOnly>
	constexpr ObjectIterator<T, ActiveOnly> ObjectIterator<T, ActiveOnly>::rend() const noexcept
	{
		if (!mBack)
		{
			return ObjectIterator<T, ActiveOnly>{ nullptr, nullptr, mSize, nullptr };
		}
		
		if constexpr (ActiveOnly && HasActiveness<T>::value)
		{
			ObjectIterator<T, ActiveOnly> toReturn{ begin() };
			toReturn.mCurrent = subtractSize(toReturn.mCurrent, mSize);
			return toReturn;
		}
		else
		{
			return ObjectIterator<T, ActiveOnly>{ mBegin, mBack, mSize, subtractSize(mBegin, mSize) };
		}
	}

	template<typename T, bool ActiveOnly>
	constexpr ObjectIterator<T, ActiveOnly>::operator T* ()
	{
		return mCurrent;
	}

	template<typename T, bool ActiveOnly>
	constexpr typename ObjectIterator<T, ActiveOnly>::reference ObjectIterator<T, ActiveOnly>::operator[](const difference_type index)
	{
		static_assert(!ActiveOnly);
		
		T* const toReturn = addSize(mBegin, index * mSize);
		if (mBack < toReturn || toReturn < mBegin)
		{
			throw std::out_of_range("");
		}

		return *toReturn;
	}

	template<typename T, bool ActiveOnly>
	constexpr typename ObjectIterator<T, ActiveOnly>::reference ObjectIterator<T, ActiveOnly>::operator[](const difference_type index) const
	{
		static_assert(!ActiveOnly);

		T* const toReturn = addSize(mBegin, index * mSize);
		if (mBack < toReturn || toReturn < mBegin)
		{
			throw std::out_of_range("");
		}

		return *toReturn;
	}

	template<typename T, bool ActiveOnly>
	constexpr typename ObjectIterator<T, ActiveOnly>::reference ObjectIterator<T, ActiveOnly>::operator*()
	{
		return *mCurrent;
	}

	template<typename T, bool ActiveOnly>
	constexpr typename ObjectIterator<T, ActiveOnly>::reference ObjectIterator<T, ActiveOnly>::operator*() const
	{
		return *mCurrent;
	}

	template<typename T, bool ActiveOnly>
	constexpr typename ObjectIterator<T, ActiveOnly>::pointer ObjectIterator<T, ActiveOnly>::operator->() noexcept
	{
		return mCurrent;
	}

	template<typename T, bool ActiveOnly>
	constexpr typename ObjectIterator<T, ActiveOnly>::pointer ObjectIterator<T, ActiveOnly>::operator->() const noexcept
	{
		return mCurrent;
	}

	template<typename T, bool ActiveOnly>
	constexpr ObjectIterator<T, ActiveOnly> ObjectIterator<T, ActiveOnly>::operator+(const difference_type amount) const noexcept
	{
		static_assert(!ActiveOnly);

		auto temp = *this;
		temp += amount;
		return temp;
	}

	template<typename T, bool ActiveOnly>
	constexpr ObjectIterator<T, ActiveOnly> operator+(int amount, const ObjectIterator<T, ActiveOnly>& rhs) noexcept
	{
		static_assert(!ActiveOnly);

		auto temp = rhs;
		temp += amount;
		return temp;
	}

	template<typename T, bool ActiveOnly>
	constexpr ObjectIterator<T, ActiveOnly> ObjectIterator<T, ActiveOnly>::operator-(const difference_type amount) const noexcept
	{
		static_assert(!ActiveOnly);

		auto temp = *this;
		temp -= amount;
		return temp;
	}

	template<typename T, bool ActiveOnly>
	constexpr typename ObjectIterator<T, ActiveOnly>::difference_type ObjectIterator<T, ActiveOnly>::operator-(const ObjectIterator<T, ActiveOnly>& rhs) const noexcept
	{
		static_assert(!ActiveOnly);

		return static_cast<difference_type>((reinterpret_cast<char*>(mCurrent) - reinterpret_cast<char*>(rhs.mCurrent)) / mSize);
	}

	template<typename T, bool ActiveOnly>
	constexpr ObjectIterator<T, ActiveOnly>& ObjectIterator<T, ActiveOnly>::operator+=(const difference_type amount) noexcept
	{
		static_assert(!ActiveOnly);

		mCurrent = addSize(mCurrent, mSize * amount);

		return *this;
	}

	template<typename T, bool ActiveOnly>
	constexpr ObjectIterator<T, ActiveOnly>& ObjectIterator<T, ActiveOnly>::operator-=(const difference_type amount) noexcept
	{
		static_assert(!ActiveOnly);

		mCurrent = subtractSize(mCurrent, mSize * amount);

		return *this;
	}

	template<typename T, bool ActiveOnly>
	constexpr ObjectIterator<T, ActiveOnly>& ObjectIterator<T, ActiveOnly>::operator++() noexcept
	{
		if constexpr (ActiveOnly && HasActiveness<T>::value)
		{
			T* next = addSize(mCurrent, mSize);
			T* end_ = end();
			while (!next->IsActive() && next != end_)
			{
				next = addSize(next, mSize);
			}

			mCurrent = next;
			_ASSERT(mCurrent <= end_);
		}
		else
		{
			mCurrent = addSize(mCurrent, mSize);
		}
		
		return *this;
	}

	template<typename T, bool ActiveOnly>
	constexpr ObjectIterator<T, ActiveOnly> ObjectIterator<T, ActiveOnly>::operator++(int) noexcept
	{
		ObjectIterator<T, ActiveOnly> temp{ *this };
		++*this;
		return temp;
	}

	template<typename T, bool ActiveOnly>
	constexpr ObjectIterator<T, ActiveOnly>& ObjectIterator<T, ActiveOnly>::operator--() noexcept
	{
		if constexpr (ActiveOnly && HasActiveness<T>::value)
		{
			T* next = subtractSize(mCurrent, mSize);
			T* rend_ = rend();
			while (!next->IsActive() && next != rend_)
			{
				next = subtractSize(next, mSize);
			}

			mCurrent = next;
		}
		else
		{
			mCurrent = subtractSize(mCurrent, mSize);
		}
		
		return *this;
	}

	template<typename T, bool ActiveOnly>
	constexpr ObjectIterator<T, ActiveOnly> ObjectIterator<T, ActiveOnly>::operator--(int) noexcept
	{
		ObjectIterator<T, ActiveOnly> temp{ *this };
		--*this;
		return temp;
	}

	template<typename T, bool ActiveOnly>
	constexpr bool ObjectIterator<T, ActiveOnly>::operator==(const ObjectIterator<T, ActiveOnly>& rhs) const noexcept
	{
		return mCurrent == rhs.mCurrent;
	}

	template<typename T, bool ActiveOnly>
	constexpr bool ObjectIterator<T, ActiveOnly>::operator!=(const ObjectIterator<T, ActiveOnly>& rhs) const noexcept
	{
		return !(*this == rhs);
	}

	template<typename T, bool ActiveOnly>
	constexpr bool ObjectIterator<T, ActiveOnly>::operator<(const ObjectIterator<T, ActiveOnly>& rhs) const noexcept
	{
		return rhs - *this > 0;
	}

	template<typename T, bool ActiveOnly>
	constexpr bool ObjectIterator<T, ActiveOnly>::operator>(const ObjectIterator<T, ActiveOnly>& rhs) const noexcept
	{
		return rhs < *this;
	}

	template<typename T, bool ActiveOnly>
	constexpr bool ObjectIterator<T, ActiveOnly>::operator<=(const ObjectIterator<T, ActiveOnly>& rhs) const noexcept
	{
		return !(*this > rhs);
	}

	template<typename T, bool ActiveOnly>
	constexpr bool ObjectIterator<T, ActiveOnly>::operator>=(const ObjectIterator<T, ActiveOnly>& rhs) const noexcept
	{
		return !(*this < rhs);
	}

	
	template<typename T, bool ActiveOnly>
	constexpr T* ObjectIterator<T, ActiveOnly>::addSize(T* p, const size_t size) noexcept
	{
		return reinterpret_cast<T*>(reinterpret_cast<char*>(p) + size);
	}
	
	template<typename T, bool ActiveOnly>
	constexpr T* ObjectIterator<T, ActiveOnly>::subtractSize(T* p, const size_t size) noexcept
	{
		return reinterpret_cast<T*>(reinterpret_cast<char*>(p) - size);
	}
}
