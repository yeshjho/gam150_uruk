/*
    File Name: ObstacleObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "ObstacleObject.h"

#include "BoxColliderComponent.h"
#include "EnemyObject.h"
#include "TextureComponent.h"
#include "TransformComponent.h"



namespace uruk
{

	ObstacleObject::ObstacleObject(Vector3D cacheCoord)
		: cacheCoord(cacheCoord)
	{}

	void ObstacleObject::OnConstructEnd()
	{
		
		AddComponent<TransformComponent>(cacheCoord);

		AddComponent<TextureComponent>("Obstacle", EObjectZOrder::OBSTACLE);

		AddComponent<BoxColliderComponent>(Vector3D{ 0, 0 }, TILE_WIDTH, TILE_HEIGHT, CollisionTag::Obstacle, CollisionTag::Uruk, false);
		
	}

}
