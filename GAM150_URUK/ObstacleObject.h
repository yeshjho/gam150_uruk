/*
    File Name: ObstacleObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"
#include "Vector3D.h"



namespace uruk
{
	class ObstacleObject final : public Object<ObstacleObject>
	{
	public:
		ObstacleObject(Vector3D cacheCoord = { 0, 0 });

		void OnConstructEnd() override;


		
	private:
		Vector3D cacheCoord;
	};
}
