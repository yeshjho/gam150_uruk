/*
    File Name: PathfinderObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "PathfinderObject.h"
#include "FlowFieldPathfinderComponent.h"
#include "TransformComponent.h"



namespace uruk
{

	PathfinderObject::PathfinderObject() : cachePathfinderDimension({0, 0}), cachePosition({0, 0}) {}
	
	PathfinderObject::PathfinderObject(const Vector3D& pathFinderDimension, const Vector3D& position) :
	cachePathfinderDimension(pathFinderDimension), cachePosition(position) {}
	
	void PathfinderObject::OnConstructEnd()
	{
		AddComponent<FlowFieldPathfinderComponent>(cachePathfinderDimension);
		AddComponent<TransformComponent> (cachePosition); // For debug draw reference
	}
	
}
