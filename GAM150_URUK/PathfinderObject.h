/*
    File Name: PathfinderObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"
#include "Vector3D.h"



namespace uruk
{

	class PathfinderObject final : public Object<PathfinderObject>
	{
	public:
		PathfinderObject();
		PathfinderObject(const Vector3D& pathFinderDimension, const Vector3D& position);
		
		void OnConstructEnd() override;

	private:
		Vector3D cachePathfinderDimension;
		Vector3D cachePosition;
	};
	
}
