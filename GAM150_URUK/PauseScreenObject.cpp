/*
	File Name: EnemyAnimationStateMachine.cpp
	Project Name: U.R.U.K
	Author(s):
		Primary: Doyoon Kim
		Secondary: Joonho Hwang - restart logic
	All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/

#include "PauseScreenObject.h"
#include <doodle/doodle.hpp>
#include "AudioPlayerComponent.h"
#include "GameEnum.h"
#include "ClickableComponent.h"
#include "HoverableComponent.h"
#include "LevelLoaderComponent.h"
#include "PauseScreenVolumeSettingBarObject.h"
#include "TextureComponent.h"
#include "UIPositionAdjustComponent.h"
#include "doodle/drawing.hpp"
#include "MovementComponent.h"
#include "PlayerBehaviorComponent.h"
#include "PlayerObject.h"
#include "PlayerStatisticsComponent.h"
#include "SpawnPointObject.h"
#include "UICallbacks.h"

namespace uruk
{
	class BackgroundDrawSettingCallback final : public CustomCallbackBase<BackgroundDrawSettingCallback, ITextureComponentCallbackBase>
	{
	public:
		void Execute() override
		{
			doodle::set_tint_color(150, 50);
		}
	};
	
	void PauseScreenObject::OnConstructEnd()
	{
		AddComponent<LevelLoaderComponent>(std::string(""));
		
		AddComponent<TransformComponent>(Vector3D{ 0.f,0.f },true);

		AddComponent<AudioPlayerComponent>();
		
		ID pauseScreenTextureID = AddComponent<TextureComponent>("PauseScreen", EObjectZOrder::BUTTONS - 1);
		
		mBackgroundTextureID = AddComponent<TextureComponent>("StartScreenBackground",EObjectZOrder::CORRIDOR_FLOOR);
		mLevel->GetComponent<TextureComponent>(mBackgroundTextureID)->SetDrawSettings(std::make_unique<BackgroundDrawSettingCallback>());
		
		mRestartLevelTextureID = AddComponent<TextureComponent>("RestartLevel",EObjectZOrder::BUTTONS);
		TextureComponent* restartLevelTexture = mLevel->GetComponent<TextureComponent>(mRestartLevelTextureID);
		restartLevelTexture->SetOffset(Vector3D{ 538.f,730.f });
		
		mExitTextureID = AddComponent<TextureComponent>("Exit",EObjectZOrder::BUTTONS);
		TextureComponent* exitTexture = mLevel->GetComponent<TextureComponent>(mExitTextureID);
		exitTexture->SetOffset(Vector3D{ 847.f,410.f });

		mSetFullScreenTextureID = AddComponent<TextureComponent>("FullScreenCheckBoxUnchecked",EObjectZOrder::BUTTONS);
		TextureComponent* setFullscreenTexture = mLevel->GetComponent<TextureComponent>(mSetFullScreenTextureID);
		setFullscreenTexture->SetOffset(Vector3D{ 1760.f,120.f });

		ID exitButtonClickableComponentID = AddComponent<ClickableComponent>(270.f,79.f);
		ClickableComponent* exitButtonClickableComponent = mLevel->GetComponent<ClickableComponent>(exitButtonClickableComponentID);
		exitButtonClickableComponent->SetOffset(Vector3D{ 847.f,410.f });
		exitButtonClickableComponent->SetOnClicked(std::make_unique<PauseMenuExitClickCallback>());
		
		ID restartLevelButtonClickableComponentID = AddComponent<ClickableComponent>(844.f, 79.f);
		ClickableComponent* restartLevelButtonClickableComponent = mLevel->GetComponent<ClickableComponent>(restartLevelButtonClickableComponentID);
		restartLevelButtonClickableComponent->SetOffset(Vector3D{ 538.f,730.f });
		restartLevelButtonClickableComponent->SetOnClicked(std::make_unique<PauseMenuRestartLevelClickCallback>());
		
		ID fullScreenToggleButtonClickableComponentID = AddComponent<ClickableComponent>(48.f, 48.f);
		ClickableComponent* fullScreenToggleButtonClickableComponent = mLevel->GetComponent<ClickableComponent>(fullScreenToggleButtonClickableComponentID);
		fullScreenToggleButtonClickableComponent->SetOffset(Vector3D{ 1760.f,120.f });
		fullScreenToggleButtonClickableComponent->SetOnClicked(std::make_unique<PauseMenuToggleFullscreenClickCallback>());
		
		ID exitButtonHoverableComponentID = AddComponent<HoverableComponent>(270.f, 79.f);
		HoverableComponent* exitButtonHoverableComponent = mLevel->GetComponent<HoverableComponent>(exitButtonHoverableComponentID);
		exitButtonHoverableComponent->SetOffset(Vector3D{ 847.f,410.f });
		exitButtonHoverableComponent->SetOnHovered(std::make_unique<PauseMenuExitHoveredCallback>());
		exitButtonHoverableComponent->SetOnUnhovered(std::make_unique<PauseMenuExitUnHoveredCallback>());
		
		ID restartLevelButtonHoverableComponentID = AddComponent<HoverableComponent>(844.f, 79.f);
		HoverableComponent* restartLevelButtonHoverableComponent = mLevel->GetComponent<HoverableComponent>(restartLevelButtonHoverableComponentID);
		restartLevelButtonHoverableComponent->SetOffset(Vector3D{ 538.f,730.f });
		restartLevelButtonHoverableComponent->SetOnHovered(std::make_unique<PauseMenuRestartLevelHoveredCallback>());
		restartLevelButtonHoverableComponent->SetOnUnhovered(std::make_unique<PauseMenuRestartLevelUnHoveredCallback>());

		mPauseScreenSoundBarID = mLevel->AddObject<PauseScreenVolumeSettingBarObject>();

		AddComponent<UIPositionAdjustComponent>(
			std::vector<ID>{mBackgroundTextureID, mRestartLevelTextureID, mExitTextureID, mSetFullScreenTextureID, pauseScreenTextureID},
			std::vector<ID>{exitButtonClickableComponentID, restartLevelButtonClickableComponentID,fullScreenToggleButtonClickableComponentID},
			std::vector<ID>{exitButtonHoverableComponentID, restartLevelButtonHoverableComponentID}
		);
	}

	void PauseScreenObject::SetStartScreenID(const ID& startScreenID)
	{
		mStartScreenID = startScreenID;
	}

	void PauseScreenObject::SetBackgroundTexture(Texture&& texture)
	{
		mLevel->GetComponent<TextureComponent>(mBackgroundTextureID)->SetTexture(std::move(texture));
	}

	void PauseScreenObject::OnEnabled()
	{
		GetComponent<AudioPlayerComponent>()->Stop("m_ingame");
		TextureComponent* setFullscreenButtonTexture = mLevel->GetComponent<TextureComponent>(mSetFullScreenTextureID);
		if (doodle::is_full_screen())
		{
			setFullscreenButtonTexture->SetTexture("FullScreenCheckBoxChecked");
		}
		else
		{
			setFullscreenButtonTexture->SetTexture("FullScreenCheckBoxUnchecked");
		}
		PauseScreenVolumeSettingBarObject* pauseScreenVolumeSettingBar = mLevel->GetObject<PauseScreenVolumeSettingBarObject>(mPauseScreenSoundBarID);
		pauseScreenVolumeSettingBar->Enable();
		mLevel->GetObject<StartScreenObject>(mStartScreenID)->Disable();
		mLevel->GetObject<StartScreenObject>(mStartScreenID)->OnDisabled();
	}

	void PauseScreenObject::OnDisabled()
	{
		GetComponent<AudioPlayerComponent>()->Play("m_ingame");
		PauseScreenVolumeSettingBarObject* pauseScreenVolumeSettingBar = mLevel->GetObject<PauseScreenVolumeSettingBarObject>(mPauseScreenSoundBarID);
		pauseScreenVolumeSettingBar->Disable();
	}

	void PauseScreenObject::OnRestartLevelClicked()
	{
		LevelLoaderComponent* const levelLoader = GetComponent<LevelLoaderComponent>();

		Level* const level = Game::GetCurrentLevel();
		PlayerObject* const player = level->GetAllActiveObjects<PlayerObject>().begin();
		if (player->GetComponent<PlayerStatisticsComponent>()->GetCurrentHealth() <= 0)
		{
			return;
		}
		
		PlayerBehaviorComponent* const playerBehavior = level->GetAllActiveComponents<PlayerBehaviorComponent>().begin();
		const int levelIndex = playerBehavior->GetCurrentLevelIndex();

		const auto& mapToLoad = LevelLoaderComponent::FIRST_LEVEL_BY_FLOOR.at(levelIndex);
		std::apply(&LevelLoaderComponent::SetLevelToLoad, std::tuple_cat(std::make_tuple(levelLoader), mapToLoad));

		int playerHealth = -1;
		ID tempLevel;
		
		if (levelIndex == 1)
		{
			playerHealth = Game::GetCurrentLevel()->GetAllComponents<PlayerStatisticsComponent>().begin()->GetCurrentHealth();
		}
		else
		{
			tempLevel = Game::AddLevel(1_MB, 20);
			player->Emigrate(tempLevel);
		}
		
		LevelLoaderComponent::LoadLevelsOnFloor("levels/levels.txt", levelIndex);
		if (levelIndex != 1)
		{
			const ID emigrateTo = LevelLoaderComponent::GetLevelID(std::get<0>(mapToLoad));
			Level* const levelToLoad = Game::GetLevel(emigrateTo);
			
			for (SpawnPointObject& spawnPoint : levelToLoad->GetAllActiveObjects<SpawnPointObject>())
			{
				if (spawnPoint.GetIndex() == std::get<1>(mapToLoad))
				{
					const Vector3D newLocation = spawnPoint.GetComponent<TransformComponent>()->GetPosition();

					Game::GetLevel(tempLevel)->GetAllActiveObjects<PlayerObject>()->Emigrate(emigrateTo, newLocation);
					break;
				}
			}
			Game::RemoveLevel(tempLevel);
		}
		
		levelLoader->Load();

		if (levelIndex == 1)
		{
			const ID firstRoomID = LevelLoaderComponent::GetLevelID(std::get<0>(LevelLoaderComponent::FIRST_LEVEL_BY_FLOOR.at(1)));
			Level* const firstRoom = Game::GetLevel(firstRoomID);
			firstRoom->GetAllActiveComponents<PlayerStatisticsComponent>().begin()->SetHealth(playerHealth);
		}
		
		Disable();
		OnDisabled();
	}

	void PauseScreenObject::OnRestartLevelHovered()
	{
		mLevel->GetComponent<TextureComponent>(mRestartLevelTextureID)->SetTexture("RestartLevelHovered");
	}

	void PauseScreenObject::OnRestartLevelUnHovered()
	{
		mLevel->GetComponent<TextureComponent>(mRestartLevelTextureID)->SetTexture("RestartLevel");
	}

	void PauseScreenObject::OnExitClicked()
	{
		Disable();
		OnDisabled();
		StartScreenObject* startScreen = mLevel->GetObject<StartScreenObject>(mStartScreenID);
		startScreen->Enable();
		startScreen->OnEnabled();
		GetComponent<AudioPlayerComponent>()->Stop("m_ingame");
	}

	void PauseScreenObject::OnExitHovered()
	{
		mLevel->GetComponent<TextureComponent>(mExitTextureID)->SetTexture("ExitHovered");
	}

	void PauseScreenObject::OnExitUnHovered()
	{
		mLevel->GetComponent<TextureComponent>(mExitTextureID)->SetTexture("Exit");
	}

	void PauseScreenObject::OnToggleFullscreenClicked()
	{
		TextureComponent* setFullscreenButtonTexture = mLevel->GetComponent<TextureComponent>(mSetFullScreenTextureID);
		if (doodle::is_full_screen())
		{
			setFullscreenButtonTexture->SetTexture("FullScreenCheckBoxUnchecked");
		}
		else
		{
			setFullscreenButtonTexture->SetTexture("FullScreenCheckBoxChecked");
		}
		doodle::toggle_full_screen();
	}
}
