/*
	File Name: EnemyAnimationStateMachine.cpp
	Project Name: U.R.U.K
	Author(s):
		Primary: Doyoon Kim
		Secondary:
	All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once
#include "Object.h"

namespace doodle {
	class Image;
}

namespace uruk
{
	class Texture;

	class PauseScreenObject : public Object<PauseScreenObject>
	{
	public:
		void OnConstructEnd() override;
		void SetStartScreenID(const ID& startScreenID);
		void SetBackgroundTexture(Texture&& texture);
		void OnEnabled();
		void OnDisabled();

		void OnRestartLevelClicked();
		void OnRestartLevelHovered();
		void OnRestartLevelUnHovered();

		void OnExitClicked();
		void OnExitHovered();
		void OnExitUnHovered();

		void OnToggleFullscreenClicked();
	private:
		ID mStartScreenID;
		ID mBackgroundTextureID;
		ID mRestartLevelTextureID;
		ID mExitTextureID;
		ID mSetFullScreenTextureID;
		ID mPauseScreenSoundBarID;
	};
}
