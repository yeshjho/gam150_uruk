/*
	File Name: EnemyAnimationStateMachine.cpp
	Project Name: U.R.U.K
	Author(s):
		Primary: Doyoon Kim
		Secondary:
	All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/

#include "PauseScreenVolumeSettingBarObject.h"
#include "DraggableComponent.h"
#include "GameEnum.h"
#include "InputComponent.h"
#include "TextureComponent.h"
#include "TransformComponent.h"
#include "UICallbacks.h"
#include "UIPositionAdjustComponent.h"

namespace uruk
{
	void PauseScreenVolumeSettingBarObject::OnConstructEnd()
	{
		AddComponent<InputComponent>();
		AddComponent<TransformComponent>(Vector3D{ 0.f,0.f }, true);
		const ID textureID = AddComponent<TextureComponent>("StartScreenSoundbar", EObjectZOrder::BUTTONS);
		GetComponent<TextureComponent>()->SetOffset(Vector3D{ 550.f,130.f });
		const ID dragID = AddComponent<DraggableComponent>(10.f, 30.f);
		GetComponent<DraggableComponent>()->SetOnMoved(std::make_unique<PauseMenuVolumeSettingBarDragMovedCallback>());
		GetComponent<DraggableComponent>()->SetOffset(Vector3D{ 550.f,130.f });

		AddComponent<UIPositionAdjustComponent>(
			std::vector<ID>{ textureID }, std::vector<ID>{}, std::vector<ID>{}, std::vector<ID>{ dragID }
		);
	}
} 