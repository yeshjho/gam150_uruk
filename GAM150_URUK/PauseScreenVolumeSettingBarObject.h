/*
    File Name: EnemyAnimationStateMachine.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary:
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once
#include "Object.h"

namespace uruk
{
	class PauseScreenVolumeSettingBarObject : public Object<PauseScreenVolumeSettingBarObject>
	{
	public:
		void OnConstructEnd() override;
	};
}

