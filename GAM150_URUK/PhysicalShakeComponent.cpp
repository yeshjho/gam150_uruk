/*
    File Name: PhysicalShakeComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "PhysicalShakeComponent.h"

#include "Math.h"
#include "Serializer.h"
#include "TransformComponent.h"



namespace uruk
{
	void PhysicalShakeComponent::OnConstructEnd()
	{
		Disable();
	}


	void PhysicalShakeComponent::OnUpdate()
	{
		mElapsedTime += mLevel->GetDeltaTimeInMillisecond();
		
		const float offsetAmount = mIntensity * calculateOffsetAmount(mIntensityDecreaseRate, mRepeat, constants::TWO_PI * inverseLerp(0.f, mDuration, mPElapsedTime), constants::TWO_PI * inverseLerp(0.f, mDuration, mElapsedTime));

		mPElapsedTime = mElapsedTime;

		Vector3D offset{ 0, 0 };
		switch (mDirection)
		{
			case EShakeDirection::HORIZONTAL:
				offset = { offsetAmount, 0.f };
				break;

			case EShakeDirection::VERTICAL:
				offset = { 0.f, offsetAmount };
				break;

			case EShakeDirection::SLASH:
				offset = { offsetAmount, offsetAmount };
				break;

			case EShakeDirection::BACKSLASH:
				offset = { offsetAmount, -offsetAmount };
				break;

			default:
				break;
		}

		GetOwner()->GetComponent<TransformComponent>()->MoveBy(offset);


		if (mElapsedTime >= mDuration)
		{
			Disable();
		}
	}


	void PhysicalShakeComponent::Shake(const EShakeDirection direction, const float intensity, const float intensityDecreaseRate, const unsigned int repeat, const float duration)
	{
		// If it's in the middle of shaking, return.
		if (IsActive())
		{
			return;
		}

		mDirection = direction;
		mIntensity = intensity;
		mIntensityDecreaseRate = clamp(intensityDecreaseRate / 100.f, -0.1f, 1.f);
		mRepeat = repeat;
		mDuration = duration;
		mError = dampedSineSecondDerivative(mIntensityDecreaseRate, mRepeat, 0.f) - dampedSineSecondDerivative(mIntensityDecreaseRate, mRepeat, constants::TWO_PI);
		mElapsedTime = 0.f;
		mPElapsedTime = 0.f;

		Enable();
	}


	void PhysicalShakeComponent::OnSave(std::ofstream& outputFileStream) const
	{
		if (!IsActive())
		{
			return;
		}
		
		serialize(outputFileStream, mDirection);
		serialize(outputFileStream, mIntensity);
		serialize(outputFileStream, mIntensityDecreaseRate);
		serialize(outputFileStream, mRepeat);
		serialize(outputFileStream, mDuration);
		serialize(outputFileStream, mError);
		serialize(outputFileStream, mElapsedTime);
		serialize(outputFileStream, mPElapsedTime);
	}

	void PhysicalShakeComponent::OnLoad(std::ifstream& inputFileStream)
	{
		if (!IsActive())
		{
			return;
		}

		deserialize(inputFileStream, mDirection);
		deserialize(inputFileStream, mIntensity);
		deserialize(inputFileStream, mIntensityDecreaseRate);
		deserialize(inputFileStream, mRepeat);
		deserialize(inputFileStream, mDuration);
		deserialize(inputFileStream, mError);
		deserialize(inputFileStream, mElapsedTime);
		deserialize(inputFileStream, mPElapsedTime);
	}


	float PhysicalShakeComponent::calculateOffsetAmount(float decreaseRate, const unsigned int repeat, const float alpha, const float beta) const
	{
		const float correction = mError / mDuration * (clamp(mElapsedTime, 0.f, mDuration) - clamp(mPElapsedTime, 0.f, mDuration));

		return dampedSineSecondDerivative(decreaseRate, repeat, beta) - dampedSineSecondDerivative(decreaseRate, repeat, alpha) + correction;
	}
}
