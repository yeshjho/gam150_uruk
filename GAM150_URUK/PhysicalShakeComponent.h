/*
    File Name: PhysicalShakeComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"

#include "ShakeDirection.h"



namespace uruk
{
	class PhysicalShakeComponent final : public Component<PhysicalShakeComponent, EUpdateType::BY_MILLISECOND, update_priorities::LOWER_PRIORITY, 0>
	{
	public:
		void OnConstructEnd() override;


		void OnUpdate() override;


		// interval and duration are in millisecond.
		void Shake(EShakeDirection direction, float intensity, float intensityDecreaseRate, unsigned int repeat, float duration);


		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;
		
	private:
		[[nodiscard]] float calculateOffsetAmount(float decreaseRate, unsigned int repeat, float alpha, float beta) const;



	private:
		EShakeDirection mDirection = EShakeDirection::HORIZONTAL;
		float mIntensity = 0.f;
		float mIntensityDecreaseRate = 0.f;
		unsigned int mRepeat = 0;
		float mDuration = 0.f;
		float mError = 0.f;
		float mElapsedTime = 0.f;
		float mPElapsedTime = 0.f;
	};
}
