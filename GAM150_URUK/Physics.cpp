/*
    File Name: Physics.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "Physics.h"
#include "BoxColliderComponent.h"
#include "CameraObject.h"
#include "Game.h"
#include "Level.h"
#include "MovementComponent.h"
#include "SphereColliderComponent.h"
#include "TransformComponent.h"
#include "Line.h"
#ifdef UI_DEBUG_DRAW
#include "doodle/drawing.hpp"
#endif

namespace uruk
{

	
	void Physics::ResolveBoxBoxCollision(IColliderComponent* a, IColliderComponent* b)
	{
		float dt = Game::GetCurrentLevel()->GetDeltaTimeInMillisecond() / 1000.f;
		BoxColliderComponent* A = static_cast<BoxColliderComponent*>(a);
		TransformComponent* AParentTransform = A->GetOwner()->GetComponent<TransformComponent>();
		MovementComponent* AMovementComponent = nullptr;
		Vector3D AVelocity = { 0.f,0.f,0.f };
		if (A->GetOwner()->HasComponent(MovementComponent::TYPE_ID))
		{
			AMovementComponent = A->GetOwner()->GetComponent<MovementComponent>();
			AVelocity = AMovementComponent->GetVelocity();
		}
		
		BoxColliderComponent* B = static_cast<BoxColliderComponent*>(b);
		TransformComponent* BParentTransform = B->GetOwner()->GetComponent<TransformComponent>();

		Vector3D ABottomLeft = AParentTransform->GetPosition() + A->mOffset;
		Vector3D ABottomRight = ABottomLeft + Vector3D{ A->mWidth,0.f };
		Vector3D ATopRight = ABottomLeft + Vector3D{ A->mWidth,A->mHeight };
		Vector3D ATopLeft = ATopRight - Vector3D{ A->mWidth,0.f };

		Line ATopRightTrace(ATopRight, ATopRight + AVelocity*dt);
		Line ATopLeftTrace(ATopLeft, ATopLeft + AVelocity*dt);
		Line ABottomRightTrace(ABottomRight, ABottomRight + AVelocity*dt);
		Line ABottomLeftTrace(ABottomLeft, ABottomLeft + AVelocity*dt);
		std::vector<Line> APointsATraces{ ATopRightTrace,ATopLeftTrace,ABottomRightTrace,ABottomLeftTrace };
		
		Vector3D BBottomLeft = BParentTransform->GetPosition() + B->mOffset;
		Vector3D BBottomRight = BBottomLeft + Vector3D{ B->mWidth,0.f };
		Vector3D BTopRight = BBottomLeft + Vector3D{ B->mWidth,B->mHeight };
		Vector3D BTopLeft = BTopRight - Vector3D{ B->mWidth,0.f };
		
		//clockwise
		Line BTop(BTopLeft,BTopRight);
		Line BRight(BTopRight, BBottomRight);
		Line BBottom(BBottomRight, BBottomLeft);
		Line BLeft(BBottomLeft, BTopLeft);
		std::vector<Line> BEdges{ BTop, BRight, BBottom, BLeft };
		
		bool doesCollide = false;
		float minimumSqrDistance = INFINITY;
		for(const Line& APointTrace : APointsATraces)
		{
			for( const Line& BEdge : BEdges)
			{
				auto [isIntersecting, intersectingPoint] = BEdge.GetIntersectingPoint(APointTrace);
				if(isIntersecting)
				{
					doesCollide = true;
					float sqrDistance = (intersectingPoint - APointTrace.BeginPoint()).GetLengthSqr();
					if(sqrDistance < minimumSqrDistance)
					{
						minimumSqrDistance = sqrDistance;
					}
				}
			}
		}

		doesCollide |= (ABottomLeftTrace.EndPoint().Y() < BTopLeft.Y() && ATopLeftTrace.EndPoint().Y() > BBottomLeft.Y() &&
						ABottomLeftTrace.EndPoint().X() < BTopRight.X() && ATopRightTrace.EndPoint().X() > BBottomLeft.X());
		
		if(doesCollide)
		{
			float ratio = sqrt(minimumSqrDistance)/AVelocity.GetLength();
			if(A->IsTriggerCollider() == false)//if it's not just a trigger, adjust position after collision so they don't overlap.
			{
				if(minimumSqrDistance != INFINITY && minimumSqrDistance > 2.f)//collision is detected by velocity trace and minimumsqr distance is not too small
				{
					AParentTransform->MoveBy(AVelocity * ratio * 0.8f);
					
				}

				if (AMovementComponent != nullptr)
				{
					AMovementComponent->SetVelocity(AVelocity * -0.1f);
				}	
			}
			if(A->WasCollidingWith(B->GetColliderID(),B->GetColliderType()) == false)
			{
				A->OnCollisionEnter(B);
			}
			else
			{
				A->OnCollisionGoing(B);
			}
		}
		else
		{
			if(A->WasCollidingWith(B->GetColliderID(), B->GetColliderType()) == true)
			{
				A->OnCollisionEnd(B);
			}
		}
	}

	void Physics::ResolveBoxSphereCollision(IColliderComponent* box, IColliderComponent* sphere)
	{
		float dt = Game::GetCurrentLevel()->GetDeltaTimeInMillisecond();
		
		BoxColliderComponent* rect = static_cast<BoxColliderComponent*>(box);
		Vector3D rectVelocity = { 0.f,0.f,0.f };
		if (rect->GetOwner()->HasComponent(MovementComponent::TYPE_ID))
		{
			MovementComponent* AMovementComponent = rect->GetOwner()->GetComponent<MovementComponent>();
			rectVelocity = AMovementComponent->GetVelocity();
		}
		Vector3D rectBottomLeft = rect->GetOwner()->GetComponent<TransformComponent>()->GetPosition() + rect->mOffset;
		Vector3D rectNextBottomLeft = rectBottomLeft + rectVelocity * dt;

		
		SphereColliderComponent* circle = static_cast<SphereColliderComponent*>(sphere);
		Vector3D circlePosition = circle->GetOwner()->GetComponent<TransformComponent>()->GetPosition() + circle->mOffset;

		//difference of the center of each shapes in each axis.
		float dx = abs((rectNextBottomLeft.X() + rect->mWidth / 2) - circlePosition.X());
		float dy = abs((rectNextBottomLeft.Y() + rect->mHeight / 2) - circlePosition.Y());
		bool isColliding = false;

		if (dx > (rect->mWidth / 2 + circle->mRadius) || dy > (rect->mHeight / 2 + circle->mRadius))
		{
			isColliding = false;
		}
		else if (dx <= rect->mWidth / 2 || dy <= rect->mHeight / 2)
		{
			isColliding = true;
		}
		else
		{
			float sqrDist = pow(dx - rect->mWidth / 2, 2) + pow(dy - rect->mHeight / 2, 2);
			if (sqrDist <= circle->mRadius * circle->mRadius)
			{
				isColliding = true;
			}
		}

		if (isColliding == true)
		{
			if (rect->WasCollidingWith(circle->GetColliderID(),circle->GetColliderType()) == false)
			{
				rect->OnCollisionEnter(circle);
			}
			else
			{
				rect->OnCollisionGoing(circle);
			}
		}
		else
		{
			if (rect->WasCollidingWith(circle->GetColliderID(),circle->GetColliderType()) == true)
			{
				rect->OnCollisionEnd(circle);
			}
		}
	}

	void Physics::ResolveSphereBoxCollision(IColliderComponent* sphere, IColliderComponent* box)
	{
		float dt = Game::GetCurrentLevel()->GetDeltaTimeInMillisecond();
		
		SphereColliderComponent* circle = static_cast<SphereColliderComponent*>(sphere);
		Vector3D circleVelocity = { 0.f,0.f,0.f };
		if (circle->GetOwner()->HasComponent(MovementComponent::TYPE_ID))
		{
			MovementComponent* movementComponent = circle->GetOwner()->GetComponent<MovementComponent>();
			circleVelocity = movementComponent->GetVelocity();
		}
		Vector3D circleNextPosition = circle->GetOwner()->GetComponent<TransformComponent>()->GetPosition() + circle->mOffset + circleVelocity * dt;

		BoxColliderComponent* rect = static_cast<BoxColliderComponent*>(box);
		Vector3D rectBottomLeft = rect->GetOwner()->GetComponent<TransformComponent>()->GetPosition() + rect->mOffset;

		//difference of the center of each shapes in each axis.
		float dx = abs((rectBottomLeft.X() + rect->mWidth / 2) - circleNextPosition.X());
		float dy = abs((rectBottomLeft.Y() + rect->mHeight / 2) - circleNextPosition.Y());
		bool isColliding = false;
		
		if(dx > (rect->mWidth/2 + circle->mRadius) || dy > (rect->mHeight/2 + circle->mRadius))
		{
			isColliding = false;
		}
		else if(dx <= rect->mWidth/2 || dy <= rect->mHeight/2)
		{
			isColliding = true;
		}
		else
		{
			float sqrDist = pow(dx - rect->mWidth / 2, 2) + pow(dy - rect->mHeight / 2, 2);
			if (sqrDist <= circle->mRadius * circle->mRadius)
			{
				isColliding = true;
			}
		}

		if(isColliding == true)
		{
			if(circle->WasCollidingWith(rect->GetColliderID(),rect->GetColliderType()) == false)
			{
				circle->OnCollisionEnter(rect);
			}
			else
			{
				circle->OnCollisionGoing(rect);
			}
		}
		else
		{
			if(circle->WasCollidingWith(rect->GetColliderID(),rect->GetColliderType()) == true)
			{
				circle->OnCollisionEnd(rect);
			}
		}
	}

	void Physics::ResolveSphereSphereCollision(IColliderComponent* a, IColliderComponent* b)
	{
		SphereColliderComponent* A = static_cast<SphereColliderComponent*>(a);
		Vector3D ANextPosition = A->GetOwner()->GetComponent<TransformComponent>()->GetPosition() + A->mOffset;
		
		SphereColliderComponent* B = static_cast<SphereColliderComponent*>(b);
		Vector3D BPosition = B->GetOwner()->GetComponent<TransformComponent>()->GetPosition() + B->mOffset;
		
		if(pow(BPosition.X() - ANextPosition.X(),2) + pow(BPosition.Y()- ANextPosition.Y(),2) < pow(A->mRadius+B->mRadius,2))
		{
			if(A->WasCollidingWith(B->GetColliderID(),B->GetColliderType()) == false)
			{
				A->OnCollisionEnter(B);
			}
			else
			{
				A->OnCollisionGoing(B);
			}
		}
		else
		{
			if(A->WasCollidingWith(B->GetColliderID(),B->GetColliderType()) == true)
			{
				A->OnCollisionEnd(B);
			}
		}
	}

	void Physics::Update()
	{
		mPColliders.clear();
	  	Level* currLevel = Game::GetCurrentLevel();
		auto boxColliders = currLevel->GetAllActiveComponents<BoxColliderComponent>();
		auto sphereColliders = currLevel->GetAllActiveComponents<SphereColliderComponent>();
		
		for(auto it = boxColliders.begin(); it != boxColliders.end(); ++it)
		{
			mPColliders.push_back(&(*it));
		}
		for(auto it = sphereColliders.begin(); it != sphereColliders.end(); ++it)
		{
			mPColliders.push_back(&(*it));
		}

		for(int i = 0; i < mPColliders.size(); i++)
		{
			IColliderComponent* A = mPColliders[i];
			for(int j = 0; j < mPColliders.size(); j++)
			{
				IColliderComponent* B = mPColliders[j];
				if(A == B || (size_t(A->GetCollisionFilter()) & size_t(B->GetCollisionTag())) == 0)
				{
					continue;
				}
				mCollisionResolveFunctions[{A->GetColliderType(),B->GetColliderType()}](A, B);
			}
		}
	}
}
