/*
    File Name: Physics.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
  #pragma once
#include "Internal.h"
#include <unordered_map>
#include "IColliderComponent.h"

namespace uruk
{
	class BoxColliderComponent;
	class SphereColliderComponent;

	struct pair_hash
	{
		template<typename T1,typename T2>
		size_t operator() (const std::pair<T1,T2>& pair) const
		{
			return std::hash<T1>()(pair.first) ^ std::hash<T2>()(pair.second);
		}
	};
	
	class Physics
	{
	public:
		static void Update();
		static void ResolveBoxBoxCollision(IColliderComponent* a,IColliderComponent* b);
		static void ResolveBoxSphereCollision(IColliderComponent* box, IColliderComponent* sphere);
		static void ResolveSphereBoxCollision(IColliderComponent* sphere, IColliderComponent* box);
		static void ResolveSphereSphereCollision(IColliderComponent* a, IColliderComponent* b);
	private:
		inline static std::vector<IColliderComponent*> mPColliders;
		inline static  std::unordered_map < std::pair<ColliderType, ColliderType>, std::function<void(IColliderComponent*, IColliderComponent*)>,pair_hash> mCollisionResolveFunctions = { {{ColliderType::BoxCollider,ColliderType::BoxCollider},Physics::ResolveBoxBoxCollision},
																																														   {{ColliderType::BoxCollider,ColliderType::SphereCollider},Physics::ResolveBoxSphereCollision},
																																														   {{ColliderType::SphereCollider,ColliderType::BoxCollider},Physics::ResolveSphereBoxCollision},
																																														   {{ColliderType::SphereCollider,ColliderType::SphereCollider},Physics::ResolveSphereSphereCollision}
																																														  };
	};
	
}