/*
    File Name: PlayerAnimationStateMachine.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "PlayerAnimationStateMachine.h"

#include "AnimationComponent.h"
#include "Game.h"
#include "Level.h"
#include "PlayerBehaviorComponent.h"
#include "SpriteMeshComponent.h"
#include "TextureComponent.h"


namespace uruk
{
	PlayerAnimationStateMachine::PlayerAnimationStateMachine()
		: CustomStateMachineBase<PlayerAnimationStateMachine>(Anim_Idle_None,
			{
				StateTransitionConditionsPair
				{
					PlayerAnimationStateMachineStates::Anim_Idle_None,
					{
						TransitionCondition
						{
							PlayerAnimationStateMachineStates::Anim_Moving_None,
							[](ID,TypeID)
							{
								Level* currentLevel = Game::GetCurrentLevel();
								PlayerBehaviorComponent& playerBehaviorComponent = currentLevel->GetAllComponents<PlayerBehaviorComponent>()[0];
								if (playerBehaviorComponent.GetState() == Moving_None)
								{
									return true;
								}
								return false;
							}
						},
						TransitionCondition
						{
							PlayerAnimationStateMachineStates::Anim_Idle_Lantern,
							[](ID ownerID,TypeID)
							{
								Level* currentLevel = Game::GetCurrentLevel();
								PlayerBehaviorComponent& playerBehaviorComponent = currentLevel->GetAllComponents<PlayerBehaviorComponent>()[0];
								AnimationComponent* animationComponent = currentLevel->GetComponent<AnimationComponent>(ownerID);
								if (playerBehaviorComponent.GetState() == Idle_Lantern)
								{
									SpriteMeshComponent* spriteMeshComponent = animationComponent->GetOwner()->GetComponent<SpriteMeshComponent>();
									const std::vector<ID> textureComponentIDs = spriteMeshComponent->GetTextureComponentIDs();
									//will swap player upper body's texture and it is the fifth texture component.
									TextureComponent* textureComponent = currentLevel->GetComponent<TextureComponent>(textureComponentIDs[5]);
									textureComponent->SetTexture("UrukLanternUpperBody");
									textureComponent->SetOffset(textureComponent->GetOffset() + Vector3D{ 0.f,-30.f });
									return true;
								}
								return false;
							}
						},
						TransitionCondition
						{
							PlayerAnimationStateMachineStates::Anim_Idle_Sword,
							[](ID ownerID,TypeID)
							{
								Level* currentLevel = Game::GetCurrentLevel();
								PlayerBehaviorComponent& playerBehaviorComponent = currentLevel->GetAllComponents<PlayerBehaviorComponent>()[0];
								AnimationComponent* animationComponent = currentLevel->GetComponent<AnimationComponent>(ownerID);
								if (playerBehaviorComponent.GetState() == Idle_Sword)
								{
									SpriteMeshComponent* spriteMeshComponent = animationComponent->GetOwner()->GetComponent<SpriteMeshComponent>();
									const std::vector<ID> textureComponentIDs = spriteMeshComponent->GetTextureComponentIDs();
									//will swap player upper body's texture and it is the fifth texture component.
									TextureComponent* textureComponent = currentLevel->GetComponent<TextureComponent>(textureComponentIDs[5]);
									textureComponent->SetTexture("UrukSwordUpperBody");
									if(playerBehaviorComponent.IsFlipped())
									{
										textureComponent->SetOffset(textureComponent->GetOffset() + Vector3D{ 20.f,0.f });
									}
									else
									{
										textureComponent->SetOffset(textureComponent->GetOffset() + Vector3D{ -20.f,0.f });
									}
									return true;
								}
								return false;
							}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerAnimationStateMachineStates::Anim_Idle_Sword,
					{
						TransitionCondition
						{
							PlayerAnimationStateMachineStates::Anim_Idle_None,
							[](ID ownerID,TypeID)
							{
								Level* currentLevel = Game::GetCurrentLevel();
								PlayerBehaviorComponent& playerBehaviorComponent = currentLevel->GetAllComponents<PlayerBehaviorComponent>()[0];
								AnimationComponent* animationComponent = currentLevel->GetComponent<AnimationComponent>(ownerID);
								if (playerBehaviorComponent.GetState() == Idle_None)
								{
									SpriteMeshComponent* spriteMeshComponent = animationComponent->GetOwner()->GetComponent<SpriteMeshComponent>();
									const std::vector<ID> textureComponentIDs = spriteMeshComponent->GetTextureComponentIDs();
									//will swap player upper body's texture and it is the fifth texture component.
									TextureComponent* textureComponent = currentLevel->GetComponent<TextureComponent>(textureComponentIDs[5]);
									textureComponent->SetTexture("UrukNoneUpperBody");
									if(playerBehaviorComponent.IsFlipped())
									{
										textureComponent->SetOffset(textureComponent->GetOffset() + Vector3D{ -20.f,0.f });
									}
									else
									{
										textureComponent->SetOffset(textureComponent->GetOffset() + Vector3D{ 20.f,0.f });
									}
									return true;
								}
								return false;
							}
						},
						TransitionCondition
						{
							PlayerAnimationStateMachineStates::Anim_Moving_Sword,
							[](ID,TypeID)
							{
								Level* currentLevel = Game::GetCurrentLevel();
								PlayerBehaviorComponent& playerBehaviorComponent = currentLevel->GetAllComponents<PlayerBehaviorComponent>()[0];
								if (playerBehaviorComponent.GetState() == Moving_Sword)
								{
									return true;
								}
								return false;
							}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerAnimationStateMachineStates::Anim_Idle_Lantern,
					{
						TransitionCondition
						{
							PlayerAnimationStateMachineStates::Anim_Idle_None,
							[](ID ownerID,TypeID)
							{
								Level* currentLevel = Game::GetCurrentLevel();
								PlayerBehaviorComponent& playerBehaviorComponent = currentLevel->GetAllComponents<PlayerBehaviorComponent>()[0];
								AnimationComponent* animationComponent = currentLevel->GetComponent<AnimationComponent>(ownerID);
								if (playerBehaviorComponent.GetState() == Idle_None)
								{
									SpriteMeshComponent* spriteMeshComponent = animationComponent->GetOwner()->GetComponent<SpriteMeshComponent>();
									const std::vector<ID> textureComponentIDs = spriteMeshComponent->GetTextureComponentIDs();
									//will swap player upper body's texture and it is the fifth texture component.
									TextureComponent* textureComponent = currentLevel->GetComponent<TextureComponent>(textureComponentIDs[5]);
									textureComponent->SetTexture("UrukNoneUpperBody");
									textureComponent->SetOffset(textureComponent->GetOffset() + Vector3D{ 0.f,30.f });

									return true;
								}
								return false;
							}
						},
						TransitionCondition
						{
							PlayerAnimationStateMachineStates::Anim_Moving_Lantern,
							[](ID,TypeID)
							{
								Level* currentLevel = Game::GetCurrentLevel();
								PlayerBehaviorComponent& playerBehaviorComponent = currentLevel->GetAllComponents<PlayerBehaviorComponent>()[0];
								if (playerBehaviorComponent.GetState() == Moving_Lantern)
								{
									return true;
								}
								return false;
							}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerAnimationStateMachineStates::Anim_Moving_None,
					{
						TransitionCondition
						{
							PlayerAnimationStateMachineStates::Anim_Idle_None,
							[](ID,TypeID)
							{
								Level* currentLevel = Game::GetCurrentLevel();
								PlayerBehaviorComponent& playerBehaviorComponent = currentLevel->GetAllComponents<PlayerBehaviorComponent>()[0];
								if (playerBehaviorComponent.GetState() == Idle_None)
								{
									return true;
								}
								return false;
							}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerAnimationStateMachineStates::Anim_Moving_Sword,
					{
						TransitionCondition
						{
							PlayerAnimationStateMachineStates::Anim_Idle_Sword,
							[](ID,TypeID)
							{
								Level* currentLevel = Game::GetCurrentLevel();
								PlayerBehaviorComponent& playerBehaviorComponent = currentLevel->GetAllComponents<PlayerBehaviorComponent>()[0];
								if (playerBehaviorComponent.GetState() == Idle_Sword)
								{
									return true;
								}
								return false;
							}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerAnimationStateMachineStates::Anim_Moving_Lantern,
					{
						TransitionCondition
						{
							PlayerAnimationStateMachineStates::Anim_Idle_Lantern,
							[](ID,TypeID)
							{
								Level* currentLevel = Game::GetCurrentLevel();
								PlayerBehaviorComponent& playerBehaviorComponent = currentLevel->GetAllComponents<PlayerBehaviorComponent>()[0];
								if (playerBehaviorComponent.GetState() == Idle_Lantern)
								{
									return true;
								}
								return false;
							}
						},
					}
				},
			}
		)
	{}
}
