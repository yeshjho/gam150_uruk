/*
    File Name: PlayerAnimationStateMachine.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "StateMachine.h"



namespace uruk
{
	enum PlayerAnimationStateMachineStates
	{
		Anim_Idle_None,
		Anim_Idle_Sword,
		Anim_Idle_Lantern,
		Anim_Moving_None,
		Anim_Moving_Sword,
		Anim_Moving_Lantern,
	};


	class PlayerAnimationStateMachine : public CustomStateMachineBase<PlayerAnimationStateMachine>
	{
	public:
		PlayerAnimationStateMachine();
	};
}
