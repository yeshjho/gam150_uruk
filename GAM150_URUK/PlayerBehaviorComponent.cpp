/*
    File Name: PlayerBehaviorComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: Joonho Hwang, Doyoon Kim, Yeongju Lee
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "PlayerBehaviorComponent.h"


#include "AnimationComponent.h"
#include "CameraObject.h"
#include "AudioPlayerComponent.h"
#include "PlayerObject.h"
#include "ShakeDirection.h"
#include "FishGateObject.h"
#include "PlayerAnimationStateMachine.h"
#include "FlowFieldPathfinderComponent.h"
#include "InputComponent.h"
#include "PlayerStatisticsComponent.h"
#include "TextureComponent.h"
#include "PlayerStateMachine.h"
#include "MovementComponent.h"
#include "Game.h"
#include "LevelLoaderComponent.h"
#include "SpriteMeshComponent.h"


namespace uruk
{
	PlayerBehaviorComponent::PlayerBehaviorComponent(ID pathFinderID) : mPathfinderID(pathFinderID)
	{
		
	}


	
	void PlayerBehaviorComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mPathfinderID);
	}

	IComponent* PlayerBehaviorComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		ID pathFinderID = ID();

		deserialize(inputFileStream, pathFinderID);
		
		return level->LoadComponent<PlayerBehaviorComponent>(id, pathFinderID);
	}


	void PlayerBehaviorComponent::OnSave(std::ofstream& outputFileStream) const
	{
		mStateMachine.OnSave(outputFileStream);
		serialize(outputFileStream, mPlayerSingleTextureID);
		serialize(outputFileStream, mCurrentLevelIndex);
	}


	void PlayerBehaviorComponent::OnLoad(std::ifstream& inputFileStream)
	{
		mStateMachine.OnLoad(inputFileStream);
		deserialize(inputFileStream, mPlayerSingleTextureID);
		deserialize(inputFileStream, mCurrentLevelIndex);
	}



	void PlayerBehaviorComponent::OnBeginPlay()
	{
		mStateMachine.SetOwner(GetID(), GetTypeID());

		GetOwner()->GetComponent<AudioPlayerComponent>()->Stop("m_ingame");
		GetOwner()->GetComponent<AudioPlayerComponent>()->Play("m_ingame");

		BindMovementKeys();
	}

	void PlayerBehaviorComponent::OnUpdate()
	{

		if (!IsAlive())
		{
			return;
		}

		TransformComponent* playerTransform = GetOwner()->GetComponent<TransformComponent>();

		const Vector3D currentGridPosition = FlowFieldPathfinderComponent::TranslateToUnitVector(playerTransform->GetPosition());

		// Update pathfinder when previous grid position does not match current grid position
		if (!(mPreviousGridPosition == currentGridPosition)) 
		{
			FlowFieldPathfinderComponent* pathfinder = mLevel->GetComponent<FlowFieldPathfinderComponent>(mPathfinderID);
			
			pathfinder->Reset();
			pathfinder->SetGoalNode(currentGridPosition);
			pathfinder->GenerateHeatMap();
			pathfinder->CalculateFlowField();
		}

		mPreviousGridPosition = FlowFieldPathfinderComponent::TranslateToUnitVector(playerTransform->GetPosition());

		

		mStateMachine.Update();

		SpriteMeshComponent* spriteMesh = GetOwner()->GetComponent<SpriteMeshComponent>();
		TextureComponent* singleTexture = mLevel->GetComponent<TextureComponent>(mPlayerSingleTextureID);
		
		if (mInvincibilityTimer > 0)
		{
			//GetOwner()->GetComponent<HealthBarComponent>()->Blink(); make it blink?
			singleTexture->SetActive(!singleTexture->IsActive());
			mInvincibilityTimer -= mLevel->GetDeltaTimeInMillisecond();
			spriteMesh->SetActive(!spriteMesh->IsActive());
		}
		else 
		{
			//GetOwner()->GetComponent<HealthBarComponent>()->SetActive(true);
			singleTexture->Enable();
			spriteMesh->Enable();
		}


		if (mIsMoving)
		if (mWalkTimer > 0)
		{
			mWalkTimer -= mLevel->GetDeltaTimeInMillisecond();
		}
		else
		{
			mWalkTimer = mWalkDuration;
			GetOwner()->GetComponent<AudioPlayerComponent>()->Play("walk_player");
		}
		

		if (mHitTimer > 0)
		{
			mHitTimer -= mLevel->GetDeltaTimeInMillisecond();
		}

		// Move actiontimer to here

		
		
		mStateMachine.Update();

		mSwordDrawOffset = 0; // Maybe should not be here, but just for now...

		switch (mStateMachine.GetCurrentState()) {
		case PlayerStateMachineStates::Hit_None:
			singleTexture->SetTexture("HitNone");
			spriteMesh->Disable();
			break;
		case PlayerStateMachineStates::Hit_Sword:
			singleTexture->SetTexture("HitSword");
			spriteMesh->Disable();
			break;
		case PlayerStateMachineStates::Hit_Lantern:
			singleTexture->SetTexture("HitLantern");
			spriteMesh->Disable();
			break;
		case PlayerStateMachineStates::Player_Ready:
			singleTexture->SetTexture("Ready");
			spriteMesh->Disable();
			break;
		case PlayerStateMachineStates::Player_Anticipation:
			singleTexture->SetTexture("Anticipation");
			spriteMesh->Disable();
			mSwordDrawOffset = 0;
			
			break;
		case PlayerStateMachineStates::Player_Slash:
			singleTexture->SetTexture("Slash");
			spriteMesh->Disable();
			mSwordDrawOffset = -20;
			
			break;
		case PlayerStateMachineStates::Player_Recovery:
			singleTexture->SetTexture("Recovery");
			spriteMesh->Disable();
			mSwordDrawOffset = -20;
			
			break;
		case PlayerStateMachineStates::Idle_None:
		case PlayerStateMachineStates::Idle_Sword:
		case PlayerStateMachineStates::Idle_Lantern:
		case PlayerStateMachineStates::Moving_None:
		case PlayerStateMachineStates::Moving_Sword:
		case PlayerStateMachineStates::Moving_Lantern:
			singleTexture->Disable();
			spriteMesh->Enable();
			break;
		}

		TransformComponent* transformComponent = GetOwner()->GetComponent<TransformComponent>();
		if (mIsFlipped) 
		{
			if(singleTexture->IsActive())
			{
				singleTexture->SetOffset({ double(80 - mSwordDrawOffset),double(-20) });
			}
			transformComponent->SetScale({ -1, 1 });
		}
		else
		{
			if(singleTexture->IsActive())
			{
				singleTexture->SetOffset({ double(-20 + mSwordDrawOffset),double(-20) });
			}
			transformComponent->SetScale({ 1,1 });
		}

		Move();
		
	}


	
	void PlayerBehaviorComponent::Hit() // Remember that some timers may be updated more than once per frame as the logic checks for exit!!!
	{
		if (mInvincibilityTimer <= 0)
		{
			GetOwner()->GetComponent<AudioPlayerComponent>()->Play("hit_player");
			
			if (mStateMachine.GetCurrentState() == Moving_None)
			{
				mStateMachine.SetState(Idle_None);
				GetOwner()->GetComponent<AnimationComponent>()->SetState(Anim_Moving_None);
			}
			if (mStateMachine.GetCurrentState() == Moving_Lantern)
			{
				mStateMachine.SetState(Idle_Lantern);
				GetOwner()->GetComponent<AnimationComponent>()->SetState(Anim_Moving_Lantern);
			}
			if (mStateMachine.GetCurrentState() == Moving_Sword)
			{
				mStateMachine.SetState(Idle_Sword);
				GetOwner()->GetComponent<AnimationComponent>()->SetState(Anim_Idle_Sword);
			}
			               
			mHitTimer = mHitDuration;
			
			mInvincibilityTimer = mHitDuration * 2;

#ifndef UI_DEBUG_DRAW
			GetOwner()->GetComponent<PlayerStatisticsComponent>()->AddCurrHealth(-1);
#endif
			if (!IsAlive())
			{
				TextureComponent* texture = mLevel->GetComponent<TextureComponent>(mPlayerSingleTextureID);
				texture->Enable();
				SpriteMeshComponent* spriteMesh = GetOwner()->GetComponent<SpriteMeshComponent>();
				spriteMesh->Disable();
			}

			CameraObject* cameraObject = mLevel->GetAllActiveObjects<CameraObject>().begin();
			cameraObject->Shake(EShakeDirection::HORIZONTAL, 10.f, 0.8f, 1, 500.f);
		}
	}


	void PlayerBehaviorComponent::SetPlayerSingleTextureID(const ID& id)
	{
		mPlayerSingleTextureID = id;
	}

	void PlayerBehaviorComponent::BindMovementKeys()
	{
		InputComponent* inputComponent = GetOwner()->GetComponent<InputComponent>();

		inputComponent->BindFunctionToKeyboardDownKey(doodle::KeyboardButtons::W, &PlayerBehaviorComponent::MoveUp, GetOwner()->GetID());
		inputComponent->BindFunctionToKeyboardDownKey(doodle::KeyboardButtons::A, &PlayerBehaviorComponent::MoveLeft, GetOwner()->GetID());
		inputComponent->BindFunctionToKeyboardDownKey(doodle::KeyboardButtons::S, &PlayerBehaviorComponent::MoveDown, GetOwner()->GetID());
		inputComponent->BindFunctionToKeyboardDownKey(doodle::KeyboardButtons::D, &PlayerBehaviorComponent::MoveRight, GetOwner()->GetID());
		inputComponent->BindFunctionToKeyboardReleaseKey(doodle::KeyboardButtons::Tilde, &PlayerBehaviorComponent::CheatOpenFishGate);
	}

	bool PlayerBehaviorComponent::IsFlipped() const
	{
		return mIsFlipped;
	}

	bool PlayerBehaviorComponent::IsAlive() const
	{
		return GetOwner()->GetComponent<PlayerStatisticsComponent>()->IsAlive();
	}

	PlayerStateMachineStates PlayerBehaviorComponent::GetState()
	{
		return PlayerStateMachineStates(mStateMachine.GetCurrentState());
	}
	void PlayerBehaviorComponent::Move()
	{
		mIsMoving = false;

		if (!GetOwner()->GetComponent<PlayerStatisticsComponent>()->IsAlive()) return;

	}
	
	void PlayerBehaviorComponent::MoveUp(ID playerObjectID)
	{
		if (Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<PlayerBehaviorComponent>()->GetHitTimer() >  0)
		{
			return;
		}
		
		if (!Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<PlayerStatisticsComponent>()->IsAlive())
		{
			return;
		}
		PlayerBehaviorComponent* behaviorComponent = Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<PlayerBehaviorComponent>();
		if (behaviorComponent->GetState() != PlayerStateMachineStates::Player_Ready &&
			behaviorComponent->GetState() != PlayerStateMachineStates::Player_Anticipation &&
			behaviorComponent->GetState() != PlayerStateMachineStates::Player_Slash)
		{
			MovementComponent* movementCompnent = Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<MovementComponent>();
			movementCompnent->SetVelocity(Vector3D{ 0.f,mMoveDistance,0.f });
			behaviorComponent->mIsMoving = true;
		}
	}

	void PlayerBehaviorComponent::MoveLeft(ID playerObjectID)
	{
		if (Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<PlayerBehaviorComponent>()->GetHitTimer() > 0)
		{
			return;
		}
		if (!Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<PlayerStatisticsComponent>()->IsAlive())
		{
			return;
		}
		PlayerBehaviorComponent* behaviorComponent = Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<PlayerBehaviorComponent>();
		behaviorComponent->mIsFlipped = false;
		if (behaviorComponent->GetState() != PlayerStateMachineStates::Player_Ready &&
			behaviorComponent->GetState() != PlayerStateMachineStates::Player_Anticipation &&
			behaviorComponent->GetState() != PlayerStateMachineStates::Player_Slash)
		{
			MovementComponent* movementCompnent = Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<MovementComponent>();
			movementCompnent->SetVelocity(Vector3D{ -mMoveDistance,0.f,0.f });
			behaviorComponent->mIsMoving = true;
		}
	}

	void PlayerBehaviorComponent::MoveDown(ID playerObjectID)
	{
		if (Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<PlayerBehaviorComponent>()->GetHitTimer() > 0)
		{
			return;
		}
		if (!Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<PlayerStatisticsComponent>()->IsAlive())
		{
			return;
		}
		PlayerBehaviorComponent* behaviorComponent = Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<PlayerBehaviorComponent>();
		if (behaviorComponent->GetState() != PlayerStateMachineStates::Player_Ready && 
			behaviorComponent->GetState() != PlayerStateMachineStates::Player_Anticipation &&
			behaviorComponent->GetState() != PlayerStateMachineStates::Player_Slash)
		{
			Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<PlayerBehaviorComponent>()->mIsMoving = true;
			MovementComponent* movementCompnent = Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<MovementComponent>();
			movementCompnent->SetVelocity(Vector3D{ 0.f,-mMoveDistance,0.f });
		}
	}

	void PlayerBehaviorComponent::CheatOpenFishGate()
	{
		Level* const currentLevel = Game::GetCurrentLevel();
		FishGateObject* const fishGate = currentLevel->GetAllActiveObjects<FishGateObject>().begin();
		if (!fishGate)
		{
			return;
		}
		
		LevelLoaderComponent* const levelLoader = fishGate->GetComponent<LevelLoaderComponent>();
		PlayerBehaviorComponent* const playerBehavior = currentLevel->GetAllActiveComponents<PlayerBehaviorComponent>().begin();
		int levelIndex = playerBehavior->GetCurrentLevelIndex();
		playerBehavior->SetCurrentLevelIndex(++levelIndex);

		std::apply(&LevelLoaderComponent::SetLevelToLoad, std::tuple_cat(std::make_tuple(levelLoader), LevelLoaderComponent::FIRST_LEVEL_BY_FLOOR.at(levelIndex)));
		LevelLoaderComponent::LoadLevelsOnFloor("levels/levels.txt", levelIndex);
		
		levelLoader->Load();
	}

	void PlayerBehaviorComponent::MoveRight(ID playerObjectID)
	{
		if (Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<PlayerBehaviorComponent>()->GetHitTimer() > 0)
		{
			return;
		}
		if (!Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<PlayerStatisticsComponent>()->IsAlive())
		{
			return;
		}
		PlayerBehaviorComponent* behaviorComponent = Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<PlayerBehaviorComponent>();
		behaviorComponent->mIsFlipped = true;
		if (behaviorComponent->GetState() != PlayerStateMachineStates::Player_Ready &&
			behaviorComponent->GetState() != PlayerStateMachineStates::Player_Anticipation &&
			behaviorComponent->GetState() != PlayerStateMachineStates::Player_Slash)
		{
			MovementComponent* movementCompnent = Game::GetCurrentLevel()->GetObject<PlayerObject>(playerObjectID)->GetComponent<MovementComponent>();
			movementCompnent->SetVelocity(Vector3D{ mMoveDistance,0.f,0.f });
			behaviorComponent->mIsMoving = true;
		}
	}

}
