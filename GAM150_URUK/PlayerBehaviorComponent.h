/*
    File Name: PlayerBehaviorComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: Joonho Hwang, Doyoon Kim, Yeongju Lee
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"
#include "Vector3D.h"
#include "PlayerStateMachine.h"



namespace uruk
{
	class PlayerBehaviorComponent final : public Component<PlayerBehaviorComponent, EUpdateType::BY_FRAME, update_priorities::HIGH_PRIORITY, 0>
	{
	public:
		PlayerBehaviorComponent(ID pathFinderID);

		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);

		// There's no member variable to be stored, just the mPathfinderID being passed into the constructor is good enough
		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;
		
		void OnBeginPlay() override;
		void OnUpdate() override;
		
		void Hit();

		float GetActionTimer() const noexcept { return mActionTimer; }
		void AddToActionTimerBy(float arg) { mActionTimer += arg; }
		void ResetActionTimer() { mActionTimer = 0; }
		
		float GetHitTimer() const noexcept { return mHitTimer; }
		void AddToHitTimerBy(float arg) { mHitTimer += arg; }
		void ResetHitTimer() { mHitTimer = 0; }

		int GetCurrentLevelIndex() { return mCurrentLevelIndex; };
		void SetCurrentLevelIndex(int arg) { mCurrentLevelIndex = arg; };
		
		void SetPathFinderID(const ID& pathfinderID) { mPathfinderID = pathfinderID; }

		void SetSlashDuration(float arg) { mSlashDuration = arg; }

		void SetPlayerSingleTextureID(const ID& id);
		
		void BindMovementKeys();
		
		bool IsMoving() const noexcept { return mIsMoving; }

		bool IsFlipped() const;
		
		bool IsAlive() const;

		static void MoveUp(ID);
		static void MoveLeft(ID);
		static void MoveRight(ID);
		static void MoveDown(ID);

		static void CheatOpenFishGate();

		PlayerStateMachineStates GetState();

		const float mHitDuration = 1000;
		const float mAnticipationDuration = 300;
		float mSlashDuration = 300;
		const float mRecoveryDuration = 300;

		const float mSlashMissDuration = 150;
		const float mSlashHitDuration = 300;

		const float mWalkDuration = 400;

	private:
		PlayerStateMachine mStateMachine;

		void Move();
		bool mIsMoving = false; 
		bool mIsFlipped = false;

		double mSwordDrawOffset = 0;

		int mCurrentLevelIndex = 1;

		float mWalkTimer = 0;
		
		ID mPathfinderID;

		ID mPlayerSingleTextureID;
		
		Vector3D mPreviousGridPosition = {-1, -1}; // Random value that won't be the same as the player pos

		inline static float mMoveDistance = 200;

		float mActionTimer = 0;
		float mHitTimer = 0;
		float mInvincibilityTimer = 0;

	};

}
