/*
    File Name: PlayerObject.cpp
    Project Name: U.R.U.K
	Author(s):
		-Joonho Hwang
		Contribution(s):
			collision-related things
		-Doyoon Kim
		Contribution(s):
			animation-related things
		-Seunggeon Kim
		Contribution(s):
			all the others
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "PlayerObject.h"

#include "AnimationComponent.h"
#include "AudioPlayerComponent.h"
#include "BoxColliderComponent.h"
#include "EnemyObject.h"
#include "Game.h"
#include "GameResourceManager.h"
#include "HealthBarComponent.h"
#include "InputComponent.h"
#include "MovementComponent.h"
#include "PlayerAnimationStateMachine.h"
#include "PlayerBehaviorComponent.h"
#include "PlayerStatisticsComponent.h"
#include "RemotePoshigiComponent.h"
#include "SkeletonComponent.h"
#include "SphereColliderComponent.h"
#include "SpriteMeshComponent.h"
#include "TextureComponent.h"
#include "TransformComponent.h"


namespace uruk
{
	class PlayerHitTriggerEnterCallback final : public CustomCallbackBase<PlayerHitTriggerEnterCallback, ICollisionCallbackBase>
	{
	public:
		void Execute(IColliderComponent* self, IColliderComponent*) override
		{
			Game::GetCurrentLevel()->GetComponent(self->GetColliderTypeID(), self->GetColliderID())->GetOwner()->GetComponent<PlayerBehaviorComponent>()->Hit();
		}
	};


	
	PlayerObject::PlayerObject() : cacheCoord({0, 0}), cachePathfinderID(ID()) {}

	PlayerObject::PlayerObject(const Vector3D& coord, ID pathFinderID)
		: cacheCoord(coord), cachePathfinderID(pathFinderID) {}

	void PlayerObject::OnConstructEnd()
	{
		AddComponent<TransformComponent>(cacheCoord);
		AddComponent<InputComponent>();
		AddComponent<PlayerBehaviorComponent>(cachePathfinderID);
		AddComponent<PlayerStatisticsComponent>();
		AddComponent<HealthBarComponent>();
		AddComponent<MovementComponent>();
		MovementComponent* playerMovement = GetComponent<MovementComponent>();
		playerMovement->SetDrag(0.8f);

		// X was 2/3, but set to / 4 because going straight up from the start will be prevented, and the game's visuals and collisions does not match exactly
		AddComponent<BoxColliderComponent>(Vector3D{ TILE_WIDTH / 3, TILE_HEIGHT / 3 }, TILE_WIDTH / 3, TILE_HEIGHT / 3, CollisionTag::Uruk, CollisionTag::Obstacle | CollisionTag::Wall, false);

		// This should be the first SphereColliderComponent (For GetComponent, it returns the first one)
		AddComponent<SphereColliderComponent>(Vector3D{ TILE_WIDTH / 4, TILE_WIDTH / 4 }, ATTACK_RANGE, CollisionTag::UrukAttack, CollisionTag::None);
		GetComponent<SphereColliderComponent>()->Disable();
		
		const ID hitboxID = AddComponent<SphereColliderComponent>(Vector3D{ TILE_WIDTH / 2, TILE_WIDTH / 2 }, TILE_WIDTH / 2, CollisionTag::Uruk, CollisionTag::SonXaaryAttack | CollisionTag::KonDooriAttack | CollisionTag::PohShigiAttack);
		mLevel->GetComponent<SphereColliderComponent>(hitboxID)->SetOnCollisionEnter(std::make_unique<PlayerHitTriggerEnterCallback>());

		const ID textureComponentID = AddComponent<TextureComponent>("IdleNone", EObjectZOrder::PLAYER, 0.0f, 0.0f, 0, 0, 0, 0, Vector3D{-20, -20});
		GetComponent<PlayerBehaviorComponent>()->SetPlayerSingleTextureID(textureComponentID);
		mLevel->GetComponent<TextureComponent>(textureComponentID)->Disable();
		
		AddComponent<AudioPlayerComponent>();
		
		auto NoneIdleAnimClip = GameResourceManager::GetAnimationClip("UrukNoneIdleAnimation");
		AddComponent<SkeletonComponent>(NoneIdleAnimClip->animSamples[0].skeleton);
		int animSpaceWidth = NoneIdleAnimClip->animSpaceWidth;
		int animSpaceHeight = NoneIdleAnimClip->animSpaceHeight;
		AddComponent<SpriteMeshComponent>(NoneIdleAnimClip->animSamples[0].spriteMesh,EObjectZOrder::PLAYER, animSpaceWidth, animSpaceHeight);
		GetComponent<SpriteMeshComponent>()->SetOffsetFromTransformOrigin({ -(float)animSpaceWidth / 10.f,0.f });
		
		const ID AnimComponentID = AddComponent<AnimationComponent>();
		std::unique_ptr<PlayerAnimationStateMachine> playerAnimSateMachine = std::make_unique<PlayerAnimationStateMachine>();
		AnimationComponent* animComponent = mLevel->GetComponent<AnimationComponent>(AnimComponentID);
		animComponent->SetStateMachine(std::move(playerAnimSateMachine));
		animComponent->MapStateWithAnimClip(Anim_Idle_None, NoneIdleAnimClip);
		animComponent->MapStateWithAnimClip(Anim_Idle_Sword, GameResourceManager::GetAnimationClip("UrukSwordIdleAnimation"));
		animComponent->MapStateWithAnimClip(Anim_Idle_Lantern, GameResourceManager::GetAnimationClip("UrukLanternIdleAnimation"));
		animComponent->MapStateWithAnimClip(Anim_Moving_None, GameResourceManager::GetAnimationClip("UrukNoneRunAnimation"));
		animComponent->MapStateWithAnimClip(Anim_Moving_Sword, GameResourceManager::GetAnimationClip("UrukSwordRunAnimation"));
		animComponent->MapStateWithAnimClip(Anim_Moving_Lantern, GameResourceManager::GetAnimationClip("UrukLanternRunAnimation"));

		AddComponent<RemotePoshigiComponent>();

	}
}
