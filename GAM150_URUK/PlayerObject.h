/*
    File Name: PlayerObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"
#include "Vector3D.h"



namespace uruk
{

	class PlayerObject final : public Object<PlayerObject>
	{
	public:
		PlayerObject();
		PlayerObject(const Vector3D& coord, ID pathFinderID);
		
		void OnConstructEnd() override;

	private:
		static constexpr float ATTACK_RANGE = 80.f;
		
		Vector3D cacheCoord;
		ID cachePathfinderID;
	};

}
