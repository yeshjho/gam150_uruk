/*
    File Name: PlayerStateMachine.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "PlayerStateMachine.h"


#include "AnimationComponent.h"
#include "AudioPlayerComponent.h"
#include "EnemyObject.h"
#include "ShakeDirection.h"
#include "AudioPlayerComponent.h"
#include "CameraObject.h"
#include "Game.h"
#include "PlayerAnimationStateMachine.h"
#include "PlayerBehaviorComponent.h"
#include "InputComponent.h"
#include "SphereColliderComponent.h"


namespace uruk
{
	PlayerStateMachine::PlayerStateMachine()
		: CustomStateMachineBase<PlayerStateMachine>(Idle_None,
			{
				StateTransitionConditionsPair
				{
					PlayerStateMachineStates::Player_Recovery,
					{
						TransitionCondition
						{
							PlayerStateMachineStates::Idle_Sword,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->GetActionTimer() > playerBehavior->mRecoveryDuration) 
	{
		playerBehavior->ResetActionTimer();
		return true;
	}
	else
	{
		playerBehavior->AddToActionTimerBy(currentLevel->GetDeltaTimeInMillisecond());
	}
	return false;
}
						},
						TransitionCondition
						{
							PlayerStateMachineStates::Hit_Sword,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->GetHitTimer() > 0)
	{
		return true;
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerStateMachineStates::Player_Anticipation,
					{
						TransitionCondition
						{
							PlayerStateMachineStates::Player_Slash,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->GetActionTimer() > playerBehavior->mAnticipationDuration)
	{
		currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID)->GetOwner()->GetComponent<AudioPlayerComponent>()->Play("slash_player");
		currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID)->GetOwner()->GetComponent<SphereColliderComponent>()->Enable();
		playerBehavior->ResetActionTimer();
		playerBehavior->SetSlashDuration(playerBehavior->mSlashMissDuration); // Slash timer is set here as miss, and will be changed in enemy behavior
		currentLevel->GetAllActiveObjects<CameraObject>().begin()->Shake(EShakeDirection::SLASH, 5.f, 2.f, 1, 100.f);                          
		return true;
	}
	else
	{
		playerBehavior->AddToActionTimerBy(currentLevel->GetDeltaTimeInMillisecond());
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerStateMachineStates::Player_Slash,
					{
						TransitionCondition
						{
							PlayerStateMachineStates::Player_Recovery,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->GetActionTimer() > playerBehavior->mSlashDuration)
	{
		currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID)->GetOwner()->GetComponent<SphereColliderComponent>()->Disable();
		playerBehavior->ResetActionTimer();
		return true;
	}
	else
	{
		playerBehavior->AddToActionTimerBy(currentLevel->GetDeltaTimeInMillisecond());
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerStateMachineStates::Idle_Sword,
					{
						TransitionCondition
						{
							PlayerStateMachineStates::Moving_Sword,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->IsMoving())
	{
		return true;
	}
	return false;
}
						},
						TransitionCondition
						{
							PlayerStateMachineStates::Idle_None,
[](ID, TypeID)
{
	if (InputComponent::IsKeyDown(doodle::KeyboardButtons::C))
	{
		return true;
	}
	return false;
}
						},
						TransitionCondition
						{
							PlayerStateMachineStates::Player_Ready,
[](ID, TypeID)
{
	if (InputComponent::IsMouseDown(doodle::MouseButtons::Right))
	{
		return true;
	}
	return false;
}
						},
						TransitionCondition
						{
							PlayerStateMachineStates::Hit_Sword,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->GetHitTimer() > 0)
	{
		return true;
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerStateMachineStates::Hit_Sword,
					{
						TransitionCondition
						{
							PlayerStateMachineStates::Idle_Sword,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->GetHitTimer() <= 0)
	{
		return true;
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerStateMachineStates::Idle_None,
					{
						TransitionCondition
						{
							PlayerStateMachineStates::Idle_Lantern,
[](ID, TypeID)
{
	if (InputComponent::IsKeyDown(doodle::KeyboardButtons::Q))
	{
		return true;
	}
	return false;
}
						},
						TransitionCondition
						{
							PlayerStateMachineStates::Idle_Sword,
[](ID, TypeID)
{
	if (InputComponent::IsKeyDown(doodle::KeyboardButtons::F))
	{
		return true;
	}
	return false;
}
						},
						TransitionCondition
						{
							PlayerStateMachineStates::Hit_None,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->GetHitTimer() > 0)
	{
		return true;
	}
	return false;
}
						},
						TransitionCondition
						{
							PlayerStateMachineStates::Moving_None,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->IsMoving())
	{
		return true;
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerStateMachineStates::Idle_Lantern,
					{
						TransitionCondition
						{
							PlayerStateMachineStates::Moving_Lantern,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->IsMoving())
	{
		return true;
	}
	return false;
}
						},
						TransitionCondition
						{
							PlayerStateMachineStates::Idle_None,
[](ID, TypeID)
{
	if (InputComponent::IsKeyDown(doodle::KeyboardButtons::C))
	{
		return true;
	}
	return false;
}
						},
						TransitionCondition
						{
							PlayerStateMachineStates::Hit_Lantern,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->GetHitTimer() > 0)
	{
		return true;
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerStateMachineStates::Hit_None,
					{
						TransitionCondition
						{
							PlayerStateMachineStates::Idle_None,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->GetHitTimer() <= 0)
	{
		return true;
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerStateMachineStates::Hit_Lantern,
					{
						TransitionCondition
						{
							PlayerStateMachineStates::Idle_Lantern,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->GetHitTimer() <= 0)
	{
		return true;
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerStateMachineStates::Player_Ready,
					{
						TransitionCondition
						{
							PlayerStateMachineStates::Player_Anticipation,
[](ID, TypeID)
{
	if (InputComponent::IsMouseDown(doodle::MouseButtons::Left))
	{
		return true;
	}
	return false;
}
						},
						TransitionCondition
						{
							PlayerStateMachineStates::Idle_Sword,
[](ID, TypeID)
{
	if (!InputComponent::IsMouseDown(doodle::MouseButtons::Right))
	{
		return true;
	}
	return false;
}
						},
						TransitionCondition
						{
							PlayerStateMachineStates::Hit_Sword,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->GetHitTimer() > 0)
	{
		return true;
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerStateMachineStates::Moving_Sword,
					{
						TransitionCondition
						{
							PlayerStateMachineStates::Idle_Sword,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (!playerBehavior->IsMoving())
	{
		return true;
	}
	return false;
}
						},
						TransitionCondition
						{
							PlayerStateMachineStates::Hit_Sword,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->GetHitTimer() > 0)
	{
		return true;
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerStateMachineStates::Moving_None,
					{
						TransitionCondition
						{
							PlayerStateMachineStates::Idle_None,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (!playerBehavior->IsMoving())
	{
		return true;
	}
	return false;
}
						},
						TransitionCondition
						{
							PlayerStateMachineStates::Hit_None,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->GetHitTimer() > 0)
	{
		return true;
	}
	return false;
}
						},
					}
				},
				StateTransitionConditionsPair
				{
					PlayerStateMachineStates::Moving_Lantern,
					{
						TransitionCondition
						{
							PlayerStateMachineStates::Idle_Lantern,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (!playerBehavior->IsMoving())
	{
		return true;
	}
	return false;
}
						},
						TransitionCondition
						{
							PlayerStateMachineStates::Hit_Lantern,
[](ID ownerID, TypeID)
{
	auto currentLevel = Game::GetCurrentLevel();
	auto playerBehavior = currentLevel->GetComponent<PlayerBehaviorComponent>(ownerID);
	if (playerBehavior->GetHitTimer() > 0)
	{
		return true;
	}
	return false;
}
						},
					}
				},
			}
		)
	{}
}
