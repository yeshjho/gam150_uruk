/*
    File Name: PlayerStateMachine.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "StateMachine.h"



namespace uruk
{
	enum PlayerStateMachineStates
	{
		Player_Recovery,
		Player_Anticipation,
		Player_Slash,
		Idle_Sword,
		Hit_Sword,
		Idle_None,
		Idle_Lantern,
		Hit_None,
		Hit_Lantern,
		Player_Ready,
		Moving_Sword,
		Moving_None,
		Moving_Lantern,
	};

	class PlayerStateMachine : public CustomStateMachineBase<PlayerStateMachine>
	{
	public:
		PlayerStateMachine();
	};
}
