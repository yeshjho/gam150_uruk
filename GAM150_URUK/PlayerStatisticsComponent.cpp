/*
    File Name: PlayerStatisticsComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim, Yeongju Lee
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "PlayerStatisticsComponent.h"

#include "PlayerBehaviorComponent.h"
#include "TextureComponent.h"
#include "Serializer.h"



namespace uruk 
{
	PlayerStatisticsComponent::PlayerStatisticsComponent(int maxHealth)
		: mMaxHealth(maxHealth), mCurrHealth(maxHealth)
	{}

	void PlayerStatisticsComponent::OnUpdate()
	{
		if (mCurrHealth > mMaxHealth) { mCurrHealth = mMaxHealth; }
	}

	void PlayerStatisticsComponent::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mCurrHealth);
		serialize(outputFileStream, mIsAlive);
	}
	
	void PlayerStatisticsComponent::OnLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mCurrHealth);
		deserialize(inputFileStream, mIsAlive);
	}

	void PlayerStatisticsComponent::CheckIsKilled() 
	{
		if (mCurrHealth <= 0)
		{
			mIsAlive = false;
			GetOwner()->GetComponent<PlayerBehaviorComponent>()->SetCurrentLevelIndex(1);
		}
		/*GetOwner()->GetComponent<TextureComponent>()->SetTexture("UrukDead");
		GetOwner()->GetComponent<TextureComponent>()->SetDrawWidth(60);
		GetOwner()->GetComponent<TextureComponent>()->SetDrawHeight(60);
		GetOwner()->GetComponent<TextureComponent>()->SetOffset(-30, -30);*/ // Commented out just for now
	}
}
