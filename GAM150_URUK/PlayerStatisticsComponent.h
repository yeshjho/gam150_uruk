/*
    File Name: PlayerStatisticsComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Yeongju Lee
        Secondary: Seunggeon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "AudioPlayerComponent.h"
#include "Component.h"



namespace uruk
{
	class PlayerStatisticsComponent final : public Component<PlayerStatisticsComponent, EUpdateType::BY_FRAME, update_priorities::HIGH_PRIORITY, 0> {
	public:
		PlayerStatisticsComponent() = default;
		PlayerStatisticsComponent(int maxHealth);

		void OnUpdate() override;

		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;

		void AddMaxHealth(int i) { mMaxHealth += i; }
		void AddCurrHealth(int i) {
			mCurrHealth += i; CheckIsKilled();
			if (i > 0)
			{
				GetOwner()->GetComponent<AudioPlayerComponent>()->Play("healthregen");
			}
		}
		void SetHealth(int i) { mCurrHealth = i; }
		void AddMuscle(int i) { mMuscle += i; }
		void AddSpeed(int i) { mSpeed += i; }
		void AddLuck(int i) { mLuck += i; }
		void AddAttackRange(int i) { mAttackRange += i; }

		int GetMaxHealth() const { return mMaxHealth; }
		int GetCurrentHealth() const { return mCurrHealth; }
		int GetMuscle() const { return mMuscle; }
		int GetSpeed() const { return mSpeed; }
		int GetLuck() const { return mLuck; }
		int GetAttackRange() const { return mAttackRange; }

		bool IsAlive() const { return mIsAlive; }
		void CheckIsKilled();

	private:
		int mMaxHealth = 5;
		int mCurrHealth = 3;

		int mDepth = 0;

		int mMuscle = 0;
		int mSpeed = 0;
		int mLuck = 0;
		int mAttackRange = 0;

		bool mIsAlive = true;
	};
}