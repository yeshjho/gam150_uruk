/*
    File Name: RemotePoshigiComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "RemotePoshigiComponent.h"


#include <doodle/random.hpp>
#include "AudioPlayerComponent.h"
#include "EnemyBehaviorComponent.h"
#include "EnemyObject.h"
#include "Game.h"


void uruk::RemotePoshigiComponent::OnUpdate()
{

	// decrement timer
	if (mHunterTimerMap.size() > 0)
	{
		std::vector<ID> hunterTimerToErase;
		for (auto i : mHunterTimerMap)
		{
			auto hunter = i.first;
			auto timer = i.second;
			timer -= Game::GetCurrentLevel()->GetDeltaTimeInMillisecond();

			if (timer <= 0)
			{
				//Kill them
				std::vector<ID> preyToErase;
				for (auto j : mPreyHunterMap)
				{
					if (j.second == i.first)
					{
						Game::GetLevel(j.second)->GetComponent<EnemyBehaviorComponent>(j.first)->Kill();

						switch (Game::GetLevel(j.second)->GetComponent<EnemyBehaviorComponent>(j.first)->GetEnemyType())
						{
						case EEnemyType::KonDoori:
							GetOwner()->GetComponent<AudioPlayerComponent>()->Play("scream_kondoori_muffled");
							GetOwner()->GetComponent<AudioPlayerComponent>()->Play("hit_kondoori_muffled");
							break;

						case EEnemyType::SonXaary:
							GetOwner()->GetComponent<AudioPlayerComponent>()->Play("scream_sonxaary_muffled");
							GetOwner()->GetComponent<AudioPlayerComponent>()->Play("hit_kondoori_muffled");
							break;

						case EEnemyType::PohShigi:
							GetOwner()->GetComponent<AudioPlayerComponent>()->Play("scream_pohshigi_muffled");
							GetOwner()->GetComponent<AudioPlayerComponent>()->Play("hit_kondoori_muffled");
							break;
						}

						auto enemyPos = Game::GetLevel(j.second)->GetComponent<EnemyBehaviorComponent>(j.first)->
							GetOwner()->GetComponent<TransformComponent>()->GetPosition();

						// Add size to poshigi
						for (auto k : mHunterPoshigiMap)
						{
							if (k.first == j.second)
							{
								if (EnemyObject* const enemy = Game::GetLevel(j.second)->GetObject<EnemyObject>(k.second); enemy)
								{
									enemy->GetComponent<EnemyBehaviorComponent>()->AddSize();
									enemy->GetComponent<TransformComponent>()->SetPosition(enemyPos);
								}
							}
						}
						preyToErase.push_back(j.first);
						break;
					}
				}
				for (const ID& id : preyToErase)
				{
					mPreyHunterMap.erase(id);
				}
				preyToErase.resize(0);
				hunterTimerToErase.push_back(i.first);
				break;
			}

			if (timer != i.second)
			{
				mHunterTimerMap.erase(hunter);
				mHunterTimerMap[hunter] = timer;
				break;
			}
		}
		for(const ID& id : hunterTimerToErase)
		{
			mPreyHunterMap.erase(id);
		}
	}
	
}

void uruk::RemotePoshigiComponent::OnRoomExit(ID levelID)
{
	
	Level* const currentLevel = Game::GetLevel(levelID);

	auto currentEnemies = currentLevel->GetAllActiveObjects<EnemyObject>();

	int poshigiCount = 0;

	ID poshigiFirst;
	ID poshigiSecond;
	
	for (auto enemy : currentEnemies)
	{
		EnemyBehaviorComponent* enemyBehavior = enemy.GetComponent<EnemyBehaviorComponent>();
		if (enemyBehavior->GetEnemyType() == EEnemyType::PohShigi &&
			enemyBehavior->IsAlive())
		{
			if (!enemyBehavior->IsPlayerInSight() || enemyBehavior->GetState() == EnemyStateMachineStates::Enemy_Knockback)
			{
				poshigiCount++; // count only the ones that are not following me
			}
		}
	}

	if (poshigiCount == 2) //prob. not the best idea, but should work...
	{
		
		for (auto enemy : currentEnemies)
		{
			if (enemy.GetComponent<EnemyBehaviorComponent>()->GetEnemyType() == EEnemyType::PohShigi)
			{
				if (poshigiCount == 2)
				{
					poshigiFirst = enemy.GetID();
					poshigiCount++;
				}
				else
				{
					poshigiSecond = enemy.GetID();
				}
			}
		}

		int firstSize = currentLevel->GetObject<EnemyObject>(poshigiFirst)->GetComponent<EnemyBehaviorComponent>()->GetSize();
		int secondSize = currentLevel->GetObject<EnemyObject>(poshigiSecond)->GetComponent<EnemyBehaviorComponent>()->GetSize();

		if (firstSize == secondSize)
		{
			return;
		}
		else
		{
			if (firstSize > secondSize)
			{
				mPreyHunterMap[currentLevel->GetObject<EnemyObject>(poshigiSecond)->GetComponent<EnemyBehaviorComponent>()->GetID()] = levelID;
				mHunterPoshigiMap[levelID] = poshigiFirst;
				//mHunterList.push_back(poshigiFirst);
			}
			else
			{
				mPreyHunterMap[currentLevel->GetObject<EnemyObject>(poshigiFirst)->GetComponent<EnemyBehaviorComponent>()->GetID()] = levelID;
				mHunterPoshigiMap[levelID] = poshigiSecond;
				//mHunterList.push_back(poshigiSecond);
			}
		}

		mHunterTimerMap[levelID] = float(int(doodle::random()) * 5000 + 5000);
		
	}
	else if (poshigiCount == 1)
	{

		for (auto enemy : currentEnemies)
		{
			EnemyBehaviorComponent* enemyBehavior = enemy.GetComponent<EnemyBehaviorComponent>();
			if (enemyBehavior->GetEnemyType() == EEnemyType::PohShigi)
			{
				if (enemyBehavior->IsAlive())
				{
					if (!enemyBehavior->IsPlayerInSight() || enemyBehavior->GetState() == EnemyStateMachineStates::Enemy_Knockback)
						poshigiFirst = enemy.GetID();
				}
			}
		}

		for (auto enemy : currentEnemies)
		{
			EnemyBehaviorComponent* enemyBehavior = enemy.GetComponent<EnemyBehaviorComponent>();
			if (enemyBehavior->GetEnemyType() != EEnemyType::PohShigi &&
				enemyBehavior->IsAlive() && !enemyBehavior->IsPlayerInSight())
			{
				mPreyHunterMap[enemy.GetComponent<EnemyBehaviorComponent>()->GetID()] = levelID;

				mHunterPoshigiMap[levelID] = poshigiFirst;
				mHunterTimerMap[levelID] = float(int(doodle::random()) * 5000 + 5000);

				break;
			}
		}

	}
	else
	{
		return; // if not 1 or 2, return... (this is intended)
	}
	
}

void uruk::RemotePoshigiComponent::OnRoomEnter(ID levelID)
{
	
	for (auto j : mPreyHunterMap)
	{
		if (j.second == levelID)
		{
			mPreyHunterMap.erase(j.first);
			mHunterTimerMap.erase(levelID);
			break;
		}
	}
	
}

void uruk::RemotePoshigiComponent::OnSave(std::ofstream& outputFileStream) const
{
	serialize(outputFileStream, mIsFirstLoad);
	serialize(outputFileStream, mPreyHunterMap);
	serialize(outputFileStream, mHunterTimerMap);
	serialize(outputFileStream, mHunterPoshigiMap);
}

void uruk::RemotePoshigiComponent::OnLoad(std::ifstream& inputFileStream)
{
	deserialize(inputFileStream, mIsFirstLoad);
	deserialize(inputFileStream, mPreyHunterMap);
	deserialize(inputFileStream, mHunterTimerMap);
	deserialize(inputFileStream, mHunterPoshigiMap);
}