/*
    File Name: RemotePoshigiComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Seunggeon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"



namespace uruk
{
	class RemotePoshigiComponent final : public Component<RemotePoshigiComponent, EUpdateType::BY_FRAME, update_priorities::HIGH_PRIORITY, 0>
	{
	public:
		void OnUpdate() override;
		float GetTimer(ID arg) { return mHunterTimerMap[arg]; }
		void SetTimer(ID left, float right) { mHunterTimerMap[left] = right; }

		void OnRoomExit(ID levelID);
		void OnRoomEnter(ID levelID);

		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;

		bool IsFirstLoad() { return mIsFirstLoad; }
		void SetFirstLoad(bool arg) { mIsFirstLoad = arg; }

	private:

		bool mIsFirstLoad = true;

		std::map<ID, ID> mHunterPoshigiMap;
		std::map<ID, ID> mPreyHunterMap;
		std::map<ID, float> mHunterTimerMap;
	};
}