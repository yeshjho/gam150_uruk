/*
    File Name: RoomFloorObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Yeongju Lee
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "RoomFloorObject.h"

#include "EnemyObject.h"
#include "TransformComponent.h"
#include "TextureComponent.h"



namespace uruk
{
	RoomFloorObject::RoomFloorObject(Vector3D cacheCoord)
		: cacheCoord(cacheCoord)
	{}

	
	void RoomFloorObject::OnConstructEnd()
	{
		AddComponent<TransformComponent>(cacheCoord);

		AddComponent<TextureComponent>("RoomFloor", EObjectZOrder::ROOM_FLOOR);
	}
}