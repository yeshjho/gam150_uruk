/*
    File Name: RoomWallObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Yeongju Lee
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "RoomWallObject.h"

#include "BoxColliderComponent.h"
#include "EnemyObject.h"
#include "TransformComponent.h"
#include "TextureComponent.h"

namespace uruk
{
	RoomWallObject::RoomWallObject(Vector3D cacheCoord, bool isTopWall, bool isSideWall)
		: cacheCoord(cacheCoord), cacheIsTopWall(isTopWall), cacheIsSideWall(isSideWall)
	{}

	void RoomWallObject::OnConstructEnd() 
	{
		AddComponent<TransformComponent>(cacheCoord);

		AddComponent<TextureComponent>(cacheIsTopWall ? "RoomTopWall" : (cacheIsSideWall ? "RoomSideWall" : "RoomWall"), cacheIsTopWall ? EObjectZOrder::TOP_ROOM_WALL : EObjectZOrder::ROOM_WALL);
		
		AddComponent<BoxColliderComponent>(Vector3D{ 0, 0 }, TILE_WIDTH, TILE_HEIGHT, CollisionTag::Wall, CollisionTag::Uruk, false);
	}
}