/*
    File Name: RoomWallObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Yeongju Lee
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"
#include "Vector3D.h"



namespace uruk
{
	class RoomWallObject final : public Object<RoomWallObject>
	{
	public:
		RoomWallObject(Vector3D cacheCoord = { 0, 0 }, bool isTopWall = false, bool isSideWall = false);

		void OnConstructEnd() override;


		
	private:
		Vector3D cacheCoord;

		bool cacheIsTopWall;
		bool cacheIsSideWall;
	};
}