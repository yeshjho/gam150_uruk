/*
    File Name: Serializer.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <deque>
#include <filesystem>
#include <fstream>
#include <list>
#include <map>
#include <set>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "Timer.h"
#include "Vector3D.h"



namespace uruk
{
	template<typename T, typename = std::enable_if_t<std::is_trivially_copyable_v<T>>>
	void serialize(std::ofstream& outputFileStream, const T& t);
	
	template<typename = std::true_type>
	void serialize(std::ofstream& outputFileStream, const std::string& t);
	
	template<typename = std::true_type>
	void serialize(std::ofstream& outputFileStream, const std::wstring& t);

	template<typename T>
	void serialize(std::ofstream& outputFileStream, const std::vector<T>& t);

	template<typename T>
	void serialize(std::ofstream& outputFileStream, const std::deque<T>& t);

	template<typename T, typename S>
	void serialize(std::ofstream& outputFileStream, const std::pair<T, S>& t);

	template<typename T, typename S>
	void serialize(std::ofstream& outputFileStream, const std::map<T, S>& t);
	
	template<typename T, typename S>
	void serialize(std::ofstream& outputFileStream, const std::unordered_map<T, S>& t);

	template<typename T>
	void serialize(std::ofstream& outputFileStream, const std::set<T>& t);
	
	template<typename T>
	void serialize(std::ofstream& outputFileStream, const std::unordered_set<T>& t);

	template<typename T>
	void serialize(std::ofstream& outputFileStream, const std::list<T>& t);

	template<typename = std::true_type>
	void serialize(std::ofstream& outputFileStream, const std::filesystem::path& t);

	template<typename = std::true_type>
	void serialize(std::ofstream& outputFileStream, const Vector3D& t);
	
	template<typename = std::true_type>
	void serialize(std::ofstream& outputFileStream, const Timer& t);


	template<typename T, typename = std::enable_if_t<std::is_trivially_copyable_v<T>>>
	void deserialize(std::ifstream& inputFileStream, T& t);
	
	template<typename = std::true_type>
	void deserialize(std::ifstream& inputFileStream, std::string& t);
	
	template<typename = std::true_type>
	void deserialize(std::ifstream& inputFileStream, std::wstring& t);

	template<typename T>
	void deserialize(std::ifstream& inputFileStream, std::vector<T>& t);

	template<typename T>
	void deserialize(std::ifstream& inputFileStream, std::deque<T>& t);

	template<typename T, typename S>
	void deserialize(std::ifstream& inputFileStream, std::pair<T, S>& t);
	
	template<typename T, typename S>
	void deserialize(std::ifstream& inputFileStream, std::map<T, S>& t);
	
	template<typename T, typename S>
	void deserialize(std::ifstream& inputFileStream, std::unordered_map<T, S>& t);

	template<typename T>
	void deserialize(std::ifstream& inputFileStream, std::set<T>& t);
	
	template<typename T>
	void deserialize(std::ifstream& inputFileStream, std::unordered_set<T>& t);

	template<typename T>
	void deserialize(std::ifstream& inputFileStream, std::list<T>& t);

	template<typename = std::true_type>
	void deserialize(std::ifstream& inputFileStream, std::filesystem::path& t);

	template<typename = std::true_type>
	void deserialize(std::ifstream& inputFileStream, Vector3D& t);
	
	template<typename = std::true_type>
	void deserialize(std::ifstream& inputFileStream, Timer& t);
}

#include "Serializer.inl"
