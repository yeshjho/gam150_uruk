/*
    File Name: Serializer.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	template<typename T, typename>
	void serialize(std::ofstream& outputFileStream, const T& t)
	{
		outputFileStream.write(reinterpret_cast<const char*>(&t), sizeof(T));
	}

	template<typename>
	void serialize(std::ofstream& outputFileStream, const std::string& t)
	{
		size_t size = t.size();
		outputFileStream.write(reinterpret_cast<const char*>(&size), sizeof(size_t));
		outputFileStream << t;
	}

	template<typename>
	void serialize(std::ofstream& outputFileStream, const std::wstring& t)
	{
		size_t size = t.size();
		outputFileStream.write(reinterpret_cast<const char*>(&size), sizeof(size_t));
		
		for (wchar_t i : t)
		{
			outputFileStream.write(reinterpret_cast<const char*>(&i), sizeof(wchar_t));
		}
	}

	template<typename T>
	void serialize(std::ofstream& outputFileStream, const std::vector<T>& t)
	{
		size_t size = t.size();
		outputFileStream.write(reinterpret_cast<const char*>(&size), sizeof(size_t));

		for (const T& i : t)
		{
			serialize(outputFileStream, i);
		}
	}

	template<typename T>
	void serialize(std::ofstream& outputFileStream, const std::deque<T>& t)
	{
		size_t size = t.size();
		outputFileStream.write(reinterpret_cast<const char*>(&size), sizeof(size_t));

		for (const T& i : t)
		{
			serialize(outputFileStream, i);
		}
	}

	template <typename T, typename S>
	void serialize(std::ofstream& outputFileStream, const std::pair<T, S>& t)
	{
		serialize(outputFileStream, t.first);
		serialize(outputFileStream, t.second);
	}

	template<typename T, typename S>
	void serialize(std::ofstream& outputFileStream, const std::map<T, S>& t)
	{
		size_t size = t.size();
		outputFileStream.write(reinterpret_cast<const char*>(&size), sizeof(size_t));
		
		for (const auto& i : t)
		{
			serialize(outputFileStream, i);
		}
	}

	template<typename T, typename S>
	void serialize(std::ofstream& outputFileStream, const std::unordered_map<T, S>& t)
	{
		size_t size = t.size();
		outputFileStream.write(reinterpret_cast<const char*>(&size), sizeof(size_t));

		for (const auto& i : t)
		{
			serialize(outputFileStream, i);
		}
	}

	template <typename T>
	void serialize(std::ofstream& outputFileStream, const std::set<T>& t)
	{
		size_t size = t.size();
		outputFileStream.write(reinterpret_cast<const char*>(&size), sizeof(size_t));

		for (const T& i : t)
		{
			serialize(outputFileStream, i);
		}
	}

	template <typename T>
	void serialize(std::ofstream& outputFileStream, const std::unordered_set<T>& t)
	{
		size_t size = t.size();
		outputFileStream.write(reinterpret_cast<const char*>(&size), sizeof(size_t));

		for (const T& i : t)
		{
			serialize(outputFileStream, i);
		}
	}

	template<typename T>
	void serialize(std::ofstream& outputFileStream, const std::list<T>& t)
	{
		size_t size = t.size();
		outputFileStream.write(reinterpret_cast<const char*>(&size), sizeof(size_t));

		for (const T& i : t)
		{
			serialize(outputFileStream, i);
		}
	}

	template<typename>
	void serialize(std::ofstream& outputFileStream, const std::filesystem::path& t)
	{
		serialize(outputFileStream, t.generic_string());
	}

	template<typename>
	void serialize(std::ofstream& outputFileStream, const Vector3D& t)
	{
		for (int i = 0; i < 3; ++i)
		{
			float f = t[i];
			outputFileStream.write(reinterpret_cast<const char*>(&f), sizeof(float));
		}
	}

	template<typename>
	void serialize(std::ofstream& outputFileStream, const Timer& t)
	{
		t.OnSave(outputFileStream);
	}

	
	template<typename T, typename>
	void deserialize(std::ifstream& inputFileStream, T& t)
	{
		inputFileStream.read(reinterpret_cast<char*>(&t), sizeof(T));
	}

	template<typename>
	void deserialize(std::ifstream& inputFileStream, std::string& t)
	{
		size_t size;
		inputFileStream.read(reinterpret_cast<char*>(&size), sizeof(size_t));
		t.resize(size);
		inputFileStream.read(&t[0], sizeof(char) * size);
	}

	template<typename>
	void deserialize(std::ifstream& inputFileStream, std::wstring& t)
	{
		size_t size;
		inputFileStream.read(reinterpret_cast<char*>(&size), sizeof(size_t));
		t.resize(size);
		inputFileStream.read(reinterpret_cast<char*>(&t[0]), sizeof(wchar_t) * size);
	}
	
	template<typename T>
	void deserialize(std::ifstream& inputFileStream, std::vector<T>& t)
	{
		size_t size;
		inputFileStream.read(reinterpret_cast<char*>(&size), sizeof(size_t));
		t.resize(size);
		
		for (T& i : t)
		{
			deserialize(inputFileStream, i);
		}
	}
	
	template<typename T>
	void deserialize(std::ifstream& inputFileStream, std::deque<T>& t)
	{
		size_t size;
		inputFileStream.read(reinterpret_cast<char*>(&size), sizeof(size_t));
		t.resize(size);
		
		for (T& i : t)
		{
			deserialize(inputFileStream, i);
		}
	}
	
	template<typename T, typename S>
	void deserialize(std::ifstream& inputFileStream, std::pair<T, S>& t)
	{
		deserialize(inputFileStream, t.first);
		deserialize(inputFileStream, t.second);
	}
	
	template<typename T, typename S>
	void deserialize(std::ifstream& inputFileStream, std::map<T, S>& t)
	{
		size_t size;
		inputFileStream.read(reinterpret_cast<char*>(&size), sizeof(size_t));
		t.clear();

		for (size_t i = 0; i < size; ++i)
		{
			std::pair<T, S> p;
			deserialize(inputFileStream, p);
			t.insert(t.end(), p);
		}
	}
	
	template<typename T, typename S>
	void deserialize(std::ifstream& inputFileStream, std::unordered_map<T, S>& t)
	{
		size_t size;
		inputFileStream.read(reinterpret_cast<char*>(&size), sizeof(size_t));
		t.clear();

		for (size_t i = 0; i < size; ++i)
		{
			std::pair<T, S> p;
			deserialize(inputFileStream, p);
			t.insert(p);
		}
	}

	template <typename T>
	void deserialize(std::ifstream& inputFileStream, std::set<T>& t)
	{
		size_t size;
		inputFileStream.read(reinterpret_cast<char*>(&size), sizeof(size_t));
		t.clear();

		for (size_t i = 0; i < size; ++i)
		{
			T temp;
			deserialize(inputFileStream, &temp);
			t.insert(t.end(), temp);
		}
	}

	template <typename T>
	void deserialize(std::ifstream& inputFileStream, std::unordered_set<T>& t)
	{
		size_t size;
		inputFileStream.read(reinterpret_cast<char*>(&size), sizeof(size_t));
		t.clear();

		for (size_t i = 0; i < size; ++i)
		{
			T temp;
			deserialize(inputFileStream, &temp);
			t.insert(temp);
		}
	}

	template<typename T>
	void deserialize(std::ifstream& inputFileStream, std::list<T>& t)
	{
		size_t size;
		inputFileStream.read(reinterpret_cast<char*>(&size), sizeof(size_t));
		t.resize(size);

		for (T& i : t)
		{
			deserialize(inputFileStream, i);
		}
	}

	template <typename>
	void deserialize(std::ifstream& inputFileStream, std::filesystem::path& t)
	{
		std::string s;
		deserialize(inputFileStream, s);
		t = s;
	}

	template<typename>
	void deserialize(std::ifstream& inputFileStream, Vector3D& t)
	{
		for (int i = 0; i < 3; ++i)
		{
			inputFileStream.read(reinterpret_cast<char*>(&t[i]), sizeof(float));
		}
	}
	
	template<typename>
	void deserialize(std::ifstream& inputFileStream, Timer& t)
	{
		t.OnLoad(inputFileStream);
	}
}
