/*
    File Name: ShakeDirection.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	enum class EShakeDirection : char
	{
		HORIZONTAL,
		VERTICAL,
		SLASH,
		BACKSLASH,
	};
}
