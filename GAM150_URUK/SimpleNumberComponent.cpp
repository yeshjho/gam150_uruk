/*
    File Name: SimpleNumberComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "SimpleNumberComponent.h"

#include "Serializer.h"



namespace uruk
{
	void SimpleNumberComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mNumber);
	}


	IComponent* SimpleNumberComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		int number;
		deserialize(inputFileStream, number);

		return level->LoadComponent<SimpleNumberComponent>(id, number);
	}
}
