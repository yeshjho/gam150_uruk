/*
    File Name: SimpleNumberComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"



namespace uruk
{
	class SimpleNumberComponent final : public Component<SimpleNumberComponent>
	{
	public:
		constexpr SimpleNumberComponent(int number);

		[[nodiscard]] constexpr int GetNumber() const noexcept;
		constexpr void SetNumber(int number) noexcept;

		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);
		


	private:
		int mNumber;
	};



	constexpr SimpleNumberComponent::SimpleNumberComponent(const int number)
		: mNumber(number)
	{}


	constexpr int SimpleNumberComponent::GetNumber() const noexcept
	{
		return mNumber;
	}


	constexpr void SimpleNumberComponent::SetNumber(const int number) noexcept
	{
		mNumber = number;
	}
}
