/*
    File Name: SkeletalMeshComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Skeleton.h"
#include "Component.h"

namespace uruk
{
	class SkeletonComponent : public Component<SkeletonComponent>
	{
	public:
		SkeletonComponent(const Skeleton& skeleton) :skeleton(skeleton) {}
	private:
		Skeleton skeleton;
	};
}
