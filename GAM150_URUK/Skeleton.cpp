/*
    File Name: Skeleton.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "Skeleton.h"
using namespace uruk;

uruk::Skeleton::Skeleton(const std::string& name)
	:mName(name)
{
}

std::string uruk::Skeleton::GetName() const
{
	return mName;
}

int uruk::Skeleton::GetJointByName(Joint& dstJoint, const std::string& jointName)
{
	for (Joint& joint : mJoints)
	{
		if (joint.name == jointName)
		{
			dstJoint = joint;
			return 1;
		}
	}
	
	return 0;
}

int uruk::Skeleton::GetNumbertOfJoints() const
{
	return mNJoints;
}

Joint& uruk::Skeleton::GetJoint(int index)
{
	return mJoints[index];
}

Joint uruk::Skeleton::GetJoint(int index) const
{
	return mJoints[index];
}

Vector3D uruk::Skeleton::CalculateJointPosInSkeletonSpace(int jointIndex) const
{
	Joint joint = mJoints[jointIndex];
	Vector3D position = joint.position;

	if (jointIndex == 0)
	{
		return position;
	}

	while (joint.parentIndex != -1)
	{
		joint = mJoints[joint.parentIndex];
		position += joint.position;
	}

	return position;
}
