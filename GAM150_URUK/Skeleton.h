/*
    File Name: Skeleton.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <vector>
#include "Joint.h"
#include "Serializer.h"

namespace uruk
{
	class [[nodiscard]] Skeleton
	{
		friend class SkeletonComponent;
		friend void LoadSkeleton(std::ifstream&, Skeleton&);
		friend void SaveSkeleton(std::ofstream&, Skeleton&);
		friend void LoadSkeletonFromEditorFile(std::ifstream&, Skeleton&);
	public:
		Skeleton(const std::string& name);
		std::string GetName() const;
		int GetJointByName(Joint& dstJoint, const std::string& jointName);//returns 1 if matching joint was found. if not, returns 0
		int GetNumbertOfJoints()const;
		Joint& GetJoint(int index);
		Joint GetJoint(int index)const;

		//the origin is the object's transform position
		Vector3D CalculateJointPosInSkeletonSpace(int jointIndex) const;
	private:
		int mNJoints = 0;
		std::string mName{""};
		std::vector<Joint> mJoints;
	};

	inline void LoadSkeletonFromEditorFile(std::ifstream& istream, Skeleton& skeleton)
	{
		istream >> skeleton.mNJoints;
		istream >> skeleton.mName;
		for(int i = 0; i < skeleton.mNJoints; i++)
		{
			Joint joint;
			LoadJointFromEditorFile(istream, joint);
			skeleton.mJoints.push_back(joint);
		}
	}

	inline void LoadSkeleton(std::ifstream& istream, Skeleton& skeleton)
	{
		deserialize(istream, skeleton.mNJoints);
		deserialize(istream, skeleton.mName);
		skeleton.mJoints.resize(skeleton.mNJoints);
		for (int i = 0; i < skeleton.mNJoints; i++)
		{
			LoadJoint(istream, skeleton.mJoints[i]);
		}
	}

	inline void SaveSkeleton(std::ofstream& ostream, Skeleton& skeleton)
	{
		serialize(ostream, skeleton.mNJoints);
		serialize(ostream, skeleton.mName);
		for(int i = 0; i < skeleton.mNJoints; i++)
		{
			SaveJoint(ostream, skeleton.mJoints[i]);
		}
	}
}
