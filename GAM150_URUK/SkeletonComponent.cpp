/*
    File Name: SkeletonComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "SkeletonComponent.h"
using namespace uruk;


Skeleton& uruk::SkeletonComponent::RefGetSkeleton()
{
	return mSkeleton;
}

void SkeletonComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
{
	SaveSkeleton(outputFileStream,mSkeleton);
}

IComponent* SkeletonComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
{
	Skeleton skeleton("");
	LoadSkeleton(inputFileStream, skeleton);
	
	return level->LoadComponent<SkeletonComponent>(id, skeleton);
}
