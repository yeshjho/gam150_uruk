/*
    File Name: SkeletonComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Skeleton.h"
#include "Component.h"

namespace uruk
{
	class SkeletonComponent : public Component<SkeletonComponent, EUpdateType::NEVER, update_priorities::NORMAL_PRIORITY, 0>
	{
	public:
		SkeletonComponent(const Skeleton& skeleton) :mSkeleton(skeleton) {}
		Skeleton& RefGetSkeleton();
		
		
		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);
	private:
		mutable Skeleton mSkeleton;
	};
}
