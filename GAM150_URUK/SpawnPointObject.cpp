/*
    File Name: SpawnPointObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "SpawnPointObject.h"

#include "SimpleNumberComponent.h"
#include "TransformComponent.h"



namespace uruk
{
	SpawnPointObject::SpawnPointObject(const Vector3D& spawnPoint, const int index)
		: cacheSpawnPoint(spawnPoint), cacheIndex(index)
	{}


	int SpawnPointObject::GetIndex() const
	{
		return GetComponent<SimpleNumberComponent>()->GetNumber();
	}


	void SpawnPointObject::OnConstructEnd()
	{
		AddComponent<TransformComponent>(cacheSpawnPoint);
		AddComponent<SimpleNumberComponent>(cacheIndex);
	}
}
