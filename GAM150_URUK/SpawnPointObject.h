/*
    File Name: SpawnPointObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Object.h"

#include "Vector3D.h"



namespace uruk
{
	class SpawnPointObject final : public Object<SpawnPointObject>
	{
	public:
		SpawnPointObject(const Vector3D& spawnPoint = { 0, 0 }, int index = -1);

		[[nodiscard]] int GetIndex() const;


		void OnConstructEnd() override;



	private:
		Vector3D cacheSpawnPoint;
		int cacheIndex;
	};
}
