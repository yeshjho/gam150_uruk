/*
    File Name: SphereColliderComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: Joonho Hwang
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "SphereColliderComponent.h"

#include "BoxColliderComponent.h"
#include "Serializer.h"
#ifdef UI_DEBUG_DRAW
#include <doodle/doodle.hpp>
#include "TransformComponent.h"
#endif

namespace uruk
{
#ifdef UI_DEBUG_DRAW
	SphereColliderComponent::SphereColliderComponent(const Vector3D& offset, float radius, CollisionTag tag, CollisionFilter filter)
		:IColliderComponent(tag, filter, true, []() {return ColliderType::SphereCollider; }),
	     ZOrderBase(10000000),
		 mOffset(offset),
		 mRadius(radius)
	{
	}

#else
	SphereColliderComponent::SphereColliderComponent(const Vector3D& offset, float radius, CollisionTag tag, CollisionFilter filter)
		:IColliderComponent(tag,filter,true,[]() {return ColliderType::SphereCollider; }),
		mOffset(offset),
		mRadius(radius)
	{
	}
#endif
	
#ifdef UI_DEBUG_DRAW
	void SphereColliderComponent::OnDraw(const Vector3D& camPosition, float,const Vector3D&) const
	{
		using namespace doodle;

		TransformComponent* parentTransform = GetOwner()->GetComponent<TransformComponent>();
		Vector3D position = parentTransform->GetPosition() + mOffset;
		position -= camPosition;
		
		push_settings();
		no_fill();
		set_outline_width(3.f);
		set_outline_color(Color(0, 255, 0));
		draw_ellipse(Width/2 + position.X(),Height/2 + position.Y(), mRadius * 2.f, mRadius * 2.f);
		pop_settings();
	}
#endif


	void SphereColliderComponent::OnMove(void* newLocation)
	{
		new (newLocation) SphereColliderComponent(std::move(*this));
	}

	void SphereColliderComponent::OnSave(std::ofstream& outputFileStream) const
	{
		IColliderComponent::OnSave(outputFileStream);
	}

	void SphereColliderComponent::OnLoad(std::ifstream& inputFileStream)
	{
		IColliderComponent::OnLoad(inputFileStream);
	}

	void SphereColliderComponent::OnDisabled()
	{
		IColliderComponent::OnDisabled();
	}

	void SphereColliderComponent::OnUpdate()
	{
		IColliderComponent::OnUpdate();
	}

	void SphereColliderComponent::OnConstructEnd()
	{
		IColliderComponent::SetTypeID(GetTypeID());
		IColliderComponent::SetID(GetID());
	}


	void SphereColliderComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mOffset);
		serialize(outputFileStream, mRadius);
		serialize(outputFileStream, mCollisionTag);
		serialize(outputFileStream, mCollisionFilter);
	}

	IComponent* SphereColliderComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		Vector3D offset{ 0,0 };
		float radius;
		CollisionTag tag;
		CollisionFilter filter;

		deserialize(inputFileStream, offset);
		deserialize(inputFileStream, radius);
		deserialize(inputFileStream, tag);
		deserialize(inputFileStream, filter);

		return level->LoadComponent<SphereColliderComponent>(id,offset,radius,tag,filter);
	}
}
