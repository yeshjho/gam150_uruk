/*
    File Name: SphereColliderComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "IColliderComponent.h"
#include "Component.h"
#include "Vector3D.h"
#ifdef UI_DEBUG_DRAW
#include "ZOrderBase.h"
#endif

namespace uruk
{
#ifdef UI_DEBUG_DRAW
	class SphereColliderComponent final : public IColliderComponent, public ZOrderBase, public Component<SphereColliderComponent, EUpdateType::BY_MILLISECOND, update_priorities::HIGHEST_PRIORITY, 0, EComponentAttribute::NO_MEMCPY|EComponentAttribute::DRAWABLE>
#else
	class SphereColliderComponent final : public IColliderComponent, public Component<SphereColliderComponent, EUpdateType::BY_MILLISECOND, update_priorities::HIGHEST_PRIORITY,0, EComponentAttribute::NO_MEMCPY>
#endif
	{
		friend class Physics;
	public:
		SphereColliderComponent(const Vector3D& offset, float radius, CollisionTag tag, CollisionFilter filter);
		
#ifdef UI_DEBUG_DRAW
		void OnDraw(const Vector3D& camPosition, float, const Vector3D&) const override;
#endif
		void OnMove(void* newLocation) override;
		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;
		void OnDisabled() override;
		void OnUpdate() override;
		void OnConstructEnd() override;
		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);
	private:
		Vector3D mOffset;
		float mRadius;
	};
}