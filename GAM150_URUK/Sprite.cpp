/*
    File Name: Sprite.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "Sprite.h"
#include "GameResourceManager.h"
#include "Texture.h"

namespace uruk
{
	void LoadSpriteFromEditorFile(std::ifstream& istream, Sprite& sprite)
	{
		istream >> sprite.connectedJointIndex;
		std::string textureName;
		istream >> textureName;
		char c;//to handle , between coords
		istream >> sprite.bottomLeftCoord[0];
		istream >> c;
		istream >> sprite.bottomLeftCoord[1];
		istream >> c;
		istream >> sprite.bottomLeftCoord[2];
		istream >> sprite.rotationOffset;
		sprite.pTexture = GameResourceManager::GetImage(textureName);
	}

	void LoadSprite(std::ifstream& istream, Sprite& sprite)
	{
		deserialize(istream, sprite.connectedJointIndex);
		deserialize(istream, sprite.bottomLeftCoord);
		deserialize(istream, sprite.rotationOffset);
		std::string textureName;
		deserialize(istream, textureName);
		sprite.pTexture = GameResourceManager::GetImage(textureName);
	}


	void SaveSprite(std::ofstream& ostream, Sprite& sprite)
	{
		serialize(ostream, sprite.connectedJointIndex);
		serialize(ostream, sprite.bottomLeftCoord);
		serialize(ostream, sprite.rotationOffset);
		serialize(ostream, sprite.pTexture->GetImageName());
	}
}