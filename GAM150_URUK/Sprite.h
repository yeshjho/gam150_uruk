/*
    File Name: Sprite.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Vector3D.h"
#include "Serializer.h"

namespace doodle
{
	class Image;
}

namespace uruk
{
	class Texture;
	struct [[nodiscard]] Sprite
	{
		int connectedJointIndex = -1;
		Vector3D bottomLeftCoord = {0,0};//relative to Object Transform position.
		float rotationOffset = 0;
		std::shared_ptr<Texture> pTexture;
	};

	void LoadSpriteFromEditorFile(std::ifstream& istream, Sprite& sprite);
	
	void LoadSprite(std::ifstream& istream, Sprite& sprite);
	
	void SaveSprite(std::ofstream& ostream, Sprite& sprite);
}
