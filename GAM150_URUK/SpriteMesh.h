/*
    File Name: SpriteMesh.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Sprite.h"
#include <vector>

namespace uruk
{
	struct [[nodiscard]] SpriteMesh
	{
		std::string name{""};
		int nSprites;
		std::vector<Sprite> sprites;
	};

	inline void LoadSpriteMeshFromEditorFile(std::ifstream& istream, SpriteMesh& spriteMesh)
	{
		istream >> spriteMesh.nSprites;
		istream >> spriteMesh.name;
		for(int i = 0; i < spriteMesh.nSprites; i++)
		{
			Sprite sprite;
			LoadSpriteFromEditorFile(istream, sprite);
			spriteMesh.sprites.push_back(sprite);
		}
	}
	
	inline void LoadSpriteMesh(std::ifstream& istream, SpriteMesh& spriteMesh)
	{
		deserialize(istream, spriteMesh.name);
		deserialize(istream, spriteMesh.nSprites);
		for(int i = 0; i < spriteMesh.nSprites; i++)
		{
			Sprite sprite;
			LoadSprite(istream, sprite);
			spriteMesh.sprites.push_back(sprite);
		}
	}
	
	inline void SaveSpriteMesh(std::ofstream& ostream,SpriteMesh& spriteMesh)
	{
		serialize(ostream, spriteMesh.name);
		serialize(ostream, spriteMesh.nSprites);
		for(int i = 0; i < spriteMesh.nSprites; i++)
		{
			SaveSprite(ostream,spriteMesh.sprites[i]);
		}
	}
}