/*
    File Name: SpriteMeshComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: Joonho Hwang
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "SpriteMeshComponent.h"
#include "SkeletonComponent.h"
#include "TextureComponent.h"

namespace uruk
{
	SpriteMeshComponent::SpriteMeshComponent(SpriteMesh& pSpriteMeshSrc, int zOrder, int animSpaceWidth, int animSpaceHeight)
		:mSpriteMesh(pSpriteMeshSrc),mZorder(zOrder),animSpaceWidth(animSpaceWidth),animSpaceHeight(animSpaceHeight),mOffsetFromTransformOrigin({0.f,0.f})
	{

	}

	void SpriteMeshComponent::OnUpdate()
	{
		for (int i = 0; i < mSpriteMesh.nSprites; i++)
		{
			const Sprite& sprite = mSpriteMesh.sprites[i];
			TextureComponent* textureComponent = mLevel->GetComponent<TextureComponent>(mTextureComponentIDs[i]);
			TransformComponent* transformComponent = GetOwner()->GetComponent<TransformComponent>();
			Vector3D offset = sprite.bottomLeftCoord + mOffsetFromTransformOrigin;
			float rotationOffset = sprite.rotationOffset;
			float scaleX = transformComponent->GetScale().X();
			float scaleY = transformComponent->GetScale().Y();
			if(scaleX < 0)
			{
				offset[0] -= (float)animSpaceWidth/2.f;
				rotationOffset = -rotationOffset;
			}
			offset[0] *= scaleX;
			offset[1] *= scaleY;
			textureComponent->SetOffset(offset);
			textureComponent->SetRotationOffset(rotationOffset);
		}
	}
	SpriteMesh& SpriteMeshComponent::RefGetSpriteMesh()
	{
		return mSpriteMesh;
	}

	void SpriteMeshComponent::SetOffsetFromTransformOrigin(const Vector3D& offset)
	{
		mOffsetFromTransformOrigin = offset;
	}

	const std::vector<ID>& SpriteMeshComponent::GetTextureComponentIDs() const
	{
		return mTextureComponentIDs;
	}

	void SpriteMeshComponent::OnDisabled()
	{
		for(ID textureID : mTextureComponentIDs)
		{
			TextureComponent* textureComponent = mLevel->GetComponent<TextureComponent>(textureID);
			textureComponent->Disable();
		}
	}

	void SpriteMeshComponent::OnEnabled()
	{
		for (ID textureID : mTextureComponentIDs)
		{
			TextureComponent* textureComponent = mLevel->GetComponent<TextureComponent>(textureID);
			textureComponent->Enable();
		}
	}

	void SpriteMeshComponent::OnLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mTextureComponentIDs);
		deserialize(inputFileStream, mOffsetFromTransformOrigin);
	}

	void SpriteMeshComponent::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mTextureComponentIDs);
		serialize(outputFileStream, mOffsetFromTransformOrigin);
	}

	void SpriteMeshComponent::OnConstructEnd()
	{
		for(int i = 0; i < mSpriteMesh.nSprites; i++)
		{
			Sprite sprite = mSpriteMesh.sprites[i];
			ID textureComponentID = GetOwner()->AddComponent<TextureComponent>(sprite.pTexture->GetImageName(),mZorder);
			TextureComponent* textureComponent = mLevel->GetComponent<TextureComponent>(textureComponentID);
			textureComponent->SetOffset(sprite.bottomLeftCoord);
			textureComponent->SetRotationOffset(sprite.rotationOffset);
			mTextureComponentIDs.push_back(textureComponentID);
		}
	}

	void SpriteMeshComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		SaveSpriteMesh(outputFileStream, mSpriteMesh);
		serialize(outputFileStream, mZorder);
		serialize(outputFileStream, animSpaceWidth);
		serialize(outputFileStream, animSpaceHeight);
	}

	IComponent* SpriteMeshComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		SpriteMesh spriteMesh;
		int zOrder;
		int animSpaceWidth;
		int animSpaceHeight;
		LoadSpriteMesh(inputFileStream, spriteMesh);
		deserialize(inputFileStream, zOrder);
		deserialize(inputFileStream, animSpaceWidth);
		deserialize(inputFileStream, animSpaceHeight);
		
		return level->LoadComponent<SpriteMeshComponent>(id, spriteMesh,zOrder,animSpaceWidth,animSpaceHeight);
	}
}
