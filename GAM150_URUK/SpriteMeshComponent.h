/*
    File Name: SpriteMeshComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"
#include "SpriteMesh.h"

namespace uruk
{
	class Skeleton;
	class SpriteMeshComponent : public Component<SpriteMeshComponent, EUpdateType::BY_FRAME, update_priorities::NORMAL_PRIORITY, 0>
	{
	public:
		SpriteMeshComponent(SpriteMesh& pSpriteMeshSrc,int zOrder,int animSpaceWidth,int animSpaceHeight);
		void OnUpdate() override final;
		SpriteMesh& RefGetSpriteMesh();

		void SetOffsetFromTransformOrigin(const Vector3D& offset);
		const std::vector<ID>& GetTextureComponentIDs() const;
		void OnDisabled() override;
		void OnEnabled() override;
		void OnLoad(std::ifstream& inputFileStream) override;
		void OnSave(std::ofstream& outputFileStream) const override;
		void OnConstructEnd() override;		
		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);
	private:
		mutable SpriteMesh mSpriteMesh;
		std::vector<ID> mTextureComponentIDs;
		int mZorder;
		int animSpaceWidth;
		int animSpaceHeight;
		Vector3D mOffsetFromTransformOrigin;
		//spriteMesh에 각 SpriteMeshInfo인덱스와 textureComponent ID 인덱스 각각 일대일 대응인 상태.
	};
}
