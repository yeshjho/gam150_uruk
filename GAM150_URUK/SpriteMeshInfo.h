/*
    File Name: SpriteMeshInfo.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Vector3D.h"
#include "doodle/texture.hpp"

namespace uruk
{
	struct Sprite
	{
		int connectedJointIndex;
		Vector3D bottom_Left_Coord;//relative to Object Transform position.
		float rotationOffset;
		doodle::Texture* p_texture;
	};
}