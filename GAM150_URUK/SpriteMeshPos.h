/*
    File Name: SpriteMeshPos.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once

#include "Vector3D.h"

namespace uruk
{
	struct SpriteInfo
	{
		SpriteInfo(int Connected_joint_Index, const Vector3D& Bottom_Left_Coordinate)
			:connectedJointIndex(Connected_joint_Index), bottom_Left_Coord(Bottom_Left_Coordinate)
		{}
		int connectedJointIndex;
		Vector3D bottom_Left_Coord;
	};
}