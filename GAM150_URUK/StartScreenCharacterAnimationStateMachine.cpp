/*
	File Name: EnemyAnimationStateMachine.cpp
	Project Name: U.R.U.K
	Author(s):
		Primary: Doyoon Kim
		Secondary:
	All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/

#include "StartScreenCharacterAnimationStateMachine.h"

namespace uruk
{
	StartScreenCharacterAnimationStateMachine::StartScreenCharacterAnimationStateMachine()
		: CustomStateMachineBase<StartScreenCharacterAnimationStateMachine>(IdleAnimation,
			{
				StateTransitionConditionsPair
				{
					IdleAnimation,
					{
						TransitionCondition
						{IdleAnimation,
							[](ID,TypeID)
							{
								return false;
							}
						}
					}
				},
				StateTransitionConditionsPair
				{
					IdleAnimation,
					{
						TransitionCondition
						{IdleAnimation,
							[](ID,TypeID)
							{
								return false;
							}
						}
					}
				},
				StateTransitionConditionsPair
				{
					IdleAnimation,
					{
						TransitionCondition
						{IdleAnimation,
							[](ID,TypeID)
							{
								return false;
							}
						}
					}
				},
				StateTransitionConditionsPair
				{
					IdleAnimation,
					{
						TransitionCondition
						{IdleAnimation,
							[](ID,TypeID)
							{
								return false;
							}
						}
					}
				},
				StateTransitionConditionsPair
				{
					IdleAnimation,
					{
						TransitionCondition
						{IdleAnimation,
							[](ID,TypeID)
							{
								return false;
							}
						}
					}
				}
			})
		{}
}
