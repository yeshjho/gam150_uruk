/*
	File Name: EnemyAnimationStateMachine.cpp
	Project Name: U.R.U.K
	Author(s):
		Primary: Doyoon Kim
		Secondary:
	All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/

#include "StartScreenObject.h"
#include "GameEnum.h"
#include "AnimationComponent.h"
#include "AudioPlayerComponent.h"
#include "GameResourceManager.h"
#include "LevelLoaderComponent.h"
#include "SpriteMeshComponent.h"
#include "StartScreenCharacterAnimationStateMachine.h"
#include "TextureComponent.h"
#include "UICallbacks.h"
#include "UIPositionAdjustComponent.h"
#include "SkeletonComponent.h"
#include "doodle/doodle.hpp"
#include "Game.h"
#include "HowToPlayScreenObject.h"
#include "PauseScreenObject.h"
#include "StartScreenVolumeSettingBarObject.h"


namespace uruk
{
	void StartScreenObject::OnConstructEnd()
	{	
		AddComponent<TransformComponent>(Vector3D{0.f,0.f},true);
		AddComponent<TextureComponent>("StartScreenBackground", EObjectZOrder::ROOM_FLOOR);
		AddComponent<TextureComponent>("StartScreen", EObjectZOrder::BUTTONS - 1);

		AddComponent<AudioPlayerComponent>();

		mQuitButtonTextureID = AddComponent<TextureComponent>("QuitButton", EObjectZOrder::BUTTONS);
		mLevel->GetComponent<TextureComponent>(mQuitButtonTextureID)->SetOffset(Vector3D{ 430.f,430.f });
		
		mNewGameButtonTextureID = AddComponent<TextureComponent>("NewGameButton", EObjectZOrder::BUTTONS);
		mLevel->GetComponent<TextureComponent>(mNewGameButtonTextureID)->SetOffset(Vector3D{ 405.f,260.f });
		
		mHowToPlayButtonTextureID = AddComponent<TextureComponent>("HowToPlayButton", EObjectZOrder::BUTTONS);
		mLevel->GetComponent<TextureComponent>(mHowToPlayButtonTextureID)->SetOffset(Vector3D{ 320.f,10.f });
		
		mSetFullscreenButtonTextureID = AddComponent<TextureComponent>("FullScreenCheckBoxChecked", EObjectZOrder::BUTTONS);
		mLevel->GetComponent<TextureComponent>(mSetFullscreenButtonTextureID)->SetOffset(Vector3D{ 1700.f,15.f });
		
		mKondooriTextureID = AddComponent<TextureComponent>("StartScreenKondoori", EObjectZOrder::ROOM_FLOOR+3);
		mLevel->GetComponent<TextureComponent>(mKondooriTextureID)->SetOffset(Vector3D{ 0.f,0.f });
		
		mPoshigiTextureID = AddComponent<TextureComponent>("StartScreenPoshigi", EObjectZOrder::ROOM_FLOOR + 2);
		mLevel->GetComponent<TextureComponent>(mPoshigiTextureID)->SetOffset(Vector3D{-100.f,-100.f});
		
		mSonxaaryTextureID = AddComponent<TextureComponent>("StartScreenSonxaary", EObjectZOrder::ROOM_FLOOR + 1);
		mLevel->GetComponent<TextureComponent>(mSonxaaryTextureID)->SetOffset(Vector3D{ -100.f, - 100.f });
		
		ID newGameClickableComponentID = AddComponent<ClickableComponent>(365.f,105.f);
		ID howToPlayClickableComponentID = AddComponent<ClickableComponent>(430.f,125.f);
		ID quitGameClickableComponentID = AddComponent<ClickableComponent>(200.f,65.f);
		ID toggleFullscreenClickableComponentID = AddComponent<ClickableComponent>(48.f,48.f);

		ID newGameHoverableComponentID = AddComponent<HoverableComponent>(365.f, 105.f);
		ID howToPlayHoverableComponentID = AddComponent<HoverableComponent>(430.f, 125.f);
		ID quitGameHoverableComponentID = AddComponent<HoverableComponent>(200.f, 65.f);

		const auto firstLevel = LevelLoaderComponent::FIRST_LEVEL_BY_FLOOR.at(1);
		AddComponent<LevelLoaderComponent>(std::get<0>(firstLevel), std::get<1>(firstLevel), std::get<2>(firstLevel));
		AddComponent<AnimationComponent>();
		AnimationComponent* animComponent = GetComponent<AnimationComponent>();
		auto idleAnim = GameResourceManager::GetAnimationClip("UrukIdleMenuAnimation");
		animComponent->SetStateMachine(std::make_unique<StartScreenCharacterAnimationStateMachine>());
		animComponent->MapStateWithAnimClip(IdleAnimation, idleAnim);
		animComponent->MapStateWithAnimClip(QuitAnimation, GameResourceManager::GetAnimationClip("UrukQuitMenuAnimation"));
		animComponent->MapStateWithAnimClip(NewGameAnimation, GameResourceManager::GetAnimationClip("UrukContinueMenuAnimation"));
		animComponent->MapStateWithAnimClip(HowToPlayAnimation, GameResourceManager::GetAnimationClip("UrukHowToPlayMenuAnimation"));
		
		AddComponent<SkeletonComponent>(idleAnim->animSamples[0].skeleton);
		AddComponent<SpriteMeshComponent>(idleAnim->animSamples[0].spriteMesh,EObjectZOrder::PLAYER,idleAnim->animSpaceWidth,idleAnim->animSpaceHeight);
		GetComponent<SpriteMeshComponent>()->SetOffsetFromTransformOrigin(Vector3D{ 1150.f,220.f });

		ClickableComponent* newGameClickableComponent = mLevel->GetComponent<ClickableComponent>(newGameClickableComponentID);
		newGameClickableComponent->SetOnClicked(std::make_unique<NewGameClickedCallback>());
		newGameClickableComponent->SetOffset(Vector3D{ 405.f,260.f });

		ClickableComponent* howToPlayClickableComponent = mLevel->GetComponent<ClickableComponent>(howToPlayClickableComponentID);
		howToPlayClickableComponent->SetOnClicked(std::make_unique<HowToPlayClickedCallback>());
		howToPlayClickableComponent->SetOffset(Vector3D{ 320.f,10.f });

		ClickableComponent* quitGameClickableComponent = mLevel->GetComponent<ClickableComponent>(quitGameClickableComponentID);
		quitGameClickableComponent->SetOnClicked(std::make_unique<QuitGameClickedCallback>());
		quitGameClickableComponent->SetOffset(Vector3D{ 430.f,430.f });

		ClickableComponent* toggleFullscreenClickableComponent = mLevel->GetComponent<ClickableComponent>(toggleFullscreenClickableComponentID);
		toggleFullscreenClickableComponent->SetOnClicked(std::make_unique<ToggleFullscreenClickedCallback>());
		toggleFullscreenClickableComponent->SetOffset(Vector3D{ 1700.f,15.f });


		HoverableComponent* newGameHoverableComponent = mLevel->GetComponent<HoverableComponent>(newGameHoverableComponentID);
		newGameHoverableComponent->SetOnHovered(std::make_unique<NewGameHoveredCallback>());
		newGameHoverableComponent->SetOnUnhovered(std::make_unique<NewGameUnHoveredCallback>());
		newGameHoverableComponent->SetOffset(Vector3D{ 405.f,260.f });

		HoverableComponent* howToPlayHoverableComponent = mLevel->GetComponent<HoverableComponent>(howToPlayHoverableComponentID);
		howToPlayHoverableComponent->SetOnHovered(std::make_unique<HowToPlayHoveredCallback>());
		howToPlayHoverableComponent->SetOnUnhovered(std::make_unique<HowToPlayUnHoveredCallback>());
		howToPlayHoverableComponent->SetOffset(Vector3D{ 320.f,10.f });

		HoverableComponent* quitGameHoverableComponent = mLevel->GetComponent<HoverableComponent>(quitGameHoverableComponentID);
		quitGameHoverableComponent->SetOnHovered(std::make_unique<QuitGameHoveredCallback>());
		quitGameHoverableComponent->SetOnUnhovered(std::make_unique<QuitGameUnHoveredCallback>());
		quitGameHoverableComponent->SetOffset(Vector3D{ 430.f,430.f });

		mSoundBarID = mLevel->AddObject<StartScreenVolumeSettingBarObject>();
		
		mHowToPlayScreenID = mLevel->AddObject<HowToPlayScreenObject>();
		HowToPlayScreenObject* howToPlayScreen = mLevel->GetObject<HowToPlayScreenObject>(mHowToPlayScreenID);
		howToPlayScreen->SetStartingScreenID(GetID());
		howToPlayScreen->Disable();
		
		mPauseScreenID = mLevel->AddObject<PauseScreenObject>();
		PauseScreenObject* pauseScreen = mLevel->GetObject<PauseScreenObject>(mPauseScreenID);
		pauseScreen->SetStartScreenID(GetID());
		pauseScreen->Disable();
		pauseScreen->OnDisabled();
		
		AddComponent<UIPositionAdjustComponent>(
			std::vector<ID>{mQuitButtonTextureID, mNewGameButtonTextureID, mHowToPlayButtonTextureID, mSetFullscreenButtonTextureID, mKondooriTextureID, mPoshigiTextureID, mSonxaaryTextureID},
			std::vector<ID>{newGameClickableComponentID, howToPlayClickableComponentID, quitGameClickableComponentID, toggleFullscreenClickableComponentID},
			std::vector<ID>{newGameHoverableComponentID, howToPlayHoverableComponentID, quitGameHoverableComponentID}
		);
	}

	void StartScreenObject::OnQuitHovered()
	{
		GetComponent<AnimationComponent>()->SetState(QuitAnimation);
		mLevel->GetComponent<TextureComponent>(mQuitButtonTextureID)->SetTexture("QuitButtonHovered");
	}

	void StartScreenObject::OnQuitUnHovered()
	{
		GetComponent<AnimationComponent>()->SetState(IdleAnimation);
		mLevel->GetComponent<TextureComponent>(mQuitButtonTextureID)->SetTexture("QuitButton");
	}

	void StartScreenObject::OnQuitClicked()
	{
		doodle::close_window();
	}

	void StartScreenObject::OnNewGameHovered()
	{
		GetComponent<AnimationComponent>()->SetState(NewGameAnimation);
		mLevel->GetComponent<TextureComponent>(mNewGameButtonTextureID)->SetTexture("NewGameButtonHovered");
	}

	void StartScreenObject::OnNewGameUnHovered()
	{
		GetComponent<AnimationComponent>()->SetState(IdleAnimation);
		mLevel->GetComponent<TextureComponent>(mNewGameButtonTextureID)->SetTexture("NewGameButton");
	}

	void StartScreenObject::OnNewGameClicked()
	{
		Disable();
		OnDisabled();
		LevelLoaderComponent::LoadLevelsOnFloor("levels/levels.txt", 1);
		GetComponent<LevelLoaderComponent>()->Load();
	}

	void StartScreenObject::OnHowToPlayHovered()
	{
		GetComponent<AnimationComponent>()->SetState(HowToPlayAnimation);
		mLevel->GetComponent<TextureComponent>(mHowToPlayButtonTextureID)->SetTexture("HowToPlayButtonHovered");
	}

	void StartScreenObject::OnHowToPlayUnHovered()
	{
		GetComponent<AnimationComponent>()->SetState(IdleAnimation);
		mLevel->GetComponent<TextureComponent>(mHowToPlayButtonTextureID)->SetTexture("HowToPlayButton");
	}

	void StartScreenObject::OnHowToPlayClicked()
	{
		mLevel->GetObject<HowToPlayScreenObject>(mHowToPlayScreenID)->Enable();
		Disable();
		OnDisabled(true);
	}

	void StartScreenObject::OnSetFullscreenToggled()
	{
		TextureComponent* setFullscreenButtonTexture = mLevel->GetComponent<TextureComponent>(mSetFullscreenButtonTextureID);
		if(doodle::is_full_screen())
		{
			setFullscreenButtonTexture->SetTexture("FullScreenCheckBoxUnchecked");
		}
		else
		{
			setFullscreenButtonTexture->SetTexture("FullScreenCheckBoxChecked");
		}
		doodle::toggle_full_screen();
	}

	void StartScreenObject::OnEnabled(bool isFromHowToPlay)
	{
		TextureComponent* setFullscreenButtonTexture = mLevel->GetComponent<TextureComponent>(mSetFullscreenButtonTextureID);
		if (doodle::is_full_screen())
		{
			setFullscreenButtonTexture->SetTexture("FullScreenCheckBoxChecked");
		}
		else
		{
			setFullscreenButtonTexture->SetTexture("FullScreenCheckBoxUnchecked");
		}
		mLevel->GetObject<StartScreenVolumeSettingBarObject>(mSoundBarID)->Enable();
		if (!isFromHowToPlay)
		GetComponent<AudioPlayerComponent>()->Play("m_mainmenu");
		mLevel->GetObject<HowToPlayScreenObject>(mHowToPlayScreenID)->Disable();
		mLevel->GetObject<PauseScreenObject>(mPauseScreenID)->Disable();
		mLevel->GetObject<PauseScreenObject>(mPauseScreenID)->OnDisabled();
	}

	void StartScreenObject::OnDisabled(bool isHowToPlay)
	{
		mLevel->GetObject<StartScreenVolumeSettingBarObject>(mSoundBarID)->Disable();
		if (!isHowToPlay)
		{
			GetComponent<AudioPlayerComponent>()->Stop("m_mainmenu");
		}
	}

	ID StartScreenObject::GetPauseScreenID() const
	{
		return mPauseScreenID;
	}
}
