/*
	File Name: EnemyAnimationStateMachine.cpp
	Project Name: U.R.U.K
	Author(s):
		Primary: Doyoon Kim
		Secondary:
	All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once
#include "Object.h"



namespace uruk
{
	class StartScreenObject final : public Object<StartScreenObject>
	{
	public:
		void OnConstructEnd() override;

		void OnQuitHovered();
		void OnQuitUnHovered();
		void OnQuitClicked();

		void OnNewGameHovered();
		void OnNewGameUnHovered();
		void OnNewGameClicked();

		void OnHowToPlayHovered();
		void OnHowToPlayUnHovered();
		void OnHowToPlayClicked();

		void OnSetFullscreenToggled();
		void OnEnabled(bool isFromHowToPlay = false);
		void OnDisabled(bool isHowToPlay = false);

		ID GetPauseScreenID() const;
	private:
		ID mQuitButtonTextureID;
		ID mNewGameButtonTextureID;
		ID mHowToPlayButtonTextureID;
		ID mSetFullscreenButtonTextureID;
		ID mKondooriTextureID;
		ID mPoshigiTextureID;
		ID mSonxaaryTextureID;

		ID mSoundBarID;
		ID mHowToPlayScreenID;
		ID mPauseScreenID;
	};
}
