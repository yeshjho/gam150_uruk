/*
	File Name: EnemyAnimationStateMachine.cpp
	Project Name: U.R.U.K
	Author(s):
		Primary: Doyoon Kim
		Secondary:
	All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/

#include "StartScreenVolumeSettingBarObject.h"
#include "GameEnum.h"
#include "DraggableComponent.h"
#include "TextureComponent.h"
#include "TransformComponent.h"
#include "UICallbacks.h"
#include "UIPositionAdjustComponent.h"

void uruk::StartScreenVolumeSettingBarObject::OnConstructEnd()
{
	AddComponent<InputComponent>();
	AddComponent<TransformComponent>(Vector3D{ 0, 0 },true);
	const ID textureID = AddComponent<TextureComponent>("StartScreenSoundbar",EObjectZOrder::BUTTONS);
	GetComponent<TextureComponent>()->SetOffset({ 1590, 105 });
	const ID dragID = AddComponent<DraggableComponent>(10.f,30.f);
	GetComponent<DraggableComponent>()->SetOnMoved(std::make_unique<MainMenuVolumeSettingBarDragMovedCallback>());
	GetComponent<DraggableComponent>()->SetOffset({ 1590, 105 });
	
	AddComponent<UIPositionAdjustComponent>(
		std::vector<ID>{ textureID }, std::vector<ID>{}, std::vector<ID>{}, std::vector<ID>{ dragID }
	);
}
