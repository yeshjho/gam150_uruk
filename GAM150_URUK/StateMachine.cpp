/*
    File Name: StateMachine.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: Joonho Hwang
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "StateMachine.h"
#include "Serializer.h"

namespace uruk
{
	StateMachine::StateMachine(EnumVal currentState, std::initializer_list<StateTransitionConditionsPair> pairs)
		:mPairs{ pairs }, mCurrentState(currentState)
	{
		for (int i = 0; i < mPairs.size(); i++)
		{
			mEnumValStateTransitConditionPairIndexMap[mPairs[i].state] = i;
		}
	}

	void StateMachine::Update()
	{
		for (const TransitionCondition& condition : mPairs[mEnumValStateTransitConditionPairIndexMap[mCurrentState]].conditions)
		{
			if (condition.CanTransfer(mOwnerComponentId, mOwnerComponentType))
			{
				mCurrentState = condition.GetStateToTransfer();
			}
		}
	}

	EnumVal StateMachine::GetCurrentState()
	{
		return mCurrentState;
	}

	void StateMachine::SetState(EnumVal newState)
	{
		mCurrentState = newState;
	}

	void StateMachine::SetOwner(ID ownerID, TypeID ownerType)
	{
		mOwnerComponentId = ownerID;
		mOwnerComponentType = ownerType;
	}

	void StateMachine::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream,mCurrentState);
		serialize(outputFileStream, mOwnerComponentId);
		serialize(outputFileStream, mOwnerComponentType);
	}

	void StateMachine::OnLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mCurrentState);
		deserialize(inputFileStream, mOwnerComponentId);
		deserialize(inputFileStream, mOwnerComponentType);
	}

	std::unique_ptr<StateMachine> StateMachineFactory::Generate(const TypeID& typeID)
	{
		return stateMachineFactoryFunctions.at(typeID)();
	}
}
