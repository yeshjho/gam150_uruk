/*
    File Name: StateMachine.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <unordered_map>
#include "Internal.h"
#include "StateTransitionConditionsPair.h"

namespace uruk
{
	using EnumVal = int;
	
	class [[nodiscard]] StateMachine
	{
	public:
		StateMachine(EnumVal currentState, std::initializer_list<StateTransitionConditionsPair> pairs);
		virtual ~StateMachine() = default;
		void Update();
		EnumVal GetCurrentState();
		void SetState(EnumVal newState);
		void SetOwner(ID ownerID,TypeID ownerType);
		void OnSave(std::ofstream& outputFileStream) const;
		void OnLoad(std::ifstream& inputFileStream);
		virtual TypeID GetStateMachineTypeIDForSerialization() = 0;
	protected:
		std::vector<StateTransitionConditionsPair> mPairs;
		EnumVal mCurrentState;
		std::unordered_map<EnumVal, int> mEnumValStateTransitConditionPairIndexMap;
		ID mOwnerComponentId;
		TypeID mOwnerComponentType;
	};

	class StateMachineFactory
	{
	public:
		static std::unique_ptr<StateMachine> Generate(const TypeID& typeID);
	protected:
		inline static std::unordered_map<TypeID, std::function<std::unique_ptr<StateMachine>()>> stateMachineFactoryFunctions{};
	};
	
	template<typename ChildType>
	class CustomStateMachineBase : public StateMachine , public StateMachineFactory
	{
	public:
		CustomStateMachineBase(EnumVal currentState, std::initializer_list<StateTransitionConditionsPair> pairs);
		TypeID GetStateMachineTypeIDForSerialization() override;
		static std::unique_ptr<StateMachine> CustomStateMachineFactoryFunction();
		inline const static TypeID typeID = IDManager::GetTypeID<ChildType>();

	private:
		class FactoryRegisterer
		{
		public:
			FactoryRegisterer();
		};

		inline static FactoryRegisterer registerer;
	};

	template <typename ChildType>
	CustomStateMachineBase<ChildType>::CustomStateMachineBase(EnumVal currentState,std::initializer_list<StateTransitionConditionsPair> pairs)
		:StateMachine(currentState,pairs)
	{
	}

	template <typename ChildType>
	TypeID CustomStateMachineBase<ChildType>::GetStateMachineTypeIDForSerialization()
	{
		return typeID;
	}

	template <typename ChildType>
	std::unique_ptr<StateMachine> CustomStateMachineBase<ChildType>::CustomStateMachineFactoryFunction()
	{
		return std::make_unique<ChildType>();
	}

	template <typename ChildType>
	CustomStateMachineBase<ChildType>::FactoryRegisterer::FactoryRegisterer()
	{
		StateMachineFactory::stateMachineFactoryFunctions[CustomStateMachineBase<ChildType>::typeID] = CustomStateMachineBase<ChildType>::CustomStateMachineFactoryFunction;
	}
}
