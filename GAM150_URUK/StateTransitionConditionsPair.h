/*
    File Name: StateTransitionConditionsPair.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <vector>
#include "TransitionCondition.h"

namespace uruk
{
	struct StateTransitionConditionsPair
	{
		StateTransitionConditionsPair(EnumVal state,std::initializer_list<TransitionCondition> conditions) : state(state), conditions{ conditions } {}
		EnumVal state;
		std::vector<TransitionCondition> conditions;
	};
}