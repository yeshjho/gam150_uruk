/*
    File Name: StringFunction.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "StringFunction.h"

#include <algorithm>



namespace uruk
{
	std::vector<std::wstring> splitString(const std::wstring& s, const std::wstring& delimiter, const bool bPreserveDelimiter)
	{
		std::vector<std::wstring> toReturn;

		const size_t delimiterLength = delimiter.length();

		size_t last = 0;
		size_t next = 0;
		while (next = s.find(delimiter, last), next != std::wstring::npos)
		{
			toReturn.push_back(s.substr(last, next - last + (bPreserveDelimiter ? delimiterLength : 0)));

			last = next + delimiterLength;
		}
		toReturn.push_back(s.substr(last));

		return toReturn;
	}

	std::vector<std::wstring> splitString(const std::wstring& s, const std::vector<std::wstring>& delimiters, const bool bPreserveDelimiter)
	{
		std::vector<std::wstring> toReturn;

		size_t last = 0;
		size_t next = 0;

		std::vector<size_t> indices;
		size_t currentDelimiterLength = 0;
		while (true)
		{
			indices.clear();
			for (const std::wstring& delimiter : delimiters)
			{
				indices.push_back(s.find(delimiter, last));
			}
			auto&& minIterator = std::min_element(indices.begin(), indices.end());;
			next = *minIterator;
			currentDelimiterLength = delimiters[minIterator - indices.begin()].length();
			if (next == std::wstring::npos)
			{
				break;
			}

			toReturn.push_back(s.substr(last, next - last + (bPreserveDelimiter ? currentDelimiterLength : 0)));

			last = next + currentDelimiterLength;
		}
		toReturn.push_back(s.substr(last));

		return toReturn;
	}
}
