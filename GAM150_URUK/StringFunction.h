/*
    File Name: StringFunction.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <string>
#include <vector>



namespace uruk
{
	std::vector<std::wstring> splitString(const std::wstring& s, const std::wstring& delimiter, bool bPreserveDelimiter = false);

	std::vector<std::wstring> splitString(const std::wstring& s, const std::vector<std::wstring>& delimiters, bool bPreserveDelimiter = false);
}
