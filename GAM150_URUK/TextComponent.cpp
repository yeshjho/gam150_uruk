/*
    File Name: TextComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "TextComponent.h"

#include <fstream>
#include <doodle/doodle.hpp>
#include "Serializer.h"
#include "StringFunction.h"



namespace uruk
{
	void TextComponent::OnDraw(const Vector3D& camPosition, float, const Vector3D& camZoomScale) const
	{
		using namespace doodle;
		
		
		const TransformComponent* const componentTransform = GetOwner()->GetComponent<TransformComponent>();
		const bool isOnUI = componentTransform->IsOnUI();
		Vector3D componentPosition = GetAdjustedPosition();
		const Vector3D& componentScale = GetAdjustedScale();

		if (!isOnUI)
		{
			componentPosition[0] = (componentPosition[0] - camPosition[0]) * camZoomScale[0] + float(Width) / 2.f;
			componentPosition[1] = (componentPosition[1] - camPosition[1]) * camZoomScale[1] + float(Height) / 2.f;
		}

		const int fontID = GetFontID();
		const float fontSize = GetFontSize();
		const float lineHeight = GetLineHeight(fontID, fontSize);

		push_settings();
		mDrawSettings->Execute(this);
		apply_translate(componentPosition[0], componentPosition[1]);
		apply_rotate(GetAdjustedRotation());
		apply_scale(componentScale[0] * camZoomScale[0], componentScale[1] * camZoomScale[1]);
		set_font(fontID);
		// For whatever reason, doodle seems to reduce the size to 3/4 of its original size.
		set_font_size(fontSize * 4 / 3);
		set_fill_color(GetColor());

		const std::vector<std::wstring>& text = GetDrawText();
		const std::vector<float>& horizontalOffset = GetHorizontalAlignOffset();
		const std::vector<float>& verticalOffset = GetVerticalAlignOffset();
		const size_t size = text.size();
		for (size_t i = 0; i < size; ++i)
		{
			// For whatever reason, doodle seems to draw the text extra 0.5 lineHeight upper.
			draw_text(text[i], horizontalOffset[i], (size - i - 1.5f) * lineHeight + verticalOffset[i]);
		}

		pop_settings();
	}

	void TextComponent::OnMove(void* newLocation)
	{
		new (newLocation) TextComponent(std::move(*this));
	}


	int TextComponent::RegisterFont(const std::filesystem::path& fontFilePath)
	{
		using namespace std;

		
		const int fontID = doodle::create_distance_field_bitmap_font(fontFilePath);
		if (fontID < 0)
		{
			return fontID;
		}

		wifstream fontFile{ fontFilePath };
		fontFile.exceptions(fontFile.exceptions() | std::ios_base::badbit);

		wstring s;

		// info
		getline(fontFile, s);
		for (const wstring& subString : splitString(s, L" "))
		{
			auto&& entry = splitString(subString, L"=");
			if (entry[0] == L"size")
			{
				fontInfos[fontID].size = stoi(entry[1]);
			}
			else if (entry[0] == L"padding")
			{
				auto&& paddings = splitString(entry[1], L",");
				for (int i = 0; i < 4; ++i)
				{
					fontInfos.at(fontID).padding[i] = stoi(paddings[i]);
				}
			}
		}

		int pageCount = 0;
		// common
		getline(fontFile, s);
		for (const wstring& subString : splitString(s, L" "))
		{
			auto&& entry = splitString(subString, L"=");
			if (entry[0] == L"lineHeight")
			{
				fontInfos.at(fontID).lineHeight = stoi(entry[1]);
			}
			else if (entry[0] == L"pages")
			{
				pageCount = stoi(entry[1]);
			}
		}

		for (; pageCount > 0; --pageCount)
		{
			// page
			getline(fontFile, s);

			// chars
			getline(fontFile, s);
			int charCount = stoi(splitString(splitString(s, L" ")[1], L"=")[1]);
			for (; charCount > 0; --charCount)
			{
				// char
				getline(fontFile, s);
				char currentChar = 0;
				for (const wstring& subString : splitString(s, L" "))
				{
					auto&& entry = splitString(subString, L"=");
					if (entry[0] == L"id")
					{
						currentChar = static_cast<char>(stoi(entry[1]));
					}
					else if (entry[0] == L"width")
					{
						fontLetterInfos[fontID][currentChar].width = stoi(entry[1]);
					}
					else if (entry[0] == L"xoffset")
					{
						fontLetterInfos[fontID][currentChar].xOffset = stoi(entry[1]);
					}
					else if (entry[0] == L"xadvance")
					{
						fontLetterInfos[fontID][currentChar].xAdvance = stoi(entry[1]);
					}
				}
			}
		}

		// kernings
		getline(fontFile, s);

		int kerningCount = stoi(splitString(splitString(s, L" ")[1], L"=")[1]);
		for (; kerningCount > 0; --kerningCount)
		{
			// kerning
			fontFile >> s;

			fontFile >> s;
			const char first = static_cast<char>(stoi(splitString(s, L"=")[1]));
			fontFile >> s;
			const char second = static_cast<char>(stoi(splitString(s, L"=")[1]));
			fontFile >> s;
			const int amount = stoi(splitString(s, L"=")[1]);

			fontKerningInfos[fontID][first][second] = amount;
		}

		return fontID;
	}

	float TextComponent::GetTextWidth(const std::wstring& s, const int fontID, const float fontSize)
	{
		if (s.length() == 0)
		{
			return 0.f;
		}

		const auto& fontLetterInfo = fontLetterInfos.at(fontID);

		const auto& frontLetterInfo = fontLetterInfo.at(s.front());
		const int padding = fontInfos.at(fontID).padding[1];

		// For whatever reason, doodle seems to reduce the size to 3/4 of its original size.
		const float proportion =  4.f / 3.f * fontSize / static_cast<float>(fontInfos.at(fontID).size);
		
		if (s.length() == 1)
		{
			return static_cast<float>(frontLetterInfo.xOffset + frontLetterInfo.width) * proportion;
		}
		
		int result = frontLetterInfo.xOffset + frontLetterInfo.xAdvance - padding;
		
		wchar_t pLetter = s.front();
		for (wchar_t letter : s.substr(1, s.length() - 2))
		{
			const FontLetterInfo& letterInfo = fontLetterInfo.at(letter);
			
			result += letterInfo.xAdvance - padding;

			const auto& fontKerningInfo = fontKerningInfos.at(fontID);
			if (const auto prevLetter = fontKerningInfo.find(pLetter); prevLetter != fontKerningInfo.end())
			{
				if (const auto nextLetter = prevLetter->second.find(letter); nextLetter != prevLetter->second.end())
				{
					result += nextLetter->second;
				}
			}
			
			pLetter = letter;
		}
		
		result += fontLetterInfo.at(s.back()).width;

		return static_cast<float>(result) * proportion;
	}

	float TextComponent::GetTextHeight(const std::wstring& s, const int fontID, const float fontSize)
	{
		const size_t lineCount = std::count(s.begin(), s.end(), '\n') + 1;
		return static_cast<float>(lineCount) * GetLineHeight(fontID, fontSize);
	}

	float TextComponent::GetLineHeight(const int fontID, const float fontSize)
	{
		const float proportion = fontSize / static_cast<float>(fontInfos.at(fontID).size);
		return static_cast<float>(fontInfos.at(fontID).lineHeight) * proportion;
	}

	IComponent* TextComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		std::wstring text;
		int fontID;
		float fontSize;
		doodle::Color color;
		ETextHorizontalAlign horizontalAlign;
		ETextVerticalAlign verticalAlign;

		deserialize(inputFileStream, text);
		deserialize(inputFileStream, fontID);
		deserialize(inputFileStream, fontSize);
		deserialize(inputFileStream, color);
		deserialize(inputFileStream, horizontalAlign);
		deserialize(inputFileStream, verticalAlign);

		return level->LoadComponent<TextComponent>(id, text, 0.f, 0.f, 0, fontID, fontSize, color, horizontalAlign, verticalAlign);
	}

	void TextComponent::OnLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mDrawText);
		deserialize(inputFileStream, mLineWrapMode);
		deserialize(inputFileStream, mHorizontalAlignOffsetCache);
		deserialize(inputFileStream, mVerticalAlignOffsetCache);

		TypeID drawSettingsCallbackType;

		deserialize(inputFileStream, drawSettingsCallbackType);
		std::unique_ptr<ITextComponentCallbackBase> drawSettingsCallback = CustomCallbackFactory<ITextComponentCallbackBase>::Generate(drawSettingsCallbackType);
		drawSettingsCallback->OnLoad(inputFileStream);
	}

	void TextComponent::OnSave(std::ofstream& outputFileStream) const
	{		
		serialize(outputFileStream, mDrawText);
		serialize(outputFileStream, mLineWrapMode);
		serialize(outputFileStream, mHorizontalAlignOffsetCache);
		serialize(outputFileStream, mVerticalAlignOffsetCache);
		serialize(outputFileStream, mDrawSettings->GetType());
		mDrawSettings->OnSave(outputFileStream);
	}

	void TextComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mText);
		serialize(outputFileStream, mFontID);
		serialize(outputFileStream, mFontSize);
		serialize(outputFileStream, mColor);
		serialize(outputFileStream, mHorizontalAlign);
		serialize(outputFileStream, mVerticalAlign);
	}

	void TextComponent::calculateDrawText()
	{
		static const std::vector<std::wstring> WHITE_SPACES{ L" ", L"\n", L"\r", L"\t", L"\v", L"\f" };
		static const std::vector<std::wstring> WHITE_SPACES_PUNCTUATIONS{ L" ", L"\n", L"\r", L"\t", L"\v", L"\f", L"!", L"\"", L"#", L"$", L"%", L"&", L"'", L"(", L")", L"*", L"+", L",", L"-", L".", L"/", L":", L";", L"<", L"=", L">", L"?", L"@", L"[", L"\\", L"]", L"^", L"_", L"`", L"{", L"|", L"}", L"~" };
		const float lineHeight = GetLineHeight(mFontID, mFontSize);

		mDrawText.clear();
		mDrawText.emplace_back();

		float currentHeight = lineHeight;
		std::wstring currentLine;

		if (mLineWrapMode == ELineWrapMode::BREAK_WORD)
		{
			for (const std::wstring& line : splitString(mText, std::vector<std::wstring>{ L"\n", L"\r" }))
			{
				for (wchar_t letter : line)
				{
					// If it's the first text ever, ignore overflowing.
					// Will the current line overflow if this letter is appended?
					if (!mDrawText.front().empty() && GetTextWidth(currentLine += letter, mFontID, mFontSize) > mWidth)
					{
						// Is there enough space down below?
						if ((currentHeight += lineHeight) > mHeight)
						{
							goto RETURN;
						}

						mDrawText.emplace_back();
						currentLine = letter;
					}
					mDrawText.back() += letter;
				}

				if ((currentHeight += lineHeight) > mHeight)
				{
					goto RETURN;
				}
				mDrawText.emplace_back();
				currentLine.clear();
			}

			goto RETURN;
		}

		for (const std::wstring& line : splitString(mText, std::vector<std::wstring>{ L"\n", L"\r" }))
		{
			for (const std::wstring& stash : splitString(line, mLineWrapMode == ELineWrapMode::BY_WHITESPACE ? WHITE_SPACES : WHITE_SPACES_PUNCTUATIONS, true))
			{
				// If it's the first text ever, ignore overflowing.
				// Will the current line overflow if this string is appended?
				if (!mDrawText.front().empty() && GetTextWidth(currentLine += stash, mFontID, mFontSize) > mWidth)
				{
					// Is there enough space down below?
					if ((currentHeight += lineHeight) > mHeight)
					{
						goto RETURN;
					}

					mDrawText.emplace_back();
					currentLine = stash;
				}
				mDrawText.back() += stash;
			}

			if ((currentHeight += lineHeight) > mHeight)
			{
				goto RETURN;
			}
			mDrawText.emplace_back();
			currentLine.clear();
		}

	RETURN:
		if (mDrawText.back().empty())
		{
			mDrawText.pop_back();
		}

		for (std::wstring& s : mDrawText)
		{
			if (s.back() == ' ')
			{
				s.pop_back();
			}
		}
	}

	void TextComponent::calculateAlignOffset()
	{
		mHorizontalAlignOffsetCache.clear();
		mVerticalAlignOffsetCache.clear();
		const size_t size = mDrawText.size();
		mHorizontalAlignOffsetCache.reserve(size);
		mVerticalAlignOffsetCache.reserve(size);

		switch (mHorizontalAlign)
		{
			case ETextHorizontalAlign::CENTER:
				for (size_t i = 0; i < size; ++i)
				{
					mHorizontalAlignOffsetCache.push_back((mWidth - GetTextWidth(mDrawText[i], mFontID, mFontSize)) / 2);
				}
				break;

			case ETextHorizontalAlign::LEFT:
				for (size_t i = 0; i < size; ++i)
				{
					mHorizontalAlignOffsetCache.push_back(0.f);
				}
				break;

			case ETextHorizontalAlign::RIGHT:
				for (size_t i = 0; i < size; ++i)
				{
					mHorizontalAlignOffsetCache.push_back(mWidth - GetTextWidth(mDrawText[i], mFontID, mFontSize));
				}
				break;

			default:
				break;
		}

		switch (mVerticalAlign)
		{
			case ETextVerticalAlign::CENTER:
				for (size_t i = 0; i < size; ++i)
				{
					mVerticalAlignOffsetCache.push_back((mHeight - size * GetLineHeight(mFontID, mFontSize)) / 2);
				}
				break;

			case ETextVerticalAlign::TOP:
				for (size_t i = 0; i < size; ++i)
				{
					mVerticalAlignOffsetCache.push_back(mHeight - size * GetLineHeight(mFontID, mFontSize));
				}
				break;

			case ETextVerticalAlign::BOTTOM:
				for (size_t i = 0; i < size; ++i)
				{
					mVerticalAlignOffsetCache.push_back(0.f);
				}
				break;

			default:
				break;
		}
	}
}
