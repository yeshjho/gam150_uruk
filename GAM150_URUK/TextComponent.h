/*
    File Name: TextComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"
#include "TransformOffsetBase.h"
#include "WidthHeightBase.h"
#include "ZOrderBase.h"

#include <filesystem>
#include <string>
#include <unordered_map>
#include <doodle/color.hpp>

#include "CustomCallbackBase.h"
#include "ITextComponentCallbackBase.h"


namespace uruk
{
	enum class ETextHorizontalAlign : char
	{
		CENTER,
		LEFT,
		RIGHT
	};

	enum class ETextVerticalAlign : char
	{
		CENTER,
		TOP,
		BOTTOM
	};

	enum class ELineWrapMode : char
	{
		BY_WHITESPACE,
		// Whitespaces + Symbols
		BY_WORD,
		BREAK_WORD
	};



	struct FontInfo
	{
		int size;
		int padding[4];  // Up - Right - Down - Left
		int lineHeight;
	};

	struct FontLetterInfo
	{
		int width;
		int xOffset;
		int xAdvance;
	};

	class DefaultTextComponentCallback final : public CustomCallbackBase<DefaultTextComponentCallback, ITextComponentCallbackBase>
	{
	public:
		void OnLoad(std::ifstream&) override {};
		void OnSave(std::ofstream&) override {};
		void Execute(const TextComponent*) override {};
	};

	class TextComponent final : public WidthHeightBase<TextComponent>, public TransformOffsetBase<TextComponent>, public ZOrderBase, public Component<TextComponent, EUpdateType::NEVER, update_priorities::NORMAL_PRIORITY, 0, EComponentAttribute::DRAWABLE | EComponentAttribute::NO_MEMCPY>
	{
	public:
		inline TextComponent(std::wstring text, float width, float height, int zOrder = 0, int fontID = DEFAULT_FONT_ID, float fontSize = 20.f, const doodle::Color& color = { 255, 255, 255 }, ETextHorizontalAlign horizontalAlign = ETextHorizontalAlign::LEFT, ETextVerticalAlign verticalAlign = ETextVerticalAlign::CENTER);


		void OnDraw(const Vector3D& camPosition, float camRotation, const Vector3D& camZoomScale) const override;
		void OnMove(void* newLocation) override;

		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);
		void OnSave(std::ofstream& outputFileStream)const override;
		void OnLoad(std::ifstream& inputFileStream) override;
		
		[[nodiscard]] constexpr const std::wstring& GetText() const noexcept;
		// These 3 below are only for drawing.
		[[nodiscard]] constexpr const std::vector<std::wstring>& GetDrawText() const noexcept;
		[[nodiscard]] constexpr const std::vector<float>& GetHorizontalAlignOffset() const noexcept;
		[[nodiscard]] constexpr const std::vector<float>& GetVerticalAlignOffset() const noexcept;

		[[nodiscard]] constexpr ETextHorizontalAlign GetHorizontalAlign() const noexcept;
		[[nodiscard]] constexpr ETextVerticalAlign GetVerticalAlign() const noexcept;
		[[nodiscard]] constexpr ELineWrapMode GetLineWrapMode() const noexcept;

		[[nodiscard]] constexpr int GetFontID() const noexcept;
		[[nodiscard]] constexpr float GetFontSize() const noexcept;
		[[nodiscard]] constexpr doodle::Color GetColor() const noexcept;

		[[nodiscard]] constexpr const std::unique_ptr<ITextComponentCallbackBase>& GetDrawSettings() const noexcept;


		inline void SetText(const std::wstring& text);

		inline void SetHorizontalAlign(ETextHorizontalAlign alignment);
		inline void SetVerticalAlign(ETextVerticalAlign alignment);
		inline void SetLineWrapMode(ELineWrapMode mode);

		inline void SetFontID(int id);
		inline void SetFontSize(float size);
		inline void SetColor(const doodle::Color& color) noexcept;

		// Both of these are overriden intentionally.
		inline void SetWidth(float width);
		inline void SetHeight(float height);

		inline void SetDrawSettings(std::unique_ptr<ITextComponentCallbackBase>&& drawSettings) noexcept;
		
		static int RegisterFont(const std::filesystem::path& fontFilePath);
		static float GetTextWidth(const std::wstring& s, int fontID, float fontSize);
		static float GetTextHeight(const std::wstring& s, int fontID, float fontSize);
		static float GetLineHeight(int fontID, float fontSize);
		
	private:
		void calculateDrawText();
		void calculateAlignOffset();



	public:
		static inline int DEFAULT_FONT_ID;
		
	private:
		std::wstring mText;
		std::vector<std::wstring> mDrawText;

		ETextHorizontalAlign mHorizontalAlign;
		ETextVerticalAlign mVerticalAlign;
		ELineWrapMode mLineWrapMode = ELineWrapMode::BY_WHITESPACE;

		int mFontID;
		float mFontSize;
		doodle::Color mColor;

		std::unique_ptr<ITextComponentCallbackBase> mDrawSettings = std::make_unique<DefaultTextComponentCallback>();
		std::vector<float> mHorizontalAlignOffsetCache;
		std::vector<float> mVerticalAlignOffsetCache;


		static inline std::unordered_map<int, FontInfo> fontInfos;
		static inline std::unordered_map<int, std::unordered_map<wchar_t, FontLetterInfo>> fontLetterInfos;
		static inline std::unordered_map<int, std::unordered_map<wchar_t, std::unordered_map<wchar_t, int>>> fontKerningInfos;
	};
}

#include "TextComponent.inl"
