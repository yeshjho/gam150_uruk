/*
    File Name: TextComponent.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	inline TextComponent::TextComponent(std::wstring text, const float width, const float height, const int zOrder, const int fontID, const float fontSize, const doodle::Color& color, const ETextHorizontalAlign horizontalAlign, const ETextVerticalAlign verticalAlign)
		: WidthHeightBase(width, height), ZOrderBase(zOrder), mText(std::move(text)), mHorizontalAlign(horizontalAlign), mVerticalAlign(verticalAlign), mFontID(fontID), mFontSize(fontSize), mColor(color)
	{
		calculateDrawText();
		calculateAlignOffset();
	}


	constexpr const std::wstring& TextComponent::GetText() const noexcept
	{
		return mText;
	}

	constexpr const std::vector<std::wstring>& TextComponent::GetDrawText() const noexcept
	{
		return mDrawText;
	}

	constexpr const std::vector<float>& TextComponent::GetHorizontalAlignOffset() const noexcept
	{
		return mHorizontalAlignOffsetCache;
	}

	constexpr const std::vector<float>& TextComponent::GetVerticalAlignOffset() const noexcept
	{
		return mVerticalAlignOffsetCache;
	}

	constexpr ETextHorizontalAlign TextComponent::GetHorizontalAlign() const noexcept
	{
		return mHorizontalAlign;
	}

	constexpr ETextVerticalAlign TextComponent::GetVerticalAlign() const noexcept
	{
		return mVerticalAlign;
	}

	constexpr ELineWrapMode TextComponent::GetLineWrapMode() const noexcept
	{
		return mLineWrapMode;
	}

	constexpr int TextComponent::GetFontID() const noexcept
	{
		return mFontID;
	}

	constexpr float TextComponent::GetFontSize() const noexcept
	{
		return mFontSize;
	}

	constexpr doodle::Color TextComponent::GetColor() const noexcept
	{
		return mColor;
	}

	constexpr const std::unique_ptr<ITextComponentCallbackBase>& TextComponent::GetDrawSettings() const noexcept
	{
		return mDrawSettings;
	}


	inline void TextComponent::SetText(const std::wstring& text)
	{
		mText = text;
		calculateDrawText();
		calculateAlignOffset();
	}

	inline  void uruk::TextComponent::SetHorizontalAlign(const ETextHorizontalAlign alignment)
	{
		mHorizontalAlign = alignment;
		calculateAlignOffset();
	}

	inline  void TextComponent::SetVerticalAlign(const ETextVerticalAlign alignment)
	{
		mVerticalAlign = alignment;
		calculateAlignOffset();
	}

	inline void TextComponent::SetLineWrapMode(const ELineWrapMode mode)
	{
		mLineWrapMode = mode;
		calculateDrawText();
		calculateAlignOffset();
	}

	inline void TextComponent::SetFontID(const int id)
	{
		mFontID = id;
		calculateDrawText();
		calculateAlignOffset();
	}

	inline void TextComponent::SetFontSize(const float size)
	{
		mFontSize = size;
		calculateDrawText();
		calculateAlignOffset();
	}

	inline void TextComponent::SetColor(const doodle::Color& color) noexcept
	{
		mColor = color;
	}

	inline void uruk::TextComponent::SetWidth(const float width)
	{
		WidthHeightBase<TextComponent>::SetWidth(width);
		calculateDrawText();
		calculateAlignOffset();
	}

	inline void TextComponent::SetHeight(const float height)
	{
		WidthHeightBase<TextComponent>::SetHeight(height);
		calculateDrawText();
		calculateAlignOffset();
	}

	inline void TextComponent::SetDrawSettings(std::unique_ptr<ITextComponentCallbackBase>&& drawSettings) noexcept
	{
		mDrawSettings = std::move(drawSettings);
	}
}
