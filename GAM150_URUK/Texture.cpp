/*
    File Name: Texture.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "Texture.h"

namespace uruk
{
	Texture::Texture(const std::string& name, const std::filesystem::path& file, bool smooth_it)
	:doodle::Image(file,smooth_it),mImageName(name)
	{
	}

	Texture::Texture(doodle::Image&& image)
		:doodle::Image(std::move(image)),mImageName("")
	{
		
	}

	const std::string& Texture::GetImageName() const
	{
		return mImageName;
	}
}
