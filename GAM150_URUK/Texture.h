/*
    File Name: Texture.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "doodle/image.hpp"

namespace uruk
{
	class Texture : public doodle::Image
	{
	public:
		Texture(const std::string& name,const std::filesystem::path& file, bool smooth_it = false);
		Texture(doodle::Image&& image);
		const std::string& GetImageName() const;
	private:
		std::string mImageName;
	};
}