/*
    File Name: TextureComponent.cpp
    Project Name: U.R.U.K
	Author(s):
		-Doyoon Kim
		Contribution(s):
			serializing / deserializing, callback-related things
		-Joonho Hwang
		Contribution(s):
			all the others
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "TextureComponent.h"
#include "Serializer.h"
#include <doodle/doodle.hpp>
#include "GameResourceManager.h"


namespace uruk
{
	TextureComponent::TextureComponent(const std::string& textureName, const int zOrder, const float drawWidth, const float drawHeight, const int texelX, const int texelY, const int texelWidth, const int texelHeight, const Vector3D& offset, const float rotationOffset) noexcept
		: TransformOffsetBase<TextureComponent>(offset, rotationOffset), ZOrderBase(zOrder), mTexture(GameResourceManager::GetImage(textureName)), mDrawWidth(drawWidth), mDrawHeight(drawHeight), mTexelX(texelX), mTexelY(texelY), mTexelWidth(texelWidth), mTexelHeight(texelHeight)
	{}

	void TextureComponent::SetTexture(const std::string& textureName) noexcept
	{
		mTexture = GameResourceManager::GetImage(textureName);
	}

	void TextureComponent::SetTexture(Texture&& texture)
	{
		mTexture = std::make_shared<Texture>(std::move(texture));
	}

	void TextureComponent::SetTextureDirectlyFromFile(const std::string& textureName,const std::filesystem::path& file)
	{
		mTexture = std::make_shared<Texture>(textureName, file);
	}

	void TextureComponent::OnDraw(const Vector3D& camPosition, float, const Vector3D& camZoomScale) const
	{
		using namespace doodle;


		const TransformComponent* const componentTransform = GetOwner()->GetComponent<TransformComponent>();
		const bool isOnUI = componentTransform->IsOnUI();
		Vector3D componentPosition = GetAdjustedPosition();
		const Vector3D& componentScale = GetAdjustedScale();

		if (!isOnUI)
		{
			componentPosition[0] = (componentPosition[0] - camPosition[0]) * camZoomScale[0] + float(Width) / 2.f;
			componentPosition[1] = (componentPosition[1] - camPosition[1]) * camZoomScale[1] + float(Height) / 2.f;
		}

		push_settings();
		GetDrawSettings()->Execute();
		apply_translate(componentPosition[0], componentPosition[1]);
		apply_rotate(GetAdjustedRotation());
		apply_scale(double(componentScale[0]) * camZoomScale[0], double(componentScale[1]) * camZoomScale[1]);
		draw_image(*mTexture, 0, 0, mDrawWidth ? mDrawWidth : mTexture->GetWidth(), mDrawHeight ? mDrawHeight : mTexture->GetHeight(), mTexelX, mTexelY, mTexelWidth ? mTexelWidth : mTexture->GetWidth(), mTexelHeight ? mTexelHeight : mTexture->GetHeight());
		pop_settings();
	}
	
	void TextureComponent::OnMove(void* newLocation)
	{
		new (newLocation) TextureComponent(std::move(*this));
	}

	void TextureComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mTexture->GetImageName());
		serialize(outputFileStream, mDrawWidth);
		serialize(outputFileStream, mDrawHeight);
		serialize(outputFileStream, mTexelX);
		serialize(outputFileStream, mTexelY);
		serialize(outputFileStream, mTexelWidth);
		serialize(outputFileStream, mTexelHeight);
		serialize(outputFileStream, mOffset);
		serialize(outputFileStream, mRotationOffset);
		serialize(outputFileStream, mZOrder);
	}

	
	IComponent* TextureComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		std::string imageName;
		float drawWidth;
		float drawHeight;
		int texelX;
		int texelY;
		int texelWidth;
		int texelHeight;
		Vector3D offset{ 0,0 };
		float rotationOffset;
		int zOrder;

		deserialize(inputFileStream, imageName);
		deserialize(inputFileStream, drawWidth);
		deserialize(inputFileStream, drawHeight);
		deserialize(inputFileStream, texelX);
		deserialize(inputFileStream, texelY);
		deserialize(inputFileStream, texelWidth);
		deserialize(inputFileStream, texelHeight);
		deserialize(inputFileStream, offset);
		deserialize(inputFileStream, rotationOffset);
		deserialize(inputFileStream, zOrder);
		
		return level->LoadComponent<TextureComponent>(id, imageName, zOrder, drawWidth, drawHeight, texelX, texelY,texelWidth, texelHeight, offset, rotationOffset);
	}


	void TextureComponent::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mDrawSettings->GetType());
		mDrawSettings->OnSave(outputFileStream);
	}


	void TextureComponent::OnLoad(std::ifstream& inputFileStream)
	{
		TypeID drawSettingCallbackType;

		deserialize(inputFileStream, drawSettingCallbackType);
		std::unique_ptr<ITextureComponentCallbackBase> drawSettingCallback = CustomCallbackFactory<ITextureComponentCallbackBase>::Generate(drawSettingCallbackType);
		drawSettingCallback->OnLoad(inputFileStream);
	}
}
