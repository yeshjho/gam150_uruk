/*
    File Name: TextureComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"
#include "TransformOffsetBase.h"
#include "ZOrderBase.h"
#include "Texture.h"
#include "ITextureComponentCallbackBase.h"
#include "CustomCallbackBase.h"


namespace uruk
{
	class DefaultTextureComponentCallback : public CustomCallbackBase<DefaultTextureComponentCallback,ITextureComponentCallbackBase>
	{
	public:

		void OnLoad(std::ifstream&) override {};
		void OnSave(std::ofstream&) override {};
		void Execute() override {};
	};
	
	class TextureComponent final : public TransformOffsetBase<TextureComponent>, public ZOrderBase, public Component<TextureComponent, EUpdateType::NEVER, update_priorities::NORMAL_PRIORITY, 0, EComponentAttribute::DRAWABLE | EComponentAttribute::NO_MEMCPY>
	{
	public:
		// If drawWidth == 0 or drawHeight == 0, they won't be used.
		// This applies to texelWidth and texelHeight, too.
		TextureComponent(const std::string& textureName, int zOrder = 0, float drawWidth = 0, float drawHeight = 0, int texelX = 0, int texelY = 0, int texelWidth = 0, int texelHeight = 0, const Vector3D& offset = { 0.f, 0.f }, float rotationOffset = 0.f) noexcept;

		void OnDraw(const Vector3D& camPosition, float camRotation, const Vector3D& camZoomScale) const override;

		void OnMove(void* newLocation) override;

		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);

		void OnSave(std::ofstream& outputFileStream) const override;
		void OnLoad(std::ifstream& inputFileStream) override;

		[[nodiscard]] const std::shared_ptr<Texture>& GetTexture() const noexcept; 
		[[nodiscard]] const std::string& GetImageName() const noexcept;
		[[nodiscard]] constexpr const std::unique_ptr<ITextureComponentCallbackBase>& GetDrawSettings() const noexcept;
		[[nodiscard]] constexpr float GetDrawWidth() const noexcept;
		[[nodiscard]] constexpr float GetDrawHeight() const noexcept;
		[[nodiscard]] constexpr int GetTexelX() const noexcept;
		[[nodiscard]] constexpr int GetTexelY() const noexcept;
		[[nodiscard]] constexpr int GetTexelWidth() const noexcept;
		[[nodiscard]] constexpr int GetTexelHeight() const noexcept;

		void SetTexture(const std::string& textureName) noexcept;
		void SetTexture(Texture&& texture);
		void SetTextureDirectlyFromFile(const std::string& textureName,const std::filesystem::path& file);
		inline void SetDrawSettings(std::unique_ptr<ITextureComponentCallbackBase>&& drawSettings) noexcept;
		constexpr void SetDrawWidth(float drawWidth) noexcept;
		constexpr void SetDrawHeight(float drawHeight) noexcept;
		constexpr void SetTexelX(int texelX) noexcept;
		constexpr void SetTexelY(int texelY) noexcept;
		constexpr void SetTexelWidth(int texelWidth) noexcept;
		constexpr void SetTexelHeight(int texelHeight) noexcept;


	private:
		std::shared_ptr<Texture> mTexture;
		
		float mDrawWidth;
		float mDrawHeight;
		int mTexelX;
		int mTexelY;
		int mTexelWidth;
		int mTexelHeight;

		std::unique_ptr<ITextureComponentCallbackBase> mDrawSettings = std::make_unique<DefaultTextureComponentCallback>();
	};
}

#include "TextureComponent.inl"
