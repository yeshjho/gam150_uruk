/*
    File Name: TextureComponent.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once


namespace uruk
{
	inline const std::shared_ptr<Texture>& TextureComponent::GetTexture() const noexcept
	{
		return mTexture;
	}

	inline const std::string& TextureComponent::GetImageName() const noexcept
	{
		return mTexture->GetImageName();
	}

	constexpr const std::unique_ptr<ITextureComponentCallbackBase>& TextureComponent::GetDrawSettings() const noexcept
	{
		return mDrawSettings;
	}

	constexpr float TextureComponent::GetDrawWidth() const noexcept
	{
		return mDrawWidth;
	}

	constexpr float TextureComponent::GetDrawHeight() const noexcept
	{
		return mDrawHeight;
	}

	constexpr int TextureComponent::GetTexelX() const noexcept
	{
		return mTexelX;
	}

	constexpr int TextureComponent::GetTexelY() const noexcept
	{
		return mTexelY;
	}

	constexpr int TextureComponent::GetTexelWidth() const noexcept
	{
		return mTexelWidth;
	}

	constexpr int TextureComponent::GetTexelHeight() const noexcept
	{
		return mTexelHeight;
	}




	constexpr void TextureComponent::SetDrawWidth(const float drawWidth) noexcept
	{
		mDrawWidth = drawWidth;
	}

	constexpr void TextureComponent::SetDrawHeight(const float drawHeight) noexcept
	{
		mDrawHeight = drawHeight;
	}

	constexpr void TextureComponent::SetTexelX(const int texelX) noexcept
	{
		mTexelX = texelX;
	}

	constexpr void TextureComponent::SetTexelY(const int texelY) noexcept
	{
		mTexelY = texelY;
	}

	constexpr void TextureComponent::SetTexelWidth(const int texelWidth) noexcept
	{
		mTexelWidth = texelWidth;
	}

	constexpr void TextureComponent::SetTexelHeight(const int texelHeight) noexcept
	{
		mTexelHeight = texelHeight;
	}

	inline void TextureComponent::SetDrawSettings(std::unique_ptr<ITextureComponentCallbackBase>&& drawSettings) noexcept
	{
		mDrawSettings = std::move(drawSettings);
	}
}
