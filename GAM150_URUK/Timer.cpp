/*
    File Name: Timer.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "Timer.h"

#include "Serializer.h"



namespace uruk
{
	void Timer::Update(const float deltaTimeInMillisecond)
	{
		if (!mDoTick)
		{
			return;
		}

		mRemainingTime -= deltaTimeInMillisecond;
		if (mRemainingTime <= 0)
		{
			onTimeOut();
		}
	}


	void Timer::OnSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mOriginalTime);
		serialize(outputFileStream, mRemainingTime);
		serialize(outputFileStream, mDoTick);
		serialize(outputFileStream, mbPauseOnTimeOut);
		
		serialize(outputFileStream, mOnTimeOut->GetType());
		mOnTimeOut->OnSave(outputFileStream);
	}

	void Timer::OnLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mOriginalTime);
		deserialize(inputFileStream, mRemainingTime);
		deserialize(inputFileStream, mDoTick);
		deserialize(inputFileStream, mbPauseOnTimeOut);

		TypeID typeID;
		deserialize(inputFileStream, typeID);
		mOnTimeOut = CustomCallbackFactory<ITimerCallbackBase>::Generate(typeID);
		mOnTimeOut->OnLoad(inputFileStream);
	}
}
