/*
    File Name: Timer.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <functional>
#include <utility>

#include "CustomCallbackBase.h"
#include "ITimerCallbackBase.h"



namespace uruk
{
	class DefaultTimerCallback final : public CustomCallbackBase<DefaultTimerCallback, ITimerCallbackBase>
	{
	public:
		void OnLoad(std::ifstream&) override {}
		void OnSave(std::ofstream&) override {}
		void Execute() override {}
	};


	
	class Timer
	{
	public:
		inline Timer(float amount, bool doTick = true, std::unique_ptr<ITimerCallbackBase>&& onTimeOut = std::make_unique<DefaultTimerCallback>(), bool bPauseOnTimeOut = true);


		// Should be called every frame.
		void Update(float deltaTimeInMillisecond);

		constexpr void Pause() noexcept;
		constexpr void Resume() noexcept;
		constexpr void TogglePause() noexcept;

		constexpr void Reset() noexcept;
		constexpr void Reset(float newAmount) noexcept;

		constexpr void AddTime(float amount) noexcept;

		[[nodiscard]] constexpr float GetRemainingTime() const noexcept;

		[[nodiscard]] constexpr bool IsPaused() const noexcept;

		inline void SetOnTimeOut(std::unique_ptr<ITimerCallbackBase>&& onTimeOut);

		void OnSave(std::ofstream& outputFileStream) const;
		void OnLoad(std::ifstream& inputFileStream);

	private:
		inline void onTimeOut();



	private:
		float mOriginalTime;
		float mRemainingTime;

		std::unique_ptr<ITimerCallbackBase> mOnTimeOut;

		bool mDoTick;
		bool mbPauseOnTimeOut;
	};
}

#include "Timer.inl"
