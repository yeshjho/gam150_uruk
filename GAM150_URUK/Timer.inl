/*
    File Name: Timer.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	inline Timer::Timer(const float amount, const bool doTick, std::unique_ptr<ITimerCallbackBase>&& onTimeOut, const bool bPauseOnTimeOut)
		: mOriginalTime(amount), mRemainingTime(amount), mOnTimeOut(std::move(onTimeOut)), mDoTick(doTick), mbPauseOnTimeOut(bPauseOnTimeOut)
	{}


	constexpr void Timer::Pause() noexcept
	{
		mDoTick = false;
	}

	constexpr void Timer::Resume() noexcept
	{
		mDoTick = true;
	}

	constexpr void Timer::TogglePause() noexcept
	{
		mDoTick = !mDoTick;
	}

	constexpr void Timer::Reset() noexcept
	{
		mRemainingTime = mOriginalTime;
	}

	constexpr void Timer::Reset(const float newAmount) noexcept
	{
		mOriginalTime = newAmount;
		mRemainingTime = newAmount;
	}

	constexpr void Timer::AddTime(const float amount) noexcept
	{
		mRemainingTime += amount;
	}

	constexpr float Timer::GetRemainingTime() const noexcept
	{
		return mRemainingTime;
	}

	constexpr bool Timer::IsPaused() const noexcept
	{
		return !mDoTick;
	}

	inline void Timer::SetOnTimeOut(std::unique_ptr<ITimerCallbackBase>&& onTimeOut)
	{
		mOnTimeOut = std::move(onTimeOut);
	}

	inline void Timer::onTimeOut()
	{
		if (mbPauseOnTimeOut)
		{
			Pause();
		}

		mOnTimeOut->Execute();
	}
}
