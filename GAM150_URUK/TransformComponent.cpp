/*
    File Name: TransformComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "TransformComponent.h"

#include "Serializer.h"



namespace uruk
{
	void TransformComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mPosition);
		serialize(outputFileStream, mRotationRadian);
		serialize(outputFileStream, mScale);
		serialize(outputFileStream, mIsOnUI);
	}

	
	IComponent* TransformComponent::OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream)
	{
		Vector3D position{0, 0};
		float rotationRadian;
		Vector3D scale{0, 0};
		bool isOnUI;

		deserialize(inputFileStream, position);
		deserialize(inputFileStream, rotationRadian);
		deserialize(inputFileStream, scale);
		deserialize(inputFileStream, isOnUI);
		
		return level->LoadComponent<TransformComponent>(id, position, rotationRadian, scale, isOnUI);
	}
}
