/*
    File Name: TransformComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Doyoon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"
#include "Math.h"



namespace uruk
{
	class TransformComponent final : public Component<TransformComponent>
	{
	public:
		inline TransformComponent(const Vector3D& position, bool isOnUI = false) noexcept;
		inline TransformComponent(float x, float y, bool isOnUI = false) noexcept;
		inline TransformComponent(const Vector3D& position, float rotationRadian, bool isOnUI = false) noexcept;
		inline TransformComponent(float x, float y, float rotationRadian, bool isOnUI = false) noexcept;
		inline TransformComponent(const Vector3D& position, float rotationRadian, const Vector3D& scale, bool isOnUI = false) noexcept;
		inline TransformComponent(const Vector3D& position, float rotationRadian, float scaleX, float scaleY, bool isOnUI = false) noexcept;
		inline TransformComponent(float x, float y, float rotationRadian, const Vector3D& scale, bool isOnUI = false) noexcept;
		inline TransformComponent(float x, float y, float rotationRadian, float scaleX, float scaleY, bool isOnUI = false) noexcept;


		[[nodiscard]] constexpr const Vector3D& GetPosition() const noexcept;
		[[nodiscard]] constexpr float GetRotationRadian() const noexcept;
		[[nodiscard]] constexpr const Vector3D& GetScale() const noexcept;

		constexpr void SetPosition(float x, float y) noexcept;
		constexpr void SetPosition(const Vector3D& vector3D) noexcept;
		inline void SetRotationRadian(float rotationRadian) noexcept;
		constexpr void SetScale(float x, float y) noexcept;
		constexpr void SetScale(const Vector3D& vector3D) noexcept;

		constexpr void MoveBy(const Vector3D& vector3D) noexcept;
		inline void MoveBy(float xAmount, float yAmount);
		inline void RotateBy(float rotationRadian) noexcept;
		inline void ScaleBy(const Vector3D& vector3D);
		inline void ScaleBy(float xScale, float yScale);

		inline void ApplyMatrixToPosition(const Matrix3X3& matrix);
		inline void ApplyMatrixToScale(const Matrix3X3& matrix);


		[[nodiscard]] constexpr bool IsOnUI() const noexcept;


		void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
		static IComponent* OnLoadConstruct(Level* level, const ID& id, std::ifstream& inputFileStream);

		

	private:
		Vector3D mPosition = { 0, 0, 0 };
		float mRotationRadian = 0.f;
		Vector3D mScale = { 1, 1, 1 };

		bool mIsOnUI;
	};
}

#include "TransformComponent.inl"
