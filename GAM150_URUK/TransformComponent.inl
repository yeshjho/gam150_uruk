/*
    File Name: TransformComponent.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	inline TransformComponent::TransformComponent(const Vector3D& position, const bool isOnUI) noexcept
		: mPosition(position), mIsOnUI(isOnUI)
	{}

	inline TransformComponent::TransformComponent(const float x, const float y, const bool isOnUI) noexcept
		: TransformComponent({ x, y }, isOnUI)
	{}

	inline TransformComponent::TransformComponent(const Vector3D& position, const float rotationRadian, const bool isOnUI) noexcept
		: mPosition(position), mRotationRadian(std::fmodf(rotationRadian, constants::TWO_PI)), mIsOnUI(isOnUI)
	{}

	inline TransformComponent::TransformComponent(const float x, const float y, const float rotationRadian, const bool isOnUI) noexcept
		: TransformComponent({ x, y }, rotationRadian, isOnUI)
	{}

	inline TransformComponent::TransformComponent(const Vector3D& position, const float rotationRadian, const Vector3D& scale, const bool isOnUI) noexcept
		: mPosition(position), mRotationRadian(std::fmodf(rotationRadian, constants::TWO_PI)), mScale(scale), mIsOnUI(isOnUI)
	{}

	inline TransformComponent::TransformComponent(const Vector3D& position, const float rotationRadian, const float scaleX, const float scaleY, const bool isOnUI) noexcept
		: TransformComponent(position, rotationRadian, { scaleX, scaleY }, isOnUI)
	{}

	inline TransformComponent::TransformComponent(const float x, const float y, const float rotationRadian, const Vector3D& scale, const bool isOnUI) noexcept
		: TransformComponent({ x, y }, rotationRadian, scale, isOnUI)
	{}

	inline TransformComponent::TransformComponent(const float x, const float y, const float rotationRadian, const float scaleX, const float scaleY, const bool isOnUI) noexcept
		: TransformComponent({ x, y }, rotationRadian, { scaleX, scaleY }, isOnUI)
	{}

	constexpr const Vector3D& uruk::TransformComponent::GetPosition() const noexcept
	{
		return mPosition;
	}

	constexpr float TransformComponent::GetRotationRadian() const noexcept
	{
		return mRotationRadian;
	}

	constexpr const Vector3D& TransformComponent::GetScale() const noexcept
	{
		return mScale;
	}

	constexpr void TransformComponent::SetPosition(float x, float y) noexcept
	{
		mPosition = { x, y, 0.f };
	}

	constexpr void TransformComponent::SetPosition(const Vector3D& vector3D) noexcept
	{
		mPosition = vector3D;
	}

	inline void TransformComponent::SetRotationRadian(const float rotationRadian) noexcept
	{
		mRotationRadian = std::fmodf(rotationRadian, constants::TWO_PI);
	}

	constexpr void TransformComponent::SetScale(const float x, const float y) noexcept
	{
		mScale = { x, y, 0.f };
	}

	constexpr void TransformComponent::SetScale(const Vector3D& vector3D) noexcept
	{
		mScale = vector3D;
	}

	constexpr void TransformComponent::MoveBy(const Vector3D& vector3D) noexcept
	{
		mPosition += vector3D;
	}

	inline void TransformComponent::MoveBy(const float xAmount, const float yAmount)
	{
		mPosition[0] += xAmount;
		mPosition[1] += yAmount;
	}

	inline void TransformComponent::RotateBy(const float rotationRadian) noexcept
	{
		mRotationRadian += rotationRadian;
		mRotationRadian = std::fmodf(mRotationRadian, constants::TWO_PI);
	}

	inline void TransformComponent::ScaleBy(const Vector3D& vector3D)
	{
		mScale[0] *= vector3D[0];
		mScale[1] *= vector3D[1];
	}

	inline void TransformComponent::ScaleBy(const float xScale, const float yScale)
	{
		mScale[0] *= xScale;
		mScale[1] *= yScale;
	}

	inline void TransformComponent::ApplyMatrixToPosition(const Matrix3X3& matrix)
	{
		mPosition = matrix * mPosition;
	}

	inline void TransformComponent::ApplyMatrixToScale(const Matrix3X3& matrix)
	{
		mScale = matrix * mScale;
	}


	constexpr bool TransformComponent::IsOnUI() const noexcept
	{
		return mIsOnUI;
	}
}
