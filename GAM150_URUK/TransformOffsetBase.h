/*
    File Name: TransformOffsetBase.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "ITransformOffsetBase.h"
#include "TransformComponent.h"



namespace uruk
{
	template<typename T>
	class TransformOffsetBase : public ITransformOffsetBase
	{
	public:
		constexpr TransformOffsetBase(const Vector3D& offset = { 0.f, 0.f }, float rotationOffset = 0.f, const Vector3D& scaleOffset = { 1.f, 1.f });


		[[nodiscard]] Vector3D GetAdjustedPosition() const;
		[[nodiscard]] float GetAdjustedRotation() const;
		// Gets multiplied to the original scale.
		[[nodiscard]] Vector3D GetAdjustedScale() const;
	};
}

#include "TransformOffsetBase.inl"
