/*
    File Name: TransformOffsetBase.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	template<typename T>
	constexpr TransformOffsetBase<T>::TransformOffsetBase(const Vector3D& offset, const float rotationOffset, const Vector3D& scaleOffset)
		: ITransformOffsetBase(offset, rotationOffset, scaleOffset)
	{}


	template<typename T>
	Vector3D TransformOffsetBase<T>::GetAdjustedPosition() const
	{
		return static_cast<const T*>(this)->GetOwner()->template GetComponent<TransformComponent>()->GetPosition() + mOffset;
	}

	template<typename T>
	float TransformOffsetBase<T>::GetAdjustedRotation() const
	{
		return static_cast<const T*>(this)->GetOwner()->template GetComponent<TransformComponent>()->GetRotationRadian() + mRotationOffset;
	}

	template <typename T>
	Vector3D TransformOffsetBase<T>::GetAdjustedScale() const
	{
		const Vector3D originalScale = static_cast<const T*>(this)->GetOwner()->template GetComponent<TransformComponent>()->GetScale();
		return Vector3D{ originalScale.X() * mScaleOffset.X(), originalScale.Y() * mScaleOffset.Y() };
	}
}
