/*
    File Name: TransitionCondition.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "TransitionCondition.h"

#include <utility>



uruk::TransitionCondition::TransitionCondition(EnumVal stateToChange, std::function<bool(ID ownerID, TypeID ownerType)> condition)
	:mStateToChange(stateToChange), mCondition(std::move(condition))
{
}

bool uruk::TransitionCondition::CanTransfer(ID ownerID, TypeID ownerType) const
{
	return mCondition(ownerID,ownerType);
}

uruk::EnumVal uruk::TransitionCondition::GetStateToTransfer() const
{
	return mStateToChange;
}
