/*
    File Name: TransitionCondition.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <functional>
#include "Internal.h"



namespace uruk
{
	using EnumVal = int;

	class [[nodiscard]] TransitionCondition
	{
	public:
		TransitionCondition(EnumVal stateToChange, std::function<bool(ID ownerID, TypeID ownerType)> condition);
		bool CanTransfer(ID ownerID, TypeID ownerType) const;
		EnumVal GetStateToTransfer() const;
	private:
		EnumVal mStateToChange;
		std::function<bool(ID ownerID, TypeID ownerType)> mCondition;
	};

	
}
