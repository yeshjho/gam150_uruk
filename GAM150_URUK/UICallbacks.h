/*
	File Name: EnemyAnimationStateMachine.cpp
	Project Name: U.R.U.K
	Author(s):
		Primary: Doyoon Kim
		Secondary: Joonho Hwang - Soundbar callback position adjustments
	All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/

#pragma once
#include "ClickableComponent.h"
#include "DraggableComponent.h"
#include "HoverableComponent.h"
#include "SpriteMeshComponent.h"
#include "StartScreenObject.h"
#include "Game.h"
#include "HowToPlayScreenObject.h"
#include "PauseScreenObject.h"
#include "TextureComponent.h"
#include <doodle/environment.hpp>

namespace uruk
{
	class NewGameClickedCallback final : public CustomCallbackBase<NewGameClickedCallback, IClickableComponentCallbackBase>
	{
	public:
		void Execute(ClickableComponent* clickableComponent) override
		{
			StartScreenObject* startScreen = static_cast<StartScreenObject*>(clickableComponent->GetOwner());
			startScreen->OnNewGameClicked();
		}
	};

	class NewGameHoveredCallback final : public CustomCallbackBase<NewGameHoveredCallback, IHoverableComponentCallbackBase>
	{
	public:
		void Execute(HoverableComponent* hoverableComponent) override
		{
			StartScreenObject* startScreen = static_cast<StartScreenObject*>(hoverableComponent->GetOwner());
			startScreen->OnNewGameHovered();
		}
	};

	class NewGameUnHoveredCallback final : public CustomCallbackBase<NewGameUnHoveredCallback, IHoverableComponentCallbackBase>
	{
	public:
		void Execute(HoverableComponent* hoverableComponent) override
		{
			StartScreenObject* startScreen = static_cast<StartScreenObject*>(hoverableComponent->GetOwner());
			startScreen->OnNewGameUnHovered();
		}
	};

	class HowToPlayClickedCallback final : public CustomCallbackBase<HowToPlayClickedCallback, IClickableComponentCallbackBase>
	{
	public:
		void Execute(ClickableComponent* clickableComponent) override
		{
			StartScreenObject* startScreen = static_cast<StartScreenObject*>(clickableComponent->GetOwner());
			startScreen->OnHowToPlayClicked();
		}
	};

	class HowToPlayHoveredCallback final : public CustomCallbackBase<HowToPlayHoveredCallback, IHoverableComponentCallbackBase>
	{
	public:
		void Execute(HoverableComponent* hoverableComponent) override
		{
			StartScreenObject* startScreen = static_cast<StartScreenObject*>(hoverableComponent->GetOwner());
			SpriteMeshComponent* spriteMeshComponent = startScreen->GetComponent<SpriteMeshComponent>();
			ID leftHandTextureID = spriteMeshComponent->GetTextureComponentIDs()[12];
			TextureComponent* headTexture = Game::GetCurrentHUDLevel()->GetComponent<TextureComponent>(leftHandTextureID);
			headTexture->SetTexture("UrukHeadHowToPlayMenu");
			startScreen->OnHowToPlayHovered();
		}
	};

	class HowToPlayUnHoveredCallback final : public CustomCallbackBase<HowToPlayUnHoveredCallback, IHoverableComponentCallbackBase>
	{
	public:
		void Execute(HoverableComponent* hoverableComponent) override
		{
			StartScreenObject* startScreen = static_cast<StartScreenObject*>(hoverableComponent->GetOwner());
			SpriteMeshComponent* spriteMeshComponent = startScreen->GetComponent<SpriteMeshComponent>();
			ID leftHandTextureID = spriteMeshComponent->GetTextureComponentIDs()[12];
			TextureComponent* headTexture = Game::GetCurrentHUDLevel()->GetComponent<TextureComponent>(leftHandTextureID);
			headTexture->SetTexture("UrukHeadMenu");
			startScreen->OnHowToPlayUnHovered();
		}
	};

	class QuitGameClickedCallback final : public CustomCallbackBase<QuitGameClickedCallback, IClickableComponentCallbackBase>
	{
	public:
		void Execute(ClickableComponent* clickableComponent) override
		{
			StartScreenObject* startScreen = static_cast<StartScreenObject*>(clickableComponent->GetOwner());
			startScreen->OnQuitClicked();
		}
	};

	class QuitGameHoveredCallback final : public CustomCallbackBase<QuitGameHoveredCallback, IHoverableComponentCallbackBase>
	{
	public:
		void Execute(HoverableComponent* hoverableComponent) override
		{
			StartScreenObject* startScreen = static_cast<StartScreenObject*>(hoverableComponent->GetOwner());
			startScreen->OnQuitHovered();
		}
	};

	class QuitGameUnHoveredCallback final : public CustomCallbackBase<QuitGameUnHoveredCallback, IHoverableComponentCallbackBase>
	{
	public:
		void Execute(HoverableComponent* hoverableComponent) override
		{
			StartScreenObject* startScreen = static_cast<StartScreenObject*>(hoverableComponent->GetOwner());
			startScreen->OnQuitUnHovered();
		}
	};

	class ToggleFullscreenClickedCallback final : public CustomCallbackBase<ToggleFullscreenClickedCallback, IClickableComponentCallbackBase>
	{
	public:
		void Execute(ClickableComponent* clickableComponent) override
		{
			StartScreenObject* startScreen = static_cast<StartScreenObject*>(clickableComponent->GetOwner());
			startScreen->OnSetFullscreenToggled();
		}
	};

	class MainMenuVolumeSettingBarDragMovedCallback final : public CustomCallbackBase<MainMenuVolumeSettingBarDragMovedCallback, IDraggableComponentCallbackBase>
	{
	public:
		void Execute(DraggableComponent* draggableComponent, float xMoveAmount, float yMoveAmount) override
		{
			IObject* owner = draggableComponent->GetOwner();
			
			owner->GetComponent<TransformComponent>()->MoveBy(-xMoveAmount, -yMoveAmount);
			
			const Vector3D originalOffset = draggableComponent->GetOffset();
			Vector3D newOffset = originalOffset + Vector3D{ xMoveAmount, 0.f };

			const float ratio = doodle::Width / 1920.f;
			newOffset[0] = clamp(newOffset[0], 1440.f * ratio, 1740.f * ratio);

			// volume: newOffset[0] / ratio - 1440.f -> 0 - 300
			
			draggableComponent->SetOffset(newOffset);
			owner->GetComponent<TextureComponent>()->SetOffset(newOffset);

			float volume = (newOffset[0] / ratio - 1440.f) / 300.f * 100.f;
			Game::GetAudioBuffer().SetVolume(volume);
		}
	};

	//Pause screen callbacks
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	class PauseMenuVolumeSettingBarDragMovedCallback final : public CustomCallbackBase<PauseMenuVolumeSettingBarDragMovedCallback, IDraggableComponentCallbackBase>
	{
	public:
		void Execute(DraggableComponent* draggableComponent, float xMoveAmount, float yMoveAmount) override
		{
			IObject* owner = draggableComponent->GetOwner();

			owner->GetComponent<TransformComponent>()->MoveBy(-xMoveAmount, -yMoveAmount);

			const Vector3D originalOffset = draggableComponent->GetOffset();
			Vector3D newOffset = originalOffset + Vector3D{ xMoveAmount, 0.f };

			const float ratio = doodle::Width / 1920.f;
			newOffset[0] = clamp(newOffset[0], 400.f * ratio, 700.f * ratio);

			// volume: newOffset[0] / ratio - 1440.f -> 0 - 300

			draggableComponent->SetOffset(newOffset);
			owner->GetComponent<TextureComponent>()->SetOffset(newOffset);
			
			float volume = (newOffset[0] / ratio - 400.f) / 300.f * 100.f;
			Game::GetAudioBuffer().SetVolume(volume);
		}
	};

	class PauseMenuExitClickCallback final : public CustomCallbackBase<PauseMenuExitClickCallback, IClickableComponentCallbackBase>
	{
	public:
		void Execute(ClickableComponent* clickableComponent) override
		{
			PauseScreenObject* pauseScreenObject = static_cast<PauseScreenObject*>(clickableComponent->GetOwner());
			pauseScreenObject->OnExitClicked();
		}
	};

	class PauseMenuExitHoveredCallback final : public CustomCallbackBase<PauseMenuExitHoveredCallback, IHoverableComponentCallbackBase>
	{
	public:
		void Execute(HoverableComponent* hoverableComponent) override
		{
			PauseScreenObject* pauseScreenObject = static_cast<PauseScreenObject*>(hoverableComponent->GetOwner());
			pauseScreenObject->OnExitHovered();
		}
	};

	class PauseMenuExitUnHoveredCallback final : public CustomCallbackBase<PauseMenuExitUnHoveredCallback, IHoverableComponentCallbackBase>
	{
	public:
		void Execute(HoverableComponent* hoverableComponent) override
		{
			PauseScreenObject* pauseScreenObject = static_cast<PauseScreenObject*>(hoverableComponent->GetOwner());
			pauseScreenObject->OnExitUnHovered();
		}
	};

	class PauseMenuRestartLevelClickCallback final : public CustomCallbackBase<PauseMenuRestartLevelClickCallback, IClickableComponentCallbackBase>
	{
	public:
		void Execute(ClickableComponent* clickableComponent) override
		{
			PauseScreenObject* pauseScreenObject = static_cast<PauseScreenObject*>(clickableComponent->GetOwner());
			pauseScreenObject->OnRestartLevelClicked();
		}
	};

	class PauseMenuRestartLevelHoveredCallback final : public CustomCallbackBase<PauseMenuRestartLevelHoveredCallback, IHoverableComponentCallbackBase>
	{
	public:
		void Execute(HoverableComponent* hoverableComponent) override
		{
			PauseScreenObject* pauseScreenObject = static_cast<PauseScreenObject*>(hoverableComponent->GetOwner());
			pauseScreenObject->OnRestartLevelHovered();
		}
	};

	class PauseMenuRestartLevelUnHoveredCallback final : public CustomCallbackBase<PauseMenuRestartLevelUnHoveredCallback, IHoverableComponentCallbackBase>
	{
	public:
		void Execute(HoverableComponent* hoverableComponent) override
		{
			PauseScreenObject* pauseScreenObject = static_cast<PauseScreenObject*>(hoverableComponent->GetOwner());
			pauseScreenObject->OnRestartLevelUnHovered();
		}
	};

	class PauseMenuToggleFullscreenClickCallback final : public CustomCallbackBase<PauseMenuToggleFullscreenClickCallback, IClickableComponentCallbackBase>
	{
	public:
		void Execute(ClickableComponent* clickableComponent) override
		{
			PauseScreenObject* pauseScreenObject = static_cast<PauseScreenObject*>(clickableComponent->GetOwner());
			pauseScreenObject->OnToggleFullscreenClicked();
		}
	};
////////////////////////////////////////////////////////////////////////////////////////////////////////
//How To Play Screen
	
	class HowToPlayBackToMenuClickCallback final : public CustomCallbackBase<HowToPlayBackToMenuClickCallback, IClickableComponentCallbackBase>
	{
	public:
		void Execute(ClickableComponent* clickableComponent) override
		{
			static_cast<HowToPlayScreenObject*>(clickableComponent->GetOwner())->OnBackToMenuClicked();
		}
	};

	class HowToPlayBackToMenuHoveredCallback final : public CustomCallbackBase<HowToPlayBackToMenuHoveredCallback, IHoverableComponentCallbackBase>
	{
	public:
		void Execute(HoverableComponent* hoverableComponent) override
		{
			static_cast<HowToPlayScreenObject*>(hoverableComponent->GetOwner())->OnBackToMenuHovered();
		}
	};

	class HowToPlayBackToMenuUnHoveredCallback final : public CustomCallbackBase<HowToPlayBackToMenuUnHoveredCallback, IHoverableComponentCallbackBase>
	{
	public:
		void Execute(HoverableComponent* hoverableComponent) override
		{
			static_cast<HowToPlayScreenObject*>(hoverableComponent->GetOwner())->OnBackToMenuUnHovered();
		}
	};
	
}
