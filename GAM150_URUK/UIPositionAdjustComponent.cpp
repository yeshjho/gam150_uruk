/*
    File Name: UIPositionAdjustComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "UIPositionAdjustComponent.h"

#include <doodle/environment.hpp>

#include "ClickableComponent.h"
#include "DraggableComponent.h"
#include "HoverableComponent.h"
#include "TextureComponent.h"
#include "TransformComponent.h"



namespace uruk
{
	UIPositionAdjustComponent::UIPositionAdjustComponent(std::vector<ID> textureComponentIDs, std::vector<ID> clickableComponentIDs, std::vector<ID> hoverableComponentIDs, std::vector<ID> draggableComponentIDs)
		: mTextureComponentIDs(std::move(textureComponentIDs)), mClickableComponentIDs(std::move(clickableComponentIDs)), mHoverableComponentIDs(std::move(hoverableComponentIDs)), mDraggableComponentIDs(std::move(draggableComponentIDs))
	{}


	void UIPositionAdjustComponent::OnBeginPlay()
	{
		mFullScreenWidth = doodle::Width;
		mFullScreenHeight = doodle::Height;

		mPrevWidth = 1920;
		mPrevHeight = 1080;

		for (const ID& id : mTextureComponentIDs)
		{
			TextureComponent* const texture = mLevel->GetComponent<TextureComponent>(id);
			mTextureComponentOriginalOffsets.push_back(texture->GetOffset());
		}
		for (const ID& id : mClickableComponentIDs)
		{
			ClickableComponent* const texture = mLevel->GetComponent<ClickableComponent>(id);
			mClickableComponentOriginalOffsets.push_back(texture->GetOffset());
		}
		for (const ID& id : mHoverableComponentIDs)
		{
			HoverableComponent* const texture = mLevel->GetComponent<HoverableComponent>(id);
			mHoverableComponentOriginalOffsets.push_back(texture->GetOffset());
		}
		for (const ID& id : mDraggableComponentIDs)
		{
			DraggableComponent* const texture = mLevel->GetComponent<DraggableComponent>(id);
			mDraggableComponentOriginalOffsets.push_back(texture->GetOffset());
		}
	}


	void UIPositionAdjustComponent::OnUpdate()
	{
		const int currentWidth = doodle::Width;
		const int currentHeight = doodle::Height;

		if (currentWidth != mPrevWidth || currentHeight != mPrevHeight)
		{
			const float currentWidthRatio = static_cast<float>(currentWidth) / mFullScreenWidth;
			const float currentHeightRatio = static_cast<float>(currentHeight) / mFullScreenHeight;

			const float widthRatio = mFullScreenWidth / 1920.f;
			const float heightRatio = mFullScreenHeight / 1080.f;

			const float finalWidthRatio = currentWidthRatio * widthRatio;
			const float finalHeightRatio = currentHeightRatio * heightRatio;

			GetOwner()->GetComponent<TransformComponent>()->SetScale(finalWidthRatio, finalHeightRatio);

			for (size_t i = 0; i < mTextureComponentIDs.size(); ++i)
			{
				TextureComponent* const texture = mLevel->GetComponent<TextureComponent>(mTextureComponentIDs.at(i));
				Vector3D offset = mTextureComponentOriginalOffsets.at(i);
				texture->SetOffset({ offset[0] * finalWidthRatio, offset[1] * finalHeightRatio });
			}
			for (size_t i = 0; i < mClickableComponentIDs.size(); ++i)
			{
				ClickableComponent* const texture = mLevel->GetComponent<ClickableComponent>(mClickableComponentIDs.at(i));
				Vector3D offset = mClickableComponentOriginalOffsets.at(i);
				texture->SetOffset({ offset[0] * finalWidthRatio, offset[1] * finalHeightRatio });
			}
			for (size_t i = 0; i < mHoverableComponentIDs.size(); ++i)
			{
				HoverableComponent* const texture = mLevel->GetComponent<HoverableComponent>(mHoverableComponentIDs.at(i));
				Vector3D offset = mHoverableComponentOriginalOffsets.at(i);
				texture->SetOffset({ offset[0] * finalWidthRatio, offset[1] * finalHeightRatio });
			}
			for (size_t i = 0; i < mDraggableComponentIDs.size(); ++i)
			{
				DraggableComponent* const texture = mLevel->GetComponent<DraggableComponent>(mDraggableComponentIDs.at(i));
				Vector3D offset = mDraggableComponentOriginalOffsets.at(i);
				texture->SetOffset({ offset[0] * finalWidthRatio, offset[1] * finalHeightRatio });
			}

			mPrevWidth = currentWidth;
			mPrevHeight = currentHeight;
		}
	}
}
