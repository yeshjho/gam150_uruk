/*
    File Name: UIPositionAdjustComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Component.h"

#include <vector>



namespace uruk
{
	class UIPositionAdjustComponent final : public Component<UIPositionAdjustComponent, EUpdateType::BY_FRAME, update_priorities::LOWEST_PRIORITY, 0, NO_SAVE>
	{
	public:
		UIPositionAdjustComponent(std::vector<ID> textureComponentIDs = {}, std::vector<ID> clickableComponentIDs = {}, std::vector<ID> hoverableComponentIDs = {}, std::vector<ID> draggableComponentIDs = {});
		
		void OnBeginPlay() override;
		
		void OnUpdate() override;



	private:
		std::vector<ID> mTextureComponentIDs;
		std::vector<Vector3D> mTextureComponentOriginalOffsets;
		std::vector<ID> mClickableComponentIDs;
		std::vector<Vector3D> mClickableComponentOriginalOffsets;
		std::vector<ID> mHoverableComponentIDs;
		std::vector<Vector3D> mHoverableComponentOriginalOffsets;
		std::vector<ID> mDraggableComponentIDs;
		std::vector<Vector3D> mDraggableComponentOriginalOffsets;
		
		int mFullScreenWidth = 0;
		int mFullScreenHeight = 0;

		int mPrevWidth = 0;
		int mPrevHeight = 0;
	};
}
