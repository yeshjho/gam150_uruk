/*
    File Name: UserDirectoryGetter.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "UserDirectoryGetter.h"

#include <Windows.h>
#include <UserEnv.h>
#undef ERROR


std::filesystem::path get_user_directory()
{
	static std::filesystem::path userPath = []()
	{
		TCHAR szDefaultPath[_MAX_PATH] = { 0 };
		DWORD dwBufLen = MAX_PATH;

		GetEnvironmentVariable("USERPROFILE", szDefaultPath, dwBufLen);
		
		return std::filesystem::path{ szDefaultPath } += "\\AppData\\Local\\URUK\\";
	}();

	std::filesystem::create_directories(userPath);

	return userPath;
}
