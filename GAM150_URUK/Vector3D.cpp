/*
    File Name: Vector3D.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Doyoon Kim
        Secondary: Joonho Hwang
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include <cassert>
#include "Matrix3X3.h"
#include "Vector3D.h"



//just a cheat to make it look clean
#define x xyz[0]
#define y xyz[1]
#define z xyz[2]


namespace uruk
{
	using namespace std;



	bool Vector3D::operator==(const Vector3D& other) const
	{
		return  (fabsf(x - other.x) <= FLT_EPSILON) &&
			(fabsf(y - other.y) <= FLT_EPSILON) &&
			(fabsf(z - other.z) <= FLT_EPSILON);
	}

	float& Vector3D::operator[](int index)
	{
		assert(index > -1 && index < 3);
		return xyz[index];
	}

	float Vector3D::operator[](int index) const
	{
		assert(index > -1 && index < 3);
		return xyz[index];
	}

	Vector3D Vector3D::GetNormal() const
	{
		float length = GetLength();
		Vector3D normal(x / length, y / length, z / length);
		
		if(length < DBL_EPSILON)
		{
			normal[0] = 0.f;
			normal[1] = 0.f;
			normal[2] = 0.f;
		}
		
		return normal;
	}

	float Vector3D::GetLength() const
	{
		return sqrtf(x * x + y * y + z * z);
	}

	void Vector3D::Normalize()
	{
		float length = GetLength();
		
		x /= length;
		y /= length;
		z /= length;
		
		if(length < DBL_EPSILON)
		{
			x = 0.f;
			y = 0.f;
			z = 0.f;
		}
	}

	ostream& operator<<(ostream& os, const Vector3D& v)
	{
		cout << "x :" << v[0] << " y :" << v[1] << " z :" << v[2];
		return os;
	}
}
