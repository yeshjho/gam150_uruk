/*
    File Name: WidthHeightBase.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "IWidthHeightBase.h"

#include <type_traits>

#include "TransformComponent.h"
#include "TransformOffsetBase.h"



namespace uruk
{
	struct Geometry
	{
		const Vector3D pointA;
		const Vector3D pointB;
		const Vector3D pointC;
		const Vector3D pointD;

		const float area;
	};



	template<typename T>
	class WidthHeightBase : public IWidthHeightBase
	{
	public:
		constexpr WidthHeightBase(float width = 0.f, float height = 0.f);


		[[nodiscard]] Geometry CalculateGeometry() const;
		[[nodiscard]] bool IsInside(const Vector3D& point) const;
	};
}

#include "WidthHeightBase.inl"
