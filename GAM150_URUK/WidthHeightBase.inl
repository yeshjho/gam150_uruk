/*
    File Name: WidthHeightBase.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	template<typename T>
	constexpr WidthHeightBase<T>::WidthHeightBase(const float width, const float height)
		: IWidthHeightBase(width, height)
	{}
	

	template<typename T>
	Geometry WidthHeightBase<T>::CalculateGeometry() const
	{
		Vector3D position{ 0.f, 0.f };
		float angle;
		Vector3D scale{ 0.f, 0.f };

		if constexpr (std::is_base_of_v<TransformOffsetBase<T>, T>)
		{
			const TransformOffsetBase<T>* const offsetBase = static_cast<const TransformOffsetBase<T>*>(static_cast<const T*>(this));
			position = offsetBase->GetAdjustedPosition();
			angle = offsetBase->GetAdjustedRotation();
			scale = offsetBase->GetAdjustedScale();
		}
		else
		{
			const TransformComponent* transform = static_cast<const T*>(this)->GetOwner()->template GetComponent<TransformComponent>();
			position = transform->GetPosition();
			angle = transform->GetRotationRadian();
			scale = transform->GetScale();
		}

		const float width = mWidth * scale[0];
		const float height = mHeight * scale[1];

		const Vector3D& pointA = { position[0], position[1] };
		const Vector3D& pointB = { pointA[0] - height * std::sinf(angle), pointA[1] + height * std::cosf(angle) };
		const Vector3D& pointC = { pointA[0] + width * std::cosf(angle), pointA[1] + width * std::sinf(angle) };
		const Vector3D& pointD = pointB + pointC - pointA;

		return { pointA, pointB, pointC, pointD, width * height };
	}

	template<typename T>
	bool WidthHeightBase<T>::IsInside(const Vector3D& point) const
	{
		auto [pointA, pointB, pointC, pointD, rectangleArea] = CalculateGeometry();

		const float pointArea = ((point - pointA).CrossProduct(point - pointB).GetLength()
								 + (point - pointB).CrossProduct(point - pointC).GetLength()
								 + (point - pointC).CrossProduct(point - pointD).GetLength()
								 + (point - pointD).CrossProduct(point - pointA).GetLength()) / 2.f;

		return rectangleArea >= pointArea;
	}
}
