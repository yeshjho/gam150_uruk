/*
    File Name: ZOrderBase.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "ZOrderBase.h"
#include "Serializer.h"



namespace uruk
{
	void ZOrderBase::OnEngineSave(std::ofstream& outputFileStream) const
	{
		serialize(outputFileStream, mZOrder);
	}

	void ZOrderBase::OnEngineLoad(std::ifstream& inputFileStream)
	{
		deserialize(inputFileStream, mZOrder);
	}
}
