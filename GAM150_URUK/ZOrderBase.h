/*
    File Name: ZOrderBase.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <fstream>



namespace uruk
{
	class ZOrderBase
	{
	public:
		constexpr ZOrderBase(int zOrder = 0) noexcept;

		[[nodiscard]] constexpr int GetZOrder() const noexcept;

		
		void OnEngineSave(std::ofstream& outputFileStream) const;
		void OnEngineLoad(std::ifstream& inputFileStream);



	protected:
		int mZOrder;
	};
}

#include "ZOrderBase.inl"
