/*
    File Name: ZOrderBase.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



namespace uruk
{
	constexpr ZOrderBase::ZOrderBase(const int zOrder) noexcept
		: mZOrder(zOrder)
	{}


	constexpr int ZOrderBase::GetZOrder() const noexcept
	{
		return mZOrder;
	}
}
