/*
    File Name: main.cpp
    Project Name: U.R.U.K
	Author(s):
		-Joonho Hwang
		Contribution(s):
			game setup
		-Doyoon Kim
		Contribution(s):
			UI setup
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include <doodle/environment.hpp>
#include <doodle/drawing.hpp>


#include "AudioPlayerComponent.h"
#include "CameraObject.h"
#include "EndingScreenObject.h"
#include "Game.h"
#include "InputComponent.h"
#include "LevelLoaderComponent.h"
#include "StartScreenObject.h"
#include "Texture.h"
#include "PauseScreenObject.h"

using namespace uruk;


int main()
{
	Game::Setup("resource/resources.txt");

	LevelLoaderComponent::LoadAllLevels("levels/levels.txt");
	
	const ID MenuLevelsID = Game::AddLevel(10_MB, 50);
	Game::SetMenuLevelID(MenuLevelsID);
	Game::SetCurrentLevel(MenuLevelsID);
	Game::SetCurrentHUDLevel(MenuLevelsID);

	Level* ingameMenus = Game::GetLevel(MenuLevelsID);

	ingameMenus->AddObject<CameraObject>();
	ID startScreenID = ingameMenus->AddObject<StartScreenObject>();
	Game::SetStartScreenID(startScreenID);
	ingameMenus->GetObject<StartScreenObject>(startScreenID)->OnEnabled();
	ID pauseScreenID = Game::GetLevel(MenuLevelsID)->GetObject<StartScreenObject>(startScreenID)->GetPauseScreenID();
	ingameMenus->GetObject<StartScreenObject>(startScreenID)->GetComponent<AudioPlayerComponent>()->Stop("m_ingame");
	ID endingScreenID = Game::GetLevel(MenuLevelsID)->AddObject<EndingScreenObject>();
	ingameMenus->GetObject<EndingScreenObject>(endingScreenID)->Disable();
	ingameMenus->GetObject<EndingScreenObject>(endingScreenID)->OnDisabled();
	Game::SetEndingScreenID(endingScreenID);

	InputComponent::StaticBindFunctionToKeyboardPressKey(doodle::KeyboardButtons::Escape, [MenuLevelsID,pauseScreenID]()
		{
			static ID minimapID;
		
			if (Game::GetCurrentLevel()->GetID() == MenuLevelsID)
				return;

			Game::GetCurrentLevel()->TogglePause();
			if (Game::GetCurrentLevel()->IsPaused())
			{
				Texture texture(doodle::capture_screenshot_to_image(0, 0, doodle::Width, doodle::Height));
				minimapID = Game::GetCurrentHUDLevel()->GetID();
				Game::SetCurrentHUDLevel(MenuLevelsID);
				Level* menulevel = Game::GetLevel(MenuLevelsID);
				PauseScreenObject* pauseScreenObject = menulevel->GetObject<PauseScreenObject>(pauseScreenID);
				pauseScreenObject->Enable();
				pauseScreenObject->OnEnabled();
				pauseScreenObject->SetBackgroundTexture(std::move(texture));
			}
			else
			{
				Game::SetCurrentHUDLevel(minimapID);
				Level* menulevel = Game::GetLevel(MenuLevelsID);
				PauseScreenObject* pauseScreenObject = menulevel->GetObject<PauseScreenObject>(pauseScreenID);
				pauseScreenObject->Disable();
				pauseScreenObject->OnDisabled();
			}
		});
	
	Game::BeginPlay();
	while (!Game::ShouldQuit())
	{
		Game::Update();
	}
	Game::CleanUp();
}