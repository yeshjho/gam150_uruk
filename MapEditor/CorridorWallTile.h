/*
    File Name: CorridorWallTile.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <doodle/image.hpp>

#include "Tile.h"



class CorridorWallTile final : public Tile<CorridorWallTile>
{
public:
	void Draw(double x, double y, double width, double height) const override;
};



inline void CorridorWallTile::Draw(double x, double y, double width, double height) const
{
	static auto img = uruk::GameResourceManager::GetImage("IconCorridorWall");
	draw_image(*img, x, y, width, height);
}
