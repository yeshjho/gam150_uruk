/*
    File Name: DoorTile.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <doodle/image.hpp>

#include "Tile.h"



class DoorTile final : public Tile<DoorTile>
{
public:
	void Draw(double x, double y, double width, double height) const override;

	void EditMetaData() override;

	void OnSave(std::ofstream& outputFileStream) override;
	void OnLoad(std::ifstream& inputFileStream) override;


	
public:
	std::string mNextLevel;
	int mSpawnPointNumber;
};



inline void DoorTile::Draw(double x, double y, double width, double height) const
{
	static auto img = uruk::GameResourceManager::GetImage("IconDoor");
	draw_image(*img, x, y, width, height);
}


inline void DoorTile::EditMetaData()
{
	edit_meta_data(mNextLevel, "Next Level");
	edit_meta_data(mSpawnPointNumber, "Spawn Point Number");
}


inline void DoorTile::OnSave(std::ofstream& outputFileStream)
{
	uruk::serialize(outputFileStream, mNextLevel);
	uruk::serialize(outputFileStream, mSpawnPointNumber);
}


inline void DoorTile::OnLoad(std::ifstream& inputFileStream)
{
	uruk::deserialize(inputFileStream, mNextLevel);
	uruk::deserialize(inputFileStream, mSpawnPointNumber);
}
