/*
    File Name: DungeonCoreEntity.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <doodle/image.hpp>

#include "Entity.h"



class DungeonCoreEntity final : public Entity<DungeonCoreEntity>
{
public:
	void Draw(double x, double y, double width, double height) const override;
};



inline void DungeonCoreEntity::Draw(double x, double y, double width, double height) const
{
	static auto img = uruk::GameResourceManager::GetImage("IconDungeonCore");
	draw_image(*img, x, y, width, height);
}
