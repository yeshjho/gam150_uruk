/*
    File Name: Entity.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "IEntity.h"

#include "GAM150_URUK/GameResourceManager.h"

#include "GeneralFunctions.h"


template<typename EntityType>
class Entity;

template<typename EntityType>
class Hack2
{
public:
	Hack2()
	{
		IEntity::constructors[uruk::IDManager::GetTypeID<EntityType>()] = Entity<EntityType>::Generate;
	}
};



template<typename EntityType>
class Entity : public IEntity
{
public:
	uruk::TypeID GetTypeID() const override final;


	static std::unique_ptr<IEntity> Generate();



private:
	static inline Hack2<EntityType> hack;
};



template <typename EntityType>
uruk::TypeID Entity<EntityType>::GetTypeID() const
{
	return uruk::IDManager::GetTypeID<EntityType>();
}


template <typename EntityType>
std::unique_ptr<IEntity> Entity<EntityType>::Generate()
{
	return std::move(std::make_unique<EntityType>());
}
