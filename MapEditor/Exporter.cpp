/*
    File Name: Exporter.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: Seunggeon Kim
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "Exporter.h"

#include "GAM150_URUK/CameraObject.h"
#include "GAM150_URUK/CorridorFloorObject.h"
#include "GAM150_URUK/CorridorWallObject.h"
#include "GAM150_URUK/DoorObject.h"
#include "GAM150_URUK/DungeonCoreObject.h"
#include "GAM150_URUK/EnemyObject.h"
#include "GAM150_URUK/FishGateObject.h"
#include "GAM150_URUK/Game.h"
#include "GAM150_URUK/HealthRestoreObject.h"
#include "GAM150_URUK/Level.h"
#include "GAM150_URUK/FlowFieldPathfinderComponent.h"
#include "GAM150_URUK/PathfinderObject.h"
#include "GAM150_URUK/ObstacleObject.h"
#include "GAM150_URUK/PlayerObject.h"
#include "GAM150_URUK/SpawnPointObject.h"
#include "GAM150_URUK/RoomFloorObject.h"
#include "GAM150_URUK/RoomWallObject.h"
#include "GAM150_URUK/TextComponent.h"

#include "CorridorFloorTile.h"
#include "CorridorWallTile.h"
#include "DoorTile.h"
#include "FishGateTile.h"
#include "NullTile.h"
#include "ObstacleTile.h"
#include "SpawnPointEntity.h"
#include "RoomFloorTile.h"
#include "RoomWallTile.h"
#include "SideWallTile.h"
#include "TopWallTile.h"

#include "DungeonCoreEntity.h"
#include "HealthRestoreEntity.h"
#include "KonDooriEntity.h"
#include "NullEntity.h"
#include "PlayerEntity.h"
#include "PoshigiEntity.h"
#include "SonXaaryEntity.h"


extern std::vector<std::vector<std::unique_ptr<ITile>>> tiles;
extern std::vector<std::vector<std::unique_ptr<IEntity>>> entities;

void strip_null_tiles();


void export_level_data()
{
	using namespace uruk;


	static constexpr unsigned int MAX_COUNT_MARGIN = 50;
	static constexpr size_t LARGEST_COMPONENT_SIZE = sizeof(TextComponent);
	static constexpr size_t LARGEST_OBJECT_SIZE = sizeof(EnemyObject);
	static constexpr unsigned int AVERAGE_COMPONENT_COUNT_PER_OBJECT = 4;
	static constexpr unsigned int COMPONENT_TYPE_COUNT = 30;
	static constexpr unsigned int OBJECT_TYPE_COUNT = 10;

	strip_null_tiles();
	
	unsigned int objectCount = 0;
	for (const auto& e : tiles)
	{
		objectCount += static_cast<unsigned int>(std::count_if(begin(e), end(e), [](const auto& tile) { return tile->GetTypeID() != IDManager::GetTypeID<NullTile>(); }));
	}
	for (const auto& e : entities)
	{
		objectCount += static_cast<unsigned int>(std::count_if(begin(e), end(e), [](const auto& entity) { return entity->GetTypeID() != IDManager::GetTypeID<NullEntity>(); }));
	}
	objectCount += MAX_COUNT_MARGIN;

	
	const ID levelID = Game::AddLevel(COMPONENT_TYPE_COUNT * LARGEST_COMPONENT_SIZE * AVERAGE_COMPONENT_COUNT_PER_OBJECT * objectCount, AVERAGE_COMPONENT_COUNT_PER_OBJECT * objectCount, 
		OBJECT_TYPE_COUNT * LARGEST_OBJECT_SIZE * objectCount, objectCount);
	Level& level = *Game::GetLevel(levelID);

	
	const size_t totalHeight = tiles.size();
	const size_t totalWidth = tiles.at(0).size();

	Vector3D playerPosition{ -1, -1 };

	for (size_t y = 0; y < totalHeight; ++y)
	{
		for (size_t x = 0; x < totalWidth; ++x)
		{
			if (entities.at(y).at(x)->GetTypeID() == IDManager::GetTypeID<PlayerEntity>())
			{
				playerPosition[0] = x * TILE_WIDTH;
				playerPosition[1] = ((totalHeight - 1) - y) * TILE_HEIGHT;
			}
		}
	}

	const ID pathfinderObjectID = level.AddObject<PathfinderObject>(Vector3D{ tiles.at(0).size(), tiles.size() }, Vector3D{ 0, 0 });
	FlowFieldPathfinderComponent& pathFinder = *level.GetObject<PathfinderObject>(pathfinderObjectID)->GetComponent<FlowFieldPathfinderComponent>();
	const ID pathfinderComponentID = level.GetObject<PathfinderObject>(pathfinderObjectID)->GetComponent<FlowFieldPathfinderComponent>()->GetID();
	
	ID playerID;
	if (playerPosition[0] != -1)
	{
		playerID = level.AddObject<PlayerObject>(playerPosition, pathfinderComponentID);
	}
	
	
	
	for (size_t y = 0; y < totalHeight; ++y)
	{
		for (size_t x = 0; x < totalWidth; ++x)
		{
			auto& tile = tiles.at(y).at(x);
			const TypeID typeID = tile->GetTypeID();
			const Vector3D position = { x * TILE_WIDTH, ((totalHeight - 1) - y) * TILE_HEIGHT };
			const Vector3D centerPosition = { (x + .5f) * TILE_WIDTH, ((totalHeight - 1) - y + .5f) * TILE_HEIGHT };
			
			if (typeID == IDManager::GetTypeID<CorridorFloorTile>())
			{
				level.AddObject<CorridorFloorObject>(position);
			}
			else if (typeID == IDManager::GetTypeID<CorridorWallTile>())
			{
				level.AddObject<CorridorWallObject>(position);
				pathFinder.SetTypeAt({ x, (totalHeight - 1) - y }, ENodeType::Obstacle);
			}
			else if (typeID == IDManager::GetTypeID<DoorTile>())
			{
				DoorTile* doorTile = dynamic_cast<DoorTile*>(tile.get());

				level.AddObject<DoorObject>(position, doorTile->mNextLevel, doorTile->mSpawnPointNumber);
				pathFinder.SetTypeAt({ x, (totalHeight - 1) - y }, ENodeType::Obstacle);
			}
			else if (typeID == IDManager::GetTypeID<FishGateTile>())
			{
				FishGateTile* fishGateTile = dynamic_cast<FishGateTile*>(tile.get());
				
				level.AddObject<FishGateObject>(position, fishGateTile->mDesiredLifeCount);
			}
			else if (typeID == IDManager::GetTypeID<ObstacleTile>())
			{
				level.AddObject<ObstacleObject>(position);
				pathFinder.SetTypeAt({ x, (totalHeight - 1) - y }, ENodeType::Obstacle);
			}
			else if (typeID == IDManager::GetTypeID<RoomFloorTile>())
			{
				level.AddObject<RoomFloorObject>(position);
			}
			else if (typeID == IDManager::GetTypeID<RoomWallTile>())
			{
				level.AddObject<RoomWallObject>(position);
				pathFinder.SetTypeAt({x, (totalHeight - 1) - y }, ENodeType::Obstacle);
			}
			else if (typeID == IDManager::GetTypeID<SideWallTile>())
			{
				level.AddObject<RoomWallObject>(position, false, true);
				pathFinder.SetTypeAt({ x, (totalHeight - 1) - y }, ENodeType::Obstacle);
			}
			else if (typeID == IDManager::GetTypeID<TopWallTile>())
			{
				level.AddObject<RoomWallObject>(position, true);
				pathFinder.SetTypeAt({ x, (totalHeight - 1) - y }, ENodeType::Obstacle);
			}
		}
	}

	for (size_t y = 0; y < totalHeight; ++y)
	{
		for (size_t x = 0; x < totalWidth; ++x)
		{
			auto& entity = entities.at(y).at(x);
			const TypeID typeID = entity->GetTypeID();
			const Vector3D position = { x * TILE_WIDTH, ((totalHeight - 1) - y) * TILE_HEIGHT };
			const Vector3D centerPosition = { (x + .5f) * TILE_WIDTH, ((totalHeight - 1) - y + .5f) * TILE_HEIGHT };

			if (typeID == IDManager::GetTypeID<DungeonCoreEntity>())
			{
				level.AddObject<DungeonCoreObject>(position);
			}
			else if (typeID == IDManager::GetTypeID<HealthRestoreEntity>())
			{
				level.AddObject<HealthRestoreObject>(position);
			}
			else if (typeID == IDManager::GetTypeID<KonDooriEntity>())
			{
				level.AddObject<EnemyObject>(EEnemyType::KonDoori, position, pathfinderComponentID, playerID);
			}
			else if (typeID == IDManager::GetTypeID<SpawnPointEntity>())
			{
				SpawnPointEntity* spawnPointTile = dynamic_cast<SpawnPointEntity*>(entity.get());

				level.AddObject<SpawnPointObject>(position, spawnPointTile->mIndex);
			}
			else if (typeID == IDManager::GetTypeID<PoshigiEntity>())
			{
				level.AddObject<EnemyObject>(EEnemyType::PohShigi, position, pathfinderComponentID, playerID);
			}
			else if (typeID == IDManager::GetTypeID<SonXaaryEntity>())
			{
				level.AddObject<EnemyObject>(EEnemyType::SonXaary, position, pathfinderComponentID, playerID);
			}
		}
	}

	const ID camID = level.AddObject<CameraObject>(playerPosition);
	if (!playerID.is_nil())
	{
		CameraObject* const cam = level.GetObject<CameraObject>(camID);
		cam->SetFollowingObject(level.GetObject<PlayerObject>(playerID));
	}
	
	std::filesystem::create_directory("saves/");
	Game::SaveLevel(levelID, get_file_name(L"saves/", L".urkl"));
	Game::RemoveLevel(levelID);
	Game::Update();
}


void strip_null_tiles()
{
	using namespace uruk;


	// Can't use remove-erase since we need to update the entities vector accordingly.

	// Remove vertical empty spaces
	for (size_t i = 0; i < tiles.size(); ++i)
	{
		if (std::all_of(begin(tiles[i]), end(tiles[i]), [](const auto& i) { return i->GetTypeID() == IDManager::GetTypeID<NullTile>(); }))
		{
			tiles.erase(begin(tiles) + i);
			// Entities "out of the bound" will be removed in this way.
			entities.erase(begin(entities) + i);

			i--;
		}
	}

	// Remove horizontal empty spaces
	const size_t SIZE_Y = tiles.size();
	for (size_t i = 0; i < tiles.at(0).size(); ++i)
	{
		bool allOf = true;
		
		for (size_t j = 0; j < SIZE_Y; ++j)
		{
			if (tiles.at(j).at(i)->GetTypeID() != IDManager::GetTypeID<NullTile>())
			{
				allOf = false;
				break;
			}
		}

		if (allOf)
		{
			for (size_t j = 0; j < SIZE_Y; ++j)
			{
				tiles.at(j).erase(begin(tiles.at(j)) + i);
				entities.at(j).erase(begin(entities.at(j)) + i);
			}
			i--;
		}
	}
}
