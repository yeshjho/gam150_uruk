/*
    File Name: FishGateTile.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <doodle/image.hpp>

#include "Tile.h"



class FishGateTile final : public Tile<FishGateTile>
{
public:
	void Draw(double x, double y, double width, double height) const override;
	void EditMetaData() override;

	void OnSave(std::ofstream& outputFileStream) override;
	void OnLoad(std::ifstream& inputFileStream) override;
	


public:
	unsigned int mDesiredLifeCount = 0;
};



inline void FishGateTile::Draw(double x, double y, double width, double height) const
{
	static auto img = uruk::GameResourceManager::GetImage("IconFishGate");
	draw_image(*img, x, y, width, height);
}


inline void FishGateTile::EditMetaData()
{
	edit_meta_data(mDesiredLifeCount, "Life Count");
}


inline void FishGateTile::OnSave(std::ofstream& outputFileStream)
{
	uruk::serialize(outputFileStream, mDesiredLifeCount);
}


inline void FishGateTile::OnLoad(std::ifstream& inputFileStream)
{
	uruk::deserialize(inputFileStream, mDesiredLifeCount);
}
