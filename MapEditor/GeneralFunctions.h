/*
    File Name: GeneralFunctions.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <string>
#include <sstream>
#include <iostream>



template<typename T>
void edit_meta_data(T& t, const std::string& name)
{
	std::string input;

	std::cout << name << "(" << t << "): ";
	std::getline(std::cin, input);
	if (input.empty())
	{
		std::cout << name << " left unchanged" << std::endl;
		return;
	}
	std::stringstream{ input } >> t;
	std::cout << name << " is now " << t << std::endl;
}

template<>
inline void edit_meta_data(std::string& t, const std::string& name)
{
	std::string input;

	std::cout << name << "(" << t << "): ";
	std::getline(std::cin, input);
	if (input.empty())
	{
		std::cout << name << " left unchanged" << std::endl;
		return;
	}

	t = input;
	std::cout << name << " is now " << t << std::endl;
}


inline std::wstring get_file_name(const std::wstring& prefix, const std::wstring& suffix, const std::wstring& defaultText = L"")
{
	std::wstring fileName;

	std::wcout << "Enter the file name(w/o extension)" << (defaultText.empty() ? L"" : L"(" + defaultText + L")") << ": ";
	std::getline(std::wcin, fileName);

	return prefix + (fileName.empty() ? defaultText : fileName) + suffix;
}
