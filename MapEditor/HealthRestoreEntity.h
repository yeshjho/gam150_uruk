/*
    File Name: HealthRestoreEntity.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <doodle/image.hpp>

#include "Entity.h"



class HealthRestoreEntity final : public Entity<HealthRestoreEntity>
{
public:
	void Draw(double x, double y, double width, double height) const override;
};



inline void HealthRestoreEntity::Draw(double x, double y, double width, double height) const
{
	static auto img = uruk::GameResourceManager::GetImage("IconHealthRestore");
	draw_image(*img, x, y, width, height);
}
