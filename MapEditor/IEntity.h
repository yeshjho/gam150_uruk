/*
    File Name: IEntity.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <functional>
#include <map>
#include <memory>

#include "GAM150_URUK/Internal.h"



class IEntity
{
public:
	virtual ~IEntity() = default;

	virtual void Draw(double x, double y, double width, double height) const = 0;
	[[nodiscard]] virtual uruk::TypeID GetTypeID() const = 0;
	virtual void EditMetaData() {}

	virtual void OnSave([[maybe_unused]] std::ofstream& outputFileStream) {}
	virtual void OnLoad([[maybe_unused]] std::ifstream& inputFileStream) {}


	static std::unique_ptr<IEntity> Generate(const uruk::TypeID& typeID);



public:
	static inline std::map<uruk::TypeID, std::function<std::unique_ptr<IEntity>()>> constructors;
};



inline std::unique_ptr<IEntity> IEntity::Generate(const uruk::TypeID& typeID)
{
	return constructors.at(typeID)();
}
