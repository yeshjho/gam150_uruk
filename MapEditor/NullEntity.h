/*
    File Name: NullEntity.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Entity.h"



class NullEntity final : public Entity<NullEntity>
{
public:
	void Draw(double, double, double, double) const override {}
};
