/*
    File Name: NullTile.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Tile.h"



class NullTile final : public Tile<NullTile>
{
public:
	void Draw(double, double, double, double) const override {}
};
