/*
    File Name: PlayerEntity.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <doodle/image.hpp>

#include "Entity.h"



class PlayerEntity final : public Entity<PlayerEntity>
{
public:
	void Draw(double x, double y, double width, double height) const override;
};



inline void PlayerEntity::Draw(double x, double y, double width, double height) const
{
	static auto img = uruk::GameResourceManager::GetImage("IconPlayer");
	draw_image(*img, x, y, width, height);
}
