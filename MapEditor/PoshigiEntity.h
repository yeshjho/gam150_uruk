/*
    File Name: PoshigiEntity.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <doodle/image.hpp>

#include "Entity.h"



class PoshigiEntity final : public Entity<PoshigiEntity>
{
public:
	void Draw(double x, double y, double width, double height) const override;
};



inline void PoshigiEntity::Draw(double x, double y, double width, double height) const
{
	static auto img = uruk::GameResourceManager::GetImage("IconPohshigi");
	draw_image(*img, x, y, width, height);
}
