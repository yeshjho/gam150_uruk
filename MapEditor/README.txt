● Tiles
[N]ull

[R]oomWall
[4]SideWall
[5]TopWall
[T(Right of R)]RoomFloor

[C]orridorWall
[V(Right of C)]CorridorFloor

[O]bstacle

[D]oor
[F]ishGate
===========================================
[Space] to switch between tiles/entities
===========================================
● Entities
[N]ull

Pla[Y]er

[P]ohShigi
Son[X]aary
[K]onDoori

[-] SpawnPoint

[Q]DungeonCore

[H]ealthRestore
===========================================
Left Click to place a tile/entity
Right Click to edit meta data
===========================================
[E]xport
[S]ave
[L]oad
===========================================
[Esc] quit
===========================================
Wheel to zoom in/out
Arrows to move around
===========================================
!WARNING!
- Do NOT edit after exporting.
- Creating new tiles(not placing) will be done only through zooming out. Thus, you won't be able to place tiles outside of existing range even if you move the camera around. In that case, simply zoom out once and zoom in back.
- All blank lines(no matter horizontal or vertical) will be cut out.
- If there's a RoomWall or RoomFloor or RoomTopWall, it'll think that this map as a Room. Otherwise, as a Corridor.