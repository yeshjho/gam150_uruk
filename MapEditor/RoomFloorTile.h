/*
    File Name: RoomFloorTile.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <doodle/image.hpp>

#include "Tile.h"



class RoomFloorTile final : public Tile<RoomFloorTile>
{
public:
	void Draw(double x, double y, double width, double height) const override;
};



inline void RoomFloorTile::Draw(double x, double y, double width, double height) const
{
	static auto img = uruk::GameResourceManager::GetImage("IconRoomFloor");
	draw_image(*img, x, y, width, height);
}
