/*
    File Name: RoomWallTile.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <doodle/image.hpp>

#include "Tile.h"



class RoomWallTile final : public Tile<RoomWallTile>
{
public:
	void Draw(double x, double y, double width, double height) const override;
};



inline void RoomWallTile::Draw(double x, double y, double width, double height) const
{
	static auto img = uruk::GameResourceManager::GetImage("IconRoomWall");
	draw_image(*img, x, y, width, height);
}
