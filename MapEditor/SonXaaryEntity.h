/*
    File Name: SonXaaryEntity.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <doodle/image.hpp>

#include "Entity.h"



class SonXaaryEntity final : public Entity<SonXaaryEntity>
{
public:
	void Draw(double x, double y, double width, double height) const override;
};



inline void SonXaaryEntity::Draw(double x, double y, double width, double height) const
{
	static auto img = uruk::GameResourceManager::GetImage("IconSonxaary");
	draw_image(*img, x, y, width, height);
}
