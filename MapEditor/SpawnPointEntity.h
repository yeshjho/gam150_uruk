/*
    File Name: SpawnPointEntity.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Entity.h"



class SpawnPointEntity final : public Entity<SpawnPointEntity>
{
public:
	void Draw(double x, double y, double width, double height) const override;

	void EditMetaData() override;

	void OnSave(std::ofstream& outputFileStream) override;
	void OnLoad(std::ifstream& inputFileStream) override;



public:
	int mIndex;
};



inline void SpawnPointEntity::Draw(double x, double y, double width, double height) const
{
	static auto img = uruk::GameResourceManager::GetImage("IconSpawnPoint");
	draw_image(*img, x, y, width, height);
}


inline void SpawnPointEntity::EditMetaData()
{
	edit_meta_data(mIndex, "Index");
}


inline void SpawnPointEntity::OnSave(std::ofstream& outputFileStream)
{
	uruk::serialize(outputFileStream, mIndex);
}


inline void SpawnPointEntity::OnLoad(std::ifstream& inputFileStream)
{
	uruk::deserialize(inputFileStream, mIndex);
}
