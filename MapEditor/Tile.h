/*
    File Name: Tile.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "ITile.h"

#include "GAM150_URUK/GameResourceManager.h"

#include "GeneralFunctions.h"


template<typename TileType>
class Tile;

template<typename TileType>
class Hack
{
public:
	Hack()
	{
		ITile::constructors[uruk::IDManager::GetTypeID<TileType>()] = Tile<TileType>::Generate;
	}
};



template<typename TileType>
class Tile : public ITile
{
public:
	uruk::TypeID GetTypeID() const override final;
	
	
	static std::unique_ptr<ITile> Generate();



private:
	static inline Hack<TileType> hack;
};



template <typename TileType>
uruk::TypeID Tile<TileType>::GetTypeID() const
{
	return uruk::IDManager::GetTypeID<TileType>();
}


template <typename TileType>
std::unique_ptr<ITile> Tile<TileType>::Generate()
{
	return std::move(std::make_unique<TileType>());
}
