/*
    File Name: TopWallTile.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <doodle/image.hpp>

#include "Tile.h"



class TopWallTile final : public Tile<TopWallTile>
{
public:
	void Draw(double x, double y, double width, double height) const override;
};



inline void TopWallTile::Draw(double x, double y, double width, double height) const
{
	static auto img = uruk::GameResourceManager::GetImage("IconTopWall");
	draw_image(*img, x, y, width, height);
}
