/*
    File Name: main.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
/***************************************************************************************************
 * Before you read through the codes:
 *
 *
 * The codes of this project are not meant to be maintained.
 * The program does not need to run fast, neither.
 * Thus, you will see a lot of dirty codes, including global vars, public vars, etc.
 * Please do not try to understand the codes of this project.
 * I just focused on "getting things done".
 * Thank you.
 *
 * Also, credits on Prof. David, since I got an inspiration from CS170's Final.
 ***************************************************************************************************/

#include <vector>

#include <doodle/doodle.hpp>

#include "GAM150_URUK/Serializer.h"

#include "Exporter.h"

#include "CorridorFloorTile.h"
#include "CorridorWallTile.h"
#include "DoorTile.h"
#include "FishGateTile.h"
#include "GAM150_URUK/GameResourceManager.h"
#include "NullTile.h"
#include "ObstacleTile.h"
#include "SpawnPointEntity.h"
#include "RoomFloorTile.h"
#include "RoomWallTile.h"
#include "SideWallTile.h"
#include "TopWallTile.h"

#include "DungeonCoreEntity.h"
#include "HealthRestoreEntity.h"
#include "KonDooriEntity.h"
#include "NullEntity.h"
#include "PlayerEntity.h"
#include "PoshigiEntity.h"
#include "SonXaaryEntity.h"



using namespace doodle;


constexpr double ratio = 1920. / 1080;
int vertical_tile_count = 30;
int horizontal_tile_count = static_cast<int>(vertical_tile_count * ratio);

std::vector<std::vector<std::unique_ptr<ITile>>> tiles(vertical_tile_count);
std::vector<std::vector<std::unique_ptr<IEntity>>> entities(vertical_tile_count);

std::unique_ptr<ITile> tile_cursor = std::make_unique<RoomWallTile>();
std::unique_ptr<IEntity> entity_cursor = std::make_unique<PoshigiEntity>();
bool is_cursor_mode_tile = true;

int x_offset = 0;
int y_offset = 0;

std::filesystem::path current_file_name;



void mark_level(int mouseX, int mouseY);
void edit_meta_data(int mouseX, int mouseY);
void draw_cursor(float tileWidth, float tileHeight);


void resize_tiles_entities(size_t oldVerticalCount = 0, size_t oldHorizontalCount = 0);


void save_level_data();
void load_level_data();



int main()
{
	resize_tiles_entities();
	
	create_window();
	set_frame_of_reference(FrameOfReference::LeftHanded_OriginTopLeft);
	set_outline_width(3);
	set_outline_color(255, 127);
	no_fill();

	uruk::GameResourceManager::Setup("resource/resources.txt");
	

	while (!is_window_closed())
	{
		update_window();
		clear_background();

		if (KeyIsPressed)
		{
			switch (Key)
			{
				case KeyboardButtons::Left:
					if (x_offset > 0)
						x_offset--;
					break;
				case KeyboardButtons::Right:
					x_offset++;
					break;
				case KeyboardButtons::Up:
					if (y_offset > 0)
						y_offset--;
					break;
				case KeyboardButtons::Down:
					y_offset++;
					break;
			}
		}


		
		const float tileWidth = static_cast<float>(Width) / static_cast<float>(horizontal_tile_count);
		const float tileHeight = static_cast<float>(Height) / static_cast<float>(vertical_tile_count);
		
		for (size_t y = 0; y < tiles.size(); ++y)
		{
			for (size_t x = 0; x < tiles.at(y).size(); ++x)
			{
				tiles.at(y).at(x)->Draw(tileWidth * (x - x_offset), tileHeight * (y - y_offset), tileWidth, tileHeight);
			}
		}
		for (size_t y = 0; y < entities.size(); ++y)
		{
			for (size_t x = 0; x < entities.at(y).size(); ++x)
			{
				entities.at(y).at(x)->Draw(tileWidth * (x - x_offset), tileHeight * (y - y_offset), tileWidth, tileHeight);
			}
		}
		draw_cursor(tileWidth, tileHeight);
	}

	uruk::GameResourceManager::CleanUp();
}


void on_mouse_moved(int mouseX, int mouseY)
{
	if (MouseIsPressed && MouseButton == MouseButtons::Left)
	{
		mark_level(mouseX, mouseY);
	}
}


void on_mouse_pressed(MouseButtons)
{
	switch (MouseButton)
	{
		case MouseButtons::Left:
			mark_level(get_mouse_x(), get_mouse_y());
			break;

		case MouseButtons::Right:
			edit_meta_data(get_mouse_x(), get_mouse_y());
			break;
	}
}

void on_mouse_wheel(int scrollAmount)
{
	const size_t oldVerticalCount = vertical_tile_count;
	vertical_tile_count -= scrollAmount;
	const size_t oldHorizontalCount = horizontal_tile_count;
	horizontal_tile_count = static_cast<int>(vertical_tile_count * ratio);

	if (vertical_tile_count > tiles.size())
	{
		resize_tiles_entities(oldVerticalCount, oldHorizontalCount);
	}
}

void on_key_released(KeyboardButtons button)
{
	switch (button)
	{
		case KeyboardButtons::Space:
			is_cursor_mode_tile = !is_cursor_mode_tile;
			break;
		
		case KeyboardButtons::V:
			tile_cursor.reset(new CorridorFloorTile);
			break;
		case KeyboardButtons::C:
			tile_cursor.reset(new CorridorWallTile);
			break;
		case KeyboardButtons::D:
			tile_cursor.reset(new DoorTile);
			break;
		case KeyboardButtons::F:
			tile_cursor.reset(new FishGateTile);
			break;
		case KeyboardButtons::N:
			is_cursor_mode_tile ? tile_cursor.reset(new NullTile) : entity_cursor.reset(new NullEntity);
			break;
		case KeyboardButtons::O:
			tile_cursor.reset(new ObstacleTile);
			break;
		case KeyboardButtons::R:
			tile_cursor.reset(new RoomWallTile);
			break;
		case KeyboardButtons::T:
			tile_cursor.reset(new RoomFloorTile);
			break;
		case KeyboardButtons::_4:
			tile_cursor.reset(new SideWallTile);
			break;
		case KeyboardButtons::_5:
			tile_cursor.reset(new TopWallTile);
			break;

		case KeyboardButtons::Q:
			entity_cursor.reset(new DungeonCoreEntity);
			break;
		case KeyboardButtons::H:
			entity_cursor.reset(new HealthRestoreEntity);
			break;
		case KeyboardButtons::K:
			entity_cursor.reset(new KonDooriEntity);
			break;
		case KeyboardButtons::Y:
			entity_cursor.reset(new PlayerEntity);
			break;
		case KeyboardButtons::Hyphen:
			entity_cursor.reset(new SpawnPointEntity);
			break;
		case KeyboardButtons::P:
			entity_cursor.reset(new PoshigiEntity);
			break;
		case KeyboardButtons::X:
			entity_cursor.reset(new SonXaaryEntity);
			break;

		case KeyboardButtons::E:
			export_level_data();
			break;
		case KeyboardButtons::S:
			save_level_data();
			break;
		case KeyboardButtons::L:
			load_level_data();
			break;

		case KeyboardButtons::Escape:
			close_window();
			break;
		default: ;
	}
}


void mark_level(const int mouseX, const int mouseY)
{
	const float tileWidth = static_cast<float>(Width) / static_cast<float>(horizontal_tile_count);
	const float tileHeight = static_cast<float>(Height) / static_cast<float>(vertical_tile_count);
	const int   mouseTileX = static_cast<int>((static_cast<float>(mouseX + x_offset * tileWidth)) / tileWidth);
	const int   mouseTileY = static_cast<int>((static_cast<float>(mouseY + y_offset * tileHeight)) / tileHeight);

	if (mouseTileX < 0 || mouseTileX >= tiles.at(0).size())
		return;
	if (mouseTileY < 0 || mouseTileY >= tiles.size())
		return;

	if (is_cursor_mode_tile)
	{
		const uruk::TypeID& typeID = tile_cursor->GetTypeID();
		tiles.at(mouseTileY).at(mouseTileX) = std::move(tile_cursor);
		tile_cursor = ITile::Generate(typeID);
	}
	else
	{
		const uruk::TypeID& typeID = entity_cursor->GetTypeID();
		entities.at(mouseTileY).at(mouseTileX) = std::move(entity_cursor);
		entity_cursor = IEntity::Generate(typeID);
	}
}


void edit_meta_data(int mouseX, int mouseY)
{
	const float tileWidth = static_cast<float>(Width) / static_cast<float>(horizontal_tile_count);
	const float tileHeight = static_cast<float>(Height) / static_cast<float>(vertical_tile_count);
	const int   mouseTileX = static_cast<int>((static_cast<float>(mouseX + x_offset * tileWidth)) / tileWidth);
	const int   mouseTileY = static_cast<int>((static_cast<float>(mouseY + y_offset * tileHeight)) / tileHeight);

	if (mouseTileX < 0 || mouseTileX >= tiles.at(0).size())
		return;
	if (mouseTileY < 0 || mouseTileY >= tiles.size())
		return;

	if (is_cursor_mode_tile)
	{
		tiles.at(mouseTileY).at(mouseTileX)->EditMetaData();
	}
	else
	{
		entities.at(mouseTileY).at(mouseTileX)->EditMetaData();
	}
}


void draw_cursor(const float tileWidth, const float tileHeight)
{
	const float mouseTileX = tileWidth * static_cast<int>(static_cast<float>(get_mouse_x()) / tileWidth);
	const float mouseTileY = tileHeight * static_cast<int>(static_cast<float>(get_mouse_y()) / tileHeight);

	if (is_cursor_mode_tile)
	{
		tile_cursor->Draw(mouseTileX, mouseTileY, tileWidth, tileHeight);
		if (tile_cursor->GetTypeID() == uruk::IDManager::GetTypeID<NullTile>())
		{
			push_settings();
			set_fill_color(0);
			draw_rectangle(mouseTileX, mouseTileY, tileWidth, tileHeight);
			pop_settings();
		}
		else
		{
			draw_rectangle(mouseTileX, mouseTileY, tileWidth, tileHeight);
		}
	}
	else
	{
		entity_cursor->Draw(mouseTileX, mouseTileY, tileWidth, tileHeight);
		if (entity_cursor->GetTypeID() == uruk::IDManager::GetTypeID<NullEntity>())
		{
			push_settings();
			set_fill_color(0);
			draw_rectangle(mouseTileX, mouseTileY, tileWidth, tileHeight);
			pop_settings();
		}
		else
		{
			draw_rectangle(mouseTileX, mouseTileY, tileWidth, tileHeight);
		}
	}
}


void resize_tiles_entities(size_t oldVerticalCount, size_t oldHorizontalCount)
{
	tiles.resize(vertical_tile_count);
	std::for_each(begin(tiles), begin(tiles) + oldVerticalCount,
		[oldHorizontalCount](auto& e)
		{
			e.resize(horizontal_tile_count);
			std::for_each(begin(e) + oldHorizontalCount, end(e), [](auto& e2) { e2.reset(new NullTile); });
		});
	std::for_each(begin(tiles) + oldVerticalCount, end(tiles),
		[](auto& e)
		{
			e.resize(horizontal_tile_count);
			std::for_each(begin(e), end(e), [](auto& e2) { e2.reset(new NullTile); });
		});

	entities.resize(vertical_tile_count);
	std::for_each(begin(entities), begin(entities) + oldVerticalCount,
		[oldHorizontalCount](auto& e)
		{
			e.resize(horizontal_tile_count);
			std::for_each(begin(e) + oldHorizontalCount, end(e), [](auto& e2) { e2.reset(new NullEntity); });
		});
	std::for_each(begin(entities) + oldVerticalCount, end(entities),
		[](auto& e)
		{
			e.resize(horizontal_tile_count);
			std::for_each(begin(e), end(e), [](auto& e2) { e2.reset(new NullEntity); });
		});
}


void save_level_data()
{
	std::filesystem::create_directory("saves/");
	const std::wstring fileName = get_file_name(L"saves/", L".uld", current_file_name);
	current_file_name = std::filesystem::path{ fileName }.stem();
	
	std::ofstream outputFileStream{ fileName };

	uruk::serialize(outputFileStream, vertical_tile_count);
	uruk::serialize(outputFileStream, horizontal_tile_count);
	
	for (const auto& tiles_ : tiles)
	{
		for (const auto& tile : tiles_)
		{
			uruk::serialize(outputFileStream, tile->GetTypeID());
			tile->OnSave(outputFileStream);
		}
	}
	for (const auto& entities_ : entities)
	{
		for (const auto& entity : entities_)
		{
			uruk::serialize(outputFileStream, entity->GetTypeID());
			entity->OnSave(outputFileStream);
		}
	}
}


void load_level_data()
{
	const std::wstring fileName = get_file_name(L"saves/", L".uld");
	if (!std::filesystem::exists(fileName))
	{
		std::cerr << "Such file doesn't exist.";
		return;
	}
	current_file_name = std::filesystem::path{ fileName }.stem();
	
	std::ifstream inputFileStream{ fileName };

	uruk::deserialize(inputFileStream, vertical_tile_count);
	uruk::deserialize(inputFileStream, horizontal_tile_count);

	resize_tiles_entities();

	uruk::TypeID typeID;
	
	for (int i = 0; i < vertical_tile_count; ++i)
	{
		for (int j = 0; j < horizontal_tile_count; ++j)
		{
			uruk::deserialize(inputFileStream, typeID);
			auto tile = ITile::Generate(typeID);
			tile->OnLoad(inputFileStream);
			tiles[i][j] = std::move(tile);
		}
	}

	for (int i = 0; i < vertical_tile_count; ++i)
	{
		for (int j = 0; j < horizontal_tile_count; ++j)
		{
			uruk::deserialize(inputFileStream, typeID);
			auto entity = IEntity::Generate(typeID);
			entity->OnLoad(inputFileStream);
			entities[i][j] = std::move(entity);
		}
	}
}
