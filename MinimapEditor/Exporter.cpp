/*
    File Name: Exporter.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "Exporter.h"

#include <map>

#include <doodle/image.hpp>

#include "GAM150_URUK/CameraObject.h"
#include "GAM150_URUK/Game.h"
#include "GAM150_URUK/HoverableComponent.h"
#include "GAM150_URUK/InputComponent.h"
#include "GAM150_URUK/Level.h"
#include "GAM150_URUK/MinimapComponent.h"
#include "GAM150_URUK/MinimapObject.h"
#include "GAM150_URUK/TransformComponent.h"
#include "GAM150_URUK/FollowComponent.h"

#include "GeneralFunctions.h"



extern std::map<std::string, std::vector<std::pair<int, int>>> corridor_pixel_coords;
extern std::map<std::string, std::pair<int, int>> room_pixel_coord;

extern doodle::Image* img;



void export_data()
{
	using namespace uruk;


	// sizeof STL in Debug differs from Release. Numbers down below is from Debug(since Debug ones are larger).
	/*const ID levelID = Game::AddLevel((sizeof(MinimapComponent) + sizeof(HoverableComponent) + sizeof(InputComponent) + sizeof(CameraComponent) + sizeof(TransformComponent) + sizeof(PhysicalShakeComponent) + sizeof(FollowComponent)) * 2, 2, 
		sizeof(MinimapObject) + sizeof(CameraObject), 1);*/
	const ID levelID = Game::AddLevel((160 + 152 + 112 + 80 + 112 + 112 + 120) * 2, 2,
		200 + 216, 1);

	Level& level = *Game::GetLevel(levelID);

	
	level.AddObject<CameraObject>();
	level.GetAllActiveObjects<CameraObject>().begin()->GetComponent<TransformComponent>()->SetScale(0.8f, 0.8f);

	
	const ID minimapID = level.AddObject<MinimapObject>();
	MinimapObject* const minimap = level.GetObject<MinimapObject>(minimapID);

	
	std::vector<doodle::Image::color> colors(img->GetWidth() * img->GetHeight());
	std::copy(img->begin(), img->end(), begin(colors));
	level.AddComponent<MinimapComponent>(minimap, img->GetWidth(), img->GetHeight(), colors, corridor_pixel_coords, room_pixel_coord);

	level.AddComponent<TransformComponent>(minimap, 0.f, 0.f, true);
	
	level.AddComponent<HoverableComponent>(minimap, float(MinimapComponent::SIDE_LENGTH), float(MinimapComponent::SIDE_LENGTH));

	level.AddComponent<InputComponent>(minimap);


	Game::SaveLevel(levelID, get_file_name(L"saves/", Level::SAVE_FILE_EXTENSION));
}
