/*
    File Name: GeneralFunctions.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <iostream>
#include <string>



inline std::wstring get_file_name(const std::wstring& prefix, const std::wstring& suffix)
{
	std::wstring fileName;

	std::cout << "Enter the file name(w/o extension): ";
	std::getline(std::wcin, fileName);

	return prefix + fileName + suffix;
}
