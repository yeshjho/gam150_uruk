[B]rush
[Space] eraser					※ 'E' is for Export! Don't mess it up!
[I]mage
[Left][Right] change image
===========================================
[C]orridor selection
corridor [D]eselection
[Enter] confirm corridor selection
[R]oom point
===========================================
Wheel to adjust the size of the cursor(including images)
===========================================
[S]ave
[L]oad
[E]xport
===========================================
[Esc] Quit
===========================================
- Fullscreen will be forced, use alt+tab to switch between console and the app.
- It'll be better to run in Release configuration.
- You can't delete once a corridor or a room is set, so type carefully. 
(Though replacing is possible, that is making a new one with the same name)
- If you want to change the gradient of the brush, go to `draw_brush` function and adjust the `OUTERMOST_GREY` value.