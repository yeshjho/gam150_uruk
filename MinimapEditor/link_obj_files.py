import glob
import sys

# Copy From Project Folder - Platform - Configuration - Copy To Project Folder
if (len(sys.argv) != 5):
	sys.exit()

sys.argv = sys.argv[1:]


library_prefix = "doodle.lib;Opengl32.lib;Gdi32.lib;User32.lib;sfml-system-d.lib;sfml-audio-d.lib;"
library_postfix = ";%(AdditionalDependencies)"

tag_prefix = "<AdditionalDependencies>"
tag_postfix = "</AdditionalDependencies>\n"

to_excludes = ["main.obj"]



copy_from = "\\".join(sys.argv[:3]) + "\\*.obj"
copy_from = copy_from.replace("\\\\", "\\")


object_files = glob.glob(copy_from)
for index, file in enumerate(object_files):
	object_files[index] = file.split("\\")[-1]

for excludant in to_excludes:
	if excludant in object_files:
		object_files.remove(excludant)

for old_file in glob.glob(sys.argv[0] + "\\*.cpp"):
	old_file_obj = old_file.split(".")[0] + ".obj"
	if old_file_obj in object_files:
		object_files.remove(old_file_obj)


library_list = ";".join(object_files)
libraries = library_prefix + library_list + library_postfix
to_write = tag_prefix + libraries + tag_postfix


copy_to = sys.argv[-1] + glob.glob("*.vcxproj")[0]

lines = []
with open(copy_to, 'r') as project_file:
	lines = project_file.readlines()
	for index, line in enumerate(lines):
		# replace including file list.
		if line.strip().startswith(tag_prefix):
			# if matches, assume we've already edited
			if line.strip() == to_write:
				sys.exit()
			lines[index] = to_write
	
with open(copy_to, 'w') as project_file:
	project_file.write(''.join(lines))
