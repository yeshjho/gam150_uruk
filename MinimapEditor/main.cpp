/*
    File Name: main.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
/***************************************************************************************************
 * Before you read through the codes:
 *
 *
 * The codes of this project are not meant to be maintained.
 * The program does not need to run fast, neither.
 * Thus, you will see a lot of dirty codes, including global vars, public vars, memory leaks, etc.
 * Please do not try to understand the codes of this project.
 * I just focused on "getting things done".
 * Thank you.
 ***************************************************************************************************/
#include <iostream>
#include <deque>
#include <map>

#include <doodle/doodle.hpp>

#include "GAM150_URUK/Math.h"
#include "GAM150_URUK/Serializer.h"

#include "Exporter.h"
#include "GeneralFunctions.h"

using namespace doodle;

enum class ECursorType
{
	BRUSH,
	CORRIDOR,
	CORRIDOR_DESELECT,
	ROOM,
	ERASER,
	IMAGE,
};


std::map<std::string, std::vector<std::pair<int, int>>> corridor_pixel_coords;
std::map<std::string, std::pair<int, int>> room_pixel_coord;

Image* img;

std::deque<Image*> cursor_images;
size_t cursor_image_index = 0;

ECursorType cursor_type = ECursorType::BRUSH;


int brush_radius = 5;
int corridor_radius = 10;
int eraser_half_side_length = 5;
int image_side_length = 100;


void draw_hover_brush();
void draw_hover_corridor();
void draw_hover_deselect_corridor();
void draw_hover_room();
void draw_hover_eraser();
void draw_hover_image();

void draw_brush();
void select_corridor();
void deselect_corridor();
void save_corridor();
void save_room();
void erase();
void embed_image();

void display_corridors();
void display_rooms();

void save_data();
void load_data();


int main()
{
	create_window();
	set_frame_of_reference(FrameOfReference::RightHanded_OriginBottomLeft);
	set_rectangle_mode(RectMode::Center);
	set_image_mode(RectMode::Corner);
	no_fill();
	show_cursor(false);
	set_outline_width(2);
	set_outline_color(255, 0, 255);
	
	toggle_full_screen();
	img = new Image{ Width, Height };
	cursor_images.push_back(new Image("thumbnails/kondoori.png"));
	cursor_images.push_back(new Image("thumbnails/sonxaary.png"));
	cursor_images.push_back(new Image("thumbnails/pohshigi.png"));
	cursor_images.push_back(new Image("thumbnails/fishgate.png"));
	cursor_images.push_back(new Image("thumbnails/heart.png"));
	
	for (auto& c : *img)
	{
		c = Color{ 255, 255, 255 };
	}

	set_callback_mouse_wheel(
		[](const int wheelAmount)
		{
			switch (cursor_type)
			{
				case ECursorType::BRUSH:
					brush_radius += wheelAmount;
					if (brush_radius < 1)
					{
						brush_radius = 1;
					}
					break;

				case ECursorType::CORRIDOR:
				case ECursorType::CORRIDOR_DESELECT:
					corridor_radius += wheelAmount;
					if (corridor_radius < 1)
					{
						corridor_radius = 1;
					}
					break;

				case ECursorType::ERASER:
					eraser_half_side_length += wheelAmount;
					if (eraser_half_side_length < 1)
					{
						eraser_half_side_length = 1;
					}
					break;

				case ECursorType::IMAGE:
					image_side_length += wheelAmount;
					if (image_side_length < 1)
					{
						image_side_length = 1;
					}
					break;
			}
		}
	);

	set_callback_key_pressed(
		[](const KeyboardButtons key)
		{
			switch (key)
			{
				case KeyboardButtons::B:
					set_outline_color(255, 0, 255);
					cursor_type = ECursorType::BRUSH;
					break;

				case KeyboardButtons::Space:
					set_outline_color(255, 255, 0);
					cursor_type = ECursorType::ERASER;
					break;

				case KeyboardButtons::C:
					set_outline_color(0, 255, 255);
					cursor_type = ECursorType::CORRIDOR;
					break;
				
				case KeyboardButtons::D:
					set_outline_color(0, 127, 127);
					cursor_type = ECursorType::CORRIDOR_DESELECT;
					break;

				case KeyboardButtons::R:
					set_outline_color(127, 255, 0);
					cursor_type = ECursorType::ROOM;
					break;

				case KeyboardButtons::Enter:
					switch (cursor_type)
					{
						case ECursorType::CORRIDOR:
							save_corridor();
							break;
					}
					break;

				case KeyboardButtons::I:
					set_outline_color(0, 127, 255);
					cursor_type = ECursorType::IMAGE;
					break;


				case KeyboardButtons::Left:
					cursor_image_index = cursor_image_index == 0 ? cursor_images.size() - 1 : cursor_image_index - 1;
					break;

				case KeyboardButtons::Right:
					cursor_image_index = cursor_image_index == cursor_images.size() - 1 ? 0 : cursor_image_index + 1;
					break;


				case KeyboardButtons::Escape:
					close_window();
					break;

				case KeyboardButtons::S:
					save_data();
					break;

				case KeyboardButtons::L:
					load_data();
					break;

				case KeyboardButtons::E:
					export_data();
					break;
			}
		}
	);
	
	while (!is_window_closed())
	{
		update_window();
		clear_background();
		draw_image(*img, 0, 0, Width, Height);
		switch (cursor_type)
		{
			case ECursorType::BRUSH:
				draw_hover_brush();
				if (MouseIsPressed)
				{
					draw_brush();
				}
				break;

			case ECursorType::CORRIDOR:
				draw_hover_corridor();
				if (MouseIsPressed)
				{
					select_corridor();
				}
				break;

			case ECursorType::CORRIDOR_DESELECT:
				draw_hover_deselect_corridor();
				if (MouseIsPressed)
				{
					deselect_corridor();
				}
				break;

			case ECursorType::ROOM:
				draw_hover_room();
				if (MouseIsPressed)
				{
					save_room();
				}
				break;
			
			case ECursorType::ERASER:
				draw_hover_eraser();
				if (MouseIsPressed)
				{
					erase();
				}
				break;

			case ECursorType::IMAGE:
				draw_hover_image();
				if (MouseIsPressed)
				{
					embed_image();
				}
				else
				{
					const int MOUSE_X = get_mouse_x();
					const int MOUSE_Y = get_mouse_y();
					
					push_settings();
					set_rectangle_mode(RectMode::Corner);
					draw_rectangle(MOUSE_X, MOUSE_Y, image_side_length, image_side_length);
					pop_settings();
				}
				break;
		}

		display_corridors();
		display_rooms();
	}
}


void draw_hover_brush()
{
	const int MOUSE_X = get_mouse_x();
	const int MOUSE_Y = get_mouse_y();

	draw_ellipse(MOUSE_X, MOUSE_Y, brush_radius * 2, brush_radius * 2);
}


void draw_hover_corridor()
{
	const int MOUSE_X = get_mouse_x();
	const int MOUSE_Y = get_mouse_y();

	draw_ellipse(MOUSE_X, MOUSE_Y, corridor_radius * 2, corridor_radius * 2);
}


void draw_hover_deselect_corridor()
{
	draw_hover_corridor();
}


void draw_hover_room()
{
	const int MOUSE_X = get_mouse_x();
	const int MOUSE_Y = get_mouse_y();

	draw_ellipse(MOUSE_X, MOUSE_Y, 5, 5);
}


void draw_hover_eraser()
{
	const int MOUSE_X = get_mouse_x();
	const int MOUSE_Y = get_mouse_y();

	draw_rectangle(MOUSE_X, MOUSE_Y, eraser_half_side_length * 2, eraser_half_side_length * 2);
}


void draw_hover_image()
{
	const int MOUSE_X = get_mouse_x();
	const int MOUSE_Y = get_mouse_y();

	draw_image(*cursor_images[cursor_image_index], MOUSE_X, MOUSE_Y, image_side_length, image_side_length);
}


void draw_brush()
{
	const int MOUSE_X = get_mouse_x();
	const int MOUSE_Y = Height - get_mouse_y();

	constexpr int OUTERMOST_GREY = 80;

	
	for (int r = brush_radius; r >= 0; --r)
	{
		const int grey = int(uruk::lerp(0.f, float(OUTERMOST_GREY), uruk::inverseLerp(0, float(brush_radius), float(r))));

		for (int x = MOUSE_X - r; x <= MOUSE_X + r; ++x)
		{
			for (int y = MOUSE_Y - r; y <= MOUSE_Y + r; ++y)
			{
				if (x < 0 || x >= Width || y < 0 || y >= Height)
				{
					continue;
				}
				
				if (auto& color = (*img)(x, y); color.red > grey)
				{
					if ((MOUSE_X - x) * (MOUSE_X - x) + (MOUSE_Y - y) * (MOUSE_Y - y) < r * r)
					{
						color = Color{ double(grey) };
					}
				}
			}
		}
	}
}


void select_corridor()
{
	const int MOUSE_X = get_mouse_x();
	const int MOUSE_Y = Height - get_mouse_y();


	for (int r = corridor_radius; r >= 0; --r)
	{
		for (int x = MOUSE_X - r; x <= MOUSE_X + r; ++x)
		{
			for (int y = MOUSE_Y - r; y <= MOUSE_Y + r; ++y)
			{
				if (x < 0 || x >= Width || y < 0 || y >= Height)
				{
					continue;
				}

				if ((MOUSE_X - x) * (MOUSE_X - x) + (MOUSE_Y - y) * (MOUSE_Y - y) < r * r)
				{
					auto& color = (*img)(x, y);
					if (color.red != 255)
					{
						color.green = 255;
						color.blue = 255;
					}
				}
			}
		}
	}
}


void deselect_corridor()
{
	const int MOUSE_X = get_mouse_x();
	const int MOUSE_Y = Height - get_mouse_y();


	for (int r = corridor_radius; r >= 0; --r)
	{
		for (int x = MOUSE_X - r; x <= MOUSE_X + r; ++x)
		{
			for (int y = MOUSE_Y - r; y <= MOUSE_Y + r; ++y)
			{
				if (x < 0 || x >= Width || y < 0 || y >= Height)
				{
					continue;
				}

				if ((MOUSE_X - x) * (MOUSE_X - x) + (MOUSE_Y - y) * (MOUSE_Y - y) < r * r)
				{
					auto& color = (*img)(x, y);
					if (color.red != 255 && color.green == 255)
					{
						color.green = color.red;
						color.blue = color.red;
					}
				}
			}
		}
	}
}


void save_corridor()
{
	std::string name;
	std::cout << "Enter the \"name\" of the corridor's Level as in levels.txt(not the file name!): ";
	std::getline(std::cin, name);

	for (int x = 0; x < img->GetWidth(); ++x)
	{
		for (int y = 0; y < img->GetHeight(); ++y)
		{
			if (auto& color = (*img)(x, y); color.red != 255 && color.green == 255)
			{
				corridor_pixel_coords[name].emplace_back(x, y);

				// reset selection
				color.green = color.red;
				color.blue = color.red;
			}
		}
	}
}


void save_room()
{
	std::string name;
	std::cout << "Enter the \"name\" of the room's Level as in levels.txt(not the file name!): ";
	std::getline(std::cin, name);

	room_pixel_coord[name] = { get_mouse_x(), Height - get_mouse_y() };
}


void erase()
{
	const int MOUSE_X = get_mouse_x();
	const int MOUSE_Y = Height - get_mouse_y();


	for (int x = MOUSE_X - eraser_half_side_length; x <= MOUSE_X + eraser_half_side_length; ++x)
	{
		for (int y = MOUSE_Y - eraser_half_side_length; y <= MOUSE_Y + eraser_half_side_length; ++y)
		{
			if (x < 0 || x >= Width || y < 0 || y >= Height)
			{
				continue;
			}

			(*img)(x, y) = Color{ 255 };
		}
	}
}


void embed_image()
{
	*img = capture_screenshot_to_image();
}


void display_corridors()
{
	push_settings();
	set_font_size(9);
	set_outline_color(255, 127, 127);
	for (const auto& [name, coords] : corridor_pixel_coords)
	{
		draw_text(name, coords[coords.size() / 2].first, Height - coords[coords.size() / 2].second);
	}
	pop_settings();
}


void display_rooms()
{
	push_settings();
	set_font_size(9);
	set_outline_color(127, 127, 255);
	for (const auto& [name, coord] : room_pixel_coord)
	{
		draw_ellipse(coord.first, Height - coord.second, 5, 5);
		draw_text(name, coord.first, Height - coord.second);
	}
	pop_settings();
}


void save_data()
{
	using namespace uruk;

	
	std::ofstream outputFileStream{ get_file_name(L"saves/", L".umd") };
	
	serialize(outputFileStream, corridor_pixel_coords);
	serialize(outputFileStream, room_pixel_coord);

	serialize(outputFileStream, img->GetWidth());
	serialize(outputFileStream, img->GetHeight());

	for (const auto& color : *img)
	{
		serialize(outputFileStream, color);
	}
}


void load_data()
{
	using namespace uruk;


	std::ifstream inputFileStream{ get_file_name(L"saves/", L".umd") };

	deserialize(inputFileStream, corridor_pixel_coords);
	deserialize(inputFileStream, room_pixel_coord);

	int width, height;
	
	deserialize(inputFileStream, width);
	deserialize(inputFileStream, height);

	img = new Image(width, height);

	for (auto& color : *img)
	{
		deserialize(inputFileStream, color);
	}
}
