/*
    File Name: ArrowComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "ArrowComponent.h"

#include <doodle/doodle.hpp>


#include "GAM150_URUK/ClickableComponent.h"
#include "GAM150_URUK/DraggableComponent.h"
#include "GAM150_URUK/InputComponent.h"
#include "GAM150_URUK/IWidthHeightBase.h"
#include "GAM150_URUK/Serializer.h"
#include "LinkObject.h"
#include "NodeObject.h"
#include "SimpleRectangleComponent.h"



void ArrowComponent::OnBeginPlay()
{
	if (!uruk::IDManager::IsIDValid(mEndNodeID))
	{
		GetOwner()->GetComponent<uruk::InputComponent>()->BindFunctionToMouseMoved([&]() { OnMouseMoved(); });
	}
}


void ArrowComponent::OnUpdate()
{
	cacheBiDirectionArrowIDs.clear();

	const bool isConnected = uruk::IDManager::IsIDValid(mEndNodeID);

	if (isConnected)
	{
		calculateStartEndPoint();
		updateOtherComponentsAttributes();
	}
	else
	{
		const uruk::TransformComponent* const componentTransform = GetOwner()->GetComponent<uruk::TransformComponent>();
		cacheStartPoint = componentTransform->GetPosition();
		cacheEndPoint = mEndPoint;
	}
}


void ArrowComponent::OnDraw(const uruk::Vector3D& camPosition, float, const uruk::Vector3D& camZoomScale) const
{
	using namespace doodle;
	using namespace uruk;


	const TransformComponent* const componentTransform = GetOwner()->GetComponent<TransformComponent>();
	const bool isOnUI = componentTransform->IsOnUI();

	const bool isConnected = IDManager::IsIDValid(mEndNodeID);

	Vector3D startPoint = cacheStartPoint;
	Vector3D endPoint = cacheEndPoint;
	

	if (!isOnUI)
	{
		startPoint[0] = (startPoint[0] - camPosition[0]) * camZoomScale[0] + float(Width) / 2.f;
		startPoint[1] = (startPoint[1] - camPosition[1]) * camZoomScale[1] + float(Height) / 2.f;
	}
	if (isConnected)
	{
		endPoint[0] = (endPoint[0] - camPosition[0]) * camZoomScale[0] + float(Width) / 2.f;
		endPoint[1] = (endPoint[1] - camPosition[1]) * camZoomScale[1] + float(Height) / 2.f;
	}

	const double xDiff = static_cast<double>(endPoint[0]) - startPoint[0];
	const double yDiff = static_cast<double>(endPoint[1]) - startPoint[1];

	const double angle = std::atan2(yDiff, xDiff);
	constexpr double length = 25;

	const Vector3D firstTip = { endPoint[0] - length * std::cos(angle + PI / 6), endPoint[1] - length * std::sin(angle + PI / 6) };
	const Vector3D secondTip = { endPoint[0] - length * std::cos(angle - PI / 6), endPoint[1] - length * std::sin(angle - PI / 6) };

	push_settings();
	set_fill_color(mColor);
	set_outline_color(mColor);
	set_outline_width(3);
	draw_line(startPoint[0], startPoint[1], endPoint[0], endPoint[1]);
	draw_line(firstTip[0], firstTip[1], endPoint[0], endPoint[1]);
	draw_line(secondTip[0], secondTip[1], endPoint[0], endPoint[1]);
	pop_settings();
}


void ArrowComponent::OnSave(std::ofstream& outputFileStream) const
{
	uruk::serialize(outputFileStream, mEndPoint);
	uruk::serialize(outputFileStream, mStartNodeID);
	uruk::serialize(outputFileStream, mEndNodeID);
	uruk::serialize(outputFileStream, mColor);
}

void ArrowComponent::OnLoad(std::ifstream& inputFileStream)
{
	uruk::deserialize(inputFileStream, mEndPoint);
	uruk::deserialize(inputFileStream, mStartNodeID);
	uruk::deserialize(inputFileStream, mEndNodeID);
	uruk::deserialize(inputFileStream, mColor);
}


void ArrowComponent::OnMouseMoved()
{
	mEndPoint = { static_cast<float>(doodle::get_mouse_x()), static_cast<float>(doodle::get_mouse_y()) };
}


void ArrowComponent::calculateStartEndPoint()
{
	const uruk::IWidthHeightBase* const startNode = static_cast<uruk::IWidthHeightBase*>(mLevel->GetObject<NodeObject>(mStartNodeID)->GetComponent<SimpleRectangleComponent>());
	const uruk::TransformComponent* const startPoint = mLevel->GetObject<NodeObject>(mStartNodeID)->GetComponent<uruk::TransformComponent>();

	const uruk::IWidthHeightBase* const endNode = static_cast<uruk::IWidthHeightBase*>(mLevel->GetObject<NodeObject>(mEndNodeID)->GetComponent<SimpleRectangleComponent>());
	const uruk::TransformComponent* const endPoint = mLevel->GetObject<NodeObject>(mEndNodeID)->GetComponent<uruk::TransformComponent>();

	const float startLeft = startPoint->GetPosition()[0];
	const float startBottom = startPoint->GetPosition()[1];
	const float startRight = startLeft + startNode->GetWidth();
	const float startTop = startBottom + startNode->GetHeight();

	const float endLeft = endPoint->GetPosition()[0];
	const float endBottom = endPoint->GetPosition()[1];
	const float endRight = endLeft + endNode->GetWidth();
	const float endTop = endBottom + endNode->GetHeight();

	if (startTop < endBottom)
	{
		if (startLeft > endRight)
		{
			cacheStartPoint = { startLeft , startTop };
			cacheEndPoint = { endRight, endBottom };
		}
		else if (startRight < endLeft)
		{
			cacheStartPoint = { startRight, startTop };
			cacheEndPoint = { endLeft, endBottom };
		}
		else
		{
			cacheStartPoint = { (startLeft + startRight) / 2.f, startTop };
			cacheEndPoint = { (endLeft + endRight) / 2.f, endBottom };
		}
	}
	else if (startBottom > endTop)
	{
		if (startLeft > endRight)
		{
			cacheStartPoint = { startLeft, startBottom };
			cacheEndPoint = { endRight, endTop };
		}
		else if (startRight < endLeft)
		{
			cacheStartPoint = { startRight, startBottom };
			cacheEndPoint = { endLeft, endTop };
		}
		else
		{
			cacheStartPoint = { (startLeft + startRight) / 2.f, startBottom };
			cacheEndPoint = { (endLeft + endRight) / 2.f, endTop };
		}
	}
	else if (startLeft > endRight)
	{
		cacheStartPoint = { startLeft, (startTop + startBottom) / 2.f };
		cacheEndPoint = { endRight, (endTop + endBottom) / 2.f };
	}
	else
	{
		cacheStartPoint = { startRight, (startTop + startBottom) / 2.f };
		cacheEndPoint = { endLeft, (endTop + endBottom) / 2.f };
	}


	uruk::Vector3D offset = { 0.f, 0.f };
	
	if (std::count(cacheBiDirectionArrowIDs.begin(), cacheBiDirectionArrowIDs.end(), mID))
	{
		offset = calculateOffset(-LinkObject::DEFAULT_HEIGHT * 2);
	}
	else
	{
		for (const LinkObject& link : mLevel->GetAllActiveObjects<LinkObject>())
		{
			if (link.GetStartNodeID() == mEndNodeID && link.GetEndNodeID() == mStartNodeID)
			{
				offset = calculateOffset(LinkObject::DEFAULT_HEIGHT * 2);
				cacheBiDirectionArrowIDs.push_back(mID);
				break;
			}
		}
	}

	cacheStartPoint[0] += offset[0];
	cacheEndPoint[1] += offset[1];
}


void ArrowComponent::updateOtherComponentsAttributes() const
{
	using namespace uruk;

	
	IObject* const owner = GetOwner();
	const float xDiff = cacheEndPoint[0] - cacheStartPoint[0];
	const float yDiff = cacheEndPoint[1] - cacheStartPoint[1];
	const float angle = std::atan2f(yDiff, xDiff);

	Vector3D offset = cacheStartPoint - owner->GetComponent<TransformComponent>()->GetPosition();
	offset += calculateOffset(LinkObject::DEFAULT_HEIGHT / 2.f);
	
	const float width = std::sqrtf(xDiff * xDiff + yDiff * yDiff);
	
	if (owner->HasComponent<ClickableComponent>())
	{
		for (const ID& id : owner->GetAllComponentIDs<ClickableComponent>())
		{
			ClickableComponent* const click = mLevel->GetComponent<ClickableComponent>(id);

			click->SetOffset(offset);
			click->SetRotationOffset(angle);
			click->SetWidth(width);
		}
	}
	if (owner->HasComponent<DraggableComponent>())
	{
		for (const ID& id : owner->GetAllComponentIDs<DraggableComponent>())
		{
			DraggableComponent* const drag = mLevel->GetComponent<DraggableComponent>(id);

			drag->SetOffset(offset);
			drag->SetRotationOffset(angle);
			drag->SetWidth(width);
		}
	}
}

uruk::Vector3D ArrowComponent::calculateOffset(float amount) const
{
	using namespace uruk;

	const float xDiff = cacheEndPoint[0] - cacheStartPoint[0];
	const float yDiff = cacheEndPoint[1] - cacheStartPoint[1];
	const float angle = std::atan2f(yDiff, xDiff);

	
	if (-1.f / 4.f * constants::PI < angle && angle < 1.f / 4.f * constants::PI)
	{
		return Vector3D{ 0.f, -amount };
	}
	else if (1.f / 4.f * constants::PI <= angle && angle < 3.f / 4.f * constants::PI)
	{
		return Vector3D{ amount, 0.f };
	}
	else if (-3.f / 4.f * constants::PI <= angle && angle <= -1.f / 4.f * constants::PI)
	{
		return Vector3D{ -amount, 0.f };
	}
	else
	{
		return  Vector3D{ 0.f, amount };
	}
}
