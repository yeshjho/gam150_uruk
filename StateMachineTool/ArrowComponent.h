/*
    File Name: ArrowComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <deque>

#include "GAM150_URUK/Component.h"
#include "GAM150_URUK/TransformOffsetBase.h"

#include <doodle/color.hpp>



class ArrowComponent : public uruk::Component<ArrowComponent, uruk::EUpdateType::BY_MILLISECOND, uruk::update_priorities::HIGHEST_PRIORITY, 0, uruk::EComponentAttribute::DRAWABLE>, public uruk::ZOrderBase
{
public:
	ArrowComponent(const uruk::ID& startNodeID = uruk::ID{}, const uruk::ID& endNodeID = uruk::ID{}, int zOrder = 0) noexcept
		: ZOrderBase(zOrder), mStartNodeID(startNodeID), mEndNodeID(endNodeID)
	{}


	void OnBeginPlay() override;
	void OnUpdate() override;
	void OnDraw(const uruk::Vector3D& camPosition, float camRotation, const uruk::Vector3D& camZoomScale) const override;

	void OnSave(std::ofstream& outputFileStream) const override;
	void OnLoad(std::ifstream& inputFileStream) override;
	

	void OnMouseMoved();

	constexpr const doodle::Color& GetColor() const noexcept { return mColor; }
	constexpr void SetColor(const doodle::Color& color) noexcept { mColor = color; }

	constexpr void FlipDirection() noexcept
	{
		const uruk::ID temp = mStartNodeID;
		mStartNodeID = mEndNodeID;
		mEndNodeID = temp;
	}

private:
	void calculateStartEndPoint();
	void updateOtherComponentsAttributes() const;
	uruk::Vector3D calculateOffset(float amount) const; 

	

private:
	// Only used while creating a link. After linking, mEndPointNodeID should be used.
	uruk::Vector3D mEndPoint = { 0.f, 0.f };

	std::deque<uruk::ID> cacheBiDirectionArrowIDs;

	uruk::ID mStartNodeID;
	uruk::ID mEndNodeID;
	doodle::Color mColor = doodle::Color{ 255 };

	uruk::Vector3D cacheStartPoint = { 0.f, 0.f }, cacheEndPoint = { 0.f, 0.f };
};
