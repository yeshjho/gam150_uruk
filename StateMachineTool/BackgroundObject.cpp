/*
    File Name: BackgroundObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "BackgroundObject.h"

#include "GAM150_URUK/CameraComponent.h"
#include "GAM150_URUK/ClickableComponent.h"
#include "GAM150_URUK/DraggableComponent.h"
#include "GAM150_URUK/Game.h"
#include "GAM150_URUK/InputComponent.h"
#include "GAM150_URUK/Serializer.h"
#include "GAM150_URUK/TransformComponent.h"
#include "CreateNodeCommand.h"
#include "DropDownMenuObject.h"
#include "GAM150_URUK/CameraObject.h"
#include "GeneralFunctions.h"



class BackgroundObjectDragMoveCallback final : public uruk::CustomCallbackBase<BackgroundObjectDragMoveCallback, uruk::IDraggableComponentCallbackBase>
{
public:
	BackgroundObjectDragMoveCallback(const uruk::ID& transformComponentID = uruk::ID{})
		: mTransformComponentID(transformComponentID)
	{}

	void Execute(uruk::DraggableComponent*, float xMoveAmount, float yMoveAmount) override
	{
		const uruk::Vector3D camScale = uruk::Game::GetCurrentLevel()->GetAllActiveObjects<uruk::CameraObject>().begin()->GetComponent<uruk::TransformComponent>()->GetScale();
		xMoveAmount *= 1 / camScale[0];
		yMoveAmount *= 1 / camScale[1];

		uruk::Game::GetCurrentLevel()->GetComponent<uruk::TransformComponent>(mTransformComponentID)->MoveBy(-2 * xMoveAmount, -2 * yMoveAmount);
	}

	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mTransformComponentID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mTransformComponentID);
	}



private:
	uruk::ID mTransformComponentID;
};

class BackgroundObjectCreateNodeCommandCallback final : public uruk::CustomCallbackBase<BackgroundObjectCreateNodeCommandCallback, uruk::IClickableComponentCallbackBase>
{
public:
	void Execute(uruk::ClickableComponent*) override
	{
		const uruk::Vector3D mousePosition{ static_cast<float>(doodle::get_mouse_x()), static_cast<float>(doodle::get_mouse_y()) };
		const uruk::Vector3D& worldPosition = uruk::Game::GetCurrentLevel()->GetAllActiveComponents<uruk::CameraComponent>().begin()->CalculateCoordProjectedOnWorld(mousePosition);

		Command::AppendAndExecute<CreateNodeCommand>(worldPosition[0], worldPosition[1]);
	}
};

class BackgroundObjectLoadCommandCallback final : public uruk::CustomCallbackBase<BackgroundObjectLoadCommandCallback, uruk::IClickableComponentCallbackBase>
{
public:
	void Execute(uruk::ClickableComponent*) override
	{
		load_level();
	}
};

class BackgroundObjectSaveCommandCallback final : public uruk::CustomCallbackBase<BackgroundObjectSaveCommandCallback, uruk::IClickableComponentCallbackBase>
{
public:
	void Execute(uruk::ClickableComponent*) override
	{
		save_level();
	}
};

class BackgroundObjectExportCommandCallback final : public uruk::CustomCallbackBase<BackgroundObjectExportCommandCallback, uruk::IClickableComponentCallbackBase>
{
public:
	void Execute(uruk::ClickableComponent*) override
	{
		export_level();
	}
};

class BackgroundObjectUndoCommandCallback final : public uruk::CustomCallbackBase<BackgroundObjectUndoCommandCallback, uruk::IClickableComponentCallbackBase>
{
public:
	void Execute(uruk::ClickableComponent*) override
	{
		Command::Undo();
	}
};

class BackgroundObjectRedoCommandCallback final : public uruk::CustomCallbackBase<BackgroundObjectRedoCommandCallback, uruk::IClickableComponentCallbackBase>
{
public:
	void Execute(uruk::ClickableComponent*) override
	{
		Command::Execute();
	}
};

class BackgroundObjectLeftPressRightPressCallback final : public uruk::CustomCallbackBase<BackgroundObjectLeftPressRightPressCallback, uruk::IClickableComponentCallbackBase>
{
public:
	void Execute(uruk::ClickableComponent*) override
	{
		remove_all_drop_down_menu_objects();
		deselect_all();
	}
};

class BackgroundObjectLeftDoubleClickCallback final : public uruk::CustomCallbackBase<BackgroundObjectLeftDoubleClickCallback, uruk::IClickableComponentCallbackBase>
{
public:
	void Execute(uruk::ClickableComponent* self) override
	{
		BackgroundObjectCreateNodeCommandCallback callback;
		callback.Execute(self);
	}
};

class BackgroundObjectRightClickCallback final : public uruk::CustomCallbackBase<BackgroundObjectRightClickCallback, uruk::IClickableComponentCallbackBase>
{
public:
	BackgroundObjectRightClickCallback(const uruk::ID& draggableComponentID = uruk::ID{})
		: mDraggableComponentID(draggableComponentID)
	{
		cacheMenuNames = {
			L"Create Node",
			L"Load",
			L"Save",
			L"Export",
			L"Undo",
			L"Redo"
		};

		cacheMenuFunctions.emplace_back(new BackgroundObjectCreateNodeCommandCallback());
		cacheMenuFunctions.emplace_back(new BackgroundObjectLoadCommandCallback());
		cacheMenuFunctions.emplace_back(new BackgroundObjectSaveCommandCallback());
		cacheMenuFunctions.emplace_back(new BackgroundObjectExportCommandCallback());
		cacheMenuFunctions.emplace_back(new BackgroundObjectUndoCommandCallback());
		cacheMenuFunctions.emplace_back(new BackgroundObjectRedoCommandCallback());
	}

	void Execute(uruk::ClickableComponent*) override
	{
		if (!uruk::Game::GetCurrentLevel()->GetComponent<uruk::DraggableComponent>(mDraggableComponentID)->HasMoved())
		{
			uruk::Game::GetCurrentHUDLevel()->AddObject<DropDownMenuObject>(static_cast<float>(doodle::get_mouse_x()), static_cast<float>(doodle::get_mouse_y()), cacheMenuNames, cacheMenuFunctions);
		}
	}

	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mDraggableComponentID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mDraggableComponentID);
	}
	

	
private:
	uruk::ID mDraggableComponentID;
	std::deque<std::wstring> cacheMenuNames;
	std::deque<IClickableComponentCallbackBase*> cacheMenuFunctions;
};



void BackgroundObject::OnConstructEnd()
{
	using namespace uruk;


	const ID TRANSFORM_ID = AddComponent<TransformComponent>(0.f, 0.f);


	AddComponent<InputComponent>();


	const ID DRAG_ID = AddComponent<DraggableComponent>(LENGTH, LENGTH, doodle::MouseButtons::Right, Z_ORDER);
	DraggableComponent* const drag = mLevel->GetComponent<DraggableComponent>(DRAG_ID);
	drag->SetOnMoved(std::make_unique<BackgroundObjectDragMoveCallback>(TRANSFORM_ID));


	const ID LEFT_CLICK_ID = AddComponent<ClickableComponent>(LENGTH, LENGTH, doodle::MouseButtons::Left, Z_ORDER);
	ClickableComponent* leftClick = mLevel->GetComponent<ClickableComponent>(LEFT_CLICK_ID);
	leftClick->SetOnPressed(std::make_unique<BackgroundObjectLeftPressRightPressCallback>());
	leftClick->SetOnDoubleClicked(std::make_unique<BackgroundObjectLeftDoubleClickCallback>());

	const ID RIGHT_CLICK_ID = AddComponent<ClickableComponent>(LENGTH, LENGTH, doodle::MouseButtons::Right, Z_ORDER);
	ClickableComponent* rightClick = mLevel->GetComponent<ClickableComponent>(RIGHT_CLICK_ID);
	rightClick->SetOnPressed(std::make_unique<BackgroundObjectLeftPressRightPressCallback>());
	rightClick->SetOnClicked(std::make_unique<BackgroundObjectRightClickCallback>(DRAG_ID));
}
