/*
    File Name: BackgroundObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "GAM150_URUK/Object.h"



class BackgroundObject final : public uruk::Object<BackgroundObject>
{
public:
	void OnConstructEnd() override;



public:
	static constexpr float LENGTH = 20000.f;
	
	static constexpr int Z_ORDER = -10000;
};
