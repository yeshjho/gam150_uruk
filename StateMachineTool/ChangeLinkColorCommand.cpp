/*
    File Name: ChangeLinkColorCommand.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "ChangeLinkColorCommand.h"

#include "GAM150_URUK/Level.h"
#include "ArrowComponent.h"
#include "LinkObject.h"



void ChangeLinkColorCommand::OnExecute()
{
	ArrowComponent* const arrow = level->GetObject<LinkObject>(mLinkID)->GetComponent<ArrowComponent>();
	mPreviousColor = arrow->GetColor();
	arrow->SetColor(cacheColor);
}

void ChangeLinkColorCommand::OnUndo()
{
	level->GetObject<LinkObject>(mLinkID)->GetComponent<ArrowComponent>()->SetColor(mPreviousColor);
}
