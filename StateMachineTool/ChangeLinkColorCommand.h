/*
    File Name: ChangeLinkColorCommand.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <doodle/color.hpp>

#include "Command.h"
#include "GAM150_URUK/Internal.h"



class ChangeLinkColorCommand final : public Command
{
public:
	ChangeLinkColorCommand(const uruk::ID& linkID, const doodle::Color& color)
		: mLinkID(linkID), cacheColor(color)
	{}
	
	void OnExecute() override;
	void OnUndo() override;
	std::string GetName() const override { return "Change Link Color"; }



private:
	uruk::ID mLinkID;
	doodle::Color cacheColor;
	doodle::Color mPreviousColor;
};
