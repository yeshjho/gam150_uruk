/*
    File Name: ChangeNodeColorCommand.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "ChangeNodeColorCommand.h"

#include "NodeObject.h"
#include "SelectableComponent.h"
#include "SimpleRectangleComponent.h"



void ChangeNodeColorCommand::OnExecute()
{
	// On re-do.
	if (!mPreviousColors.empty())
	{
		for (auto [id, _] : mPreviousColors)
		{
			level->GetObject<NodeObject>(id)->GetComponent<SimpleRectangleComponent>()->GetShapeAttributes().SetFillColor(cacheColor);
		}
	}

	for (NodeObject& node : level->GetAllActiveObjects<NodeObject>())
	{
		if (node.GetComponent<SelectableComponent>()->IsSelected())
		{
			ShapeAttributes& shapeAttributes = node.GetComponent<SimpleRectangleComponent>()->GetShapeAttributes();
			mPreviousColors.push_back({ node.GetID(), shapeAttributes.fillColor });
			shapeAttributes.SetFillColor(cacheColor);
		}
	}
}

void ChangeNodeColorCommand::OnUndo()
{
	for (auto [id, color] : mPreviousColors)
	{
		level->GetObject<NodeObject>(id)->GetComponent<SimpleRectangleComponent>()->GetShapeAttributes().SetFillColor(color);
	}
}
