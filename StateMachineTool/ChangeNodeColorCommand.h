/*
    File Name: ChangeNodeColorCommand.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Command.h"

#include <doodle/color.hpp>

#include "GAM150_URUK/Internal.h"



class ChangeNodeColorCommand final : public Command
{
public:
	inline ChangeNodeColorCommand(const doodle::Color& color) noexcept;

	
	void OnExecute() override;
	void OnUndo() override;
	std::string GetName() const override { return "Change Node Color"; }



private:
	doodle::Color cacheColor;

	std::deque<std::pair<uruk::ID, doodle::Color>> mPreviousColors;
};

#include "ChangeNodeColorCommand.inl"
