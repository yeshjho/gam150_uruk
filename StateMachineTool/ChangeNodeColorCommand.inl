/*
    File Name: ChangeNodeColorCommand.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



inline ChangeNodeColorCommand::ChangeNodeColorCommand(const doodle::Color& color) noexcept
	: cacheColor(color)
{}