/*
    File Name: ChangeNodeNameColorCommand.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "ChangeNodeNameColorCommand.h"


#include "GAM150_URUK/TextComponent.h"
#include "NodeObject.h"
#include "SelectableComponent.h"



void ChangeNodeNameColorCommand::OnExecute()
{
	// On re-do.
	if (!mPreviousColors.empty())
	{
		for (auto [id, _] : mPreviousColors)
		{
			level->GetObject<NodeObject>(id)->GetComponent<uruk::TextComponent>()->SetColor(cacheColor);
		}
	}

	for (NodeObject& node : level->GetAllActiveObjects<NodeObject>())
	{
		if (node.GetComponent<SelectableComponent>()->IsSelected())
		{
			uruk::TextComponent* const text = node.GetComponent<uruk::TextComponent>();
			mPreviousColors.push_back({ node.GetID(), text->GetColor() });
			text->SetColor(cacheColor);
		}
	}
}

void ChangeNodeNameColorCommand::OnUndo()
{
	for (auto [id, color] : mPreviousColors)
	{
		level->GetObject<NodeObject>(id)->GetComponent<uruk::TextComponent>()->SetColor(color);
	}
}
