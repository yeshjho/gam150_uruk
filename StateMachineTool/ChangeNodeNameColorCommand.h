/*
    File Name: ChangeNodeNameColorCommand.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Command.h"

#include <doodle/color.hpp>

#include "GAM150_URUK/Internal.h"



class ChangeNodeNameColorCommand final : public Command
{
public:
	inline ChangeNodeNameColorCommand(const doodle::Color& color) noexcept;


	void OnExecute() override;
	void OnUndo() override;
	std::string GetName() const override { return "Change Node Name Color"; }



private:
	doodle::Color cacheColor;

	std::deque<std::pair<uruk::ID, doodle::Color>> mPreviousColors;
};

#include "ChangeNodeNameColorCommand.inl"
