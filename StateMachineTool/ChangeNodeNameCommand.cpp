/*
    File Name: ChangeNodeNameCommand.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "ChangeNodeNameCommand.h"


#include "GAM150_URUK/ClickableComponent.h"
#include "GAM150_URUK/DraggableComponent.h"
#include "GAM150_URUK/TextComponent.h"
#include "NodeObject.h"
#include "SimpleRectangleComponent.h"



void ChangeNodeNameCommand::OnExecute()
{
	using namespace uruk;
	
	NodeObject* const node = level->GetObject<NodeObject>(cacheID);
	TextComponent* const text = node->GetComponent<TextComponent>();
	mBeforeName = text->GetText();
	text->SetText(cacheName);

	float textWidth = 2 * NodeObject::MINIMUM_MARGIN + TextComponent::GetTextWidth(cacheName, text->GetFontID(), text->GetFontSize());
	if (textWidth < NodeObject::DEFAULT_NODE_WIDTH)
	{
		textWidth = NodeObject::DEFAULT_NODE_WIDTH;
	}

	text->SetWidth(textWidth);
	node->GetComponent<SimpleRectangleComponent>()->SetWidth(textWidth);
	if (node->HasComponent<ClickableComponent>())
	{
		for (const ID id : node->GetAllComponentIDs<ClickableComponent>())
		{
			level->GetComponent<ClickableComponent>(id)->SetWidth(textWidth);
		}
	}
	if (node->HasComponent<DraggableComponent>())
	{
		for (const ID id : node->GetAllComponentIDs<DraggableComponent>())
		{
			level->GetComponent<DraggableComponent>(id)->SetWidth(textWidth);
		}
	}
}

void ChangeNodeNameCommand::OnUndo()
{
	using namespace uruk;
	
	NodeObject* const NODE = level->GetObject<NodeObject>(cacheID);
	TextComponent* const text = NODE->GetComponent<TextComponent>();
	text->SetText(mBeforeName);

	float textWidth = 2 * NodeObject::MINIMUM_MARGIN + TextComponent::GetTextWidth(mBeforeName, text->GetFontID(), text->GetFontSize());
	if (textWidth < NodeObject::DEFAULT_NODE_WIDTH)
	{
		textWidth = NodeObject::DEFAULT_NODE_WIDTH;
	}
	text->SetWidth(textWidth);
	NODE->GetComponent<SimpleRectangleComponent>()->SetWidth(textWidth);
	for (const ID id : NODE->GetAllComponentIDs<ClickableComponent>())
	{
		level->GetComponent<ClickableComponent>(id)->SetWidth(textWidth);
	}
	for (const ID id : NODE->GetAllComponentIDs<DraggableComponent>())
	{
		level->GetComponent<DraggableComponent>(id)->SetWidth(textWidth);
	}
}
