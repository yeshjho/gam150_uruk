/*
    File Name: ChangeNodeNameCommand.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Command.h"

#include <string>
#include <utility>

#include "GAM150_URUK/Internal.h"



class ChangeNodeNameCommand final : public Command
{
public:
	inline ChangeNodeNameCommand(uruk::ID nodeID, std::wstring name);

	
	void OnExecute() override;
	void OnUndo() override;
	std::string GetName() const override { return "Change Node Name"; }



private:
	uruk::ID cacheID;
	std::wstring cacheName;

	std::wstring mBeforeName;
};

#include "ChangeNodeNameCommand.inl"
