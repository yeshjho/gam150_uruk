/*
    File Name: ChangeNodeNameCommand.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



inline ChangeNodeNameCommand::ChangeNodeNameCommand(const uruk::ID nodeID, std::wstring name)
	: cacheID(nodeID), cacheName(std::move(name))
{}
