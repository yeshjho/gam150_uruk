/*
    File Name: Command.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "Command.h"

#include "StateMachineLogger.h"



void Command::AppendAndExecute(Command* command)
{
	removeBackwards();
	commands.emplace_back(command);
	Execute();
}

void Command::Execute()
{
	if (currentPosition >= commands.size())
	{
		return;
	}

	auto& command = commands.at(currentPosition++);
	command->OnExecute();
	state_machine_logger.LogDisplay(uruk::ELogCategory::CUSTOM_ACTION, "[" + std::to_string(currentPosition - 1) + "] Execute Command " + command->GetName());
}

void Command::Undo()
{
	if (currentPosition == 0)
	{
		return;
	}
	
	auto& command = commands.at(--currentPosition);
	command->OnUndo();
	state_machine_logger.LogDisplay(uruk::ELogCategory::CUSTOM_ACTION, "[" + std::to_string(currentPosition) + "] Undo Command " + command->GetName());

}


void Command::removeBackwards()
{
	commands.erase(commands.begin() + currentPosition, commands.end());
}
