/*
    File Name: Command.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <deque>
#include <memory>
#include <string>
#include <type_traits>



namespace uruk
{
	class Level;
}





class Command
{
public:
	Command(const Command&) = default;
	Command& operator=(const Command&) = default;
	
	Command(Command&&) = default;
	Command& operator=(Command&&) = default;
	
	virtual ~Command() = default;
	
	
	virtual void OnExecute() = 0;
	virtual void OnUndo() = 0;

	virtual std::string GetName() const = 0;


	template<typename CommandType, typename ...Args>
	static void AppendAndExecute(Args&&... args);

	static void AppendAndExecute(Command* command);
	
	static void Execute();
	static void Undo();

	static void SetLevel(uruk::Level* level_) noexcept;

protected:
	Command() = default;

private:
	static void removeBackwards();


protected:
	inline static uruk::Level* level = nullptr;
	
private:
	inline static std::deque<std::unique_ptr<Command>> commands;
	inline static size_t currentPosition = 0;
};

#include "Command.inl"
