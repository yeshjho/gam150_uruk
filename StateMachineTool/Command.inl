/*
    File Name: Command.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



template<typename CommandType, typename ...Args>
void Command::AppendAndExecute(Args&& ...args)
{
	static_assert(std::is_base_of_v<Command, CommandType>);

	removeBackwards();
	commands.push_back(std::make_unique<CommandType>(std::forward<Args>(args)...));
	Execute();
}


inline void Command::SetLevel(uruk::Level* level_) noexcept
{
	level = level_;
	commands.clear();
	currentPosition = 0;
}
