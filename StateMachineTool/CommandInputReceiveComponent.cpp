/*
    File Name: CommandInputReceiveComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "CommandInputReceiveComponent.h"

#include <doodle/input.hpp>


#include "DeleteLinksCommand.h"
#include "GAM150_URUK/CameraObject.h"
#include "GAM150_URUK/Game.h"
#include "GAM150_URUK/InputComponent.h"

#include "DeleteNodesCommand.h"
#include "GAM150_URUK/TransformComponent.h"
#include "GeneralFunctions.h"
#include "SelectableComponent.h"



void CommandInputReceiveComponent::OnBeginPlay()
{
	using namespace uruk;
	
	
	InputComponent* input = GetOwner()->GetComponent<InputComponent>();

	// Wheel (Camera zoom)
	input->BindFunctionToMouseWheeled([&]()
		{
			CameraObject* const cam = Game::GetCurrentLevel()->GetAllActiveObjects<CameraObject>().begin();
			const float wheelAmount = static_cast<float>(InputComponent::GetWheelAmount()) / 10.f;
			if (wheelAmount > 0 || cam->GetComponent<TransformComponent>()->GetScale()[0] > 0.1)
			{
				cam->AddZoomScale(wheelAmount);
			}
		});

	// Ctrl+A (Select All)
	input->BindFunctionToKeyboardReleaseKey(doodle::KeyboardButtons::A, [&]()
		{
			if (!InputComponent::IsKeyDown(doodle::KeyboardButtons::Control))
			{
				return;
			}

			for (SelectableComponent& component : mLevel->GetAllActiveComponents<SelectableComponent>())
			{
				component.Select();
			}
		});

	// Ctrl+D (Delete Nodes)
	input->BindFunctionToKeyboardReleaseKey(doodle::KeyboardButtons::D, []()
		{
			if (!InputComponent::IsKeyDown(doodle::KeyboardButtons::Control))
			{
				return;
			}
		
			Command::AppendAndExecute<DeleteLinksCommand>(ID{}, true);
			Command::AppendAndExecute<DeleteNodesCommand>();
		});

	// Ctrl+E (Export)
	input->BindFunctionToKeyboardReleaseKey(doodle::KeyboardButtons::E, []()
		{
			if (!InputComponent::IsKeyDown(doodle::KeyboardButtons::Control))
			{
				return;
			}

			export_level();
		});

	// Ctrl+S (Save)
	input->BindFunctionToKeyboardReleaseKey(doodle::KeyboardButtons::S, []()
		{
			if (!InputComponent::IsKeyDown(doodle::KeyboardButtons::Control))
			{
				return;
			}

			save_level();
		});

	// Ctrl+L (Load)
	input->BindFunctionToKeyboardReleaseKey(doodle::KeyboardButtons::L, []()
		{
			if (!InputComponent::IsKeyDown(doodle::KeyboardButtons::Control))
			{
				return;
			}

			load_level();
		});

	// Ctrl+Y (Redo)
	input->BindFunctionToKeyboardReleaseKey(doodle::KeyboardButtons::Y, []()
		{
			if (InputComponent::IsKeyDown(doodle::KeyboardButtons::Control))
			{
				Command::Execute();
			}
		});

	// Ctrl+Z (Undo), Ctrl+Shift+Z (Redo)
	input->BindFunctionToKeyboardReleaseKey(doodle::KeyboardButtons::Z, []()
		{
			if (InputComponent::IsKeyDown(doodle::KeyboardButtons::Control))
			{
				if (InputComponent::IsKeyDown(doodle::KeyboardButtons::Shift))
				{
					Command::Execute();
					return;
				}
				Command::Undo();
			}
		});

	// Delete (Delete Nodes)
	input->BindFunctionToKeyboardReleaseKey(doodle::KeyboardButtons::Delete, []()
		{
			Command::AppendAndExecute<DeleteLinksCommand>(ID{}, true);
			Command::AppendAndExecute<DeleteNodesCommand>();
		});
}
