/*
    File Name: CommandInputReceiveComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "GAM150_URUK/Component.h"



class CommandInputReceiveComponent : public uruk::Component<CommandInputReceiveComponent>
{
public:
	void OnBeginPlay() override;
};
