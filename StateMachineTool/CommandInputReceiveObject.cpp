/*
    File Name: CommandInputReceiveObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "CommandInputReceiveObject.h"

#include "GAM150_URUK/InputComponent.h"
#include "CommandInputReceiveComponent.h"



void CommandInputReceiveObject::OnConstructEnd()
{
	AddComponent<uruk::InputComponent>();


	AddComponent<CommandInputReceiveComponent>();
}
