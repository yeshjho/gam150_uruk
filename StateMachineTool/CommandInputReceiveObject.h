/*
    File Name: CommandInputReceiveObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "GAM150_URUK/Object.h"



class CommandInputReceiveObject final : public uruk::Object<CommandInputReceiveObject>
{
public:
	void OnConstructEnd() override;
};
