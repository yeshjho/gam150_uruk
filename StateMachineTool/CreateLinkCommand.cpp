/*
    File Name: CreateLinkCommand.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "CreateLinkCommand.h"



void CreateLinkCommand::OnExecute()
{
	level->GetObject<LinkObject>(mLinkID)->Enable();
}

void CreateLinkCommand::OnUndo()
{
	level->GetObject<LinkObject>(mLinkID)->Disable();
}
