/*
    File Name: CreateLinkCommand.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Command.h"

#include "LinkObject.h"



class CreateLinkCommand final : public Command
{
public:
	template<typename ...Args>
	CreateLinkCommand(Args... args);

	
	void OnExecute() override;
	void OnUndo() override;
	std::string GetName() const override { return "Create Link"; }



private:
	uruk::ID mLinkID;
};



template <typename ... Args>
CreateLinkCommand::CreateLinkCommand(Args... args)
{
	mLinkID = level->AddObject<LinkObject>(std::forward<Args>(args)...);
	level->GetObject<LinkObject>(mLinkID)->Disable();
}
