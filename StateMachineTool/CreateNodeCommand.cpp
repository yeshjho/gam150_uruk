/*
    File Name: CreateNodeCommand.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "CreateNodeCommand.h"



void CreateNodeCommand::OnExecute()
{
	level->GetObject<NodeObject>(mNodeID)->Enable();
}

void CreateNodeCommand::OnUndo()
{
	level->GetObject<NodeObject>(mNodeID)->Disable();
}
