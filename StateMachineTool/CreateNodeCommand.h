/*
    File Name: CreateNodeCommand.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Command.h"

#include "NodeObject.h"



class CreateNodeCommand final : public Command
{
public:
	template<typename ...Args>
	CreateNodeCommand(Args... args);
	
	
	void OnExecute() override;
	void OnUndo() override;
	std::string GetName() const override { return "Create Node"; }



private:
	uruk::ID mNodeID;
};

#include "CreateNodeCommand.inl"
