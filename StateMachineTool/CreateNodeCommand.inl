/*
    File Name: CreateNodeCommand.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



template<typename ...Args>
CreateNodeCommand::CreateNodeCommand(Args ...args)
{
	mNodeID = level->AddObject<NodeObject>(std::forward<Args>(args)...);
	level->GetObject<NodeObject>(mNodeID)->Disable();
}
