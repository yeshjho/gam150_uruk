/*
    File Name: DeleteLinksCommand.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "DeleteLinksCommand.h"

#include "GAM150_URUK/Level.h"
#include "LinkObject.h"
#include "NodeObject.h"
#include "SelectableComponent.h"



void DeleteLinksCommand::OnExecute()
{
	const bool isLinkID = uruk::IDManager::IsIDValid(cacheLinkID);
	
	if (isLinkID)
	{
		level->GetObject<LinkObject>(cacheLinkID)->Disable();
		return;
	}

	// On re-do.
	if (!mLinkIDs.empty())
	{
		for (uruk::ID id : mLinkIDs)
		{
			level->GetObject<LinkObject>(id)->Disable();
		}

		return;
	}

	for (LinkObject& link : level->GetAllActiveObjects<LinkObject>())
	{
		if (level->GetObject<NodeObject>(link.GetStartNodeID())->GetComponent<SelectableComponent>()->IsSelected()
		 || level->GetObject<NodeObject>(link.GetEndNodeID())->GetComponent<SelectableComponent>()->IsSelected())
		{
			mLinkIDs.push_back(link.GetID());
		}
	}
	for (const uruk::ID& id : mLinkIDs)
	{
		level->GetObject<LinkObject>(id)->Disable();
	}

	if (mIsFromDeleteNodes)
	{
		Execute();
	}
}

void DeleteLinksCommand::OnUndo()
{
	const bool isLinkID = uruk::IDManager::IsIDValid(cacheLinkID);

	if (isLinkID)
	{
		level->GetObject<LinkObject>(cacheLinkID)->Enable();
		return;
	}
	
	for (uruk::ID id : mLinkIDs)
	{
		level->GetObject<LinkObject>(id)->Enable();
	}
}
