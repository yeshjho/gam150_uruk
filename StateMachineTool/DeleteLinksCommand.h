/*
    File Name: DeleteLinksCommand.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Command.h"

#include <deque>

#include "GAM150_URUK/Internal.h"



class DeleteLinksCommand final : public Command
{
public:
	inline DeleteLinksCommand(uruk::ID id = uruk::ID{}, bool isFromDeleteNodes = false) noexcept;

	
	void OnExecute() override;
	void OnUndo() override;
	std::string GetName() const override { return "Delete Link"; }



private:
	bool mIsFromDeleteNodes;
	uruk::ID cacheLinkID;
	std::deque<uruk::ID> mLinkIDs;
};

#include "DeleteLinksCommand.inl"
