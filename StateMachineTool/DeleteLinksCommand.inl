/*
    File Name: DeleteLinksCommand.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



inline DeleteLinksCommand::DeleteLinksCommand(const uruk::ID id, bool isFromDeleteNodes) noexcept
	: mIsFromDeleteNodes(isFromDeleteNodes), cacheLinkID(id)
{}
