/*
    File Name: DeleteNodesCommand.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "DeleteNodesCommand.h"

#include "DeleteLinksCommand.h"
#include "NodeObject.h"
#include "SelectableComponent.h"



void DeleteNodesCommand::OnExecute()
{
	// On re-do.
	if (!mNodeIDs.empty())
	{
		for (uruk::ID id : mNodeIDs)
		{
			level->GetObject<NodeObject>(id)->Disable();
		}

		return;
	}

	for (NodeObject& node : level->GetAllActiveObjects<NodeObject>())
	{
		if (node.GetComponent<SelectableComponent>()->IsSelected())
		{
			mNodeIDs.push_back(node.GetID());
		}
	}
	for (const uruk::ID& id : mNodeIDs)
	{
		level->GetObject<NodeObject>(id)->Disable();
	}
}

void DeleteNodesCommand::OnUndo()
{
	for (uruk::ID id : mNodeIDs)
	{
		level->GetObject<NodeObject>(id)->Enable();
	}

	Undo();
}
