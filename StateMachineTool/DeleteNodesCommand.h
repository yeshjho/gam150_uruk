/*
    File Name: DeleteNodesCommand.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Command.h"

#include <deque>

#include "GAM150_URUK/Internal.h"



class DeleteNodesCommand final : public Command
{
public:
	void OnExecute() override;
	void OnUndo() override;
	std::string GetName() const override { return "Delete Node"; }



private:
	std::deque<uruk::ID> mNodeIDs;
};
