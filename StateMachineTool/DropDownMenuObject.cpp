/*
    File Name: DropDownMenuObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "DropDownMenuObject.h"

#include "GAM150_URUK/Game.h"
#include "GAM150_URUK/GameResourceManager.h"
#include "GAM150_URUK/HoverableComponent.h"
#include "GAM150_URUK/InputComponent.h"
#include "GAM150_URUK/TextComponent.h"
#include "GAM150_URUK/Serializer.h"
#include "GAM150_URUK/TransformComponent.h"
#include "SimpleRectangleComponent.h"



class DropDownMenuObjectClickCallback final : public uruk::CustomCallbackBase<DropDownMenuObjectClickCallback, uruk::IClickableComponentCallbackBase>
{
public:
	DropDownMenuObjectClickCallback(uruk::IClickableComponentCallbackBase* function = nullptr)
		: mFunction(function)
	{}

	void Execute(uruk::ClickableComponent* self) override
	{
		mFunction->Execute(self);
	}



private:
	uruk::IClickableComponentCallbackBase* mFunction;
};

class DropDownMenuObjectHoverCallback final : public uruk::CustomCallbackBase<DropDownMenuObjectHoverCallback, uruk::IHoverableComponentCallbackBase>
{
public:
	DropDownMenuObjectHoverCallback(const uruk::ID& rectID = uruk::ID{})
		: mRectID(rectID)
	{}

	void Execute(uruk::HoverableComponent*) override
	{
		uruk::Game::GetCurrentHUDLevel()->GetComponent<SimpleRectangleComponent>(mRectID)->SetShapeAttributes({ true, DropDownMenuObject::COLOR_HOVERED, false });
	}



private:
	uruk::ID mRectID;
};

class DropDownMenuObjectUnhoverCallback final : public uruk::CustomCallbackBase<DropDownMenuObjectUnhoverCallback, uruk::IHoverableComponentCallbackBase>
{
public:
	DropDownMenuObjectUnhoverCallback(const uruk::ID& rectID = uruk::ID{})
		: mRectID(rectID)
	{}

	void Execute(uruk::HoverableComponent*) override
	{
		uruk::Game::GetCurrentHUDLevel()->GetComponent<SimpleRectangleComponent>(mRectID)->SetShapeAttributes({ true, DropDownMenuObject::COLOR_UNHOVERED, false });
	}



private:
	uruk::ID mRectID;
};



DropDownMenuObject::DropDownMenuObject(const float x, const float y, const std::deque<std::wstring>& menuNames, const std::deque<uruk::IClickableComponentCallbackBase*>& menuFunctions)
	: cacheX(x), cacheY(y), cacheMenuNames(menuNames), cacheMenuFunctions(menuFunctions), cacheMenuCount(cacheMenuNames.size())
{
	_ASSERT(cacheMenuCount == cacheMenuFunctions.size());
}

void DropDownMenuObject::OnConstructEnd()
{
	using namespace uruk;
	
	
	AddComponent<TransformComponent>(cacheX, cacheY, true);


	AddComponent<InputComponent>();

	
	
	for (size_t i = 0; i < cacheMenuCount; ++i)
	{
		const float OFFSET = -MENU_HEIGHT * (i + 1);

		const ID RECT_ID = AddComponent<SimpleRectangleComponent>(DEFAULT_MENU_WIDTH, MENU_HEIGHT, Z_ORDER);
		SimpleRectangleComponent* const rect = mLevel->GetComponent<SimpleRectangleComponent>(RECT_ID);
		rect->SetOffset(0.f, OFFSET);
		rect->SetShapeAttributes({ true, COLOR_UNHOVERED, false });


		const ID TEXT_ID = AddComponent<TextComponent>(cacheMenuNames.at(i), DEFAULT_MENU_WIDTH, MENU_HEIGHT, Z_ORDER);
		TextComponent* const text = mLevel->GetComponent<TextComponent>(TEXT_ID);
		text->SetOffset(0.f, OFFSET);
		text->SetFontID(GameResourceManager::GetFont("Arial"));
		text->SetFontSize(9.f);
		text->SetColor({ 255, 255, 255 });
		text->SetHorizontalAlign(ETextHorizontalAlign::CENTER);
		text->SetVerticalAlign(ETextVerticalAlign::CENTER);
		
		
		const ID CLICK_ID = AddComponent<ClickableComponent>(DEFAULT_MENU_WIDTH, MENU_HEIGHT, doodle::MouseButtons::Left, Z_ORDER, false);
		ClickableComponent* const click = mLevel->GetComponent<ClickableComponent>(CLICK_ID);
		click->SetOffset(0.f, OFFSET);
		click->SetOnPressed(std::make_unique<DropDownMenuObjectClickCallback>(cacheMenuFunctions.at(i)));

		
		const ID HOVER_ID = AddComponent<HoverableComponent>(DEFAULT_MENU_WIDTH, MENU_HEIGHT, Z_ORDER);
		HoverableComponent* const hover = mLevel->GetComponent<HoverableComponent>(HOVER_ID);
		hover->SetOffset(0.f, OFFSET);
		hover->SetOnHovered(std::make_unique<DropDownMenuObjectHoverCallback>(RECT_ID));
		hover->SetOnUnhovered(std::make_unique<DropDownMenuObjectUnhoverCallback>(RECT_ID));
	}
}
