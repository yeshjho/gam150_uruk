/*
    File Name: DropDownMenuObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "GAM150_URUK/Object.h"

#include <deque>
#include <string>
#include <doodle/color.hpp>

#include "GAM150_URUK/ClickableComponent.h"



class DropDownMenuObject final : public uruk::Object<DropDownMenuObject>
{
public:
	DropDownMenuObject(float x = 0, float y = 0, const std::deque<std::wstring>& menuNames = {}, const std::deque<uruk::IClickableComponentCallbackBase*>& menuFunctions = {});
	
	void OnConstructEnd() override;



public:
	static constexpr int Z_ORDER = 10000;
	
	static constexpr float DEFAULT_MENU_WIDTH = 150.f;
	static constexpr float MENU_HEIGHT = 30.f;

	static constexpr doodle::Color COLOR_UNHOVERED{ 50, 53, 59 };
	static constexpr doodle::Color COLOR_HOVERED{ 70, 73, 79 };

private:
	float cacheX;
	float cacheY;
	
	const std::deque<std::wstring>& cacheMenuNames;
	const std::deque<uruk::IClickableComponentCallbackBase*>& cacheMenuFunctions;
	
	size_t cacheMenuCount;
};
