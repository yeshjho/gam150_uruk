/*
    File Name: EditCodeCommand.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "EditCodeCommand.h"

#include <cstdio>
#include <fstream>
#include <process.h>
#include <sstream>

#include "GAM150_URUK/Level.h"
#include "LinkObject.h"



void EditCodeCommand::OnExecute()
{
	LinkObject* const link = level->GetObject<LinkObject>(cacheLinkID);

	// On re-do.
	if (!mAfterCode.empty())
	{
		link->SetCode(mAfterCode);
		return;
	}
	
	mBeforeCode = link->GetCode();

	static const char* fileName = "temp.txt";
	
	std::wofstream tempFile{ fileName };
	tempFile << mBeforeCode;
	tempFile.close();
	system(fileName);  // Let the user edit the code text.
	
	std::wifstream resultFile{ fileName };
	std::wstringstream buffer;
	buffer << resultFile.rdbuf();

	mAfterCode = buffer.str();
	link->SetCode(mAfterCode);
	
	std::remove(fileName);
}

void EditCodeCommand::OnUndo()
{
	level->GetObject<LinkObject>(cacheLinkID)->SetCode(mBeforeCode);
}
