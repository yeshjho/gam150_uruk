/*
    File Name: EditCodeCommand.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Command.h"

#include <string>

#include "GAM150_URUK/Internal.h"



class EditCodeCommand final : public Command
{
public:
	inline EditCodeCommand(uruk::ID linkID) noexcept;

	
	void OnExecute() override;
	void OnUndo() override;
	std::string GetName() const override { return "Edit Code"; }



private:
	uruk::ID cacheLinkID;
	
	std::wstring mBeforeCode;
	std::wstring mAfterCode;
};

#include "EditCodeCommand.inl"
