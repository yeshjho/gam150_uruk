/*
    File Name: EditCodeCommand.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



inline EditCodeCommand::EditCodeCommand(uruk::ID linkID) noexcept
	: cacheLinkID(linkID)
{}
