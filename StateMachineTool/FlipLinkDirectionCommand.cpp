/*
    File Name: FlipLinkDirectionCommand.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "FlipLinkDirectionCommand.h"

#include "GAM150_URUK/Level.h"
#include "LinkObject.h"



void FlipLinkDirectionCommand::OnExecute()
{
	level->GetObject<LinkObject>(cacheLinkID)->FlipDirection();
}

void FlipLinkDirectionCommand::OnUndo()
{
	level->GetObject<LinkObject>(cacheLinkID)->FlipDirection();
}
