/*
    File Name: FlipLinkDirectionCommand.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Command.h"

#include "GAM150_URUK/Internal.h"



class FlipLinkDirectionCommand final : public Command
{
public:
	inline FlipLinkDirectionCommand(uruk::ID linkID);

	
	void OnExecute() override;
	void OnUndo() override;
	std::string GetName() const override { return "Flip Link Direction"; }



private:
	uruk::ID cacheLinkID;
};

#include "FlipLinkDirectionCommand.inl"
