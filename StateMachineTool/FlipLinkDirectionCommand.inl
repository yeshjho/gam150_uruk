/*
    File Name: FlipLinkDirectionCommand.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



inline FlipLinkDirectionCommand::FlipLinkDirectionCommand(const uruk::ID linkID)
	: cacheLinkID(linkID)
{}
