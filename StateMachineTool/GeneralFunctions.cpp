/*
    File Name: GeneralFunctions.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "GeneralFunctions.h"

#include <filesystem>
#include <iostream>
#include <stdexcept>
#include <string>


#include "Command.h"
#include "GAM150_URUK/Game.h"
#include "GAM150_URUK/Level.h"
#include "DropDownMenuObject.h"
#include "GAM150_URUK/TextComponent.h"
#include "LinkComponent.h"
#include "LinkObject.h"
#include "NodeObject.h"
#include "SelectableComponent.h"



doodle::Color get_color_input()
{
	doodle::Color color;
	std::string input;

	std::cout << std::endl;
	try
	{
		std::cout << "R(0)  : ";
		std::getline(std::cin, input);
		color.red = input.empty() ? 0 : std::stoi(input);
	}
	catch (std::invalid_argument&)
	{
		color.red = 0;
	}

	try
	{
		std::cout << "G(0)  : ";
		std::getline(std::cin, input);
		color.green = input.empty() ? 0 : std::stoi(input);
	}
	catch (std::invalid_argument&)
	{
		color.green = 0;
	}

	try
	{
		std::cout << "B(0)  : ";
		std::getline(std::cin, input);
		color.blue = input.empty() ? 0 : std::stoi(input);
	}
	catch (std::invalid_argument&)
	{
		color.blue = 0;
	}

	try
	{
		std::cout << "A(200): ";
		std::getline(std::cin, input);
		color.alpha = input.empty() ? 200 : std::stoi(input);
	}
	catch (std::invalid_argument&)
	{
		color.alpha = 200;
	}

	return color;
}

std::wstring get_name_input()
{
	std::wstring input;

	std::cout << std::endl << "Enter name(leave empty to leave unchanged): ";
	std::getline(std::wcin, input);

	std::wstring result;
	std::wstringstream{ input } >> result;

	return result;
}


std::wstring get_file_name(const std::wstring& prefix, const std::wstring& suffix)
{
	std::wstring fileName;

	std::cout << "Enter the file name(w/o extension): ";
	std::getline(std::wcin, fileName);

	return prefix + fileName + suffix;
}


void remove_all_drop_down_menu_objects()
{
	uruk::Game::GetCurrentHUDLevel()->RemoveAllObjects<DropDownMenuObject>();
}

void deselect_all()
{
	for (SelectableComponent& component : uruk::Game::GetCurrentLevel()->GetAllActiveComponents<SelectableComponent>())
	{
		component.Deselect();
	}
}


void save_level()
{
	std::filesystem::create_directory("saves/");
	const std::wstring fileName = get_file_name(L"saves/", uruk::Level::SAVE_FILE_EXTENSION);

	uruk::Game::GetCurrentLevel()->RemoveAllInactiveObjects<NodeObject>();
	uruk::Game::GetCurrentLevel()->RemoveAllInactiveObjects<LinkObject>();
	uruk::Game::SaveLevel(uruk::Game::GetCurrentLevelID(), fileName);
}


void load_level()
{
	const std::wstring fileName = get_file_name(L"saves/", uruk::Level::SAVE_FILE_EXTENSION);

	if (std::filesystem::exists(fileName))
	{
		const uruk::ID level = uruk::Game::LoadLevel(fileName);
		const uruk::ID currentLevel = uruk::Game::GetCurrentLevelID();
		if (currentLevel != level)
		{
			uruk::Game::RemoveLevel(currentLevel);
		}
		uruk::Game::SetCurrentLevel(level);
		Command::SetLevel(uruk::Game::GetLevel(level));
	}
	else
	{
		std::cerr << "Such file doesn't exist.\n";
	}
}


void export_level()
{
	uruk::Level* const level = uruk::Game::GetCurrentLevel();
	
	const std::wstring className = get_file_name();

	std::filesystem::create_directory("saves/");
	std::wofstream headerFile{ L"saves/" + className + L".h" };
	std::wofstream sourceFile{ L"saves/" + className + L".cpp" };

	const std::wstring enumName = className + L"States";

	headerFile << "#pragma once\n";  // pragma once
	headerFile << "#include \"StateMachine.h\"\n\n\n\n";  // Including StateMachine.h
	headerFile << "namespace uruk\n{\n";  // Namespace uruk start
	
	headerFile << "\tenum " << enumName << "\n\t{\n";  // Enum start
	for (const NodeObject& node : level->GetAllActiveObjects<NodeObject>())
	{
		headerFile << "\t\t" << node.GetComponent<uruk::TextComponent>()->GetText() << ",\n";  // Enum members
	}
	headerFile << "\t};\n\n\n";  // Enum end

	headerFile << "\tclass " << className << " : public CustomStateMachineBase<" << className << ">\n\t{\n";  // Class start
	headerFile << "\tpublic:\n";
	headerFile << "\t\t" << className << "();\n";
	headerFile << "\t};\n";  // Class end

	headerFile << "}\n";  // Namespace uruk end


	sourceFile << "#include \"" << className << ".h" << "\"\n\n\n\n";  // Including header file
	sourceFile << "namespace uruk\n{\n";  // Namespace uruk start

	sourceFile << "\t" << className << "::" << className << "()\n";  // Constructor start
	sourceFile << "\t\t: CustomStateMachineBase<" << className << ">(/*Insert Starting State Here*/,\n";  // Delegating constructor start
	sourceFile << "\t\t\t{\n";  // Pair list start
	for (const NodeObject& node : level->GetAllActiveObjects<NodeObject>())
	{
		const uruk::ID nodeID = node.GetID();

		sourceFile << "\t\t\t\tStateTransitionConditionsPair\n\t\t\t\t{\n";  // Pair start
		sourceFile << "\t\t\t\t\t" << enumName << "::" << node.GetComponent<uruk::TextComponent>()->GetText() << ",\n";  // "From" state
		sourceFile << "\t\t\t\t\t{\n";  // TransitionCondition list start
		for (const LinkComponent& link : level->GetAllActiveComponents<LinkComponent>())
		{
			if (link.GetStartNodeID() == nodeID)
			{
				const std::wstring& endNodeName = level->GetObject<NodeObject>(link.GetEndNodeID())->GetComponent<uruk::TextComponent>()->GetText();
				sourceFile << "\t\t\t\t\t\tTransitionCondition\n\t\t\t\t\t\t{\n";  // TransitionCondition start
				sourceFile << "\t\t\t\t\t\t\t" << enumName << "::" << endNodeName << ",\n";  // "To" state
				sourceFile << link.GetCode() << "\n";  // code
				sourceFile << "\t\t\t\t\t\t},\n";  // TransitionCondition end
			}
		}
		sourceFile << "\t\t\t\t\t}\n";  // TransitionCondition list end
		sourceFile << "\t\t\t\t},\n";  // Pair end
	}
	sourceFile << "\t\t\t}\n";  // Pair list end
	sourceFile << "\t\t)\n";  // Delegating constructor end
	sourceFile << "\t{}\n";  // Constructor end
	sourceFile << "}\n";  // Namespace uruk end
}
