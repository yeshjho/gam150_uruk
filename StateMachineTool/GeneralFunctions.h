/*
    File Name: GeneralFunctions.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <string>
#include <doodle/color.hpp>



doodle::Color get_color_input();
std::wstring get_name_input();
std::wstring get_file_name(const std::wstring& prefix = L"", const std::wstring& suffix = L"");

void remove_all_drop_down_menu_objects();

void deselect_all();

void save_level();
void load_level();

void export_level();
void import_level();
