/*
    File Name: ISelectableComponentCallbackBase.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "GAM150_URUK/ICustomCallbackCommon.h"



class SelectableComponent;


class ISelectableComponentCallbackBase : public uruk::ICustomCallbackCommon
{
public:
	virtual ~ISelectableComponentCallbackBase() = default;
	virtual void Execute(SelectableComponent*) = 0;
};
