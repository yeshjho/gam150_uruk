/*
    File Name: LinkComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "LinkComponent.h"

#include "GAM150_URUK/Serializer.h"



void LinkComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
{
	uruk::serialize(outputFileStream, mStartNodeID);
	uruk::serialize(outputFileStream, mEndNodeID);
}

uruk::IComponent* LinkComponent::OnLoadConstruct(uruk::Level* level, const uruk::ID& id, std::ifstream& inputFileStream)
{
	uruk::ID startNodeID;
	uruk::ID endNodeID;

	uruk::deserialize(inputFileStream, startNodeID);
	uruk::deserialize(inputFileStream, endNodeID);
	
	return level->LoadComponent<LinkComponent>(id, startNodeID, endNodeID);
}

void LinkComponent::OnSave(std::ofstream& outputFileStream) const
{
	uruk::serialize(outputFileStream, mCode);
}

void LinkComponent::OnLoad(std::ifstream& inputFileStream)
{
	uruk::deserialize(inputFileStream, mCode);
}
