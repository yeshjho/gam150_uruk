/*
    File Name: LinkComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "GAM150_URUK/Component.h"



class LinkComponent : public uruk::Component<LinkComponent>
{
public:
	inline LinkComponent(const uruk::ID& startNodeID, const uruk::ID& endNodeID) noexcept;


	void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
	static uruk::IComponent* OnLoadConstruct(uruk::Level* level, const uruk::ID& id, std::ifstream& inputFileStream);
	void OnSave(std::ofstream& outputFileStream) const override;
	void OnLoad(std::ifstream& inputFileStream) override;
	

	[[nodiscard]] constexpr const uruk::ID& GetStartNodeID() const noexcept;
	[[nodiscard]] constexpr const uruk::ID& GetEndNodeID() const noexcept;

	[[nodiscard]] constexpr const std::wstring& GetCode() const noexcept;

	inline void FlipDirection() noexcept;

	inline void SetCode(const std::wstring& code);
	


private:
	uruk::ID mStartNodeID;
	uruk::ID mEndNodeID;

	std::wstring mCode;
};

#include "LinkComponent.inl"
