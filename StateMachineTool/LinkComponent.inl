/*
    File Name: LinkComponent.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



inline LinkComponent::LinkComponent(const uruk::ID& startNodeID, const uruk::ID& endNodeID) noexcept
	: mStartNodeID(startNodeID), mEndNodeID(endNodeID)
{}


constexpr const uruk::ID& LinkComponent::GetStartNodeID() const noexcept
{
	return mStartNodeID;
}

constexpr const uruk::ID& LinkComponent::GetEndNodeID() const noexcept
{
	return mEndNodeID;
}

constexpr const std::wstring& LinkComponent::GetCode() const noexcept
{
	return mCode;
}


inline void LinkComponent::FlipDirection() noexcept
{
	swap(mStartNodeID, mEndNodeID);
}

inline void LinkComponent::SetCode(const std::wstring& code)
{
	mCode = code;
}
