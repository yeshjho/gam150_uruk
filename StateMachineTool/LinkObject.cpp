/*
    File Name: LinkObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "LinkObject.h"

#include <deque>


#include "GAM150_URUK/ClickableComponent.h"
#include "GAM150_URUK/Game.h"
#include "GAM150_URUK/InputComponent.h"
#include "GAM150_URUK/Serializer.h"
#include "GAM150_URUK/TransformComponent.h"
#include "ArrowComponent.h"
#include "ChangeLinkColorCommand.h"
#include "Command.h"
#include "DeleteLinksCommand.h"
#include "DropDownMenuObject.h"
#include "EditCodeCommand.h"
#include "FlipLinkDirectionCommand.h"
#include "GeneralFunctions.h"
#include "LinkComponent.h"



class LinkObjectEditCodeCommandCallback final : public uruk::CustomCallbackBase<LinkObjectEditCodeCommandCallback, uruk::IClickableComponentCallbackBase>
{
public:
	LinkObjectEditCodeCommandCallback(const uruk::ID& id = uruk::ID{})
		: mID(id)
	{}

	void Execute(uruk::ClickableComponent*) override
	{
		Command::AppendAndExecute<EditCodeCommand>(mID);
	}

	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mID);
	}
	


private:
	uruk::ID mID;
};

class LinkObjectDeleteLinksCommandCallback final : public uruk::CustomCallbackBase<LinkObjectDeleteLinksCommandCallback, uruk::IClickableComponentCallbackBase>
{
public:
	LinkObjectDeleteLinksCommandCallback(const uruk::ID& id = uruk::ID{})
		: mID(id)
	{}

	void Execute(uruk::ClickableComponent*) override
	{
		Command::AppendAndExecute<DeleteLinksCommand>(mID);
	}
	
	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mID);
	}



private:
	uruk::ID mID;
};

class LinkObjectFlipDirectionCommandCallback final : public uruk::CustomCallbackBase<LinkObjectFlipDirectionCommandCallback, uruk::IClickableComponentCallbackBase>
{
public:
	LinkObjectFlipDirectionCommandCallback(const uruk::ID& id = uruk::ID{})
		: mID(id)
	{}

	void Execute(uruk::ClickableComponent*) override
	{
		Command::AppendAndExecute<FlipLinkDirectionCommand>(mID);
	}

	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mID);
	}



private:
	uruk::ID mID;
};

class LinkObjectChangeColorCommandCallback final : public uruk::CustomCallbackBase<LinkObjectChangeColorCommandCallback, uruk::IClickableComponentCallbackBase>
{
public:
	LinkObjectChangeColorCommandCallback(const uruk::ID& id = uruk::ID{})
		: mID(id)
	{}

	void Execute(uruk::ClickableComponent*) override
	{
		Command::AppendAndExecute<ChangeLinkColorCommand>(mID, get_color_input());
	}

	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mID);
	}



private:
	uruk::ID mID;
};


class LinkObjectLeftPressRightPressCallback final : public uruk::CustomCallbackBase<LinkObjectLeftPressRightPressCallback, uruk::IClickableComponentCallbackBase>
{
public:
	void Execute(uruk::ClickableComponent*) override
	{
		remove_all_drop_down_menu_objects();
		deselect_all();
	}
};

class LinkObjectLeftDoubleClickCallback final : public uruk::CustomCallbackBase<LinkObjectLeftDoubleClickCallback, uruk::IClickableComponentCallbackBase>
{
public:
	LinkObjectLeftDoubleClickCallback(const uruk::ID& id = uruk::ID{})
		: mID(id)
	{}

	void Execute(uruk::ClickableComponent* self) override
	{
		LinkObjectEditCodeCommandCallback callback(mID);
		callback.Execute(self);
	}

	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mID);
	}



private:
	uruk::ID mID;
};

class LinkObjectRightClickCallback final : public uruk::CustomCallbackBase<LinkObjectRightClickCallback, uruk::IClickableComponentCallbackBase>
{
public:
	LinkObjectRightClickCallback(const uruk::ID& id = uruk::ID{})
		: mID(id)
	{
		cacheMenuNames = {
			L"Edit Code",
			L"Delete",
			L"Flip Direction",
			L"Change Color",
		};

		if (!id.is_nil())
		{
			cacheMenuFunctions.emplace_back(new LinkObjectEditCodeCommandCallback(mID));
			cacheMenuFunctions.emplace_back(new LinkObjectDeleteLinksCommandCallback(mID));
			cacheMenuFunctions.emplace_back(new LinkObjectFlipDirectionCommandCallback(mID));
			cacheMenuFunctions.emplace_back(new LinkObjectChangeColorCommandCallback(mID));
		}
	}

	void Execute(uruk::ClickableComponent*) override
	{
		uruk::Game::GetCurrentHUDLevel()->AddObject<DropDownMenuObject>(static_cast<float>(doodle::get_mouse_x()), static_cast<float>(doodle::get_mouse_y()), cacheMenuNames, cacheMenuFunctions);
	}

	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mID);
		cacheMenuFunctions.emplace_back(new LinkObjectEditCodeCommandCallback(mID));
		cacheMenuFunctions.emplace_back(new LinkObjectDeleteLinksCommandCallback(mID));
		cacheMenuFunctions.emplace_back(new LinkObjectFlipDirectionCommandCallback(mID));
		cacheMenuFunctions.emplace_back(new LinkObjectChangeColorCommandCallback(mID));
	}
	


private:
	std::deque<std::wstring> cacheMenuNames;
	std::deque<uruk::IClickableComponentCallbackBase*> cacheMenuFunctions;
	uruk::ID mID;
};



void LinkObject::OnConstructEnd()
{
	using namespace uruk;


	AddComponent<LinkComponent>(cacheStartNodeID, cacheEndNodeID);

	
	AddComponent<TransformComponent>(cacheX, cacheY);


	AddComponent<ArrowComponent>(cacheStartNodeID, cacheEndNodeID, Z_ORDER);


	AddComponent<InputComponent>();


	const ID LEFT_CLICK_ID = AddComponent<ClickableComponent>(0.f, DEFAULT_HEIGHT, doodle::MouseButtons::Left, Z_ORDER);
	ClickableComponent* const leftClick = mLevel->GetComponent<ClickableComponent>(LEFT_CLICK_ID);
	leftClick->SetOnDoubleClicked(std::make_unique<LinkObjectLeftDoubleClickCallback>(mID));
	leftClick->SetOnPressed(std::make_unique<LinkObjectLeftPressRightPressCallback>());

	
	const ID RIGHT_CLICK_ID = AddComponent<ClickableComponent>(0.f, DEFAULT_HEIGHT, doodle::MouseButtons::Right, Z_ORDER);
	ClickableComponent* const rightClick = mLevel->GetComponent<ClickableComponent>(RIGHT_CLICK_ID);
	rightClick->SetOnClicked(std::make_unique<LinkObjectRightClickCallback>(mID));
	rightClick->SetOnPressed(std::make_unique<LinkObjectLeftPressRightPressCallback>());
}


const uruk::ID& LinkObject::GetStartNodeID() const noexcept
{
	return GetComponent<LinkComponent>()->GetStartNodeID();
}

const uruk::ID& LinkObject::GetEndNodeID() const noexcept
{
	return GetComponent<LinkComponent>()->GetEndNodeID();
}


const std::wstring& LinkObject::GetCode() const noexcept
{
	return GetComponent<LinkComponent>()->GetCode();
}


void LinkObject::FlipDirection() const noexcept
{
	GetComponent<LinkComponent>()->FlipDirection();
	GetComponent<ArrowComponent>()->FlipDirection();
}


void LinkObject::SetCode(const std::wstring& code) const
{
	GetComponent<LinkComponent>()->SetCode(code);
}
