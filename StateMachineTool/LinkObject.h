/*
    File Name: LinkObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "GAM150_URUK/Object.h"

#include <string>



class LinkObject final : public uruk::Object<LinkObject>
{
public:
	inline LinkObject(float x = 0, float y = 0, const uruk::ID& startNodeID = uruk::ID{}, const uruk::ID& endNodeID = uruk::ID{}) noexcept;

	
	void OnConstructEnd() override;


	[[nodiscard]] const uruk::ID& GetStartNodeID() const noexcept;
	[[nodiscard]] const uruk::ID& GetEndNodeID() const noexcept;

	[[nodiscard]] const std::wstring& GetCode() const noexcept;

	void FlipDirection() const noexcept;
	
	void SetCode(const std::wstring& code) const;



public:
	static constexpr int Z_ORDER = -5000;
	static constexpr float DEFAULT_HEIGHT = 10.f;
	
private:
	float cacheX;
	float cacheY;
	
	uruk::ID cacheStartNodeID;
	uruk::ID cacheEndNodeID;
};

#include "LinkObject.inl"
