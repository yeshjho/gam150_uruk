/*
    File Name: LinkObject.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



inline LinkObject::LinkObject(const float x, const float y, const uruk::ID& startNodeID, const uruk::ID& endNodeID) noexcept
	: cacheX(x), cacheY(y), cacheStartNodeID(startNodeID), cacheEndNodeID(endNodeID)
{}
