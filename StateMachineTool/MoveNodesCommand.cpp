/*
    File Name: MoveNodesCommand.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "MoveNodesCommand.h"


#include "GAM150_URUK/TransformComponent.h"
#include "NodeObject.h"
#include "SelectableComponent.h"



void MoveNodesCommand::OnExecute()
{
	// On re-do.
	if (!mNodeIDs.empty())
	{
		for (const uruk::ID& id : mNodeIDs)
		{
			level->GetObject<NodeObject>(id)->GetComponent<uruk::TransformComponent>()->MoveBy(cacheXMoveAmount, cacheYMoveAmount);
		}

		return;
	}

	for (NodeObject& node : level->GetAllActiveObjects<NodeObject>())
	{
		if (node.GetComponent<SelectableComponent>()->IsSelected())
		{
			node.GetComponent<uruk::TransformComponent>()->MoveBy(cacheXMoveAmount, cacheYMoveAmount);
			mNodeIDs.push_back(node.GetID());
		}
	}
}

void MoveNodesCommand::OnUndo()
{
	for (const uruk::ID& id : mNodeIDs)
	{
		level->GetObject<NodeObject>(id)->GetComponent<uruk::TransformComponent>()->MoveBy(-cacheXMoveAmount, -cacheYMoveAmount);
	}
}
