/*
    File Name: MoveNodesCommand.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "Command.h"
#include "GAM150_URUK/Internal.h"



class MoveNodesCommand final : public Command
{
public:
	MoveNodesCommand(float xMoveAmount, float yMoveAmount) noexcept
		: cacheXMoveAmount(xMoveAmount), cacheYMoveAmount(yMoveAmount)
	{}
	
	void OnExecute() override;
	void OnUndo() override;
	std::string GetName() const override { return "Move Node"; }


private:
	float cacheXMoveAmount;
	float cacheYMoveAmount;

	std::deque<uruk::ID> mNodeIDs;
};
