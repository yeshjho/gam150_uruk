/*
    File Name: NodeObject.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "NodeObject.h"


#include "GAM150_URUK/CameraComponent.h"
#include "GAM150_URUK/ClickableComponent.h"
#include "GAM150_URUK/DraggableComponent.h"
#include "GAM150_URUK/Game.h"
#include "GAM150_URUK/GameResourceManager.h"
#include "GAM150_URUK/InputComponent.h"
#include "GAM150_URUK/Serializer.h"
#include "GAM150_URUK/TextComponent.h"
#include "GAM150_URUK/TransformComponent.h"
#include "ArrowComponent.h"
#include "ChangeNodeColorCommand.h"
#include "ChangeNodeNameCommand.h"
#include "ChangeNodeNameColorCommand.h"
#include "CreateLinkCommand.h"
#include "DeleteLinksCommand.h"
#include "DeleteNodesCommand.h"
#include "DropDownMenuObject.h"
#include "GAM150_URUK/CameraObject.h"
#include "GeneralFunctions.h"
#include "LinkObject.h"
#include "MoveNodesCommand.h"
#include "SelectableComponent.h"
#include "SimpleRectangleComponent.h"



class NodeObjectChangeNameCommandCallback final : public uruk::CustomCallbackBase<NodeObjectChangeNameCommandCallback, uruk::IClickableComponentCallbackBase>
{
public:
	NodeObjectChangeNameCommandCallback(const uruk::ID& id = uruk::ID{})
		: mID(id)
	{}
	
	void Execute(uruk::ClickableComponent*) override
	{
		std::wstring input = get_name_input();
		if (input.empty())
		{
			return;
		}

		for (const NodeObject& node : uruk::Game::GetCurrentLevel()->GetAllActiveObjects<NodeObject>())
		{
			if (node.GetComponent<uruk::TextComponent>()->GetText() == input)
			{
				return;
			}
		}

		Command::AppendAndExecute<ChangeNodeNameCommand>(mID, input);
	}

	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mID);
	}



private:
	uruk::ID mID;
};

class NodeObjectDeleteNodesCommandCallback final : public uruk::CustomCallbackBase<NodeObjectDeleteNodesCommandCallback, uruk::IClickableComponentCallbackBase>
{
public:
	void Execute(uruk::ClickableComponent*) override
	{
		Command::AppendAndExecute<DeleteLinksCommand>(uruk::ID{}, true);
		Command::AppendAndExecute<DeleteNodesCommand>();
	}
};

class NodeObjectDeleteLinksCommandCallback final : public uruk::CustomCallbackBase<NodeObjectDeleteLinksCommandCallback, uruk::IClickableComponentCallbackBase>
{
public:
	void Execute(uruk::ClickableComponent*) override
	{
		Command::AppendAndExecute<DeleteLinksCommand>();
	}
};

class NodeObjectChangeNodeColorCommandCallback final : public uruk::CustomCallbackBase<NodeObjectChangeNodeColorCommandCallback, uruk::IClickableComponentCallbackBase>
{
public:
	void Execute(uruk::ClickableComponent*) override
	{
		Command::AppendAndExecute<ChangeNodeColorCommand>(get_color_input());
	}
};

class NodeObjectChangeTextColorCommandCallback final : public uruk::CustomCallbackBase<NodeObjectChangeTextColorCommandCallback, uruk::IClickableComponentCallbackBase>
{
public:
	void Execute(uruk::ClickableComponent*) override
	{
		Command::AppendAndExecute<ChangeNodeNameColorCommand>(get_color_input());
	}
};

class NodeObjectLeftDragDragCallback final : public uruk::CustomCallbackBase<NodeObjectLeftDragDragCallback, uruk::IDraggableComponentCallbackBase>
{
	void Execute(uruk::DraggableComponent*, float, float) override
	{
		NodeObject::moveAmountSum = { 0.f, 0.f };
		NodeObject::nodesBeingMoved.clear();

		for (NodeObject& node : uruk::Game::GetCurrentLevel()->GetAllActiveObjects<NodeObject>())
		{
			if (node.GetComponent<SelectableComponent>()->IsSelected())
			{
				NodeObject::nodesBeingMoved.push_back(node.GetID());
			}
		}
	}
};

class NodeObjectLeftDragMovedCallback final : public uruk::CustomCallbackBase<NodeObjectLeftDragMovedCallback, uruk::IDraggableComponentCallbackBase>
{
public:
	void Execute(uruk::DraggableComponent* self, float xMoveAmount, float yMoveAmount) override
	{
		self->GetOwner()->GetComponent<uruk::TransformComponent>()->MoveBy(-xMoveAmount, -yMoveAmount);
		
		const uruk::Vector3D camScale = uruk::Game::GetCurrentLevel()->GetAllActiveObjects<uruk::CameraObject>().begin()->GetComponent<uruk::TransformComponent>()->GetScale();
		xMoveAmount *= 1 / camScale[0];
		yMoveAmount *= 1 / camScale[1];
		
		NodeObject::moveAmountSum += { xMoveAmount, yMoveAmount };
		
		for (const uruk::ID& id : NodeObject::nodesBeingMoved)
		{
			uruk::Game::GetCurrentLevel()->GetObject<NodeObject>(id)->GetComponent<uruk::TransformComponent>()->MoveBy(xMoveAmount, yMoveAmount);
		}
	}
};

class NodeObjectLeftDragDropCallback final : public uruk::CustomCallbackBase<NodeObjectLeftDragDropCallback, uruk::IDraggableComponentCallbackBase>
{
public:
	void Execute(uruk::DraggableComponent*, float, float) override
	{
		Command::AppendAndExecute<MoveNodesCommand>(NodeObject::moveAmountSum[0], NodeObject::moveAmountSum[1]);
		for (const uruk::ID& id : NodeObject::nodesBeingMoved)
		{
			uruk::Game::GetCurrentLevel()->GetObject<NodeObject>(id)->GetComponent<uruk::TransformComponent>()->MoveBy(-NodeObject::moveAmountSum[0], -NodeObject::moveAmountSum[1]);
		}
	}
};

class NodeObjectRightDragMovedCallback final : public uruk::CustomCallbackBase<NodeObjectRightDragMovedCallback, uruk::IDraggableComponentCallbackBase>
{
public:
	NodeObjectRightDragMovedCallback(const uruk::ID& transformComponentID = uruk::ID{})
		: mTransformComponentID(transformComponentID)
	{}
	
	void Execute(uruk::DraggableComponent*, float xMoveAmount, float yMoveAmount) override
	{
		uruk::Game::GetCurrentLevel()->GetComponent<uruk::TransformComponent>(mTransformComponentID)->MoveBy(-xMoveAmount, -yMoveAmount);
	}

	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mTransformComponentID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mTransformComponentID);
	}



private:
	uruk::ID mTransformComponentID;
};

class NodeObjectSelectCallback final : public uruk::CustomCallbackBase<NodeObjectSelectCallback, ISelectableComponentCallbackBase>
{
public:
	NodeObjectSelectCallback(const uruk::ID& rectangleID = uruk::ID{})
		: mRectangleID(rectangleID)
	{}
	
	void Execute(SelectableComponent*) override
	{
		ShapeAttributes& shapeAttributes = uruk::Game::GetCurrentLevel()->GetComponent<SimpleRectangleComponent>(mRectangleID)->GetShapeAttributes();
		shapeAttributes.SetHasOutline(true);
		shapeAttributes.SetOutlineColor(NodeObject::OUTLINE_COLOR);
		shapeAttributes.SetOutlineWidth(NodeObject::OUTLINE_WIDTH);
	}

	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mRectangleID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mRectangleID);
	}



private:
	uruk::ID mRectangleID;
};

class NodeObjectDeselectCallback final : public uruk::CustomCallbackBase<NodeObjectDeselectCallback, ISelectableComponentCallbackBase>
{
public:
	NodeObjectDeselectCallback(const uruk::ID& rectangleID = uruk::ID{})
		: mRectangleID(rectangleID)
	{}

	void Execute(SelectableComponent*) override
	{
		ShapeAttributes& shapeAttributes = uruk::Game::GetCurrentLevel()->GetComponent<SimpleRectangleComponent>(mRectangleID)->GetShapeAttributes();
		shapeAttributes.SetHasOutline(false);
	}

	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mRectangleID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mRectangleID);
	}



private:
	uruk::ID mRectangleID;
};

class NodeObjectLeftPressCallback final : public uruk::CustomCallbackBase<NodeObjectLeftPressCallback, uruk::IClickableComponentCallbackBase>
{
public:
	NodeObjectLeftPressCallback(const uruk::ID& selectID = uruk::ID{})
		: mSelectID(selectID)
	{}
	
	void Execute(uruk::ClickableComponent*) override
	{
		SelectableComponent* selectableComponent = uruk::Game::GetCurrentLevel()->GetComponent<SelectableComponent>(mSelectID);

		remove_all_drop_down_menu_objects();
		if (uruk::InputComponent::IsKeyDown(doodle::KeyboardButtons::Control) || uruk::InputComponent::IsKeyDown(doodle::KeyboardButtons::Shift))
		{
			selectableComponent->ToggleSelect();
		}
		else if (!selectableComponent->IsSelected())
		{
			deselect_all();
			selectableComponent->Select();
		}
	}

	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mSelectID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mSelectID);
	}



private:
	uruk::ID mSelectID;
};

class NodeObjectLeftDoubleClickCallback final : public uruk::CustomCallbackBase<NodeObjectLeftDoubleClickCallback, uruk::IClickableComponentCallbackBase>
{
public:
	NodeObjectLeftDoubleClickCallback(const uruk::ID& id = uruk::ID{})
		: mID(id)
	{}
	
	void Execute(uruk::ClickableComponent* self) override
	{
		NodeObjectChangeNameCommandCallback callback(mID);
		callback.Execute(self);
	}

	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mID);
	}



private:
	uruk::ID mID;
};

class NodeObjectRightClickCallback final : public uruk::CustomCallbackBase<NodeObjectRightClickCallback, uruk::IClickableComponentCallbackBase>
{
public:
	NodeObjectRightClickCallback(const uruk::ID& id = uruk::ID{})
		: mID(id)
	{
		cacheMenuNames = {
			L"Change Name",
			L"Delete",
			L"Disconnect All Links",
			L"Change Color",
			L"Change Text Color"
		};

		if (!id.is_nil())
		{
			cacheMenuFunctions.emplace_back(new NodeObjectChangeNameCommandCallback(mID));
			cacheMenuFunctions.emplace_back(new NodeObjectDeleteNodesCommandCallback());
			cacheMenuFunctions.emplace_back(new NodeObjectDeleteLinksCommandCallback());
			cacheMenuFunctions.emplace_back(new NodeObjectChangeNodeColorCommandCallback());
			cacheMenuFunctions.emplace_back(new NodeObjectChangeTextColorCommandCallback());
		}
	}

	void Execute(uruk::ClickableComponent*) override
	{
		const bool isSingle = SelectableComponent::IsSelectionSingular();
		uruk::Game::GetCurrentHUDLevel()->AddObject<DropDownMenuObject>(static_cast<float>(doodle::get_mouse_x()), static_cast<float>(doodle::get_mouse_y()), std::deque<std::wstring>{ cacheMenuNames.begin() + (isSingle ? 0 : 1), cacheMenuNames.end() }, std::deque<uruk::IClickableComponentCallbackBase*>{ cacheMenuFunctions.begin() + (isSingle ? 0 : 1), cacheMenuFunctions.end() });
	}

	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mID);
		
		cacheMenuFunctions.emplace_back(new NodeObjectChangeNameCommandCallback(mID));
		cacheMenuFunctions.emplace_back(new NodeObjectDeleteNodesCommandCallback());
		cacheMenuFunctions.emplace_back(new NodeObjectDeleteLinksCommandCallback());
		cacheMenuFunctions.emplace_back(new NodeObjectChangeNodeColorCommandCallback());
		cacheMenuFunctions.emplace_back(new NodeObjectChangeTextColorCommandCallback());
	}

	

private:
	std::deque<std::wstring> cacheMenuNames;
	std::deque<uruk::IClickableComponentCallbackBase*> cacheMenuFunctions;
	uruk::ID mID;
};

class NodeObjectRightPressCallback final : public uruk::CustomCallbackBase<NodeObjectRightPressCallback, uruk::IClickableComponentCallbackBase>
{
public:
	NodeObjectRightPressCallback(const uruk::ID& selectID = uruk::ID{})
		: mSelectID(selectID)
	{}
	
	void Execute(uruk::ClickableComponent*) override
	{
		SelectableComponent* selectableComponent = uruk::Game::GetCurrentLevel()->GetComponent<SelectableComponent>(mSelectID);

		remove_all_drop_down_menu_objects();
		if (uruk::InputComponent::IsKeyDown(doodle::KeyboardButtons::Control) || uruk::InputComponent::IsKeyDown(doodle::KeyboardButtons::Shift))
		{
			selectableComponent->ToggleSelect();
		}
		else if (!selectableComponent->IsSelected())
		{
			deselect_all();
			selectableComponent->Select();
		}
		
		NodeObject::arrowComponentID = uruk::Game::GetCurrentLevel()->GetComponent<SelectableComponent>(mSelectID)->GetOwner()->AddComponent<ArrowComponent>(uruk::ID{}, uruk::ID{}, LinkObject::Z_ORDER);
	}
	
	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mSelectID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mSelectID);
	}



private:
	uruk::ID mSelectID;
};

class NodeObjectRightReleaseCallback final : public uruk::CustomCallbackBase<NodeObjectRightReleaseCallback, uruk::IClickableComponentCallbackBase>
{
public:
	NodeObjectRightReleaseCallback(const uruk::ID& id = uruk::ID{})
		: mID(id)
	{}
	
	void Execute(uruk::ClickableComponent*) override
	{
		uruk::Level* level = uruk::Game::GetCurrentLevel();
		
		for (NodeObject& node : level->GetAllActiveObjects<NodeObject>())
		{
			uruk::Vector3D mousePosition = level->GetAllActiveComponents<uruk::CameraComponent>().begin()->CalculateCoordProjectedOnWorld({ static_cast<float>(doodle::get_mouse_x()), static_cast<float>(doodle::get_mouse_y()) });
			if (static_cast<uruk::WidthHeightBase<SimpleRectangleComponent>*>(node.GetComponent<SimpleRectangleComponent>())->IsInside(mousePosition))
			{
				if (node.GetID() == mID)
				{
					level->RemoveComponent<ArrowComponent>(NodeObject::arrowComponentID);
					return;
				}
				
				for (LinkObject& link : level->GetAllActiveObjects<LinkObject>())
				{
					if (link.GetStartNodeID() == mID && link.GetEndNodeID() == node.GetID())
					{
						Command::AppendAndExecute<DeleteLinksCommand>(link.GetID());
						level->RemoveComponent<ArrowComponent>(NodeObject::arrowComponentID);
						return;
					}
				}

				uruk::Vector3D pos = level->GetObject<NodeObject>(mID)->GetComponent<uruk::TransformComponent>()->GetPosition();

				Command::AppendAndExecute<CreateLinkCommand>(pos[0], pos[1], mID, node.GetID());
				level->RemoveComponent<ArrowComponent>(NodeObject::arrowComponentID);
				return;
			}
		}

		level->RemoveComponent<ArrowComponent>(NodeObject::arrowComponentID);
	}

	void OnSave(std::ofstream& outputFileStream) override
	{
		uruk::serialize(outputFileStream, mID);
	}

	void OnLoad(std::ifstream& inputFileStream) override
	{
		uruk::deserialize(inputFileStream, mID);
	}



private:
	uruk::ID mID;
};



void NodeObject::OnConstructEnd()
{
	using namespace uruk;


	const ID TRANSFORM_ID = AddComponent<TransformComponent>(cacheX, cacheY);


	const ID RECT_ID = AddComponent<SimpleRectangleComponent>(DEFAULT_NODE_WIDTH, DEFAULT_NODE_HEIGHT, Z_ORDER);
	SimpleRectangleComponent* const rect = GetComponent<SimpleRectangleComponent>();
	rect->SetShapeAttributes({ true, DEFAULT_NODE_COLOR, false });


	AddComponent<TextComponent>(L"Invalid State", DEFAULT_NODE_WIDTH, DEFAULT_NODE_HEIGHT, Z_ORDER);
	TextComponent* const text = GetComponent<TextComponent>();
	text->SetColor(DEFAULT_TEXT_COLOR);
	text->SetFontID(GameResourceManager::GetFont("NanumSquareRoundEB"));
	text->SetHorizontalAlign(ETextHorizontalAlign::CENTER);
	text->SetVerticalAlign(ETextVerticalAlign::CENTER);


	AddComponent<InputComponent>();


	const ID LEFT_DRAG_ID = AddComponent<DraggableComponent>(DEFAULT_NODE_WIDTH, DEFAULT_NODE_HEIGHT, doodle::MouseButtons::Left, Z_ORDER);
	DraggableComponent* const leftDrag = mLevel->GetComponent<DraggableComponent>(LEFT_DRAG_ID);
	leftDrag->SetOnDrag(std::make_unique<NodeObjectLeftDragDragCallback>());
	leftDrag->SetOnMoved(std::make_unique<NodeObjectLeftDragMovedCallback>());
	leftDrag->SetOnDrop(std::make_unique<NodeObjectLeftDragDropCallback>());

	// Cancelling out the background drag
	const ID RIGHT_DRAG_ID = AddComponent<DraggableComponent>(DEFAULT_NODE_WIDTH, DEFAULT_NODE_HEIGHT, doodle::MouseButtons::Right, Z_ORDER);
	DraggableComponent* const rightDrag = mLevel->GetComponent<DraggableComponent>(RIGHT_DRAG_ID);
	rightDrag->SetOnMoved(std::make_unique<NodeObjectRightDragMovedCallback>(TRANSFORM_ID));
	

	const ID SELECT_ID = AddComponent<SelectableComponent>();
	SelectableComponent* const select = GetComponent<SelectableComponent>();
	select->SetOnSelect(std::make_unique<NodeObjectSelectCallback>(RECT_ID));
	select->SetOnDeselect(std::make_unique<NodeObjectDeselectCallback>(RECT_ID));


	const ID LEFT_CLICK_ID = AddComponent<ClickableComponent>(DEFAULT_NODE_WIDTH, DEFAULT_NODE_HEIGHT, doodle::MouseButtons::Left, Z_ORDER);
	ClickableComponent* const leftClick = mLevel->GetComponent<ClickableComponent>(LEFT_CLICK_ID);
	leftClick->SetOnPressed(std::make_unique<NodeObjectLeftPressCallback>(SELECT_ID));
	leftClick->SetOnDoubleClicked(std::make_unique<NodeObjectLeftDoubleClickCallback>(mID));


	const ID RIGHT_CLICK_ID = AddComponent<ClickableComponent>(DEFAULT_NODE_WIDTH, DEFAULT_NODE_HEIGHT, doodle::MouseButtons::Right, Z_ORDER);
	ClickableComponent* const rightClick = mLevel->GetComponent<ClickableComponent>(RIGHT_CLICK_ID);
	rightClick->SetOnClicked(std::make_unique<NodeObjectRightClickCallback>(mID));
	rightClick->SetOnPressed(std::make_unique<NodeObjectRightPressCallback>(SELECT_ID));
	rightClick->SetOnReleased(std::make_unique<NodeObjectRightReleaseCallback>(mID));
}
