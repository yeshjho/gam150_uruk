/*
    File Name: NodeObject.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include <deque>
#include <doodle/color.hpp>

#include "GAM150_URUK/Object.h"
#include "GAM150_URUK/Vector3D.h"



class NodeObject final : public uruk::Object<NodeObject>
{
	friend class NodeObjectLeftDragDragCallback;
	friend class NodeObjectLeftDragMovedCallback;
	friend class NodeObjectLeftDragDropCallback;

	friend class NodeObjectRightPressCallback;
	friend class NodeObjectRightReleaseCallback;
	
public:
	inline NodeObject(float x = 0, float y = 0) noexcept;

	
	void OnConstructEnd() override;



public:
	static constexpr int Z_ORDER = 5000;
	
	static constexpr float DEFAULT_NODE_WIDTH = 100.f * 1.618f;
	static constexpr float MINIMUM_MARGIN = 15.f;
	static constexpr float DEFAULT_NODE_HEIGHT = 100.f;

	static constexpr doodle::Color DEFAULT_NODE_COLOR{ 32, 34, 37, 200 };
	static constexpr doodle::Color DEFAULT_TEXT_COLOR{ 255, 255, 255 };
	static constexpr doodle::Color OUTLINE_COLOR{ 255, 176, 56 };
	static constexpr float OUTLINE_WIDTH = 5.f;

private:
	float cacheX;
	float cacheY;

	static inline std::deque<uruk::ID> nodesBeingMoved;
	static inline uruk::Vector3D moveAmountSum{ 0.f, 0.f };

	static inline uruk::ID arrowComponentID;
};

#include "NodeObject.inl"
