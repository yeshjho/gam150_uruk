/*
    File Name: NodeObject.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



inline NodeObject::NodeObject(float x, float y) noexcept
	: cacheX(x), cacheY(y)
{}
