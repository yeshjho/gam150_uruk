/*
    File Name: SelectableComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "SelectableComponent.h"

#include "GAM150_URUK/Serializer.h"



void SelectableComponent::OnMove(void* newLocation)
{
	new (newLocation) SelectableComponent(std::move(*this));
}


void SelectableComponent::OnConstructorArgumentSave(std::ofstream& outputFileStream) const
{
	uruk::serialize(outputFileStream, mOnSelect->GetType());
	mOnSelect->OnSave(outputFileStream);
	
	uruk::serialize(outputFileStream, mOnDeselect->GetType());
	mOnDeselect->OnSave(outputFileStream);
}

uruk::IComponent* SelectableComponent::OnLoadConstruct(uruk::Level* level, const uruk::ID& id, std::ifstream& inputFileStream)
{
	uruk::TypeID typeID;

	uruk::deserialize(inputFileStream, typeID);
	std::unique_ptr<ISelectableComponentCallbackBase> onSelectCallback = uruk::CustomCallbackFactory<ISelectableComponentCallbackBase>::Generate(typeID);
	onSelectCallback->OnLoad(inputFileStream);
	
	uruk::deserialize(inputFileStream, typeID);
	std::unique_ptr<ISelectableComponentCallbackBase> onDeselectCallback = uruk::CustomCallbackFactory<ISelectableComponentCallbackBase>::Generate(typeID);
	onDeselectCallback->OnLoad(inputFileStream);
	
	return level->LoadComponent<SelectableComponent>(id, std::move(onSelectCallback), std::move(onDeselectCallback));
}


void SelectableComponent::OnSave(std::ofstream& outputFileStream) const
{
	uruk::serialize(outputFileStream, mIsSelected);
}

void SelectableComponent::OnLoad(std::ifstream& inputFileStream)
{
	uruk::deserialize(inputFileStream, mIsSelected);
}
