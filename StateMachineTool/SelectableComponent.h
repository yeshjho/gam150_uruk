/*
    File Name: SelectableComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "GAM150_URUK/Component.h"
#include "GAM150_URUK/CustomCallbackBase.h"

#include "ISelectableComponentCallbackBase.h"



class DefaultSelectableComponentCallback final : public uruk::CustomCallbackBase<DefaultSelectableComponentCallback, ISelectableComponentCallbackBase>
{
public:
	void OnLoad(std::ifstream&) override {}
	void OnSave(std::ofstream&) override {}
	void Execute(SelectableComponent*) override {}
};



class SelectableComponent final : public uruk::Component<SelectableComponent, uruk::EUpdateType::NEVER, 0, 0, uruk::EComponentAttribute::NO_MEMCPY>
{
public:
	inline SelectableComponent(std::unique_ptr<ISelectableComponentCallbackBase>&& onSelect = std::make_unique<DefaultSelectableComponentCallback>(), std::unique_ptr<ISelectableComponentCallbackBase>&& onDeselect = std::make_unique<DefaultSelectableComponentCallback>());


	inline void OnEnabled() override;
	inline void OnDisabled() override;

	void OnMove(void* newLocation) override;


	void OnConstructorArgumentSave(std::ofstream& outputFileStream) const override;
	static uruk::IComponent* OnLoadConstruct(uruk::Level* level, const uruk::ID& id, std::ifstream& inputFileStream);

	void OnSave(std::ofstream& outputFileStream) const override;
	void OnLoad(std::ifstream& inputFileStream) override;
	

	[[nodiscard]] constexpr bool IsSelected() const noexcept;

	inline void Select() noexcept;
	inline void Deselect() noexcept;
	inline void ToggleSelect() noexcept;

	inline void SetOnSelect(std::unique_ptr<ISelectableComponentCallbackBase>&& onSelect);
	inline void SetOnDeselect(std::unique_ptr<ISelectableComponentCallbackBase>&& onDeselect);


	static inline bool IsSelectionSingular() noexcept;



private:
	bool mIsSelected = false;

	std::unique_ptr<ISelectableComponentCallbackBase> mOnSelect;
	std::unique_ptr<ISelectableComponentCallbackBase> mOnDeselect;


	static inline unsigned int selectionCount;
};

#include "SelectableComponent.inl"
