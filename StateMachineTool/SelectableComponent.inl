/*
    File Name: SelectableComponent.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



inline SelectableComponent::SelectableComponent(std::unique_ptr<ISelectableComponentCallbackBase>&& onSelect, std::unique_ptr<ISelectableComponentCallbackBase>&& onDeselect)
	: mOnSelect(std::move(onSelect)), mOnDeselect(std::move(onDeselect))
{}


inline void SelectableComponent::OnEnabled()
{
	if (mIsSelected)
	{
		Select();
	}
}


inline void SelectableComponent::OnDisabled()
{
	if (mIsSelected)
	{
		Deselect();
	}
}


constexpr bool SelectableComponent::IsSelected() const noexcept
{
	return mIsSelected;
}

inline void SelectableComponent::Select() noexcept
{
	if (mIsSelected)
	{
		return;
	}

	mIsSelected = true;
	selectionCount++;

	mOnSelect->Execute(this);
}

inline void SelectableComponent::Deselect() noexcept
{
	if (!mIsSelected)
	{
		return;
	}

	mIsSelected = false;
	selectionCount--;

	mOnDeselect->Execute(this);
}

inline void SelectableComponent::ToggleSelect() noexcept
{
	mIsSelected ? Deselect() : Select();
}


inline void SelectableComponent::SetOnSelect(std::unique_ptr<ISelectableComponentCallbackBase>&& onSelect)
{
	mOnSelect = std::move(onSelect);
}

inline void SelectableComponent::SetOnDeselect(std::unique_ptr<ISelectableComponentCallbackBase>&& onDeselect)
{
	mOnDeselect = std::move(onDeselect);
}


inline bool SelectableComponent::IsSelectionSingular() noexcept
{
	return selectionCount == 1;
}
