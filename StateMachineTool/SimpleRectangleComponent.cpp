/*
    File Name: SimpleRectangleComponent.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "SimpleRectangleComponent.h"

#include <doodle/doodle.hpp>

#include "GAM150_URUK/Serializer.h"



void SimpleRectangleComponent::OnDraw(const uruk::Vector3D& camPosition, float, const uruk::Vector3D& camZoomScale) const
{
	using namespace doodle;
	using namespace uruk;

	
	const TransformComponent* const componentTransform = GetOwner()->GetComponent<TransformComponent>();
	const bool isOnUI = componentTransform->IsOnUI();
	const Vector3D& componentPosition = componentTransform->GetPosition();
	const Vector3D& componentScale = componentTransform->GetScale();
	const Vector3D& componentOffset = GetOffset();

	float x = componentPosition[0] + componentOffset[0];
	float y = componentPosition[1] + componentOffset[1];

	if (!isOnUI)
	{
		x = (x - camPosition[0]) * camZoomScale[0] + float(Width) / 2.f;
		y = (y - camPosition[1]) * camZoomScale[1] + float(Height) / 2.f;
	}

	const ShapeAttributes& attributes = GetShapeAttributes();

	push_settings();
	apply_translate(x, y);
	apply_rotate(componentTransform->GetRotationRadian() + GetRotationOffset());
	apply_scale(componentScale[0] * camZoomScale[0], componentScale[1] * camZoomScale[1]);
	if (attributes.doFill) { set_fill_color(attributes.fillColor); }
	else { no_fill(); }
	if (attributes.hasOutline) { set_outline_color(attributes.outlineColor); set_outline_width(attributes.outlineWidth); }
	else { no_outline(); }
	draw_rectangle(0, 0, GetWidth(), GetHeight());
	pop_settings();
}


void SimpleRectangleComponent::OnSave(std::ofstream& outputFileStream) const
{
	uruk::serialize(outputFileStream, mShapeAttributes);
}


void SimpleRectangleComponent::OnLoad(std::ifstream& inputFileStream)
{
	uruk::deserialize(inputFileStream, mShapeAttributes);
}
