/*
    File Name: SimpleRectangleComponent.h
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once
#include "GAM150_URUK/Component.h"

#include <doodle/color.hpp>

#include "GAM150_URUK/TransformOffsetBase.h"
#include "GAM150_URUK/WidthHeightBase.h"
#include "GAM150_URUK/ZOrderBase.h"



struct ShapeAttributes
{
	[[nodiscard]] constexpr bool DoFill() const noexcept;
	[[nodiscard]] constexpr const doodle::Color& GetFillColor() const noexcept;

	[[nodiscard]] constexpr bool HasOutline() const noexcept;
	[[nodiscard]] constexpr const doodle::Color& GetOutlineColor() const noexcept;
	[[nodiscard]] constexpr float GetOutlineWidth() const noexcept;


	constexpr void SetDoFill(bool doFill_) noexcept;
	constexpr void SetFillColor(const doodle::Color& fillColor_) noexcept;

	constexpr void SetHasOutline(bool hasOutline_) noexcept;
	constexpr void SetOutlineColor(const doodle::Color& outlineColor_) noexcept;
	constexpr void SetOutlineWidth(float outlineWidth_) noexcept;


	
	bool doFill = true;
	doodle::Color fillColor;

	bool hasOutline = true;
	doodle::Color outlineColor;
	float outlineWidth = 1.f;
};



class SimpleRectangleComponent final : public uruk::TransformOffsetBase<SimpleRectangleComponent>, public uruk::WidthHeightBase<SimpleRectangleComponent>, public uruk::ZOrderBase, public uruk::Component<SimpleRectangleComponent, uruk::EUpdateType::NEVER, uruk::update_priorities::NORMAL_PRIORITY, 0, uruk::EComponentAttribute::DRAWABLE>
{
public:
	inline SimpleRectangleComponent(float width = 0.f, float height = 0.f, int zOrder = 0);


	void OnDraw(const uruk::Vector3D& camPosition, float camRotation, const uruk::Vector3D& camZoomScale) const override;

	void OnSave(std::ofstream& outputFileStream) const override;
	void OnLoad(std::ifstream& inputFileStream) override;
	

	[[nodiscard]] constexpr ShapeAttributes& GetShapeAttributes() noexcept;
	[[nodiscard]] constexpr const ShapeAttributes& GetShapeAttributes() const noexcept;
	
	inline void SetShapeAttributes(const ShapeAttributes& shapeAttributes) noexcept;



private:
	ShapeAttributes mShapeAttributes;
};

#include "SimpleRectangleComponent.inl"
