/*
    File Name: SimpleRectangleComponent.inl
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#pragma once



constexpr bool ShapeAttributes::DoFill() const noexcept
{
	return doFill;
}

constexpr const doodle::Color& ShapeAttributes::GetFillColor() const noexcept
{
	return fillColor;
}

constexpr bool ShapeAttributes::HasOutline() const noexcept
{
	return hasOutline;
}

constexpr const doodle::Color& ShapeAttributes::GetOutlineColor() const noexcept
{
	return outlineColor;
}

constexpr float ShapeAttributes::GetOutlineWidth() const noexcept
{
	return outlineWidth;
}


constexpr void ShapeAttributes::SetDoFill(const bool doFill_) noexcept
{
	doFill = doFill_;
}

constexpr void ShapeAttributes::SetFillColor(const doodle::Color& fillColor_) noexcept
{
	fillColor = fillColor_;
}

constexpr void ShapeAttributes::SetHasOutline(const bool hasOutline_) noexcept
{
	hasOutline = hasOutline_;
}

constexpr void ShapeAttributes::SetOutlineColor(const doodle::Color& outlineColor_) noexcept
{
	outlineColor = outlineColor_;
}

constexpr void ShapeAttributes::SetOutlineWidth(const float outlineWidth_) noexcept
{
	outlineWidth = outlineWidth_;
}



inline SimpleRectangleComponent::SimpleRectangleComponent(const float width, const float height, const int zOrder)
	: uruk::WidthHeightBase<SimpleRectangleComponent>(width, height), uruk::ZOrderBase(zOrder)
{}


constexpr ShapeAttributes& SimpleRectangleComponent::GetShapeAttributes() noexcept
{
	return mShapeAttributes;
}

constexpr const ShapeAttributes& SimpleRectangleComponent::GetShapeAttributes() const noexcept
{
	return mShapeAttributes;
}

inline void SimpleRectangleComponent::SetShapeAttributes(const ShapeAttributes& shapeAttributes) noexcept
{
	mShapeAttributes = shapeAttributes;
}
