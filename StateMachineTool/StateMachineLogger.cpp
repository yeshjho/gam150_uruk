/*
    File Name: StateMachineLogger.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include "StateMachineLogger.h"



uruk::Logger state_machine_logger{ uruk::ELogCategory::ALL, uruk::ELogVerbosity::VERBOSE, true };
