/*
    File Name: main.cpp
    Project Name: U.R.U.K
    Author(s):
        Primary: Joonho Hwang
        Secondary: 
    All content (C) 2020 DigiPen (USA) Corporation, all rights reserved.
*/
#include <doodle/doodle.hpp>

#include "GAM150_URUK/CameraObject.h"
#include "GAM150_URUK/InputComponent.h"
#include "GAM150_URUK/TextComponent.h"
#include "BackgroundObject.h"
#include "Command.h"
#include "CommandInputReceiveObject.h"
#include "GAM150_URUK/ClickableComponent.h"
#include "GAM150_URUK/DraggableComponent.h"
#include "GAM150_URUK/Game.h"
#include "GAM150_URUK/HoverableComponent.h"


using namespace uruk;
using namespace std;



int main()
{
	Game::Setup("resource/resources.txt");

	const ID LEVEL_ID = Game::AddLevel(100_MB, 100);
	const ID HUD_ID = Game::AddLevel(100_MB, 100);
	Game::SetCurrentLevel(LEVEL_ID);
	Game::SetCurrentHUDLevel(HUD_ID);
	Level& l = *Game::GetCurrentLevel();
	Level& hud = *Game::GetCurrentHUDLevel();

	Command::SetLevel(&l);

	
	const ID CAM_ID = l.AddObject<CameraObject>();
	hud.AddObject<CameraObject>();
	const ID BACKGROUND_ID = l.AddObject<BackgroundObject>();
	l.AddObject<CommandInputReceiveObject>();

	l.GetObject<CameraObject>(CAM_ID)->SetFollowingObject(l.GetObject<BackgroundObject>(BACKGROUND_ID), 0.f, { BackgroundObject::LENGTH / 2.f, BackgroundObject::LENGTH / 2.f });

	
	Game::BeginPlay();
	while (!Game::ShouldQuit())
	{
		Game::Update();
	}
	Game::CleanUp();
}
