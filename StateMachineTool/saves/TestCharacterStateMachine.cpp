#include "TestCharacterStateMachine.h"



namespace uruk
{
	TestCharacterStateMachine::TestCharacterStateMachine()
		: CustomStateMachineBase<TestCharacterStateMachine>(/*Insert Starting State Here*/,
			{
				StateTransitionConditionsPair
				{
					TestCharacterStateMachineStates::Idle,
					{
					}
				},
			}
		)
	{}
}
